
#Rules

* Every controller should extend Controller class (located at _`./pixelhumain-php7/code/pixelhumain/ph/protected/components/Controller.php`_)
* Every controller->action should return the value other than `void`
* any function/method called inside the view should be echoed explitly. for example: `echo $this->render()` or `echo $this->renderPartial()`, etc.
* Every class should be declared within any `namespace`
* If a class reference needs to be given, it should be given with fully qualified namespace. You'll need this in controller::actions method. Not the one earlier used like dot notation ('folder1.folder2.folder3.className')
* Every action class must extend `\PixelHumain\PixelHumain\components\Action` class
* No `echo` inside a function/method, only return the value
* Any future asset(css, js), should be added using Yii2 Asset: https://www.yiiframework.com/doc/guide/2.0/en/structure-assets
* Inside the action class, call controller property instead of method: `$this->controller;` Not this -> `$this->controller();`
* For importing a class prefer PSR-4, PSR-0, classmap respectively
* For setting a theme, use `\PixelHumain\PixelHumain\components\ThemeHelper::setWebsiteTheme('themeName')` helper
* View is object of View not the controller. So, don't try to access controller property directly from the view. For this, pass the param when calling `render()` method.
* Avoid dot notation view path specifying, instead use php directory separator
* Avoid camelCase URL, instead use - (hyphen) for word separation
* Use container instead of service locater: https://www.yiiframework.com/doc/guide/2.0/en/concept-di-container
  * Try to use method/constructer injection. For example give dataType when defining the method/constructor `function methodA(ClassB $obj) { /* $obj is used here */}`, it's container headache how to resolve it.
  * If the above fails, use `Yii::$container->get('key/FqnClass/etc');` for the time.
*