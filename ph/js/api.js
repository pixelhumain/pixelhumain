function checkLoggued(ajaxUrl) {
    if (userId === "") {
        //TODO Replace with a modal version of the login page
        bootbox.confirm("You need to be loggued to do this, login first ?",
            function (result) {
                if (result)
                    window.location.href = baseUrl + "/" + moduleId + "/person/login?backUrl=" + ajaxUrl;
            });
    } else
        return true;
}

function lazyFunction(path, v) {
    if (typeof path == "function")
        path(v);
    else
        setTimeout(function () { lazyFunction(path, v) }, 200);
}

function lazyCSS(path, v) {
    if (typeof path == "function")
        path(v);
    else
        setTimeout(function () { lazyFunction(path, v) }, 200);
}

function escapeHtml(text) {
    var map = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#039;'
    };
    return text.replace(/[&<>"']/g, function (m) { return map[m]; });
}
/*
change text inside curly to variable
exemple : str = "a {assetPath} with curly braces",
result : "a /assets/468c3f32 with curly braces"
*/
function curlyBrace(str) {
    var rxp = /{([^}]+)}/g,
        curMatch;
    while (curMatch = rxp.exec(str)) {
        str = str.replace(new RegExp('{' + curMatch[1] + '}', 'gi'), window[curMatch[1]]);
    }
    return str;
}
function ajaxPost(domId, ajaxUrl, params, callback, errorCalB, datatype, extraParams) {
    mylog.log("url", "ajaxPost 2", domId, ajaxUrl, params, datatype);
    var domAjaxId = domId;

    var ajaxOptions = {
        type: "POST",
        url: ajaxUrl,
        success: function (data) {
            if (datatype == "none")
                console.log();
            else if (domAjaxId)
				$(domAjaxId).empty().html(data);
            //else if(typeof data.msg === "string" )
            //  toastr.success(data.msg);
            // else if (domAjaxId != null)
            //     $(domAjaxId).html(JSON.stringify(data, null, 4));

            if (typeof callback === "function")
                callback(data);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            if (typeof errorCalB === "function")
                errorCalB(xhr, ajaxOptions, thrownError);
            else
                mylog.error("url", thrownError);
        },
        statusCode: {
            301:function(xhr){
                if(xhr.getResponseHeader("X-Redirect")) {
                    location.href = xhr.getResponseHeader("X-Redirect");
                }
            },
            302:function(xhr){
                if(xhr.getResponseHeader("X-Redirect")) {
                    location.href = xhr.getResponseHeader("X-Redirect");
                }
            }
        }
    }
    if (!params)
        params = {};

    if (activeModuleId == "costum") {
        mylog.log("url", "costum LBH", costum.slug, contextData);
        //WARNING pour template costum le vrai sulg c'est contextSlug
        params.costumSlug = costum.contextSlug ? costum.contextSlug : costum.slug;
        params.costumEditMode = costum.editMode;
        //cas d'un costum utilisé comme un template par plusieru element 
        if (contextData && contextData.id && costum.contextId == contextData.id) {
            params.costumId = contextData.id;
            params.costumType = contextData.collection ? contextData.collection : contextData.type;
        }
    }
    // Ajax de jquery ne persiste pas les clés avec des objets vide, donc il faut transformer les objets vide en string vide {} --> ""
    ajaxOptions.data = cleanEmptyObjectInParams(params);
    if (typeof datatype != "undefined")
        ajaxOptions.dataType = datatype;
    if (typeof extraParams != "undefined") {
        if (typeof extraParams.headers != "undefined")
            ajaxOptions.headers = extraParams.headers;
        if (typeof extraParams.contentType != "undefined")
            ajaxOptions.contentType = extraParams.contentType;
        if (typeof extraParams.async != "undefined")
            ajaxOptions.async = extraParams.async;
        if (typeof extraParams.global != "undefined")
            ajaxOptions.global = extraParams.global;
        if (typeof extraParams.cache != "undefined")
            ajaxOptions.cache = extraParams.cache;
        if (typeof extraParams.processData != "undefined")
            ajaxOptions.processData = extraParams.processData;
        if (typeof extraParams.beforeSend != "undefined")
            ajaxOptions.beforeSend = extraParams.beforeSend;
        if (typeof extraParams.xhr != "undefined")
            ajaxOptions.xhr = extraParams.xhr;

    }

    mylog.log("ajaxPost.ExtraParams", ajaxOptions);
    $.ajax(ajaxOptions);
}

function cleanEmptyObjectInParams(params) {
    for (const [key, value] of Object.entries(params)) {
        if (value===null || (typeof value == "object" && Object.keys(value).length == 0)) {
            params[key] = "";
        }
    }
    return params;
}

function getAjax(id, ajaxUrl, callback, datatype, blockUI) {
    $.ajaxSetup({ cache: true });
    mylog.log("url", "getAjax", id, ajaxUrl, callback, datatype, blockUI);
    if (datatype != "html")
        $(id).html("<div class='cblock'><div class='centered'><i class='fa fa-cog fa-spin fa-2x icon-big text-center'></i> Loading</div></div>");
    $.ajax({
        url: ajaxUrl,
        type: "GET",
        cache: true,
        success: function (data) {
            if (data.error) {
                mylog.warn(data.error);
                toastr.error(data.error.msg);
            } else if (datatype === "html")
                $(id).html(data);
            else if (datatype === "norender")
                mylog.log("no render", ajaxUrl)
            else if (typeof data === "string" && datatype != null && data.trim())
                toastr.success(data);
            else
                $(id).html(JSON.stringify(data, null, 4));
            if (typeof callback === "function")
                callback(data, id);
            //if(blockUI)
            //$.unblockUI();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            mylog.error("url", "unavailable ressource. try contacting an admin." + ajaxUrl);
            //mylog.error(thrownError);
            //mylog.log("URL : ", url);
            setTimeout(function () { urlCtrl.loadByHash('#') }, 3000);
            if (blockUI)
                $.unblockUI();
        }
    });
}
/*
what : can be a simple string which will go into the title bar 
or an aboject with properties like title, icon, desc
getModal({title:"toto"},"/communecter/project/projectsv")
 */
function getModal(what, ajaxUrl, id) {

    loaded = {};
    $('#ajax-modal').modal("hide");
    if (id) ajaxUrl
    ajaxUrl = ajaxUrl + id;
    mylog.log("url", "getModal", what, "ajaxUrl", ajaxUrl, "event", id);
    //var params = $(form).serialize();
    //$("#ajax-modal-modal-body").html("<i class='fa fa-cog fa-spin fa-2x icon-big'></i> Loading");

    $.unblockUI();
    $("#ajax-modal-modal-title").html("<i class='fa fa-refresh fa-spin'></i> Chargement en cours. Merci de patienter.");
    $("#ajax-modal-modal-body").html("");
    $('#ajax-modal').modal("show");
    $.ajax({
        type: "GET",
        url: baseUrl + ajaxUrl
        //dataType : "json"
        //data: params
    })
        .done(function (data) {
            if (data) {
                /*if(!selectContent)
                    selectContent = data.selectContent;*/
                title = (typeof what === "object" && what.title) ? what.title : what;
                icon = (typeof what === "object" && what.icon) ? what.icon : "fa-pencil";
                desc = (typeof what === "object" && what.desc) ? what.desc + '<div class="space20"></div>' : "";

                $("#ajax-modal-modal-title").html("<i class='fa fa-" + icon + "'></i> " + title);
                $("#ajax-modal-modal-body").html(desc + data);
                $('#ajax-modal').modal("show");
            } else {
                mylog.error("url", "bug get " + what, ajaxUrl, id);
            }
        })
        .error(function (data) {
            mylog.log("url", "error getModal");
            mylog.dir(data);
        });
}

//js ex : "/themes/ph-dori/assets/plugins/summernote/dist/summernote.min.js"
//css ex : "/themes/ph-dori/assets/plugins/summernote/dist/summernote.css"
function lazyLoad(js, css, callback, external) {
    $.ajaxSetup({
        cache: true
    });
    mylog.warn("lazyLoad", js, css, callback, external);

    var urlCSS = css;
    var urlJS = js;
    var versionAssetsUrl = (typeof versionAssets != "undefined" && versionAssets) ? '?v=' + versionAssets : '';
    if (!notNull(external)) {
        urlCSS = (notNull(css)) ? constructSourcePath(css) : null;
        urlJS = (notNull(js)) ? constructSourcePath(js) : null;
    }
    if (urlCSS)
        $("<link/>", {
            rel: "stylesheet",
            type: "text/css",
            href: urlCSS + versionAssetsUrl
        }).appendTo("head");
    if (!$('script[src="' + urlJS + versionAssetsUrl + '"]').length) {
        mylog.log("lazyLoad  before getScript", urlJS + versionAssetsUrl);
        $.getScript(urlJS + versionAssetsUrl, function (data, textStatus, jqxhr) {
            //mylog.log("lazyLoad getScript");
            //if (typeof dynform !== undefined) alert("script has been loaded!");
            if (typeof callback === "function")
                callback(data);
        });
    } else {
        mylog.log("lazyLoad notScript");
        if (typeof callback === "function")
            callback();
    }
}

function lazyImgLoading(domArray, callB) {
    if (domArray.length == 1) {
        if ($(domArray[0]).length > 0) {

            $(domArray[0]).on('load', function () {
                callB();
            }).each(function () {
                if (this.complete) {
                    callB();
                    // lazyImgLoading(domArray, callB);
                }
            });
        } else {
            //alert();
            callB();
        }
    } else {
        domImg = $(domArray[0]);
        domArray.splice(0, 1);
        if (domImg.length > 0) {
            domImg.on('load', function () {
                domArray.splice(0, 1);
                lazyImgLoading(domArray, callB);
            }).each(function () {
                if (this.complete) {
                    lazyImgLoading(domArray, callB);
                }
            });
        } else {
            lazyImgLoading(domArray, callB);
        }
    }

}

function htmlExists(selector,callback){
    var interval = setInterval(function() {
        if ($(selector).length > 0 ) {
            clearInterval(interval); 
            callback($(selector));
        }
    }, 900);
}

function removeFromArray(arr, val) {
    var filtered = arr.filter(function (value, index, arr) {
        return value != val;
    });
    return filtered;
    /*const index = arr.indexOf(val);
    if (index > -1) {
       arr.splice(index, 1);
    }
    return arr;*/
}

function isTimestamp(n) {
    const parsed = parseFloat(n);
    return !Number.isNaN(parsed) && Number.isFinite(parsed) && /^\d+\.?\d+$/.test(n);
}

function constructSourcePath(url) {
    //alert(url+" / "+baseUrl);
    construct = baseUrl.split("/");
    checkUrl = url.split("/");
    // console.log("path inspector : ",construct,checkUrl);
    inc = 0;
    $.each(construct, function (e, v) {
        if ($.inArray(v, checkUrl) < 0)
            checkUrl.splice(inc, 0, v);
        inc++;
    });
    return checkUrl.join("/");
}
var countLazyLoad = null;

function lazyLoadMany(list, callback, notBase) {
    $.ajaxSetup({
        cache: true
    });
    var versionAssetsUrl = (typeof versionAssets != "undefined" && versionAssets) ? '?v=' + versionAssets : '';
    mylog.warn("lazyLoadMany", list.length, callback, notBase);
    countLazyLoad = 0;
    $.each(list, function (i, v) {
        var url = (notBase == true ? v : baseUrl + v);
        mylog.log("LLM --> url", url);
        if (url.indexOf("js") > 0 && !$('script[src="' + url + versionAssetsUrl + '"]').length) {
            //!mylog.log("lazyLoad  before getScript",js);
            $.getScript(url + versionAssetsUrl, function (data, textStatus, jqxhr) {
                //mylog.log("lazyLoad getScript");
                //if (typeof dynform !== undefined) alert("script has been loaded!");
                //countLazyLoad++; 
                if (typeof callback === "function")
                    callback(data);
            });
        } else if (url.indexOf("css") > 0) {
            $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: url + versionAssetsUrl
            }).appendTo("head");
            //countLazyLoad++;
            if (typeof callback === "function")
                callback();
        } else {
            mylog.error("lazyLoadMany notScript");
            if (typeof callback === "function")
                callback();
        }
        countLazyLoad++;
    })
}

var countLazyLoad2 = null;

function lazyLoadMany2(list, callback, notBase) {
    mylog.warn("lazyLoadMany", list.length, callback, notBase);
    countLazyLoad2 = 0;
    $.each(list, function (i, v) {
        var url = (notBase == true ? v : baseUrl + v);
        mylog.log("LLM --> url", url);
        if (url.indexOf("js") > 0 && !$('script[src="' + url + '"]').length) {
            //!mylog.log("lazyLoad  before getScript",js);
            $.getScript(url, function (data, textStatus, jqxhr) {
                //mylog.log("lazyLoad getScript");
                //if (typeof dynform !== undefined) alert("script has been loaded!");
                countLazyLoad2++;
            });
        } else if (url.indexOf("css") > 0) {
            $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: url
            }).appendTo("head");
            countLazyLoad2++;

        } else {
            mylog.error("lazyLoadMany notScript");
        }
    });

    if (typeof callback === "function")
        callback();
}

var mylog = (function () {

    return {
        log: function () {
            if (debug) {
                var args = Array.prototype.slice.call(arguments);
                console.log.apply(console, args);
            }
        },
        warn: function () {
            if (debug) {
                var args = Array.prototype.slice.call(arguments);
                console.warn.apply(console, args);
            }
        },
        debug: function () {
            if (debug) {
                var args = Array.prototype.slice.call(arguments);
                console.debug.apply(console, args);
            }
        },
        info: function () {
            if (debug) {
                var args = Array.prototype.slice.call(arguments);
                console.info.apply(console, args);
            }
        },
        dir: function () {
            if (debug) {
                var args = Array.prototype.slice.call(arguments);
                console.warn.apply(console, args);
            }
        },
        error: function () {
            if (debug) {
                var args = Array.prototype.slice.call(arguments);
                console.error.apply(console, args);
            }
        }
    }
}());

/* --------------------------------------------------------------- */
//hide all children
//show all elements corresponding to class id
function toggle(id, siblingsId, activate, callback) {
    mylog.log("toggle('" + id + "','" + siblingsId + "')");
    $(siblingsId).addClass("hide");
    if (activate)
        $(siblingsId + "Btn").removeClass("active");
    //if( !$("."+id).is(":visible") ) 
    $(id).removeClass("hide");
    if (activate) {
        idT = id.split(",");
        $(idT[0] + "Btn").addClass("active");
    }
    if (typeof callback === "function")
        callback();
}

/* --------------------------------------------------------------- */

function scrollTo(id) {
    if ($(id).length) {
        //mylog.log(".my-main-container initscrollTo ", id);
        //mylog.log($(id).position().top);

        $(".my-main-container").animate({ scrollTop: $(id).position().top - 50 }, 1200);
    }
}
/* --------------------------------------------------------------- */
function delay(callback, ms) {
    var timer = 0;
    return function () {
        var context = this,
            args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            callback.apply(context, args);
        }, ms || 0);
    };
}
/* --------------------------------------------------------------- */

function Object2Array(obj) {
    jsonAr = [];
    $.each(obj, function (k, v) {
        v.id = k;
        delete v._id;
        jsonAr.push(v);
    });
    mylog.dir(jsonAr);
    return jsonAr;
}

/* --------------------------------------------------------------- */

function arrayCompare(a1, a2) {
    if (a1.length != a2.length) return false;
    var length = a2.length;
    for (var i = 0; i < length; i++) {
        if (a1[i] !== a2[i]) return false;
    }
    return true;
}

/* --------------------------------------------------------------- */

function inArray(needle, haystack) {
    var length = haystack.length;
    for (var i = 0; i < length; i++) {
        if (typeof haystack[i] == 'object') {
            if (arrayCompare(haystack[i], needle)) return true;
        } else {
            if (haystack[i] == needle) return true;
        }
    }
    return false;
}

/*var list = [];
jsonHelper.getKeys = function(obj)
{
  list = Object.keys(obj);
  $.each( obj,function(k,v)
  {
    if(v.subtype)
    {
      var s = jsonHelper.getKeys(v.subtype);
      $.each( s ,(i,ki){
        list.push(ki);
      });
    }
  })
  return list;
} 
jsonHelper.getKeys(typeObj);
*/

/* ------------------------------- */

function intersection_destructive(array1, array2) {
    var result = $.grep(array1, function (element) {
        return $.inArray(element, array2) !== -1;
    });

    return result;
}

/*function log(msg,type){
  if(debug){
     try {
      if(type){
        switch(type){
          case 'info': mylog.info(msg); break;
          case 'warn': mylog.warn(msg); break;
          case 'debug': mylog.debug(msg); break;
          case 'error': mylog.error(msg); break;
          case 'dir': mylog.dir(msg); break;
          default : mylog.log(msg);
        }
      } else
            mylog.log(msg);
    } catch (e) { 
       //alert(msg);
    }
  }
}*/

/* --------------------------------------------------------------- */

function showAsColumn(resp, id) {
    //log(resp,"dir");
    if ($("#" + id).hasClass("columns")) {
        mylog.log("rebuild");
        $("#" + id).columns('setMaster', Object2Array(resp));
        $("#" + id).columns('create');
    } else {
        $("#" + id).columns({
            data: Object2Array(resp),
            schema: [
                { "header": "Name", "key": "name" },
                { "header": "Edit", "key": "id", "template": "<a class='openModal' href='{{id}}' data-id='{{id}}' data-name='{{name}}'>{{id}}</a>" }
            ]
        });

        $(".openModal").click(function (e) {
            e.preventDefault();
            openModal($("#getbyMicroformat").val(), $("#getbyCollection").val(), this.dataset.id, "dynamicallyBuild");
        })
    }
}

/* --------------------------------------------------------------- */

function slugify(value, slug) {

    //mylog.log("slugify",value,slug);
    var rExps = [
        { re: /[\xC0-\xC6]/g, ch: 'A' },
        { re: /[\xE0-\xE6]/g, ch: 'a' },
        { re: /[\xC8-\xCB]/g, ch: 'E' },
        { re: /[\xE8-\xEB]/g, ch: 'e' },
        { re: /[\xCC-\xCF]/g, ch: 'I' },
        { re: /[\xEC-\xEF]/g, ch: 'i' },
        { re: /[\xD2-\xD6]/g, ch: 'O' },
        { re: /[\xF2-\xF6]/g, ch: 'o' },
        { re: /[\xD9-\xDC]/g, ch: 'U' },
        { re: /[\xF9-\xFC]/g, ch: 'u' },
        { re: /[\xC7-\xE7]/g, ch: 'c' },
        { re: /[\xD1]/g, ch: 'N' },
        { re: /[\xF1]/g, ch: 'n' },
        { re: /['"]/g, ch: '-' },
        { re: /[&]/g, ch: '' }
    ];

    // converti les caractères accentués en leurs équivalent alpha
    for (var i = 0, len = rExps.length; i < len; i++)
        value = value.replace(rExps[i].re, rExps[i].ch);

    // 1) met en bas de casse
    // 2) remplace les espace par des tirets
    // 3) enleve tout les caratères non alphanumeriques
    // 4) enlève les doubles tirets
    if (typeof slug != "undefined" && slug) {
        return value.replace(/\s+/g, '-')
            .replace(/[^a-z0-9A-Z]/g, '')
            .replace(/\-{2,}/g, '-');
    } else {
        return value.replace(/\s+/g, '-')
            .replace(/\-{2,}/g, '-');
    }

};

function generateIdFromString(value, preffix = "") {
    return preffix + value.toLowerCase().replace(/[^\w]/g, '_')
}

function parseBool(val) {
    return typeof val === 'number' && val > 0 || val === true || val === "true"
}


function addslashes(str) {
    //  discuss at: http://phpjs.org/functions/addslashes/
    // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // improved by: Ates Goral (http://magnetiq.com)
    // improved by: marrtins
    // improved by: Nate
    // improved by: Onno Marsman
    // improved by: Brett Zamir (http://brett-zamir.me)
    // improved by: Oskar Larsson Högfeldt (http://oskar-lh.name/)
    //    input by: Denny Wardhana
    //   example 1: addslashes("kevin's birthday");
    //   returns 1: "kevin\\'s birthday"

    return (str + '')
        .replace(/[\\"']/g, '\\$&')
        .replace(/\u0000/g, '\\0');
}

function checkAndCutLongString(text, limitLength, id, className, readMore) {
    if (text.length > limitLength) {
        allText = text;
        text = text.substring(0, limitLength) +
            "<span class='ppp'> ...</span>" +
            "<span class='endtext hidden'>" + text.substring(limitLength, text.length) + "</span>";

        if (readMore) {
            text += //"<span class='removeReadNews'> ...
                "<span>" + //<br>"+
                "<button class='btn btn-xs btn-link letter-blue btn-" + className + "' data-id='" + id + "' target='_blank'>" +
                trad.readmore +
                "</button>" +
                "</span>";
            // "<div class='allText' style='display:none;'>"+allText+"</div>";
        } else {
            text += " ..."
        }
    }
    return "<span class='cls-" + id + "'>" + text + "</span>";
}
var jsonHelper = {
    /*
  
     */
    a: { x: { b: 2 } },

    test: function () {
        mylog.log("init", JSON.stringify(this.a));

        this.getValueByPath(this.a, "x.b");
        mylog.log("this.a set x.b => 1000");
        this.setValueByPath(this.a, "x.b", 1000);
        this.getValueByPath(this.a, "x.b")
        mylog.log(JSON.stringify(this.a));

        mylog.log("this.a.x set b => 2000");
        this.setValueByPath(this.a.x, "b", 2000);
        this.getValueByPath(this.a, "x.b");
        mylog.log(JSON.stringify(this.a));

        mylog.log("this.a.x set b => {m:1000}");
        this.setValueByPath(this.a.x, "b", { m: 1000 });
        this.getValueByPath(this.a, "x.b");
        mylog.log(JSON.stringify(this.a));

        mylog.log("this.a set x.b.a => 4000");
        this.setValueByPath(this.a, "x.b.a", 4000);
        this.getValueByPath(this.a, "x.b");
        mylog.log(JSON.stringify(this.a));

        mylog.log("this.a set x.b.a => {m:1000}");
        this.getValueByPath(this.a, "x.b.a");
        this.setValueByPath(this.a, "x.b.a", { m: 1000 });
        this.getValueByPath(this.a, "x.b.a");
        mylog.log(JSON.stringify(this.a));

        mylog.log("this.a set x.b.a.b.c.d => {xx:1000}");
        this.getValueByPath(this.a, "x.b.a.b.c.d");
        this.setValueByPath(this.a, "x.b.a.b.c.d", { xx: 1000, yy: 25000 });
        mylog.log(JSON.stringify(this.a));

        mylog.log("this.a reset x.b.a.b.c.d.yy => 100000");
        this.getValueByPath(this.a, "x.b.a.b.c.d.yy");
        this.setValueByPath(this.a, "x.b.a.b.c.d.yy", 100000);
        mylog.log(JSON.stringify(this.a));

        mylog.log("this.a delete x.b.a.b.c.d.yy");
        this.deleteByPath(this.a, "x.b.a.b.c.d.yy");
        mylog.log(JSON.stringify(this.a));

        mylog.log("this.a delete x.b.a.b.c.d.yy");
        this.deleteByPath(this.a, "x.b.a.b.c.d.yy");
        mylog.log(JSON.stringify(this.a));
    },
    strTest: "\n>name:xxx" +
        "\n>desc:yyyyyyyyyy" +
        "\n>email:zzzz@yyy.com" +
        "\n>adr:102 rue ppppppp" +
        "\n>cp:97421" +
        "\n>latlon:21,55" +
        "\n>type:NGO" +
        "\n>admin:admin",
    testStringForm: function () {
        mylog.log("strTest", this.strTest);
        mylog.warn("------------string to json------------------");
        this.stringFormtoJson(this.strTest);
        mylog.warn("------------building form ------------------");
        this.stringFormtoJson(this.strTest, "testForm");
        mylog.warn("------------ form serialised ------------------");
        mylog.log($("#testForm").serialize());
        mylog.warn("------------ form serialised as JSON ------------------");
        mylog.dir($("#testForm").serializeFormJSON());
    },
    /* 
      Convert a structured string to json object
      the simplest Form in internet history
      can be used as a HTML hidden Form builder as well
    */
    stringFormtoJson: function (str, buildForm) {
        mylog.log("strTest", str);
        separator = "\n>";
        fields = str.split(separator);
        res = {};
        if (buildForm) {
            if (!$("#" + buildForm).length) {
                $(".my-main-container").append($("<form>", { id: buildForm, "method": "POST" }));
                $(".my-main-container").prepend($("<span>", { class: "errorHandler" }));
            } else
                $("#" + buildForm).html("");
        }
        $.each(fields, function (i, v) {
            mylog.log(v);
            keyVal = v.split(":");
            if (typeof keyVal[1] != "undefined" && keyVal[1] != "") {
                res[keyVal[0]] = keyVal[1];
                if (buildForm)
                    $("#" + buildForm).append("<input type='hidden' id='" + keyVal[0] + "' name='" + keyVal[0] + "' value='" + keyVal[1] + "'/>");
            }
        })
        mylog.dir(res);
        return res;
    },
    /*
    srcObj = any json OBJCT
    path =  STRING "node1.node2.node3"
     */
    getValueByPath: function (srcObj, path) {
        node = srcObj;
        //mylog.log("path",path);
        if (!path)
            return node;
        else if (typeof path == "object" && path.value) {
            return path.value;
        } else if (path.indexOf(".")) {
            pathArray = path.split(".");
            $.each(pathArray, function (i, v) {
                if (node != undefined && node[v] != undefined)
                    node = node[v];
                else {
                    node = undefined;
                    return;
                }
            });
            return node;
        } else if (node[path])
            return node[path];
        else
            return "";
    },
    setValueByPath: function (srcObj, path, value) {
        node = srcObj;
        if (!path) {
            node = value;
        } else if (path.indexOf(".")) {
            pathArray = path.split(".");
            nodeParent = null;
            lastKey = null;
            $.each(pathArray, function (i, v) {
                if (!node[v]) {
                    //mylog.log("building node",v);
                    node[v] = {};
                }
                nodeParent = node;
                node = node[v];
                lastKey = v;
            });
            //mylog.log(node,nodeParent,lastKey);
            nodeParent[lastKey] = value;
        } else
            node[path] = value;
    },
    insertKeyValueByPathAndInc: function(srcObj, path, key, value, inc) {
        /* @srcObj : object to modify
            @path : string to indicate the path inside obj
            @key/value : key/value to insert in object
            @inc : place of key/value inside obj
        */ 
        node = srcObj;
        mylog.log("set val", path, value, inc);
        if (!path) {
            node = value;
        } else if (path.indexOf(".")) {
            pathArray = path.split(".");
            nodeParent = null;
            lastKey = null;
            $.each(pathArray, function(i, v) {
                if (!node[v]) {
                    mylog.log("building node",v);
                    node[v] = {};
                }
                nodeParent = node;
                node = node[v];
                lastKey = v;

            });
            nodeParent[lastKey]=Object.entries(nodeParent[lastKey]); //convert object to keyValues ["key1", "value1"] ["key2", "value2"]
            nodeParent[lastKey].splice(inc,0, [key,value]); // insert key value at the index you want like 1.
            nodeParent[lastKey]=Object.fromEntries(nodeParent[lastKey]);
          } else{
            node = (notEmpty(path)) ? node[path] : node;  
            node = Object.entries(node); //convert object to keyValues ["key1", "value1"] ["key2", "value2"]
            node.splice(inc,0, [key,value]); // insert key value at the index you want like 1.
            node = Object.fromEntries(node);
        }
    },
    deleteByPath: function(srcObj, path) {
        mylog.log("DELETING PROCCESSSUS", path);
        nodeParent = srcObj;
        lastChild = null;
        node = srcObj;
        if (path.indexOf(".")) {
            pathArray = path.split(".");
            if (pathArray.length) {
                $.each(pathArray, function (i, v) {
                    nodeParent = node;
                    lastChild = v;
                    node = node[v];
                });
            }
            mylog.log("befforooore delette deleting", nodeParent, lastChild)
            delete nodeParent[lastChild];
            mylog.log("on deleteeeeee processssss", costumizer.obj);
        } else
            delete nodeParent[path];

    },
    swapJsonKeyValues: function (srcObj) {
        var one, output = {};
        for (one in srcObj) {
            if (srcObj.hasOwnProperty(one)) {
                output[srcObj[one]] = one;
            }
        }
        return output;
    },
    /*
    convert
    { "clim":75,"informatique": 223 }
    to 
    [{  "label" : "Climatisation",  "value":75},{"label" : "Climatisation", "value":75}]
     */
    Object2GraphArray: function (srcObj) {
        destArray = [];
        //mylog.dir(srcObj);
        $.each(srcObj, function (k, v) {
            if (v.value != 0)
                destArray.push(v);
        });
        //mylog.dir(destArray);
        return destArray;
    },
    eval: function (obj, pathJson) {
        var pathT = pathJson.split(".");
        res = null;
        var dynPath = obj;
        pathT.shift();

        $.each(pathT, function (i, k) {
            dynPath = dynPath[k];
            if (typeof dynPath != "undefined" && i == pathT.length - 1)
                res = dynPath;
            //console.log(k,res);

        });
        return res;
    },
    pathExists : function (fullPath) {
        try {
            var pathArr = fullPath.split('.');
            var obj = window;
            for (var i = 0; i < pathArr.length; i++) {
            obj = obj[pathArr[i]];
            if (typeof obj === "undefined" || obj === null) {
                return false;
            }
            }
            return true;
        } catch (err) {
            return false;
        }
    },
    //tests existance of every step of a json path 
    //can also validate element type 
    // ex : 
    //jsonHelper.notNull("themeObj.blockUi") > test themeObj > test themeObj.blockUi ...etc if more steps
    //jsonHelper.notNull("themeObj.blockUi","function") > false or true accordingly
    notNull: function (pathJson, type) {
        //mylog.log("notNull",pathJson);
        if (pathJson.indexOf("[") >= 0)
            pathJson = pathJson.replace(/\[/g, '.').replace(/]/g, '');

        var pathT = pathJson.split(".");
        res = false;
        var dynPath = "";
        $.each(pathT, function (i, k) {
            dynPath = (i == 0) ? k : dynPath + "." + k;
            //mylog.log(dynPath);
            //typeof eval("typeObj.poi") != "undefined"

            if (typeof eval(dynPath) != "undefined") {
                //mylog.log(dynPath);
                if (i == pathT.length - 1) {
                    res = true;
                    if (type != null && typeof eval(dynPath) != type)
                        res = false;
                }
            } else {
                //  mylog.error("notNull", dynPath," is undefined");
                res = false;
                return false;
            }
        });
        return res;
    },

    jsonFromToJson: {

        "fromjson": [
            { "x": 1, "y": { "c": 20, "o": 30 } },
            { "x": 2, "y": { "c": 60, "o": 50 } }
        ],
        "rules": {
            "a": "x",
            "b": function (obj) { return obj.y.c + obj.y.o }
        },
        "outputLine": { "a": "", "b": "" },
        "toJson": [],

        "test": function () {
            mylog.log("test");
            this.convert();
        },

        "convert": function () {
            mylog.log("convert");
            this.toJson = [];
            $.each(this.fromJson, function (i, fromObj) {

                newLine = this.outputLine;
                /*$.each(this.rules,function(keyTo, convertTo){
                  mylog.log("convert rules ",fromObj,keyTo,convertTo);     
                    if(typeof convertTo == "function")
                      newLine[ keyTo ] = convertTo( fromObj );
                    else
                      newLine[ keyTo ] = fromObj[convertTo];
                });*/

                this.toJson.push(newLine);
            });
            return this.toJson;
        }

    }

};

$.fn.serializeFormJSON = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

function showDebugMap() {
    if (debugMap && debugMap.length > 0) {
        $.each(debugMap, function (i, val) {
            mylog.dir(val);
        });
        toastr.info("check Console for " + debugMap.length + " maps");
    } else
        toastr.error("no maps to show, please do debugMap.push(something)");

}


function exists(val) {
    return typeof val != "undefined";
}

function notNull(val) {
    return (typeof val != "undefined" && val != null);
}

function notEmpty(val) {
    if (typeof val == "object" && val != null)
        return (Object.keys(val).length > 0) ? true : false;
    else
        return (typeof val != "undefined" && val != null && val != "");
}

function notEmptyAllType(val) {
    if (typeof val == "object" && val != null)
        return (Object.keys(val).length > 0) ? true : false;
    else if (typeof val == "boolean")
        return true;
    else return (typeof val != "undefined" && (val != null && val != ""));;
}

function removeEmptyAttr(jsonObj, sourceObj) {

    $.each(jsonObj, function (key, value) {
        if (sourceObj && sourceObj[key]) {

        }

        if (value === "" || value === null || value === undefined) {
            delete jsonObj[key];
        }
    });
}

function buildSelectOptions(list, value, dataValue, order, isPolice = false, isImagepicker = false) {
    mylog.log("fieldObj buildSelectOptions ", value, list, order);
    mylog.log("fieldObj buildSelectOptions tradCategory", tradCategory);
    var html = "";
    var orderList = list;
    if (typeof order != "undefined" && order != null && order == true)
        orderList = dataHelper.sortObjectByValue(list);
    if (orderList) {
        mylog.log("dataHelper.sortObjectByValue buildSelectOptions", orderList);
        $.each(orderList, function (optKey, optVal) {
            mylog.log("fieldObj buildSelectOptions optVal", optVal, tradCategory[optVal]);
            valueName = (typeof tradCategory[optVal] != "undefined") ? tradCategory[optVal] : optVal;
            selected = (value == optVal || value == optKey) ? "selected" : "";
            if (Array.isArray(value) && ($.inArray(optVal, value) >= 0 || $.inArray(optKey, value) >= 0))
                selected = "selected";
            mylog.log("fieldObj buildSelectOptions last", value, optKey, optVal, selected);
            if (isPolice == true) {
                html += `<option style="font-family:'${(valueName.split(" ").join("_"))}'" value="${optKey}" ${selected}>${valueName}</option>`;
            } else if (isImagepicker == true) {
                html += `<option data-img-label="${valueName}" data-img-src="${assetPath}/images/blockCmsPaint/${optKey}" value="${optKey}" ${selected}>${valueName}</option>`;
            } else {
                //html += '<option value="'+optKey+'" data-value="'+(exists(dataValue[optKey]) ? JSON.stringify(dataValue[optKey]) : "")+'" '+selected+'>'+valueName+'</option>';
                html += `<option value="${optKey}" data-value="${(exists(dataValue) && exists(dataValue[optKey]) ? escapeHtml(JSON.stringify(dataValue[optKey])) : "")}" ${selected} >${valueName}</option>`;
            }
        });
    }

    return html;
}

function buildSelectGroupOptions(list, value, groupSelected = false) {
    mylog.log("----- fieldObj buildSelectGroupOptions start", value, list, groupSelected);
    var html = "";
    //mylog.log("fieldObj buildSelectGroupOptions html", html);
    if (typeof list != "undefined" && list != null && Object.keys(list).length > 0) {
        mylog.log("fieldObj buildSelectGroupOptions list", list);
        $.each(list, function (groupKey, groupVal) {
            mylog.log("fieldObj buildSelectGroupOptions groupKey", groupKey, groupVal, value);
            var data = "";
            var style = "";
            if (groupSelected === true) {
                selected = (groupKey == value || $.inArray(groupKey, value) >= 0) ? "selected" : "";
                mylog.log("fieldObj buildSelectGroupOptions selected", selected);
                html += '<option class="optgroup" value="' + groupKey + '" ' + selected + '>' + groupKey + '</option>';
                style = 'class="subgroup" ';
            } else {
                data = (groupKey) ? 'data-type="' + groupKey + '"' : "";
                html += '<optgroup value="' + groupKey + '" label="' + groupKey + '" >';
            }

            if (typeof groupVal != "undefined" && groupVal != null && Object.keys(groupVal).length > 0) {
                $.each(groupVal, function (optKey, optVal) {
                    mylog.log("fieldObj buildSelectGroupOptions groupVal", optKey, optVal, value);
                    //$.each(groupVal.options, function(optKey, optVal) {
                    selected = (optVal == value || $.inArray(optVal, value) >= 0) ? "selected" : "";
                    html += '<option  value="' + optVal + '" ' + style + ' ' + selected + ' ' + data + '>' + optVal + '</option>';
                });
            }
            if (groupSelected != true)
                html += '</optgroup>';
            mylog.log("---- fieldObj buildSelectGroupOptions end html2", html);
        });
    }
    return html;
}


function buildSelectFromList(list, value) {
    mylog.log("buildSelectFromList ", value, list);
    var html = "";
    mylog.log("list", list);
    if (list) {
        $.each(list, function (groupKey, groupVal) {
            var data = (groupKey) ? 'data-type="' + groupKey + '"' : "";
            html += '<optgroup label="' + trad[groupKey] + '" >';
            $.each(groupVal, function (optKey, optVal) {
                selected = (optKey == value) ? "selected" : "";
                html += '<option value="' + optVal["_id"]["$id"] + '" ' + selected + ' ' + data + '>' + optVal.name + '</option>';
            });
            html += '</optgroup>';
        });
    }
    return html;
}


function buildRadioOptions(list, value, nameOption) {
    var html = "";
    if (list) {
        $.each(list, function (optKey, optVal) {

            rchecked = (value == optKey) ? "checked" : "";
            ricon = (optVal.icon) ? '<span class="fa fa-' + optVal.icon + '"></span> ' : '';
            rlbl = (optVal.lbl) ? optVal.lbl : '';
            rclass = (optVal.class) ? optVal.class : '';
            mylog.log("builing RadioOption", optKey, rchecked, ricon, rlbl, rclass);
            html += '<label class="btn btn-default ' + rclass + '">' +
                '<input type="radio" name="' + nameOption + '" value="' + optKey + '" ' + rchecked + '>' +
                ricon + rlbl +
                '</label>';


        });
    }
    return html;
}

function ucfirst(s) {
    if (typeof s !== 'string') return '';
    return s.charAt(0).toUpperCase() + s.slice(1);
}

function formatDate(date) {
    var monthNames = [
        "Jan", "Fev", "Mars",
        "Avril", "Mai", "Juin", "Juil",
        "Aout", "Sept", "Oct",
        "Nov", "Dec"
    ];

    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();

    return day + ' ' + monthNames[monthIndex] + ' ' + year;
}

function archi() {
    // $('.archi').each(function(i,k){
    //   var str = "<div class='center text-white bold bg-red'>"+
    //               $(k).attr('id')+
    //             "</div>";
    //   $(k).append(str); 
    // })
    mylog.log("*********************\nREQUEST : \n**********************\n", location.hash.toUpperCase());

    if (typeof costum.app[location.hash] != "undefined") {
        mylog.log("costum.app." + location.hash);
        mylog.log(costum.app[location.hash]);
        mylog.log("*** TODO : cumuler et enregistrer la stacktrace de tout les appels JS");
        mylog.log("*** TODO : afficher le nombre d'appel à la DB pour cette page, \ncheck DbAccessCount\nbaseUrl/moduleId/log/dbaccess\nsession[dbAccess]\nMonitoringAction");
    }

    mylog.log("*********************\n ALL DIVS : \n**********************\n");
    $('div').each(function (i, k) {
        if ($(k).attr('id')) {

            var excludeList = ["diigo", "lightbox", "FN-"]
            var showArchi = true;
            $.each(excludeList, function (ix, exid) {
                if ($(k).attr('id').indexOf(exid) >= 0)
                    showArchi = false;
            });

            if (showArchi) {
                var bgColor = "red";
                var dataArchi = "block : " + $(k).attr('id');

                if ($(k).data('archi')) {
                    bgColor = "green";
                    dataArchi = "block : " + $(k).attr('id'), $(k).data('archi'), $(k);
                }

                mylog.log(dataArchi);


                if ($(k).attr('id')) {
                    $(k).append("<span class='archi text-white bold bg-" + bgColor + "'>" +
                        $(k).attr('id') + "</span>").css("border", bgColor + " 1px solid");
                }
            }

        }

    })
}

function alignInput2(sectionDyfProperties, beginWith, column, columnXs, offsetBefore = null, offsetRepeat = null, label = "", color = "", hidden = "", opt = {}) {
    var count = 0;
    var entry = beginWith;
    var beginWith = new RegExp('^' + beginWith);
    var arrayClass = [];
    var styleWrapped = "";
    $.each(sectionDyfProperties, function (k, v) {
        var selector = null;
        if (Array.isArray(sectionDyfProperties)) {
            selector = '.' + v;
            count++;
            arrayClass = sectionDyfProperties.map(((v, i) => '.' + v));
        } else {
            if (typeof v.inputType == "undefined") v.inputType = "text"
            if (beginWith.test(k) && v.inputType.indexOf("hidden") == -1) {
                count++;
                selector = '.' + k + v.inputType;
                arrayClass.push(selector);
            }
        }
        $(selector + ' .fa-chevron-down').hide();
        if (count == 1 && offsetBefore != null) {
            $(selector).removeClass('form-group').addClass('col-xs-' + columnXs + ' col-md-' + column + ' col-md-offset-' + offsetBefore);
        } else {
            $(selector).removeClass('form-group').addClass('col-xs-' + columnXs + ' col-md-' + column + ' col-md-offset-' + offsetRepeat);
        }
    });
    if (hidden == "hidden")
        $(arrayClass.join()).wrapAll("<div class='col-xs-12 fieldset toHide fieldset" + entry + "' style='visibility:hidden;height:0px;margin: 0px;padding:0px'></div>");
    else
        $(arrayClass.join()).wrapAll("<div class='col-xs-12 fieldset fieldset" + entry + "'></div>");

    //fieldsetClass.push(".fieldset" + entry);
    styleWrapped = `.modal-content .fieldset${entry}:before {content: "${label}";color: ${color};
                    position: absolute;
                    top: -15px;left: 14px;
                    background: white;
                    font-size: medium;
                    font-weight: bold;
                    padding: 0 15px;
                  }
                  .modal-content .fieldset${entry}{
                    border: ${opt.borderSize + ' ' + opt.borderType + ' ' + opt.borderColor} !important;
                  }`;
    $("head style").last().append(styleWrapped);
}

function getUrlParameter(keyword) {
    var urlParams = decodeURIComponent(urlCtrl.getUrlSearchParams());
    var spitedUrlParams = urlParams.split('&');
    var value = "";
    $.each(spitedUrlParams, (k, v) => {
        var splitV = v.split("=");
        if (splitV[0] == keyword)
            value = splitV[1]; return false;
    })
    if (value != "")
        return value
    else {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === keyword) {
                return typeof sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
        return false;
    }
}
function generateIntArray(start, end) {
    var intArray = [];
    for (var i = start; i <= end; i++) {
        intArray.push(i);
    }
    return intArray;
}

/**
 * use format d/m/Y h:m
 * return boolean
 */
function isDateTimeInRange(dateTime,startDate,endDate) {
    startDate = new Date(convertToYYYYMMDD(startDate));
    endDate = new Date(convertToYYYYMMDD(endDate));
    
    var inputDate = new Date(convertToYYYYMMDD(dateTime));
    
    return {
        in : inputDate >= startDate && inputDate <= endDate,
        notStarted : inputDate < startDate,
        finished: inputDate > endDate
    }
}

/**
 * return current datetime in format d/m/Y H:i
 */
function getCurrentDateTime() {
    var today = new Date();
  
    var day = today.getDate();
    var month = today.getMonth() + 1; // Les mois sont indexés à partir de 0, donc on ajoute 1
    var year = today.getFullYear();
    var hours = today.getHours();
    var minutes = today.getMinutes();
  
    // Ajoute un zéro devant les chiffres < 10 pour le formatage
    if (day < 10) {
      day = '0' + day;
    }
    if (month < 10) {
      month = '0' + month;
    }
    if (hours < 10) {
      hours = '0' + hours;
    }
    if (minutes < 10) {
      minutes = '0' + minutes;
    }
  
    var formattedDateTime = day + '/' + month + '/' + year + ' ' + hours + ':' + minutes;
    return formattedDateTime;
}

/**
 * return datetime in format dd/mm/yyyy H:i to yyyy/mm/dd H:i
 */
function convertToYYYYMMDD(dateString) {
    var dateParts = dateString.split(' ')[0].split('/');
    var timePart = dateString.split(' ')[1];

    var day = dateParts[0];
    var month = dateParts[1];
    var year = dateParts[2];

    var convertedDate = year + '/' + month + '/' + day + ' ' + timePart;
    return convertedDate;
}

/**
 * return datetime in format yyyy/mm/dd H:i to dd/mm/yyyy H:i
 */
function convertToDDMMYYYY(dateString) {
    var dateParts = dateString.split(' ')[0].split('/');
    var timePart = dateString.split(' ')[1];
    
    var year = dateParts[0];
    var month = dateParts[1];
    var day = dateParts[2];
    
    var convertedDate = day + '/' + month + '/' + year + ' ' + timePart;
    return convertedDate;
  }
  

function hashParams () {
    var urlQuery = document.location.href;
    var qs = '';
    if (urlQuery.indexOf('?') > -1 && urlQuery.indexOf('#') > -1) {
        qSplit = urlQuery.split('?');
        qs = qSplit[1].substring(qSplit[1].indexOf('?') + 1).split("&").concat(qSplit[0].substring(qSplit[0].indexOf('#') + 1).split("."));
        for (var i = 0, result = {}; i < qs.length; i++) {
            if (qs[i].indexOf('=') > -1) {
                qs[i] = qs[i].split('=');
            } else if (qs[i + 1]) {
                qs[i] = [qs[i], qs[i + 1]];
            }
            result[qs[i][0]] = qs[i][1];
        }
    } else if (urlQuery.indexOf('?') > -1) {
        qs = urlQuery.substring(urlQuery.indexOf('?') + 1).split("&");
        for (var i = 0, result = {}; i < qs.length; i++) {
            qs[i] = qs[i].split('=');
            result[qs[i][0]] = qs[i][1];
        }
    } else {
        qs = urlQuery.substring(urlQuery.indexOf('#') + 1).split(".");
        for (var i = 0, result = {}; i < qs.length; i++) {
            if (qs[i + 1]) {
                qs[i] = [qs[i], qs[i + 1]];
                result[qs[i][0]] = qs[i][1];
            }
        }
    }

    return result;
}

function addSeparatorMillier(nombre,separator=" ") {
    if(typeof nombre != "undefined"){
        var nombreString = nombre.toString();
        var parties = nombreString.split(".");
        var partieEntiere = parties[0].replace(/\B(?=(\d{3})+(?!\d))/g, separator);
        var resultat = partieEntiere;
        if (parties.length > 1) {
            resultat += "." + parties[1];
        }
        return resultat;
    }
}
function generateRandomString(length) {
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var result = '';

    for (var i = 0; i < length; i++) {
    var randomIndex = Math.floor(Math.random() * characters.length);
    result += characters.charAt(randomIndex);
    }

    return result;
}
  

(function ($) {
    $.fn.moreAndLess = function (options) {
        var settings = $.extend({
            showChar: 200,
            ellipsestext: "...",
            moretext: "Show more >",
            lesstext: "Show less",
            color: "#000",
            class: ""
        }, options);
        $(this).each(function () {
            var content = $(this).html();
            if (content.length > settings.showChar) {
                var c = content.substr(0, settings.showChar);
                var h = content.substr(settings.showChar, content.length - settings.showChar);
                var html = c + '<span class="moreellipses">' + settings.ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="javascript:void(0);" class="morelink ' + settings.class + '" style="color:' + settings.btnColor + '">' + settings.moretext + '</a></span>';
                $(this).html(html);
            }
        });
        $(this).find(".morelink").click(function () {
            if ($(this).hasClass("less")) {
                $(this).removeClass("less");
                $(this).html(settings.moretext);
            } else {
                $(this).addClass("less");
                $(this).html(settings.lesstext);
            }
            $(this).parent().prev().fadeToggle(500);
            $(this).prev().fadeToggle(500);
            return false;
        });
        var style = `
        .morecontent span {
            display: none;
        }
        .morelink {
            display: block;
        }
    `;
        $('style').last().append(style);
    }
}(jQuery))

jQuery.fn.sortDivs = function sortDivs() {
    $("> div", this[0]).sort(dec_sort).appendTo(this[0]);

    function dec_sort(a, b) { return ($(b).data("sort")) < ($(a).data("sort")) ? 1 : -1; }
}
