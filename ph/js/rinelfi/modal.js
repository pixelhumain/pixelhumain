$.fn.rinModal = function ($state) {
    if (typeof $state === 'string') {
        switch ($state) {
            case 'show':
                $(this).fadeIn(50, function () {
                    $(this).addClass('shown');
                    $(this).css('display', '');
                    $(this).trigger('rinmodal.shown');
                });
                break;
            case 'close':
                $(this).fadeOut(100, function () {
                    $(this).removeClass('shown');
                    $(this).trigger('rinmodal.closed');
                });
                break;
        }
    }
    return this;
}

$(function () {
    var interval = setInterval(function () {
        var modal = '.rin-modal';
        if ($(modal).length > 0) {
            clearInterval(interval);
            $(modal).on('click', function ($e) {
                if ($e.target == this)
                    $(this).rinModal('close');
            });
            $(modal).find('.rin-modal-close').on('click', function (__e) {
                __e.preventDefault();
                $(this).parents('.rin-modal').rinModal('close');
            });
        }
    });
});