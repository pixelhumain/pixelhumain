
import { Provider } from 'react-redux';
import { store } from '../store';
import { PureComponent } from 'preact/compat';

class AppStoreProvider extends PureComponent {
  render() {
    const { children } = this.props;
    return (
      <Provider store={store}>
        {children}
      </Provider>
    );
  }
}

export default AppStoreProvider;
