export const dashboardObjects = (state) => state.dashboardObjects;
export const isLoading = (state) => state.dashboardObjects.meta.isLoading;
export const meta = (state) => state.dashboardObjects.meta;
export const error = (state) => state.dashboardObjects.meta.error;
export const success = (state) => state.dashboardObjects.meta.success;
export const value = (state) => state.dashboardObjects.items;
export const count = (state) => state.dashboardObjects.count;