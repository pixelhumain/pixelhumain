import { TYPES } from './action';
const initialState = {
  meta: {
    isLoading: false,
    error: "",
    success: ""
  },
  items: [],
  count: 0,
};
const mutations = {
  [TYPES.setLoading](state, isLoading) {
    state = { ...state };
    state.meta.isLoading = isLoading;
    return state;
  },
  [TYPES.setError](state, error) {
    state = { ...state };
    state.meta.error = error;
    return state;
  },
  [TYPES.setSuccess](state, success) {
    state = { ...state };
    state.meta.success = success;
    return state;
  },
  [TYPES.setItems](state, payload) {
    let itemsFormat = [];
    Object.keys(payload).forEach(key => {
      itemsFormat.push({
        type: key,
        all:payload[key]['all'] || [],
        in: payload[key]['in'] || [],
        out: payload[key]['out'] || []
      })
    })
    return {
      ...state,
      items: itemsFormat,
    };
  },
  [TYPES.setCount](state, payload) {
    return {
      ...state,
      count: payload,
    };
  },
};
export default (state = initialState, action) =>
  mutations[action.type]
    ? mutations[action.type](state, action.payload)
    : state;
