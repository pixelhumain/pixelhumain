export const dashboardCharts = (state) => state.dashboardCharts;
export const isLoading = (state) => state.dashboardCharts.meta.isLoading;
export const meta = (state) => state.dashboardCharts.meta;
export const error = (state) => state.dashboardCharts.meta.error;
export const success = (state) => state.dashboardCharts.meta.success;
export const value = (state) => state.dashboardCharts.items;
export const count = (state) => state.dashboardCharts.count;

export const labels = (state) => state.dashboardCharts.items.labels;
export const datasets = (state) => state.dashboardCharts.items.datasets;