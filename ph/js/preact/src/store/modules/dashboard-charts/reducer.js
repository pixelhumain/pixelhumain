import { TYPES } from './action';
const initialState = {
  meta: {
    isLoading: false,
    error: "",
    success: ""
  },
  items: null,
  count: 0,
};
const mutations = {
  [TYPES.setLoading](state, isLoading) {
    state = { ...state };
    state.meta.isLoading = isLoading;
    return state;
  },
  [TYPES.setError](state, error) {
    state = { ...state };
    state.meta.error = error;
    return state;
  },
  [TYPES.setSuccess](state, success) {
    state = { ...state };
    state.meta.success = success;
    return state;
  },
  [TYPES.setItems](state, payload) {
    let activityReceived = [];
    if(payload.groupedData.in){

      for (const month in payload.groupedData.in) {
        const activities = payload.groupedData.in[month];
        const size = activities.length;
        activityReceived.push(size);
      }

    }
    let activitySends = [];
    if(payload.groupedData.out){
    for (const month in payload.groupedData.out) {
      const activities = payload.groupedData.out[month];
      const size = activities.length;
      activitySends.push(size);
    }
    }
    let data = {
      labels: payload.labels,
      datasets: [{
        label: 'Activités entrantes',
        data: activityReceived,
        borderColor: '#55d64a',
        backgroundColor: '#55d64a',
      },{
        label: 'Activités sortantes',
        data:activitySends,
        borderColor: '#fda700',
        backgroundColor: '#fda700',
      }]
    }
    return {
      ...state,
      items: data,
    };
  },
  [TYPES.setCount](state, payload) {
    return {
      ...state,
      count: payload,
    };
  },
};
export default (state = initialState, action) =>
  mutations[action.type]
    ? mutations[action.type](state, action.payload)
    : state;
