import { TYPES } from './action';
import moment from "moment";
const initialState = {
  dateRange: [moment(),moment()],
  type: 'all',
  platform: 'all'
};
const mutations = {
  [TYPES.setFilters](state, {prop,value}) {
    return { ...state, [prop]: value };
  },
};
export default (state = initialState, action) =>
  mutations[action.type]
    ? mutations[action.type](state, action.payload)
    : state;
