export const filters = state => state.filters;
export const platform = state => state.filters.platform;
export const dateRange = state => state.filters.dateRange;
export const type = state => state.filters.type;