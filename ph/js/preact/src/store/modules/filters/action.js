export const TYPES = {
  setFilters: null,
};
for (const key of Object.keys(TYPES)) {
  TYPES[key] = `filters__${key}`;
}
export const filterUpdate = ({ prop, value }) => {
  return {
    type: TYPES.setFilters,
    payload: { prop, value },
  };
};

export const setDateRange = dateRange => {
  return async dispatch => {
    dispatch(
      filterUpdate({
        prop: 'dateRange',
        value: dateRange,
      }),
    );
  };
};
export const setPlatform = platform => {
  return async dispatch => {
    dispatch(
      filterUpdate({
        prop: 'platform',
        value: platform,
      }),
    );
  };
};

export const setType = type => {
  return async dispatch => {
    dispatch(
      filterUpdate({
        prop: 'type',
        value: type,
      }),
    );
  };
};
