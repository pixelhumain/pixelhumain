export const dashboardUsage = (state) => state.dashboardUsages;
export const isLoading = (state) => state.dashboardUsages.meta.isLoading;
export const meta = (state) => state.dashboardUsages.meta;
export const error = (state) => state.dashboardUsages.meta.error;
export const success = (state) => state.dashboardUsages.meta.success;
export const value = (state) => state.dashboardUsages.items;
export const count = (state) => state.dashboardUsages.count;