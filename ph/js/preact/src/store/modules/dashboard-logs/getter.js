export const dashboardLogs = (state) => state.dashboardLogs;
export const isLoading = (state) => state.dashboardLogs.meta.isLoading;
export const meta = (state) => state.dashboardLogs.meta;
export const error = (state) => state.dashboardLogs.meta.error;
export const success = (state) => state.dashboardLogs.meta.success;
export const value = (state) => state.dashboardLogs.items;
export const count = (state) => state.dashboardLogs.count;