import api, { apiConfig } from "../../api";

export const TYPES = {
  setData: null,
  setLoading: null,
  setSuccess: null,
  setError: null,
  setItems: null,
  setCount: null
};
for (const key of Object.keys(TYPES)) {
  TYPES[key] = `dashboard_logs__${key}`;
}
const setLoading = (payload) => async (dispatch) => {
  dispatch({ type: TYPES.setLoading, payload: payload });
};
const setSuccess = (payload) => async (dispatch) => {
  dispatch({ type: TYPES.setSuccess, payload: payload });
};
const setError = (payload) => async (dispatch) => {
  dispatch({ type: TYPES.setError, payload: payload });
};
const setItems = (payload) => async (dispatch) => {
  dispatch({ type: TYPES.setItems, payload: payload });
};
const setCount = (payload) => async (dispatch) => {
  dispatch({ type: TYPES.setCount, payload: payload });
};



export const getDashboardLogs = () => async (dispatch) => {
  dispatch(setError(""));
  dispatch(setSuccess(""));
  dispatch(setLoading(true));
  return api
    .get(`${apiConfig.baseURL}${'activitypub/logs'}`)
    .then((json) => {
      dispatch(setItems(json.rows));
      dispatch(setCount(json.count));
      dispatch(setError(""));
      dispatch(
        setSuccess(`Récuperation des logs reussies`)
      );
      dispatch(setLoading(false));
      console.log(`recuperation des logs`);
    })
    .catch((err) => {
      console.log(`recuperation des logs error`);
      dispatch(
        setError(`Une erreur s'est produite lors de la récuperation  des logs`)
      );
      dispatch(setSuccess(""));
      dispatch(setLoading(false));
    });
};