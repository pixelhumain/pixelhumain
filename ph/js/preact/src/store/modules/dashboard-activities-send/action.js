import api, { apiConfig } from "../../api";
import {store} from "../../store.js";

export const TYPES = {
  setData: null,
  setLoading: null,
  setSuccess: null,
  setError: null,
  setItems: null,
  setCount: null
};
for (const key of Object.keys(TYPES)) {
  TYPES[key] = `dashboard_activities_send__${key}`;
}
const setLoading = (payload) => async (dispatch) => {
  dispatch({ type: TYPES.setLoading, payload: payload });
};
const setSuccess = (payload) => async (dispatch) => {
  dispatch({ type: TYPES.setSuccess, payload: payload });
};
const setError = (payload) => async (dispatch) => {
  dispatch({ type: TYPES.setError, payload: payload });
};
const setItems = (payload) => async (dispatch) => {
  dispatch({ type: TYPES.setItems, payload: payload });
};
const setCount = (payload) => async (dispatch) => {
  dispatch({ type: TYPES.setCount, payload: payload });
};



export const getDashboardActivities = () => async (dispatch) => {
  dispatch(setError(""));
  dispatch(setSuccess(""));
  dispatch(setLoading(true));
  const filters = store.getState().filters;
  return api
    .get(`${apiConfig.baseURL}${'activitypub/getactivitybydaterange'}`,{
        startDate: new Date(filters.dateRange[0]),
        endDate: new Date(filters.dateRange[1]),
        type: filters.type,
        isLocalActivity: true
    })
    .then((json) => {
      dispatch(setItems(json.rows));
      dispatch(setCount(json.count));
      dispatch(setError(""));
      dispatch(
        setSuccess(`Récuperation des logs reussies`)
      );
      dispatch(setLoading(false));
      console.log(`recuperation des logs`);
    })
    .catch((err) => {
      console.log(`recuperation des logs error`);
      dispatch(
        setError(`Une erreur s'est produite lors de la récuperation  des logs`)
      );
      dispatch(setSuccess(""));
      dispatch(setLoading(false));
    });
};