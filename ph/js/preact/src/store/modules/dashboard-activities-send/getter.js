export const dashboardActivitiesSend = (state) => state.dashboardActivitiesSend;
export const isLoading = (state) => state.dashboardActivitiesSend.meta.isLoading;
export const meta = (state) => state.dashboardActivitiesSend.meta;
export const error = (state) => state.dashboardActivitiesSend.meta.error;
export const success = (state) => state.dashboardActivitiesSend.meta.success;
export const value = (state) => state.dashboardActivitiesSend.items;
export const count = (state) => state.dashboardActivitiesSend.count;