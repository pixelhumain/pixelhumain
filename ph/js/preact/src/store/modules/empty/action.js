export const TYPES = {
  setData: null,
};
for (const key of Object.keys(TYPES)) {
  TYPES[key] = `empty__${key}`;
}
export const setData = () => ({
  type: TYPES.setData,
  payload: ['it is data'],
});