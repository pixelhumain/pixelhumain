import { TYPES } from './action';
const initialState = {
  data: [],
  lastId: null,
  isEnd: false,
  text: '',
};
const mutations = {
  [TYPES.setData](state, payload) {
    return { ...state, text: payload };
  },
};
export default (state = initialState, action) =>
  mutations[action.type]
    ? mutations[action.type](state, action.payload)
    : state;
