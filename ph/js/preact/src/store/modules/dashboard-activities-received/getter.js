export const dashboardActivitiesReceived = (state) => state.dashboardActivitiesReceived;
export const isLoading = (state) => state.dashboardActivitiesReceived.meta.isLoading;
export const meta = (state) => state.dashboardActivitiesReceived.meta;
export const error = (state) => state.dashboardActivitiesReceived.meta.error;
export const success = (state) => state.dashboardActivitiesReceived.meta.success;
export const value = (state) => state.dashboardActivitiesReceived.items;
export const count = (state) => state.dashboardActivitiesReceived.count;