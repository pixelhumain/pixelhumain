import {useMemo, useCallback} from 'react';
import {useSelector} from 'react-redux';
import * as modules from './modules';
const defaultGetter = state => state;
const getter = (moduleName, key) => {
  if (modules[moduleName] == undefined) {
    console.log('module not found', moduleName, key);
  }
  const getters = modules[moduleName].getter;
  if (getters && getters[key]) return getters[key];
  return defaultGetter;
};
const useGetter = (moduleName, key, props) => {
  const memoGetter = useMemo(() => {
    return getter(moduleName, key);
  }, []);
  const selecteor = useCallback(state => memoGetter(state, props), [props]);
  return useSelector(selecteor);
};
export {getter, useGetter};
