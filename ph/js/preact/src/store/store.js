import { legacy_createStore as createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { initSubscriber } from './subscriber';
import { combinedReducers } from './reducer';
import { composeWithDevTools } from 'redux-devtools-extension';
composeWithDevTools
const bindMiddleware = (middleware) => {
  if (process.env.NODE_ENV !== "production") {
    return composeWithDevTools(applyMiddleware(...middleware));
  }
  return applyMiddleware(...middleware);
};

const middlewares = [thunkMiddleware];
const store = createStore(combinedReducers, bindMiddleware([thunkMiddleware]));
initSubscriber(store);
export { store };


