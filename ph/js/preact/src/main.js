import '@wojtekmaj/react-daterange-picker/dist/DateRangePicker.css';
import 'react-calendar/dist/Calendar.css';
import preactCustomElement from './preact.js'
import Dashboard from './components/modules/Dashboard/index.js';
import { App } from './app.js';
import { Provider } from './store/index.js';
function WrappedComponent(Component) {
    return function inject(props) {
      const EnhancedComponent = () => (
        <Provider>
          <Component {...props} />
        </Provider>
      );
      return <EnhancedComponent />;
    };
  }
preactCustomElement('co-app', WrappedComponent(App));
preactCustomElement('co-dashboard',  WrappedComponent(Dashboard));