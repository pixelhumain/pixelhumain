import { h, createContext } from 'preact';
export const MapContext = createContext({});

export const MapProvider = ({ map, children }) => {
  return (
    <MapContext.Provider value={{ map }}>
      {children}
    </MapContext.Provider>
  );
};