import { h } from 'preact';
import { useEffect, useRef } from 'preact/hooks';
import { useMapContext } from '../hooks/useMapContext.js';
export const TileLayer = (props) => {

  const { url, options = {} } = props;

  const tileLayer = useRef(null);
  const prevProps = useRef(props);

  const { map } = useMapContext();

  useEffect(() => {
    if (map) {
      tileLayer.current = L.tileLayer(url, { ...options } ).addTo(map);
    }

    return () => {
      if (map && tileLayer.current) tileLayer.current.removeFrom(map);
    }
  }, [map, url, options]);

  return <div />;
};
