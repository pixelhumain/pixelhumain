import { h } from 'preact';
import { useEffect, useRef } from 'preact/hooks';
import { useMapContext } from '../hooks/useMapContext.js';
export const Marker = (props) => {
  const { position, icon, options = {} } = props;
  const marker = useRef(null);
  const { map } = useMapContext();
  useEffect(() => {
    if (map) {
      marker.current = L.marker(position, { ...options, icon }).addTo(map);
    }
    return () => {
      if (map && marker.current) marker.current.removeFrom(map);
    }
  }, [map, position]);
  return <div />;
};
