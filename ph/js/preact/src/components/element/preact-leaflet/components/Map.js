import { h } from 'preact';
import { useEffect, useRef } from 'preact/hooks';
export const Map = ({ containerId, setMap, options }) => {
  const map = useRef(null);
  const  { center, zoom } = options;
  useEffect(() => {
    map.current = L.map(containerId, options).setView(center, zoom);
    setMap(map.current);
    return () => {
      if (map && map.current) map.current.remove();
    }
  }, []);
  return <div />;
};
