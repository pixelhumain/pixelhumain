import { useState } from 'preact/hooks';

import { MapProvider } from '../contexts/MapContext.js';
import { Map } from './Map';
export const MapContainer = ({
  ref,
  center,
  zoom,
  children,
  containerId = 'map',
  options = {},
  height = '400px',
  style = {},
}) => {

  const [map, setMap] = useState(null);
  
  return (
    <div ref={ref} id={containerId} style={{ height, ...style }}>
      <Map containerId={containerId} setMap={setMap} options={{ ...options, center, zoom }} />
      <MapProvider map={map}>
        {children}
      </MapProvider>
    </div>
  );
};
