import { useEffect, useMemo, useRef } from "preact/hooks";
import { useDispatch, useGetter } from "../../../../store";
import {MapContainer, Marker, TileLayer} from "../../../element/preact-leaflet/index.js";
const greenIcon = new L.Icon({
    iconUrl: 'js/preact/dist/icons/green-ellipse.png',
    iconSize: [8, 8],
    iconAnchor: [8, 8],
});

const MapComponent = () => {
    const isLoading = useGetter("dashboardUsages", "isLoading");
    const value = useGetter("dashboardUsages", "value");
    const getDashboardUsages = useDispatch("dashboardUsages", "getDashboardUsages");
    useEffect(() => {
        getDashboardUsages();
    }, []);
    return (

        <>
            <MapContainer
                // ref={this.mapRef}
                center={[0, 0]}
                zoom={1.5}
                options={{
                    // Options
                    // preferCanvas: false,

                    // Control Options
                    // attributionControl: true,
                    // zoomControl: true,

                    // Interaction Options
                    // closePopupOnClick: true,
                    // zoomSnap: 1,
                    // zoomDelta: 1,
                    // trackResize: true,
                    // boxZoom: true,
                    // doubleClickZoom: true,
                    // dragging: true,

                    // Map State Options
                    // crs:
                    // center: [51.505, -0.09],
                    // zoom: 13,
                    // minZoom: // Number
                    // maxZoom: // Number
                    // layers: [],
                    // maxBounds: null,
                    // rendered:

                    // Animation Options
                    // zoomAnimation: true,
                    // zoomAnimationThreshold: 4,
                    // fadeAnimation: true,
                    // markerZoomAnimation: true,
                    // transform3DLimit:

                    // Panning Inertia Options
                    // inertia:
                    // inertiaDeceleration:
                    // inertiaMaxSpeed:
                    // easeLinearity:
                    // worldCopyJump:
                    // maxBoundsViscosity:

                    // Keyboard Navigation Options
                    // keyboard:
                    // keyboardPanDelta:

                    // Mouse wheel options
                    // scrollWheelZoom:
                    // wheelDebounceTime:
                    // wheelPxPerZoomLevel:

                    // Touch interaction options
                    // tapHold:
                    // tapTolerance:
                    // touchZoom:
                    // bounceAtZoomLimits:
                }}
            >
                <TileLayer
                    // to use mapbox
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    options={{
                         attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',

                        id: 'openstreatmap',
                        tileSize: 512,
                        zoomOffset: -1,
                        className:"map-tiles"
                    }}

                />
                {value.filter(loc => loc.geo != null).map(v => (
                <Marker
                    position={[
                        v.geo.latitude,
                        v.geo.longitude
                    ]}
                    icon={greenIcon}
                    options={{
                        title: v.name
                    }}
                />
                ))}
            </MapContainer>
        </>
    )
}
export default MapComponent;
