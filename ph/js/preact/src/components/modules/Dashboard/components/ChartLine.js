import { useEffect, useState, useMemo, useRef } from "preact/hooks";
import _ from "lodash";
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
    ArcElement
} from 'chart.js';
import { Line, Pie } from 'react-chartjs-2';


import { useDispatch, useGetter } from "../../../../store";
ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
    ArcElement
);
export const options = {
    x: {
        beginAtZero: true
    },
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
    responsive: true,
    negative: false,
    width: '200px',
    height: '200px',
    plugins: {
        legend: {
            position: 'top',
        },
        title: {
            display: false,
        },
    },
};
export const data = {
    labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
    datasets: [
        {
            label: '# of Votes',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)',
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)',
            ],
            borderWidth: 1,
        },
    ],
};
const ChartLineComponent = () => {
    const isLoading = useGetter("dashboardCharts", "isLoading");
    const count = useGetter("dashboardCharts", "count");
    const items = useGetter("dashboardCharts", "value")
    const filters = useGetter("filters", "filters")
    const getDashboardCharts = useDispatch("dashboardCharts", "getDashboardCharts");

    useEffect(() => {
        getDashboardCharts();
    }, [filters]);
    console.log('chartData', items);
    return useMemo(() => {
        return (<div>
            {isLoading && (<div className="">Loading ....</div>)}
            {!_.isNull(items) && <Line options={options} data={items} />}
        </div>)
    }, [isLoading, items, count]);
};
export default ChartLineComponent;