import { useEffect, useMemo, useRef } from "preact/hooks";
import { useDispatch, useGetter } from "../../../../store";
function translate(value) {
    if(value == 'Note'){
        return 'Actualités'
    }else if(value == 'Project'){
        return 'Projets'
    }else if(value == 'Event'){
        return 'Evenements'
    }
}
const ObjectCountComponent = () => {
    const isLoading = useGetter("dashboardObjects", "isLoading");
    const count = useGetter("dashboardObjects", "count");
    const items = useGetter("dashboardObjects", "value");
    const filters = useGetter("filters", "filters");
    const getDashboardObjects = useDispatch("dashboardObjects", "getDashboardObjects");
    useEffect(() => {
        getDashboardObjects();
    }, [filters]);
    return useMemo(() => {
        return (<div>

            <div className="objects__list__container">
                {items.map((item, index) => (
                    <div className={`objects__list__items_container item-${index + 1}`}>
                        <div className="icon__list__container">
                            <div className="icon">
                                <img src={`/js/preact/dist/icons/icon-${index+1}.png`} width={32} height={32} />
                            </div>
                        </div>
                        <div className="stat__object__container">
                            <h3 className="objects__list__item__type">{translate(item.type)}</h3>
                            <p className="objects__list__item__subtitle">Fédérés</p>
                            <div className="objects_list_item_stat">
                                <div className="objects_list_item_stat_all">

                                    <p className="objects__list__item">{item.all.length}</p>
                                </div>
                                <div className="objects_list_item_stat_in">
                                    <img src="/js/preact/dist/icons/arrow-up.png" width={20} />
                                    <p className="objects__list__item">{item.in.length}</p>
                                </div>
                                <div className="objects_list_item_stat_out">
                                    <img src="/js/preact/dist/icons/arrow_down.png" width={20} />
                                    <p className="objects__list__item">{item.out.length}</p>
                                </div>

                            </div>
                        </div>

                    </div>
                ))}
            </div>

        </div>)
    }, [isLoading, count, items]);
};
export default ObjectCountComponent;