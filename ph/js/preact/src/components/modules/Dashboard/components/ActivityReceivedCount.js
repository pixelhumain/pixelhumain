import {useEffect, useMemo, useRef} from "preact/hooks";
import {useDispatch, useGetter} from "../../../../store";

const ActivityReceivedCountComponent = () => {
    const isLoading = useGetter("dashboardActivitiesReceived", "isLoading");
    const count = useGetter("dashboardActivitiesReceived", "count");
    const filters = useGetter("filters", "filters");
    const getDashboardActivities = useDispatch("dashboardActivitiesReceived", "getDashboardActivities");
    useEffect(() => {
        getDashboardActivities();
    }, [filters]);
    return useMemo(() => {
        return (<div className="activity_received_count">
            <h3>Activités <span>entrantes</span></h3>
            {isLoading && (<div className="">Loading ....</div>)}
            <div className="activity_count_container">
                
                   <div className="activity_received_total">{count}</div>
                   <img src="/js/preact/dist/icons/down.png" width={48} height={48}/>
            </div>
        </div>)
    }, [isLoading, count]);
};
export default ActivityReceivedCountComponent;