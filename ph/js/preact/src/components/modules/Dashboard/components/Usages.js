import { useEffect, useMemo, useRef } from "preact/hooks";
import { useDispatch, useGetter } from "../../../../store";

const UsagesComponent = () => {
    const isLoading = useGetter("dashboardUsages", "isLoading");
    const count = useGetter("dashboardUsages", "count");
    const getDashboardUsages = useDispatch("dashboardUsages", "getDashboardUsages");
    useEffect(() => {
        getDashboardUsages();
    }, []);
    return useMemo(() => {
        return (<div className="usage_container">
            <h3>Total <span>utilisateurs</span></h3>
            {isLoading && <div className="">Loading ....</div>}
            <div className="usage_count_container">
                <div className="usage_total">{count}</div>
                <img src="/js/preact/dist/icons/people.png" width={48} height={48} />
            </div>
        </div>)
    }, [isLoading, count]);
};
export default UsagesComponent;