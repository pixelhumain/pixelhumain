import { useEffect, useMemo, useRef } from "preact/hooks";
import { useDispatch, useGetter } from "../../../../store";

const ActivitySendCountComponent = () => {
    const isLoading = useGetter("dashboardActivitiesSend", "isLoading");
    const count = useGetter("dashboardActivitiesSend", "count");
    const filters = useGetter("filters", "filters");
    const getDashboardActivities = useDispatch("dashboardActivitiesSend", "getDashboardActivities");
    useEffect(() => {
        getDashboardActivities();
    }, [filters]);
    return useMemo(() => {
        return (<div className="activity_send_count">
            <h3>Activités  <span>sortantes</span></h3>
            {isLoading && (<div className="">Loading ....</div>)}
            <div className="activity_count_container">
                <div className="activity_send_total">{count}</div>
                <img src="/js/preact/dist/icons/up.png" width={48} height={48} />
            </div>
        </div>)
    }, [isLoading, count]);
};
export default ActivitySendCountComponent;