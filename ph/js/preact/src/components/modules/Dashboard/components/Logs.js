import {useEffect, useMemo, useRef} from "preact/hooks";
import {useDispatch, useGetter} from "../../../../store";
function convertToFrenchDate(dateString) {
    const options = { year: 'numeric', month: 'long', day: 'numeric' };
    const convertedDate = new Date(dateString).toLocaleDateString('fr-FR', options);
    return convertedDate;
}
const LogsComponent = () => {
    const unSubscribeRef = useRef();
    const isLoading = useGetter("dashboardLogs", "isLoading");
    const count = useGetter("dashboardLogs", "count");
    const items = useGetter("dashboardLogs", "value")
    const getDashboardLogs = useDispatch("dashboardLogs", "getDashboardLogs");
    useEffect(() => {
        getDashboardLogs();
    }, []);
    return useMemo(() => {
        return (<>
            <div className={"log_container"}>
                <table className={"table table-striped table-bordered"}>
                    <caption style={{
                        marginBottom:10

                    }}>{count} Erreur trouvé dans le log</caption>
                    <thead className={"thead-dark"}>
                    <tr>
                        <th>Date du log</th>
                        <th>Adresse IP</th>
                        <th>Level</th>
                        <th>Message</th>
                    </tr>
                    </thead>
                    <tbody>

                    {items.map((v) => (
                        <tr >
                            <td>{convertToFrenchDate(v.time)}</td>
                            <td  className={'text-center'}><span className="badge">{v.ip}</span></td>
                            <td  className={'text-center'}><span className="badge" style={{
                                backgroundColor:'#dc3545'
                            }}>{v.Level}</span></td>
                            <td >{v.message}</td>
                        </tr>
                    ))}

                    </tbody>
                </table>
            </div>


        </>)
    }, [isLoading, items, count]);
};
export default LogsComponent;