import { useEffect, useRef, useCallback, useState } from "preact/hooks";
import { useDispatch, useGetter } from "../../../../store";
import DateRangePicker from "@wojtekmaj/react-daterange-picker";
const activities = [
    "Accept",
    "Add",
    "Announce",
    "Arrive",
    "Block",
    "Create",
    "Delete",
    "Dislike",
    "Flag",
    "Follow",
    "Ignore",
    "Invite",
    "Join",
    "Leave",
    "Like",
    "Listen",
    "Move",
    "Offer",
    "Question",
    "Read",
    "Reject",
    "Remove",
    "TentativeReject",
    "TentativeAccept",
    "Travel",
    "Undo",
    "Update",
    "View"
];
const FilterComponent = () => {
    const [value, onChange] = useState([new Date(), new Date()]);
    const empty = useGetter("empty", "empty");
    const setDateRange = useDispatch("filters", "setDateRange");
    const setType = useDispatch("filters", "setType");
    const setPlatform = useDispatch("filters", "setPlatform");
    const onChangeDate = useCallback((dates) => {
        onChange(dates);
        setDateRange(dates);
    }, [])

    return (
        <div className="filter_container">
            <div className="date_range_filter_container">
                <h3>Activités</h3>
                <div><DateRangePicker onChange={onChangeDate} value={value} /></div>
            </div>
            <div className="criteria_filter_container">
                <div>
                    <p>Type</p>
                    <select onChange={(e) => {
                        setType(e.target.value);
                    }}>
                        <option value={"all"} selected>Tous</option>
                        {activities.map(activity => (
                            <option value={activity}>{activity}</option>
                        ))}
                    </select>
                </div>
                <div>
                    <p>Platforme</p>
                    <select onChange={(e) => {
                        setPlatform(e.target.value);
                    }}>
                        <option value={"all"} selected>Tous</option>
                        <option value={"https://mastodon.social"}>Mastodon</option>
                        <option value={"https://mobilizon.fr"}>Mobilizon</option>
                    </select>
                </div>
            </div>
        </div>
    )
}
export default FilterComponent;