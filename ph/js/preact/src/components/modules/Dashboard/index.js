import LogsComponent from "./components/Logs.js";
import UsagesComponent from "./components/Usages.js";
import ActivitySendCountComponent from "./components/ActivitySendCount.js";
import ActivityReceivedCountComponent from "./components/ActivityReceivedCount.js";
import ObjectCountComponent from "./components/ObjectCount.js";
import ChartLineComponent from "./components/ChartLine.js";
import FilterComponent from "./components/FilterComponent.js";
import { useState, useCallback } from "react";
import MapComponent from "./components/Map.js";
function getCurrentDateFormatted() {
  const months = [
    'janvier', 'février', 'mars', 'avril', 'mai', 'juin',
    'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre'
  ];

  const currentDate = new Date();
  const day = currentDate.getDate();
  const monthIndex = currentDate.getMonth();
  const year = currentDate.getFullYear();

  return `${day} ${months[monthIndex]} ${year}`;
}
const Dashboard = () => {
  const [value, onChange] = useState([new Date(), new Date()]);
  const [isLineChart, setIsLineChart] = useState(true);
  const [logShow, setLogShow] = useState(false);

  return (
    <div className="dashboard__ap__container">

      <div className="dashboad_ap_sidebar">
        <div>

        </div>

        <div className="menu_container">
          <ul className="menu">
            <li className="menu_item" onClick={() => setLogShow(false)}>
              <img src="/js/preact/dist/icons/macbook.png" alt="macbook" width={20} />
              <a className={!logShow ? "active" : ""}>Tableau de bord</a>
            </li>
            <li className="menu_item" onClick={() => setLogShow(true)}>
              <img src="/js/preact/dist/icons/document.png" alt="document" width={20} />
              <a className={logShow ? "active" : ""}>Application log</a>
            </li>
          </ul>
        </div>
      </div>

      <div className="dashboad_ap_content">
        <div className="dasboard__ap__header">
          <div className="dashboard__ap__title__container">
            <h3 className="dashboard__ap__title">Activitypub Dashboard</h3>
            <p className="dashboard__ap__subtitle">ActivityPub est un standard ouvert pour réseaux sociaux décentralisés basé sur le format ActivityStreams 2.0</p>
          </div>
          <div className="dashboard__ap__action__container">
            <p className="dashboard__ap__action__title">{getCurrentDateFormatted()}</p>
            <div className="dashboard_ap_action_icon">
              <img src="/js/preact/dist/icons/icons8-tear-off-calendar-48.png" width={25} height={25} />
            </div>
          </div>
        </div>
        {!logShow ? (
          <>
            <div className="dashboard_ap_object_container">
              <ObjectCountComponent />
            </div>
            <FilterComponent />
            <div className="chart_stat_container">
              <div className="chart_stat_content">
                {isLineChart ? <ChartLineComponent /> : <div></div>}
              </div>
              <div className="activities_stat_content">
                <ActivityReceivedCountComponent />
                <ActivitySendCountComponent />
              </div>
            </div>
            <div className="choice_chart_container">
              {/**<div className={isLineChart ? 'active' : ''} onClick={() => setIsLineChart(true)}>Line Chart</div>
                <div className={!isLineChart ? 'active' : ''} onClick={() => setIsLineChart(false)}>Pie Chart
            </div>
            **/}
            </div>
            <div className="usage_map_header">
              <h3>Utilisateurs</h3>
            </div>

            <div className="map_container">
              <div className="map_content">
                <MapComponent />
              </div>
              <div className="map_stat_content">
                <UsagesComponent />
              </div>
            </div>
          </>
        ) : (
          <LogsComponent/>
      )}


      </div>
    </div>

  )
}
export default Dashboard;