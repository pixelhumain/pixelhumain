const API_GLOBAL_AUTOCOMPLETION = 'https://www.communecter.org/co2/search/globalautocomplete';
const API_EXTRACT_PROCESS = 'https://www.communecter.org/news/co/extractprocess';
class SpotLight extends HTMLElement {

    constructor() {
        super();
        this.attachShadow({ mode: "open" });
        this.shadowRoot.innerHTML = `
            <style>
                :host{
                   box-sizing: border-box;
                   padding: 0;
                   margin:0;
                   font-size: 13px;
                  font-family: "Montserrat", "Helvetica Neue", Helvetica, Arial,serif;
                }
                #searchEngine{
                width: 45%;
                max-height:  70%;
                overflow: hidden;
                box-sizing: border-box;
                position: absolute;
                top: 10%;
                left: 50%;
                transform: translate(-50%, -10%);
                border: 1px solid #9fbd38;
                box-shadow: 0 5px 5px 0 rgba(0, 0, 0, 0.2), 0 9px 9px 0 rgba(0, 0, 0, 0.19);
               
                background-color: #fff;
                }

                #customElement {
                z-index: 999999;
                width: 100%;
                height: 100%;
                padding:0;
                margin:0;
                overflow: hidden;
                position: fixed;
                top: 0;
                right: 0;
                bottom: 0;
                left: 0;
                background-color: rgba(0,0,0,0.7);
                display: none;
                }
                
                input::placeholder {
                    font-size: 16px;
                    color: black;
                },
                input:focus,input:focus-visible {
                   outline: none;
                }
                .input-search-container {
                    position: relative;
                }
                .spotlight {
                    padding: 0!important;;
                    border: 0!important;
                    font-family:inherit;
                    width: 100%;
                    font-size: 18px;
                    color: black;
                    position: relative;
                    z-index: 2;
                    background: transparent;
                    outline: none!important;
                  }
                
                  #match-shadow {
                    position: absolute;
                    width: 100%;
                    font-size: 18px;
                    font-family:inherit;
                    color: #cccccc;
                    z-index: 1;
                    user-select: none;
                    top: -1px;
                    left: 0px;
                    background: transparent;
                  }
                  .spotlight-inner{
                    padding: 10px 10px;
                  }

                .spotlight:-moz-placeholder {
                  color: #666666;
                }
                .spotlight::-moz-placeholder {
                  color: #666666;
                }
                .spotlight:-ms-input-placeholder {
                  color: #666666;
                }
                .spotlight::-webkit-input-placeholder {
                  color: #666666;
                }

                .desc {
                  position: absolute;
                  top: 35%;
                  left: 0;
                  right: 0;
                  width: 100%;
                  text-align: center;
                  padding: 0 40%;
                  -moz-box-sizing: border-box;
                  -webkit-box-sizing: border-box;
                  box-sizing: border-box;
                  font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
                  font-size: 1.6rem;
                  line-height: 1.5;
                  color: #666666;
                }
                #result-container{
                    max-height: 400px;
                    overflow-x:hidden;
                    overflow-y:scroll;
                }
                #result-container .list-group-item{
                    border:none;
                    cursor: pointer;
                }
                 #result-container .list-group-item i{
                    margin-right: 5px;
                }
                #result-container #spotlight-links h5{
                    margin-left: 10px;
                }
                #customElement .suggestions-container {
                    max-height: 400px;
                    overflow-y: auto;
                    overflow-x: hidden;
                }
                #customElement .suggestion-item,#customElement .suggestion-menu-item {
                    position: relative;
                    padding: 5px 10px;
                    cursor: pointer;
                    border: 1px solid rgba(169,169,169,0.7);
                    margin: 10px;
                    border-radius: 5px;

                }
                #customElement .suggestion-item-content{
                    display:flex;
                    flex-direction: row;
                    justify-content: center;
                    gap:10px;
                    align-items: center;
                }
                #customElement .suggestion-item p,#customElement .suggestion-menu-item  p {
                    padding: 5px 0px;
                    margin: 0;
                }
                
                #customElement .suggestion-menu-item{
                    display:flex;
                    flex-direction: row;
                    justify-content: space-between;
                    padding: 3px 8px;
                    margin-bottom: 10px;
                    border: 1px solid whitesmoke;
                }
                 #customElement .suggestion-text-container{
                    display:flex;
                    flex-direction: row;
                    gap:10px;
                    align-items: center;
                 }
                #customElement .suggestion-item:hover,#customElement .suggestion-menu-item:hover  {
                    background-color: #f0f0f0;
                }
                #customElement .suggestion-content{
                    display: flex;
                    flex-direction: row;
                    align-items: center;
                    gap: 5px;
                    padding-bottom: 8px;
                }

                .breadcrumb-suggestion{
                    display: flex;
                    flex-direction: row;
                    justify-content: flex-end;
                    list-style: none;
                    margin: 0;
                    padding: 0;
                }
                .breadcrumb-suggestion li{
                   list-style-type: none;
                    color: #9fbd38;
                    font-weight: bold;
                    font-size: 1.1rem;
                }
                .breadcrumb-suggestion p{
                  color: #2c3e50;
                  padding : 0;
                  margin: 0;
                  font-size: 1.5rem;
                }
                .breadcrumb-suggestion li:not(:last-child)::after {
                    content: ">";
                    margin: 0 5px 0px 5px;
                }
                .suggestion-item{
                  cursor: pointer;
                }
                .input-container{
                    width: 100%;
                    padding-left: 10px;
                    padding-right: 10px;


                }
                 .input-container input{
                    z-index: 200;
                    border: 1px solid #9fbd38!important;
                    padding: 20px;
                    box-shadow: none!important;
                    outline: none!important;
                    border-top-right-radius: 20px;
                    border-bottom-right-radius: 20px;


                 }
                .input-container input:focus{
                    border: 1px solid #9fbd38!important;
                    border-top-right-radius: 20px;
                    border-bottom-right-radius: 20px;
                }
                .input-container .input-group-addon{
                    background-color:#fff!important;
                    border-top-left-radius: 20px;
                    border-bottom-left-radius: 20px;
                    border-top: 1px solid #9fbd38;
                    border-bottom: 1px solid #9fbd38;
                    border-left: 1px solid #9fbd38;
                }
                #loader {
                    position: relative;
                    width: 40px;
                    height: 40px;
                }
                #loader div:nth-child(1) {
                    animation-delay: -0.45s;
                }
                @keyframes loader {
                    0% {
                        transform: rotate(0deg);
                    }
                    100% {
                        transform: rotate(360deg);
                    }
                }
                #loader div {
                    box-sizing: border-box;
                    display: block;
                    position: absolute;
                    width: 50px;
                    height: 50px;
                    margin: 8px;
                    border: 6px solid #9fbd38;
                    border-radius: 50%;
                    animation: loader 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
                    border-color: #9fbd38 transparent transparent transparent;
                }
                #loader-container,.empty-container{
                    display: flex;
                    flex-direction: row;
                    justify-content: center;
                    padding-top: 10px;
                    padding-bottom: 10px;
                }
                #loader-container{
                    display: none;
                }
                .icon-embeded{

                }
                .icon-embeded span{
                    
                    top:8px;
                    right:10px;
                    z-index:100000;
                    width: 32px;
                    height: 32px;
                    border-radius: 100%;
                    display: flex;
                    flex-direction: row;
                    justify-content:center;
                    align-items: center;
                }
                .icon-embeded span i {
                color:#fff;
                }
                .spotlight-footer{
                    display: flex;
                    flex-direction: row;
                    justify-content: space-between;
                    align-items: center;
                    padding: 0.5rem 1rem;
                    background-color: rgb(241,239,239);
                }
                .spotlight-helper-key{
                    padding: 2px 10px;
                    background-color: white;
                    border: 1px solid #9fbd38;
                    color: #0a0a0a;
                    border-radius: 5px;
                }
                .spotlight-ft-helper-key,#spotlight-text{
                   color: gray;
                   font-size: 12px;
                }
                #spotlight-text{
                    font-weight:400;
                }
                .spotlight-links{
                    margin-left: 5px;
                    margin-right: 5px;
                }
                .spotlight-keyboard{
                    position:absolute;
                    top: 10px;
                    right: 15px;
                 
                }
                .action-list{
                    display:flex;
                    flex-direction:row;
                    justify-content:space-between;
                    padding-top: 10px!important;
                    padding-bottom: 10px!important;
                }
                .suggestion-title {
                    background-color: #9fbd38;
                    text-transform:uppercase;
                    padding: 5px 10px;
                    font-size: 13px;
                    color: #fff;
                }
                .suggestion-title h4{
                     padding: 0;
                    margin:0;
                    color: white;
                }
                #close-ctrl-k-btn {
                    background: #a0a6ac;
                    color: white;
                    border: none;
                    padding: 6px 10px;
                    border-radius: 3px;
                    cursor: pointer;
                }
                #close-ctrl-k-btn:hover {
                    background: #6c757d;
                }
                ::-webkit-scrollbar {
                  width: 3px;
                }

                ::-webkit-scrollbar-track {
                  background: #f1f1f1;
                }

                ::-webkit-scrollbar-thumb {
                  background: #9fbd38;
                }

                ::-webkit-scrollbar-thumb:hover {
                  background: #9fbd38;
                }
            </style>
            <div id="customElement">
                <div id="searchEngine">
                    <div class="spotlight-wrapper">
                        <div class="spotlight-inner">
                            <div class="input-search-container">
                                <input type="text" class="spotlight" autocomplete="off"  id="searchInput" placeholder="Rechercher ou taper un commande ...">
                                <div id="match-shadow"></div>
                            </div>
                        </div>
                        <div id="loader-container">
                            <div id="loader">
                                <div></div>
                                <div></div>
                                <div></div>
                                <div></div>
                            </div>
                        </div>
                        <div id="result-container"></div>
                        <div class="spotlight-footer">
                            <div class="spotlight-tip">
                                <span id="spotlight-text"></span>
                            </div>
                            <div class="close-ctrl-k-content">
                                <button type="button" id="close-ctrl-k-btn">Fermer</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        `;
        this.tipsList = [
            "Tapez > pour activer le mode commande",
            "Tapez # pour chercher un element",
            "Tapez @ pour chercher un citoyens",
            "Tapez #element:@name pour chercher un element par nom",
            "Tapez ? pour obtenir de l'aide et des astuces"
        ];
        this.tipIndex = -1;
        this.elementTypes = [
            "event",
            "organization",
            "project",
            "poi",
            "classifieds",
            "ressources",
            "jobs",
            "proposal",
            "#element.invite"
        ];
        this.pluralElementTypes = [
            "events",
            "organizations",
            "projects",
            "poi",
            "classifieds",
            "ressources",
            "jobs",
            "proposals"
        ];
        this.pages = {
            "live": "live",
            "co": "costum",
            "o": "oceco",
            "info": "i",
            "g": "gallery",
            "c": "community",
            "cal": "agenda",
            "a": "annonces",
            "f": "form",
            "l": "live"
        }
        this.menus = [{
            title: "Géneral",
            links: [
                {
                    id: 1,
                    label: "Rechercher un element (event,organization,badges,ect...)",
                    action: "#",
                    type: 'action',
                    icon: 'fa fa-search'
                },
                {
                    label: "Activer le mode commande",
                    action: ">",
                    type: 'action',
                    icon: 'fa fa-terminal'
                },
                {
                    label: "Rechercher un citoyen quelconque",
                    action: "@",
                    type: 'action',
                    icon: 'fa fa-users'
                },
                {
                    label: "Aide et autre astuces",
                    action: "?",
                    type: 'action',
                    icon: 'fa fa-circle-question'
                }
            ]
        }, {
            title: 'Navigation',
            links: [
                {
                    id: 1,
                    label: "Rechercher",
                    icon: 'fa fa-search',
                    action: "search",
                    type: 'navigation'
                },
                {
                    id: 2,
                    label: "Live",
                    action: "live",
                    type: 'navigation',
                    icon: 'fa fa-newspaper-o'
                },
                {
                    id: 3,
                    label: "Evenements",
                    action: "agenda",
                    type: 'navigation',
                    icon: 'fa fa-calendar'
                },
                {
                    id: 4,
                    label: "Annonces",
                    action: "annonces",
                    type: 'navigation',
                    icon: 'fa fa-bullhorn'
                },
                {
                    id: 5,
                    label: "Agora citoyens",
                    action: "dda",
                    type: 'navigation',
                    icon: 'fa fa-calendar'
                },
                {
                    id: 6,
                    label: "Badges",
                    action: "badge",
                    type: 'navigation',
                    icon: 'fa fa-gavel'
                },
            ]
        }];

        this.helpItems = [
            {
                id: 1,
                label: "Rechercher un element (event,organization,badges,ect...)",
                action: "#"
            },
            {
                label: "Activer le mode commande",
                action: ">"
            },
            {
                label: "Rechercher un citoyens quelconque",
                action: "@"
            }
        ];
        this.actions = [
            {
                label: "Créer un evenement",
                action: ">new:event"
            },
            {
                label: "Créer un organization",
                action: ">new:organization"
            },
            {
                label: "Créer un projet",
                action: ">new:projet"
            }, {
                label: "Créer un Point d'intérêt",
                action: ">new:poi"
            }, {
                label: "Créer une annonce",
                action: ">new:classifieds"
            }, {
                label: "Créer un resource",
                action: ">new:ressources"
            }
            , {
                label: "Créer un offre d'emploi",
                action: ">new:jobs"
            }
            , {
                label: "Créer un sondage",
                action: ">new:proposal"
            }
        ];
        this.elts = [
            {
                label: "Rechercher une organization",
                action: "#organizations:@"
            },
            {
                label: "Rechercher un Badge",
                action: "#badges:@"
            },
            {
                label: "Rechercher une citoyen",
                action: "#citoyens:@",
            },
            {
                label: "Rechercher une evenement",
                action: "#events:@",
            },
            {
                label: "Rechercher un projet",
                action: "#projects:@",
            }
        ];
        this.elementActions = [
            {
                label: "Journal",
                type: 'link_child',
                keyword: 'live',
                icon: 'fa-newspaper-o',
                action: "view.newspaper"
            },
            {
                label: "A propos",
                type: 'link_child',
                keyword: 'detail',
                icon: 'fa-info',
                action: "view.detail"
            },
            {
                label: "Galerie",
                type: 'link_child',
                keyword: 'gallery',
                icon: 'fa-picture-o',
                action: "view.gallery"
            },
            {
                label: "Communauté",
                type: 'link_child',
                icon: 'fa-users',
                keyword: 'community',
                action: "view.directory.dir.members",
                subactions: [
                    {
                        label: "Membres",
                        icon: 'fa-users',
                        action: "view.directory.dir.memberOf",
                    },
                    {
                        label: "Organisations",
                        icon: '',
                        action: "view.directory.dir.memberOf",
                    },
                    {
                        label: "Abonnés",
                        icon: 'fa-rss',
                        action: "view.directory.dir.followers",
                    },
                    {
                        label: "Réseau externes",
                        icon: 'fa-rss',
                        action: "view.directory.dir.externalNetwork",

                    }, {
                        label: 'Annonces',
                        icon: 'fa-bullhorn',
                        action: 'view.directory.dir.classifieds'
                    }
                ]
            },
            {
                label: "Annonces",
                type: 'link_child',
                icon: 'fa-bullhorn',
                keyword: 'annonces',
                action: "view.directory.dir.classifieds"
            },
            {
                label: "Agenda",
                type: 'link_child',
                icon: 'fa-calendar',
                keyword: 'agenda',
                action: "view.agenda"
            },
            {
                label: "Projets & POI",
                type: 'link_child',
                icon: 'fa-lightbulb-o',
                keyword: 'projects',
                action: "view.directory.dir.projects"
            },
            {
                label: "COform",
                type: 'link_child',
                icon: 'fa-pencil',
                keyword: 'form',
                action: "view.forms"
            },
            /**{
                label: "Costum",
                type: 'link_child_custom',
                keyword: 'costum',
                icon: '/assets/images/logo_costum.png',
                action: "costum/co/index/slug/lorem/edit/true"
            }**/
            {
                label: "Graph de communauté",
                type: 'link_child',
                keyword: 'node',
                icon: 'https://www.communecter.org/images/logo/node.png',
                action: "view.conode"
            }
        ]
        /**const bootstrapStylesheets = document.querySelectorAll(
            'link[rel="stylesheet"]'
        );
        const filteredBootstrapStylesheets = Array.from(
            bootstrapStylesheets
        ).filter((link) => /font-montserrat/i.test(link.getAttribute("href")));
        filteredBootstrapStylesheets.forEach((stylesheet) => {
            const fullLink = document.createElement("link");
            fullLink.rel = "stylesheet";
            fullLink.href = stylesheet.href;
            this.shadowRoot.insertBefore(fullLink, this.shadowRoot.firstChild);
        });**/
        this.globalSuggestionsList = [];
        const fontawesomeStyleSheets = document.querySelectorAll(
            'link[rel="stylesheet"]'
        );
        const filteredFontawesomeStyleSheets = Array.from(
            fontawesomeStyleSheets
        ).filter((link) =>
            /font-awesome.min/i.test(link.getAttribute("href"))
        );
        filteredFontawesomeStyleSheets.forEach((stylesheet) => {
            const fullLink = document.createElement("link");
            fullLink.rel = "stylesheet";
            fullLink.href = stylesheet.href;
            this.shadowRoot.insertBefore(fullLink, this.shadowRoot.firstChild);
        });
        const coColorStyleSheets = document.querySelectorAll(
            'link[rel="stylesheet"]'
        );
        const filteredCoColorStyleSheets = Array.from(
            coColorStyleSheets
        ).filter((link) => /co-color/i.test(link.getAttribute("href")));
        filteredCoColorStyleSheets.forEach((stylesheet) => {
            const fullLink = document.createElement("link");
            fullLink.rel = "stylesheet";
            fullLink.href = stylesheet.href;
            this.shadowRoot.insertBefore(fullLink, this.shadowRoot.firstChild);
        });
        const mainStyles = document.querySelectorAll(
            'link[rel="stylesheet"]'
        );
        const filteredmainStyles = Array.from(mainStyles).filter((link) =>
            /main/i.test(link.getAttribute("href"))
        );
        filteredmainStyles.forEach((stylesheet) => {
            const fullLink = document.createElement("link");
            fullLink.rel = "stylesheet";
            fullLink.href = stylesheet.href;
            this.shadowRoot.appendChild(fullLink);
        });
        this.customElement = this.shadowRoot.getElementById("customElement");
        this.searchEngine = this.shadowRoot.getElementById('searchEngine');
        this.searchInput = this.shadowRoot.getElementById("searchInput");
        this.matchShadow = this.shadowRoot.getElementById("match-shadow");
        this.loaderElement =
            this.shadowRoot.getElementById("loader-container");
        this.resultContainer =
            this.shadowRoot.getElementById("result-container");
        this.spotTips = this.shadowRoot.getElementById("spotlight-text");
        this.searchEngine.addEventListener('click', (event) => {
            event.stopPropagation();
        });
        this.closectrlKBtn = this.shadowRoot.getElementById("close-ctrl-k-btn");
        document.addEventListener("click", this.handleDocumentClick.bind(this));

        this.searchInput.addEventListener(
            "input",
            this.handleInputChange.bind(this)
        );
        this.searchInputValue = "";
        this.debounceTimeout = null;
        this.isSearchVisible = false;
        this.isLoading = false;
        this.alias = {
            "org": "organizations",
            "pr": "projects",
            "p": "projects",
            "ba": "badges",
            "b": "badges",
            "ev": "events",
            "e": "events",
            "ci": "citoyens",
            "ci": "cities",
            "poi": "poi",
            "organizations": "organizations",
            "projects": "projects",
            "badges": "badges",
            "events": "events",
            "citoyens": "citoyens",
            "cities": "cities",
            "costum": "costum",
            "agenda": "agenda",
            "form": "form",
            "annonces": "annonces",
            "a": "annonces"

        };
        this.showLoader();
        this.showHomeLink();
    }
    showCtrlK(){
        this.isSearchVisible = !this.isSearchVisible;
        this.customElement.style.display = this.isSearchVisible
            ? "block"
            : "none";
        if (this.isSearchVisible) {
            this.searchInput.value = "";
            this.searchInput.focus();
            this.checkActualAction();
        }
    }
    connectedCallback() {
        //window.app_spotlight.on('open',this.showCtrlK.bind(this));
        document.addEventListener("keydown", this.handleKeyPress.bind(this));
        this.searchInput.addEventListener(
            "input",
            this.handleInputChange.bind(this)
        );
        this.customElement.addEventListener("focusin", () => {
            this.checkActualAction();
            this.matchShadow.textContent = '';
        });
        document.removeEventListener("click", this.documentClickHandler);
        this.searchInput.addEventListener(
            "keydown",
            this.handleKeyDown.bind(this)
        );
        this.spotTips.textContent = this.getRandomTip();
        this.closectrlKBtn.addEventListener('click', () => {
            this.close()
        });
    }
    disconnectedCallback() {
        //global.app_spotlight.off('open',this.showCtrlK.bind(this));
        document.removeEventListener(
            "keydown",
            this.handleKeyPress.bind(this)
        );
        document.removeEventListener(
            "click", 
            this.handleDocumentClick.bind(this)
        );
        this.customElement.removeEventListener("focusin", () => {
            this.checkActualAction();
            this.matchShadow.textContent = '';
        });
        this.searchInput.removeEventListener(
            "keydown",
            this.handleKeyDown.bind(this)
        );
        this.searchInput.removeEventListener(
            "input",
            this.handleInputChange.bind(this)
        );
        document.removeEventListener("click", this.documentClickHandler);
    }
    checkActualAction() {
        if (this.searchInputValue.startsWith(">")) {
            return true;
        } else return false

    }
    isValidUrl(urlString) {
        let urlPattern = new RegExp('^(https?:\\/\\/)?' +
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' +
            '((\\d{1,3}\\.){3}\\d{1,3}))' +
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' +
            '(\\?[;&a-z\\d%_.~+=-]*)?' +
            '(\\#[-a-z\\d_]*)?$', 'i');
        return !!urlPattern.test(urlString);
    }
    getRandomTip() {
        var newIndex;
        do {
            newIndex = Math.floor(Math.random() * this.tipsList.length);
        } while (newIndex === this.tipIndex);
        this.tipIndex = newIndex;
        return this.tipsList[this.tipIndex];
    }
    exactMatch(query, text) {
        let regex = new RegExp(`^${query}`);
        return regex.test(text);
    }
    handleInputChange() {
        if (this.debounceTimeout) {
            clearTimeout(this.debounceTimeout);
        }
        this.debounceTimeout = setTimeout(() => {
            this.searchInputValue = this.searchInput.value;

            if (this.searchInputValue !== "") {
                if (this.searchInputValue.startsWith("#")) {
                    const search = this.searchInputValue.replace(
                        /[.*+?^${}()|[\]\\]/g,
                        "\\$&"
                    );
                    const regex = new RegExp(search, "i");
                    const results = [];
                    this.elts.forEach((action) => {
                        if (action.action.match(regex)) {
                            results.push(action);
                        }
                    });
                    if (results.length > 0) {
                        this.globalSuggestionsList = results;
                        this.matchShadow.textContent = this.closestSuggestion(results, 'action', this.searchInputValue, '#');
                        this.showSuggestionsSearch(results);
                    } else {
                        this.matchShadow.textContent = '';
                        this.globalSuggestionsList = [];
                    }


                } else if (this.searchInputValue.startsWith("@")) {
                    let regex = /@(\w+)/;
                    let match = this.searchInputValue.match(regex);
                    if (match) {
                        let name = match[1];
                        this.showLoader();
                        this.getElement(name, ["citoyens", 'organizations'], '@');
                    }
                } else if (this.searchInputValue.startsWith(">")) {
                    const search = this.searchInputValue.replace(
                        /[.*+?^${}()|[\]\\]/g,
                        "\\$&"
                    );
                    const regex = new RegExp(search, "i");
                    const results = [];
                    this.actions.forEach((action) => {
                        if (action.action.match(regex)) {
                            results.push(action);
                        }
                    });
                    if (results.length > 0) {
                        this.globalSuggestionsList = results;
                        this.matchShadow.textContent = this.closestSuggestion(results, 'action', this.searchInputValue, '>');
                        this.showSuggestionsSearch(results);
                    } else {
                        this.globalSuggestionsList = [];
                        this.matchShadow.textContent = '';
                    }

                    setTimeout(() => {
                        this.showPossibActions(results);
                    }, 200);

                } else if (this.searchInputValue.startsWith("?")) {
                    this.showLoader();
                    setTimeout(() => {
                        this.showHelp();
                    }, 200);
                } else {
                    let detectPointRegex = /[^.!?]+[.!?]+/g;
                    let points = this.searchInputValue.match(detectPointRegex);

                    if (points) {
                        let collection = points[0].replace('.', '');
                        if (this.alias[collection] && this.pluralElementTypes.includes(this.alias[collection])) {
                            if (points.length == 1) {
                                let lastPointIndex = this.searchInputValue.lastIndexOf('.');
                                if (lastPointIndex !== -1) {
                                    let search = this.searchInputValue.substring(lastPointIndex + 1).trim();
                                    if (search != '') {
                                    if (typeof this.alias[collection] !== undefined) {
                                        this.showLoader();
                                        this.getElement(search, this.alias[collection]);
                                    } else {
                                        this.showLoader();
                                        this.getElement(search, collection);
                                    }
                                }else{
                                    if(!this.pluralElementTypes.includes(this.alias[collection])){
                                        setTimeout(() => {
                                            this.showPossibActionsForPoints(this.elementActions, points);
                                        }, 200);
                                    }

                                }
                                }
                            } else {
                                let collection = points[0].replace('.', '');
                                if (collection == '') {
                                    setTimeout(() => {
                                        this.showPossibActionsForPoints(this.elementActions, points);
                                    }, 200);

                                }

                            }
                        } else {

                            let collection = points[0].replace('.', '');

                            let lastPointIndex = this.searchInputValue.lastIndexOf('.');

                            if (lastPointIndex !== -1) {
                                let search = this.searchInputValue.substring(lastPointIndex + 1).trim();
                                if (search != '') {
                                    if (this.alias[search] && this.pluralElementTypes.includes(this.alias[search])) {
                                        if (typeof this.alias[search] !== undefined) {
                                            this.getElement(collection, this.alias[search]);
                                        } else {
                                            this.getElement(collection, search);
                                        }
                                    } else {
                                        if (collection == '') {

                                            setTimeout(() => {
                                                this.showPossibActionsForPoints(this.elementActions, points);
                                            }, 200);

                                        } else {
                                            if (typeof this.pages[search] !== undefined) {
                                                let el = this.elementActions.find((el) => el.keyword == this.pages[search]);
                                                let navigate = collection + '.' + el.action;
                                                if (search == 'co') {
                                                    /** urlCtrl.loadByHash(
                                                         navigate
                                                     );
                                                      */
                                                    // cas costum
                                                } else {
                                                    urlCtrl.loadByHash(
                                                        "#@" + navigate
                                                    );
                                                }

                                                this.close();
                                            } else {
                                                if (typeof this.alias[search] !== undefined) {
                                                    this.getElement(collection, this.alias[search]);
                                                } else {
                                                    this.getElement(collection, search);
                                                }
                                            }

                                        }
                                    }
                                } else {
                                    setTimeout(() => {
                                        this.showPossibActionsForPoints(this.elementActions, points);
                                    }, 200);
                                }
                            } else {
                                if (collection == '') {
                                    setTimeout(() => {
                                        this.showPossibActionsForPoints(this.elementActions, points);
                                    }, 200);

                                } else {
                                    let el = this.elementActions.find((el) => el.keyword == this.pages[search]);
                                    let navigate = collection + '.' + el.action;
                                    if (search == 'co') {
                                        /** urlCtrl.loadByHash(
                                             navigate
                                         );
                                          */
                                        // cas costum
                                    } else {
                                        urlCtrl.loadByHash(
                                            "#@" + navigate
                                        );
                                    }

                                    this.close();
                                }

                            }
                        }


                    } else {
                        this.showLoader();
                        this.getElement(this.searchInputValue);
                    }

                }
            } else {

                this.matchShadow.textContent = '';
                this.showHomeLink();
            }
        }, 1000);
    }
    handleKeyPress(event) {
        if (event.ctrlKey && event.key === "k") {
            this.isSearchVisible = !this.isSearchVisible;
            this.customElement.style.display = this.isSearchVisible
                ? "block"
                : "none";
            if (this.isSearchVisible) {
                this.searchInput.value = "";
                this.searchInput.focus();
                this.showHomeLink();
            }
            event.preventDefault();
        } else if (event.key === "Escape") {
            this.close();
        }

    }
    runSearchCommand() {
        const regex = />(\w+):(\w+)(?::([^>]+(.+)))?/;
        let match = this.searchInputValue.match(regex);
        if (match) {
            let act = match[1];
            let element = match[2];
            let paramEl = match[3];
            let params = null;
            if (paramEl) {
                if (isValidUrl(paramEl)) {
                    const urlForm = new FormData();
                    urlForm.append("url", paramEl);
                    this.showLoader();
                    axios
                        .post(
                            `https://www.communecter.org/news/co/extractprocess`,
                            urlForm
                        )
                        .then(({ data }) => {
                            this.hideLoader();
                            params = {
                                name: data.name,
                                shortDescription: data.description,
                                url: data.url
                            }
                            if (act === "new" && this.elementTypes.includes(element)) {
                                this.close();
                                dyFObj.openForm(element, null, params);
                            }
                        })
                        .catch(error => {
                            this.hideLoader();
                        });

                    // si c'est un url recuper ses meta (title, description,tags)
                } else {
                    if (act === "new" && this.elementTypes.includes(element)) {
                        this.close();
                        dyFObj.openForm(element, null, {
                            name: paramEl
                        });
                    }
                }

            } else {

                const regexWithoutParams = />(\w+):(\w+)?/;
                let regexmatch = this.searchInputValue.match(regexWithoutParams);
                if (regexmatch) {
                    let act = regexmatch[1];
                    let element = regexmatch[2];
                    if (act === "new" && this.elementTypes.includes(element)) {
                        this.close();
                        dyFObj.openForm(element, null);
                    }
                }
                this.searchInputValue = "";
            }
        } else {

            const regexWithoutParams = />(\w+):(\w+)?/;
            let regexmatch = this.searchInputValue.match(regexWithoutParams);
            if (regexmatch) {
                let act = regexmatch[1];
                let element = regexmatch[2];
                if (act === "new" && this.elementTypes.includes(element)) {
                    this.close();
                    dyFObj.openForm(element, null);
                }
            }
            this.searchInputValue = "";
        }
    }
    chooseAction(el, sub) {
        switch (sub) {
            case 'members':
                sub = 'directory.dir.members';
                break;
            case 'classifieds':
                sub = 'directory.dir.classifieds';
                break;
            case 'projects':
                sub = 'directory.dir.projects';
                break;
            case 'poi':
                sub = 'directory.dir.poi';
                break;
            case 'crowdfunding':
                sub = 'directory.dir.crowdfunding';
                break;
            case 'memberOf':
                sub = 'directory.dir.memberOf';
                break;
            case 'friends':
                sub = 'directory.dir.friends';
                break;
            case 'follows':
                sub = 'directory.dir.follows';
                break;
            case 'followers':
                sub = 'directory.dir.followers';
                break;
            case 'externalNetwork':
                sub = 'directory.dir.externalNetwork';
                break;
            default:

        }
        if (sub != 'parent') {
            if (el) {
                urlCtrl.loadByHash(
                    "#@" + el.slug + ".view." + sub
                );
            }

        }
    }
    runClassicSearch() {

        let regex = /#(\w+):@(\w+)/;
        let match = this.searchInputValue.match(regex);
        if (match) {
            let type = match[1];
            let id = match[2];
            this.showLoader();
            if (typeof this.alias[type] !== undefined) {
                this.getElement(id, this.alias[type], '#');
            } else {
                this.getElement(id, type);
            }

        }
        let orgaregex = /@(\w+)(?:\.(\w+))?/;
        let matchregex = this.searchInputValue.match(orgaregex);
        if (matchregex) {
            let name = matchregex[1];
            let sub = matchregex[2] || "";
            this.showLoader();
            if (sub == "") {
                this.getElementData(name, ['citoyens', 'organizations'], function callback(value) {
                    urlCtrl.loadByHash(
                        "#page.type." + value.collection + ".id." + value.id
                    );
                });
                this.close();
            } else {
                this.getElementData(name, ['citoyens', 'organizations'], function callback(value) {
                    this.chooseAction(value, sub);
                }.bind(this));
                this.close();
            }
        }
        if (!this.searchInputValue.startsWith("#") && !this.searchInputValue.startsWith("@") && !this.searchInputValue.startsWith(">")) {
            this.getElement(this.searchInputValue);
        }

    }


    handleKeyDown(event) {
        let isCommand = this.checkActualAction();
        if (isCommand && event.key === "Enter") {
            setTimeout(() => {
                this.runSearchCommand();
            }, 200);

        }
        if (!isCommand && event.key == "Enter") {
            setTimeout(() => {
                this.runClassicSearch();
            }, 200);
        }
        if (event.key === "Tab") {
            let keyObj = 'slug';
            let prefix = '';
            event.preventDefault();
            if (this.searchInputValue === '') return;
            if (this.searchInputValue.startsWith("#")) {
                keyObj = 'action';
                prefix = '#';
            } else if (this.searchInputValue.startsWith("@")) {
                keyObj = 'slug';
                prefix = '@';
            } else if (this.searchInputValue.startsWith(">")) {
                keyObj = 'action';
                prefix = '>';
            }

            let bestSuggestion = this.closestSuggestion(this.globalSuggestionsList, keyObj, this.searchInputValue, prefix);
            if (bestSuggestion) {
                this.searchInput.value = bestSuggestion;
                this.searchInputValue = bestSuggestion;
            }
            if (prefix == '') {
                this.getElement(this.searchInputValue);
            } else if (prefix == '@') {
                this.getElement(this.searchInputValue);
            } else {

                this.showHelp()
            }

        }
    }
    close() {
        this.isSearchVisible = false;
        this.customElement.style.display = "none";
    }
    showLoader() {
        this.isLoading = true;
        this.loaderElement.style.display = "flex";
    }

    hideLoader() {
        this.isLoading = false;
        this.loaderElement.style.display = "none";
    }
    getElement(name, type = null, prefix = '') {
        const params = new FormData();

        params.append("name", name);
        if (!type) {
            params.append("searchType[]", "organizations");
            params.append("searchType[]", "badges");
            params.append("searchType[]", "events");
            params.append("searchType[]", "citoyens");
            params.append("searchType[]", "projects");
        } else {
            if (Array.isArray(type)) {
                type.forEach((t) => params.append("searchType[]", t));
            } else {
                params.append("searchType[]", type);
            }
        }
        params.append("indexMin", 0);
        params.append("indexStep", 30);
        params.append("fediverse", false);
        axios
            .post(
                `https://www.communecter.org/co2/search/globalautocomplete`,
                params
            )
            .then(({ data }) => {
                let elements = [];
                let results = data.results ? data.results : [];
                if (results) {
                    Object.keys(results).forEach((id) => {
                        const item = results[id];
                        elements.push({
                            id,
                            ...item,
                        });
                    });
                }
                //  console.log('elements',elements,'results',results);
                this.hideLoader();
                if (elements.length == 0) {
                    this.showEmptySuggestions(elements);
                    this.matchShadow.textContent = '';
                }
                if (elements.length > 0) {
                    this.globalSuggestionsList = elements;
                    this.matchShadow.textContent = this.closestSuggestion(elements, 'slug', this.searchInputValue, prefix);
                } else {
                    this.globalSuggestionsList = [];
                    this.matchShadow.textContent = '';
                }

                this.showSuggestions(elements);
            })
            .catch(error => {
                //console.log('error',error);
                this.hideLoader();
            });

    }
    getElementData(name, type = null, callback = null) {
        const params = new FormData();
        params.append("name", name);
        if (Array.isArray(type)) {
            type.forEach((t) => params.append("searchType[]", t));
        } else {
            params.append("searchType[]", type);
        }
        params.append("indexMin", 0);
        params.append("indexStep", 30);
        params.append("fediverse", false);
        return axios
            .post(
                `https://www.communecter.org/co2/search/globalautocomplete`,
                params
            )
            .then(({ data }) => {
                let elements = [];
                let results = data.results ? data.results : [];
                if (results) {
                    Object.keys(results).forEach((id) => {
                        const item = results[id];
                        elements.push({
                            id,
                            ...item,
                        });
                    });
                }
                this.hideLoader();
                callback(elements[0]);
            })
            .catch(error => {
                this.hideLoader();
                callback(null);
            });
    }
    handleDocumentClick(event) {
        const isOutsideClick = !this.shadowRoot.contains(event.target) && !this.isClickOnOpenButton(event);
        if (isOutsideClick) {
            this.close();
        }
    }
    isClickOnOpenButton(event) {
        const openButton = document.querySelector('#ctrlK .search-btn');
        return openButton && openButton.contains(event.target);
    }
    showEmptySuggestions() {
        if (elements.length == 0) {
            let emptyContainer = document.createElement("div");
            emptyContainer.classList.add("empty-container");
            let emptyText = document.createElement("p");
            emptyText.innerHTML = `Aucune resultat trouvé pour "<strong>${this.searchInputValue}</strong>"`;
            emptyContainer.appendChild(emptyText);
            this.resultContainer.appendChild(emptyContainer);
        }
    }
    showSuggestions(suggestions) {

        this.resultContainer.scrollTop = 0;
        this.resultContainer.innerHTML = "";
        var suggestionsContainer = this.resultContainer.querySelector(
            ".suggestions-container"
        );
        if (suggestions.length === 0) {
            if (suggestionsContainer) {
                suggestionsContainer.remove();
            }
            return;
        }
        if (suggestionsContainer) {
            suggestionsContainer.innerHTML = "";
        } else {
            const newSuggestionsContainer = document.createElement("div");
            newSuggestionsContainer.classList.add("suggestions-container");
            let actionTitle = document.createElement("div");
            actionTitle.classList.add('suggestion-title');
            actionTitle.innerHTML = `<h4>${suggestions.length} Resultat trouvés </h4>`;
            this.resultContainer.appendChild(actionTitle);
            this.resultContainer.appendChild(newSuggestionsContainer);
            suggestionsContainer = newSuggestionsContainer;
        }

        suggestions.forEach((suggestion) => {
            let suggestionItem = document.createElement("div");
            let breadcrumb = document.createElement("ul");
            breadcrumb.classList.add("breadcrumb-suggestion");
            let breadcrumbType = document.createElement("li");
            breadcrumbType.textContent = suggestion.collection;
            breadcrumb.appendChild(breadcrumbType);
            if (suggestion.slug !== "" && suggestion.slug != null) {
                let breadcrumbSlug = document.createElement("li");
                breadcrumbSlug.textContent = suggestion.slug;
                breadcrumb.appendChild(breadcrumbSlug);
            }
            let title = document.createElement("p");
            if (suggestion.postalCode != null) {
                title.style = "color: #caa91a";
                title.innerHTML = this.highlightMatchingText(suggestion.name + " - " + suggestion.postalCode);
            } else {
                title.innerHTML = this.highlightMatchingText(suggestion.name);
            }
            let resultItem = document.createElement("div");
            resultItem.classList.add("suggestion-content");
            let icon = document.createElement("div");
            icon.classList.add("icon-embeded");
            if (suggestion.collection == "citoyens") {
                icon.innerHTML = `<span class=" bg-yellow"><i class="fa fa-user"></i></span>`;
            } else if (suggestion.collection == "organizations") {
                icon.innerHTML = `<span class=" bg-green"><i class="fa fa fa-users"></i></span>`;
            } else if (suggestion.collection == "badges") {
                icon.innerHTML = `<span class=" bg-dark"><i class="fa fa-bookmark"></i></span></span>`;
            } else if (suggestion.collection == "events") {
                icon.innerHTML = `<span class=" bg-orange"><i class="fa fa-calendar bg-orange"></i></span>`;
            } else if (suggestion.collection == "projects") {
                icon.innerHTML = `<span class="bg-purple"><i class="fa fa-lightbulb-o bg-purple"></i></span>`;
            } else if (suggestion.collection == "cities") {
                icon.innerHTML = `<span style="color: #caa91a"><i class="fa fa fa-university"></i></span>`;
            }
            if (suggestion.type == 'city') {
                icon.innerHTML = `<span><i class="fa fa fa-university" style="color: #caa91a"></i></span>`;
            }

            suggestionItem.classList.add("suggestion-item");
            let subtitle = document.createElement("p");
            subtitle.style = "color : grey; font-size : 13px";
            let subtitleContent = "";
            if (typeof suggestion.level4Name != "undefined") {
                subtitleContent += suggestion.level4Name + ",";
            }
            if (typeof suggestion.level3Name != "undefined") {
                subtitleContent += suggestion.level3Name + ",";
            }
            if (typeof suggestion.level1Name != "undefined") {
                subtitleContent += suggestion.level1Name;
            }
            subtitle.innerHTML = subtitleContent;


            resultItem.appendChild(icon);
            resultItem.appendChild(title);
            resultItem.appendChild(subtitle);

            suggestionItem.appendChild(breadcrumb);
            suggestionItem.appendChild(resultItem);

            suggestionItem.addEventListener("click", () => {
                if (typeof suggestion.type != undefined && suggestion.type == "city") {
                    urlCtrl.loadByHash("#search?scopeType=open&cities=" + suggestion._id.$id);
                } else {
                    urlCtrl.loadByHash(
                        "#page.type." + suggestion.collection + ".id." + suggestion.id
                    );
                }
                this.close();
            });

            suggestionsContainer.appendChild(suggestionItem);
        });
    }
    showHelp() {
        this.resultContainer.innerHTML = "";
        let helperContainer = document.createElement("div");
        let actionTitle = document.createElement("div");
        actionTitle.classList.add('suggestion-title');
        actionTitle.innerHTML = "<h4>Liste des actions possible à faire </h4>";
        let commandes = [...this.helpItems, ...this.elts, ...this.actions];
        for (var i = 0; i < commandes.length; i++) {
            var nouvelElement = document.createElement("div");
            nouvelElement.classList.add('suggestion-item');
            nouvelElement.classList.add('action-list');
            var text = document.createElement("div");
            text.textContent = commandes[i].label;
            var action = document.createElement("div");
            action.classList.add('spotlight-helper-key');
            action.innerHTML = '<kbd>' + commandes[i].action + '</kbd>';
            nouvelElement.appendChild(text);
            nouvelElement.appendChild(action);
            (function (index, commandes, searchInput, resultContainer) {
                nouvelElement.addEventListener("click", function (event) {
                    searchInput.value = commandes[index].action;
                    searchInput.focus()
                    resultContainer.innerHTML = "";
                });
            })(i, commandes, this.searchInput, this.resultContainer);
            helperContainer.appendChild(nouvelElement);
        }
        this.hideLoader();
        this.resultContainer.appendChild(actionTitle);
        this.resultContainer.appendChild(helperContainer);
    }

    showHomeLink() {
        this.resultContainer.innerHTML = "";
        let menuContainer = document.createElement("div");
        let homeContainer = document.createElement("div");

        for (let m = 0; m < this.menus.length; m++) {
            let linkContainer = document.createElement("div");
            let linkTitle = document.createElement("div");
            let textContent = document.createElement("div");
            textContent.classList.add('suggestion-title');
            textContent.textContent = this.menus[m].title;
            linkTitle.appendChild(textContent);
            linkContainer.appendChild(linkTitle);
            for (var i = 0; i < this.menus[m].links.length; i++) {
                var nouvelElement = document.createElement("div");
                nouvelElement.classList.add('suggestion-menu-item');

                (function (index, links, customElement, isSearchVisible, searchInput, resultContainer, handleInputChange) {
                    nouvelElement.addEventListener("click", function (event) {
                        if (links[index].type === 'navigation') {
                            urlCtrl.loadByHash("#" + links[index].action);
                            isSearchVisible = false;
                            customElement.style.display = "none";
                        } else {
                            searchInput.value = links[index].action;
                            searchInput.focus()
                            resultContainer.innerHTML = "";
                            searchInput.dispatchEvent(new Event('change'));
                        }
                    });
                    var text = document.createElement("div");
                    text.classList.add("suggestion-text-container")
                    var titleText = document.createElement("div");
                    titleText.innerText = links[i].label;
                    var iconText = document.createElement("i");
                    var iconClasses = links[i].icon.split(' ');
                    iconClasses.forEach(function (className) {
                        iconText.classList.add(className);
                    });

                    text.appendChild(iconText);
                    text.appendChild(titleText);


                    var action = document.createElement("div");
                    action.classList.add('spotlight-helper-key');
                    action.innerHTML = '<kbd>' + links[i].action + '</kbd>';
                    nouvelElement.appendChild(text);
                    if (links[i].type != 'navigation') {
                        nouvelElement.appendChild(action);
                    }
                })(i, this.menus[m].links, this.customElement, this.isSearchVisible, this.searchInput, this.resultContainer, this.handleInputChange);

                linkContainer.appendChild(nouvelElement);
            }
            homeContainer.appendChild(linkContainer);

        }
        this.hideLoader();
        menuContainer.appendChild(homeContainer);
        this.resultContainer.appendChild(menuContainer);
    }
    showPossibActions(values) {
        this.resultContainer.innerHTML = "";
        let actionTitle = document.createElement("div");
        actionTitle.classList.add('suggestion-title');
        actionTitle.innerHTML = "<h4>Liste d'action possible à faire </h4>";
        let actionsLists = document.createElement("div");

        for (var i = 0; i < values.length; i++) {
            var nouvelElement = document.createElement("div");
            nouvelElement.classList.add('suggestion-item');
            nouvelElement.classList.add('action-list');
            var text = document.createElement("div");
            text.textContent = values[i].label;
            var action = document.createElement("div");
            action.classList.add('spotlight-helper-key');
            action.innerHTML = '<kbd>' + values[i].action + '</kbd>';
            nouvelElement.appendChild(text);
            nouvelElement.appendChild(action);

            (function (index, values, customElement, elementTypes, isSearchVisible, searchInput, resultContainer) {
                nouvelElement.addEventListener("click", function (event) {
                    searchInput.value = values[index].action;
                    resultContainer.innerHTML = '';
                });
            })(i, values, this.customElement, this.elementTypes, this.isSearchVisible, this.searchInput, this.resultContainer);
            actionsLists.appendChild(nouvelElement);
        }
        this.hideLoader();
        this.resultContainer.appendChild(actionTitle);
        this.resultContainer.appendChild(actionsLists);
    }
    showPossibActionsForPoints(values, points = null) {
        this.resultContainer.innerHTML = "";
        let actionTitle = document.createElement("div");
        actionTitle.classList.add('suggestion-title');
        actionTitle.innerHTML = "<h4>Liste d'action possible à faire </h4>";
        let actionsLists = document.createElement("div");

        for (var i = 0; i < values.length; i++) {
            var nouvelElement = document.createElement("div");
            nouvelElement.classList.add('suggestion-item');
            nouvelElement.classList.add('action-list');
            var text = document.createElement("div");
            text.textContent = values[i].label;


            if (points == null) {
                var action = document.createElement("div");
                action.classList.add('spotlight-helper-key');
                action.innerHTML = '<kbd>' + values[i].action + '</kbd>';
                nouvelElement.appendChild(action);
            } else {
                var content = document.createElement("div");
                content.classList.add('suggestion-item-content');
                var text = document.createElement("div");

                text.textContent = values[i].label;


                var action = document.createElement("div");
                action.innerHTML = `<span ><i style="color: #9fbd38;font-size: 20px" class="fa fa-link"></i></span>`;
                action.classList.add("icon-embeded");


                var icon = document.createElement("div");
                icon.classList.add("icon-embeded");
                if(this.isValidUrl(values[i].icon)){
                    icon.innerHTML = `<span ><img width="32"  height="32" src="${values[i].icon}"/></span>`;
                }else{
                    icon.innerHTML = `<span ><i style="color: #9fbd38" class="fa fa-2x ${values[i].icon}"></i></span>`;
                }

                content.appendChild(icon);
                content.appendChild(text);
                nouvelElement.appendChild(content);
                nouvelElement.appendChild(action);
            }
            (function (index, values, customElement, elementTypes, isSearchVisible, searchInput, resultContainer) {
                nouvelElement.addEventListener("click", function (event) {

                    if (points != null && points.length > 0) {
                        let txt = points[0].toLowerCase();
                        let navigate = txt + values[index].action;
                        urlCtrl.loadByHash(
                            "#@" + navigate
                        );
                        isSearchVisible = false;
                        customElement.style.display = "none";
                    } else {
                        searchInput.value = values[index].action;
                    }
                    resultContainer.innerHTML = '';
                });
            })(i, values, this.customElement, this.elementTypes, this.isSearchVisible, this.searchInput, this.resultContainer);
            actionsLists.appendChild(nouvelElement);
        }
        this.hideLoader();
        this.resultContainer.appendChild(actionTitle);
        this.resultContainer.appendChild(actionsLists);
    }
    showSuggestionsSearch(values) {
        this.resultContainer.innerHTML = "";
        let actionsLists = document.createElement("div");
        for (var i = 0; i < values.length; i++) {
            var nouvelElement = document.createElement("div");
            nouvelElement.classList.add('suggestion-item');
            nouvelElement.classList.add('action-list');
            var text = document.createElement("div");
            text.textContent = values[i].label;
            var action = document.createElement("div");
            action.classList.add('spotlight-helper-key');
            action.innerHTML = '<kbd>' + values[i].action + '</kbd>';
            nouvelElement.appendChild(text);
            nouvelElement.appendChild(action);
            (function (index, values, searchInput, resultContainer) {
                nouvelElement.addEventListener("click", function (event) {
                    searchInput.value = values[index].action;
                    resultContainer.innerHTML = "";
                });
            })(i, values, this.searchInput, this.resultContainer);
            actionsLists.appendChild(nouvelElement);
        }
        this.hideLoader();
        let actionTitle = document.createElement("div");
        actionTitle.classList.add('suggestion-title');
        actionTitle.innerHTML = "<h4>Actions </h4>";
        this.resultContainer.appendChild(actionTitle);
        this.resultContainer.appendChild(actionsLists);
    }
    highlightMatchingText(suggestion) {
        const words = suggestion.split(/\s+/);
        const regex = new RegExp(`(${this.searchInputValue})`, "gi");
        const highlightedWords = words.map((word) =>
            word.replace(regex, (match) => `<mark>${match}</mark>`)
        );
        return highlightedWords.join(" ");
    }
    similarityWords(word1, word2) {
        word1 = word1.toLowerCase();
        word2 = word2.toLowerCase();
        let minLength = Math.min(word1.length, word2.length);
        let maxLength = Math.max(word1.length, word2.length);
        let score = 0;
        for (let i = 0; i < minLength; i++) {
            if (word1[i] === word2[i]) {
                score += 1;
            } else {
                break;
            }
        }
        return score / maxLength;
    }
    prefixScoreWords(word1, word2) {
        word1 = word1.toLowerCase();
        word2 = word2.toLowerCase();
        const minLength = Math.min(word1.length, word2.length);
        let diffPosition = minLength;
        for (let i = 0; i < minLength; i++) {
            if (word1[i] !== word2[i]) {
                diffPosition = i;
                break;
            }
        }
        return diffPosition / minLength;
    }
    positionWeightScore(position, maxPosition) {
        return 1 - position / maxPosition;
    }
    closestSuggestion(suggestionsList, objKey, query, prefix = '') {
        let bestMatch = null;
        let bestScore = 0

        for (let i = 0; i < suggestionsList.length; i++) {
            let position = i + 1;
            const maxPosition = suggestionsList.length;
            const suggestion = typeof suggestionsList[i][objKey] == 'undefined' ? suggestionsList[i]['name'] : suggestionsList[i][objKey];
            const similarity = this.similarityWords(query, suggestion);
            const prefixScore = this.prefixScoreWords(query, suggestion);
            const positionScore = this.positionWeightScore(position, maxPosition);

            const totalScore = similarity + prefixScore + positionScore;
            if (totalScore > bestScore) {
                bestScore = totalScore;
                bestMatch = suggestionsList[i];
            }
        }
        if (this.searchInputValue != "") {
            if (this.searchInputValue.startsWith(">") || this.searchInputValue.startsWith("#")) {
                return bestMatch[objKey];
            } else {
                return prefix + bestMatch[objKey].toLowerCase();
            }
        } else {
            return null;
        }
    }
}
window.customElements.define("spotlight-search", SpotLight);
