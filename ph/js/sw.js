self.addEventListener("fetch", function(e){
    e.respondWith((async() => {
        var request = e.request,
            url = new URL(request.url)
        if(url.hash && url.hash.trim() !== "" && url.hash.replace("#", "").trim() !== ""){
            var newURL = new URL(url)
            newURL.searchParams.append("hash", url.hash.replace("#", ""))
            request = new Request(newURL, new Request(request, {
                credentials: 'include'
            }));
        }

        return await fetch(request);
    })())
})
