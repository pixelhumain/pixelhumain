<?php

namespace PixelHumain\PixelHumain\assets;

class JQuery2Assets extends AppAssets
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'plugins/jQuery/jquery-2.1.1.min.js'
    ];
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
}