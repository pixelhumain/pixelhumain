<?php

namespace PixelHumain\PixelHumain\assets;

class MainSearchLayoutAsset extends AppAssets
{
    public $depends = [
        JQuery2Assets::class,
    ];
}