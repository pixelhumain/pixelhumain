<?php
    return [
        'January'   => 'Januar',
        'February'  => 'Februar',
        'March'     => 'März',
        'April'     => 'April',
        'Mai'       => 'Mai',
        'June'      => 'Juni',
        'July'      => 'Juli',
        'August'    => 'August',
        'September' => 'September',
        'October'   => 'Oktober',
        'November'  => 'November',
        'December'  => 'Dezember'
    ];
