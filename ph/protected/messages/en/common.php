<?php

return array(

	//LANGUAGES
	"french" => "French",
	"english" => "English",
	"spanish" => "Spanish",
    'date.From' => 'From',
    'date.Till' => 'Till',
	"act.discuter" => "to discuss",
	"act.next" => "next",
	"act.todo" => "to do",
	"act.tracking" => "in progress",
	"act.totest" => "to test",
	"act.done" => "done",
);

?>