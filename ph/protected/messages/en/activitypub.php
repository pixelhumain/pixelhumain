<?php

return array(
    "{who} has create new note in {where} :  '{what}'" => "{who} has created a new note in {where}: '{what}'",
    "{who} has create new note '{what}'" => "{who} has created a new note '{what}'",
    "{who} has create new event '{what}'" => "{who} has created a new event '{what}'",
    "{who} follow you" => "You have a new follower: {who}! 😎",
    "{who} leave participation in  '{what}'" => "{who} has left participation in '{what}'",
    "{who} send invitation for become member of '{what}'" => "{who} has sent an invitation to become a member of '{what}'",
    "{who} has accepted your follow request" => "{who} has accepted your follow request! 🙌",
    "{who} has delete note named {what}" => "{who} has deleted the note named {what} 🗑️",
    "{who} has updated info {what} of project {where}" => "{who} has updated the information {what} of the project {where} 🔄",
    "{who} has create project named {what}" => "{who} has created a project named {what} 🎉",
    "{who} request to join the project {what}  as contributor" => "{who} has requested to join the project {what} as a contributor",
    "{who} accept as contributor  into {what}" => "{who} has been accepted as a contributor into {what}",
    "{who} request to join the project {what}  as admin" => "{who} has requested to join the project {what} as an admin",
    "{who} invite you with rules {rules} in projet {where}" => "{who} invites you with rules {rules} in project {where}",
    "{who} request to join the project {what}  as follower" => "{who} has requested to join the project {what} as a follower",
    "{who} invite to become admin in projet {what}" => "{who} invites you to become an admin in project {what}",
    "{who} invite you to manage with rules {rules} in projet {what}" => "{who} invites you to manage with rules {rules} in project {what}",
    "{who} invite you to contribute on projet {what}" => "{who} invites you to contribute to project {what}",
    "{who} add {invitor} as contributor with rules {rules} in projet {what}" => "{who} adds {invitor} as a contributor with rules {rules} in project {what}",
    "{who} add you as admin in projet {where} in projet {what}" => "{who} adds you as an admin in project {where} in project {what}",
    "{who} leave contribution in project {where}" => "{who} leaves a contribution in project {where}",
    "{who} leave his follow in  {where}" => "{who} stops following you in {where}",
    "{who} delete contribution in project {where}" => "{who} deletes a contribution in project {where}",
    "{who} has remove his follow" => "{who} has removed their follow",
    "{who} has reject his follow" => "{who} has rejected their follow",
    "{who} has undo his follow" => "{who} has undone their follow",
    "{who} has refused your follow request" => "{who} has refused your follow request",
    "{who} has accepted your invitation" => "{who} has accepted your invitation",
    '{who} add you as admin in projet {where}' => '{who} has added you as an admin in project {where}',
    '{who} with rules {rules} in projet {what}' => '{who} has added you with rules {rules} in project {what}',
    '{who} has undo his follow request' => '{who} has undone his follow request',
    '{who} leave contribution in  {where}' => '{who} has left a contribution in {where}',
    '{who} add {invitor} with rules {rules} in projet {what}' => '{who} adds {invitor} with rules {rules} in project {what}',
    '{who} delete contribution in  {where}' => '{who} has deleted a contribution in {where}',
    '{who} has create note \'{what}\'' => '{who} has created a note \'{what}\'',
    '{who} has create note {what}' => '{who} has created a note {what}'
);


?>