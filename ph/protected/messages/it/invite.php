<?php

	return array(
		"Search or invite your contacts" => "Cerca o invita i tuoi contatti",
		"Search or invite members" => "Cerca o invita i membri",
		"Import a file" => "Importare un file",
		"List of persons invited" => "Elenco delle persone invitate",
		"Add to the list" => "Aggiungere all'elenco",
		"Files (CSV)" => "File (CSV)",
		"Custom message" => "Messaggio personalizzato", 
		"You must use a CSV format" => "È indispensabile utilizzare un formato CSV",
		"We were unable to read your file." => "Non siamo riusciti a leggere il tuo file.",
		"Already in the list" => "Già nell'elenco",
		"My contacts" => "I miei contatti",
		"This user is already in your contacts" => "Questo utente è già nei tuoi contatti",
		"A name, an e-mail..." => "Un nome, un indirizzo e-mail...",
		"Please validate the current invites" => "Si prega di convalidare le richieste correnti",
		"This person is already on your contacts" => "Questa persona è già presente nei vostri contatti",
		"Write" => "Scrivere",
		"Others" => "Altri",
		"Check" => "Verificare",
		"invitation link description" => "Chiunque utilizzi CO potrà unirsi al vostro gruppo seguendo questo link. Se si sceglie di revocare il link, questo smetterà di funzionare immediatamente. È possibile assegnare ruoli alle persone che si uniscono al gruppo.",
		"Roles" => "Ruoli",
		"Add as administrator" => "Aggiungi come amministratore Link di reindirizzamento",
		"Redirect link" => "Link di reindirizzamento",
		"The link is incorrect" => "Il link non è corretto",
		"Redirect to" => "Reindirizza a",
		"joined" => "si è unito",
		"Search" => "Cerca",
		"See the community"=>"Vedi la comunità",
		"Generate link" => "Generare il link",
		"Name (copy and paste your list here, respecting the format)" => "Nome (copiare e incollare qui la vostra lista, rispettando il formato)",
		"Import your CSV file containing the list of e-mail addresses" => "Importare il file CSV contenente l'elenco degli indirizzi e-mail."
	);

?>
