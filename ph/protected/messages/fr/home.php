<?php

	return array(

		// new tranlations refactor home March 2021
		
		"Local toolbox" => "Boite à outils locale",
		"Your" => "Votre",
		"Your <strong> local and free </strong> social network" => "Votre réseau social <strong>libre et local</strong>",
		"A theme, an association, a citizen, a keyword" => "Un thème, une association, citoyen, un mot clé",
		"Environment and biodiversity" => "Environnement et biodiversité",
		"Solidarity grocery and short food channels" => "Épiceries solidaires et circuits courts alimentaires",
		"Local and sustainable energies" => "Énergies renouvelables et locales",
		"Agriculture nutrition food" => "Agriculture alimentation nourriture",
		"Free currency, local currency and exchange system" => "Monnaie libre, monnaie locale et système d’échange",
		"Education, extracurricular activities and teaching" => "Education, périscolaire et enseignement",
		"Health, medical care and medicine" => "Santé, soins médicaux et medicine",
		"Society, citizenship, democracy" => "Societé, citoyenneté, démocratie",
		"Culture, animation and concert" => "Culture, animation et concert",
		"Sport, adventure, discovery and leisure" => "Sport, aventure, découverte et loisir",
		"Urban planning, transportation and construction" => "Urbanisme, transport et construction",
		"Organic and shared gardens" => "Jardins partagés et bio",
		"Territorial and societal development" => "Développement social et territorial",
		"Find local eco-responsible initiatives" => "Trouver des initiatives éco-responsables près de chez vous",
		"<strong>What</strong> is" => "<strong>Qu'est ce </strong> que",
		"COmmunecter is an open source, territorial network with endless possibilities" => "Le réseau des possibles. COmmunecter est un réseau open-source territorial.",
		"Communecter has been created to apply open systemic experiments <strong> in all domains</strong>." => "COmmunecter est une plateforme créée pour appliquer des expérimentations systémiques et ouvertes, <strong>dans tout les domaines</strong>",
		"It's <strong>territorial approach </strong> and provides an <strong>open toolbox</strong> for all local stakeholders (citizens, NGOs, local businesses, government and public organizations), that gives the opportunity to publish its Informations, to create a network, dynamics, or develop links within a given territory." => "Elle propose une <strong>approche territoriale</strong> et met à disposition de tous les acteurs locaux (citoyens, associations, entreprises, collectivités) une <strong>boite à outils participative</strong> permettant à chacun de publier son information, de créer un réseau, une dynamique, ou de développer des liens sur un territoire donné.",

		"Communecter is open source and open data. It is developed and maintained by a community of committed developers." => "Communecter est open-source et open-data. Elle est maintenue et développée par une communauté de développeurs engagés.",
		"Yippee ! " => "Youpi ! ",
		"<strong>is finally a cooperative</strong> to continue building together<br/>We'll soon open for participation."=> "est <strong>enfin une coopérative</strong> pour mieux construire ensemble<br/>On va bientôt ouvrir au sociétariat.",
		"Become a member" => "Devenir sociétaire",
		"What<strong> we propose</strong>" => "Ce que <strong>nous proposons</strong>",
		"Customize your tools to match your personal use" => "Personnalisez vos outils pour vos usages",
		"Collect and gather the knowledge in order to build your ecosystem" => "Récoltez la connaissance pour construire votre écosystème",
		"Visualize, connect and act" => "Visualiser, connecter et agir",
		"A sectoral and connected approach for our territories" => "Une approche sectorielle et connectée pour nos territoires",
		"Experiment partipative municipalities" => "Expérimentons les communes participatives",
		"Experiment a horizontal governance" => "Experimentez une gouvernance horizontale",
		"Free and Open Source Software" => "Free and Open Source Software",
		"Cities connected with participative and open sectors. Tools that enhance knowledge, understanding and collaboration" => "Des villes connectées avec des secteurs participatifs et ouverts. Des outils qui améliorent la connaissance, la compréhension et la collaboration",
		"Experimenting cities differently, with participative sectors, knowledge transfers and fluid information flows will open up our cities and create new means of communications and knowledge" => "Expérimenter les villes autrement, avec des secteurs participatifs, des transferts de connaissances et des flux d'informations fluides, permettra d'ouvrir nos villes et de créer de nouveaux moyens de communication et de connaissance.",
		"New tools for collaboration and methods of cooperative economy" => "Nouveaux outils de collaboration et méthodes d'économie coopérative",
		"Improve collective work dynamics through a new digital technology that supports decision making processes, horizontal teams and organization management.<br>Experiment a process that promotes intrapreneurship, role clarity and activities management through a simplified process coupled with a strong methodology.<br>Encourage employee involvement and switch posture.<br>Develop activity towards new innovative, agile and collaborative work forms and organisation" => "Améliorer la dynamique du travail collectif grâce à une nouvelle technologie numérique qui soutient les processus de prise de décision, les équipes horizontales et la gestion des organisations.<br>Expérimenter un processus qui favorise l'intrapreneuriat, la clarté des rôles et la gestion des activités grâce à un processus simplifié associé à une méthodologie solide.<br>Encourager l'implication des salariés et la posture de changement.<br>Développer l'activité vers de nouvelles formes de travail et d'organisation innovantes, agiles et collaboratives.",

		"<strong>A personal use </strong> for everyone" => "A chacun <strong>son utilisation </strong>",
		"Support us" => "Nous Soutenir",
		"Join the team" => "Rejoindre l'équipe",
		"You work in the IT field, you want to develop, translate or document new features ?" => "Vous travaillez dans l'informatique, vous voulez développer, traduire ou documenter les nouvelles fonctionnalités ?",
		"Join the community of developers." => "Rejoignez la communauté de développeurs de la plateforme.",
		"Act together" => "Agir ensemble",
		"We believe that NGOs and clubs are invaluable social fabric creators that enhance community life" => "Nous croyons que les associations et les clubs sont créateurs d'un lien inestimable pour le vivre ensemble. C'est pour les aider dans leurs activités que notre équipe a construit une solution à leur image.",
		"Know more about the HelloAsso website" => "En savoir plus sur le site d'HelloAsso",
		"Who are we ?" => "Qui sommes-nous ?",
		"Our open source project : now and tomorrow" => "Notre projet Open Source : aujourd'hui et demain",
		"More documentation" => "Plus de documentation",
		"Become a Communecter ambassador" => "Devenir ambassadeur de Communecter",
		"Want to participate to development ?" => "Vous souhaitez participer au developpement ?",
		"Freely create forms that are connected to the whole Communecter's toolbox" => "Créer librement des formulaires et connectés à toute la boîte à outils de communecter",
		"Your customized Communecter, tailored social network" => "Votre COmmunecter personnalisé, Réseau social sur mesure",
		"Designed to allow you to create your own specific Communecter that matches your needs.<br>You can enjoy all the new features that have been developed for Communecter.org, while having the opportunity to manage numerous settings in order to customize your plateform. Try out our brand new COstum CMS" => "Conçu pour vous permettre de créer un COmmunecter spécifique à vos besoins.<br>Vous bénéficiez de toutes les nouveautés développées pour Communecter.org, tout en ayant la main sur de nombreux paramètres permettant de personnaliser votre plateforme. Faites le vous meme avec le COstum CMS",
		"Everyone uses Google Forms. An new type of open source solution totally connected to the goals and stakes that actors, institutions and territories are setting and facing, and that would respect the securing of the collected data. That is why we offer COForms in order to launch a survey, a call for proposals, forms, ... <br> and many more options." => "Tout le monde utilise Google Forms. Il fallait une solution open source modifiable et surtout totalement connectée aux objectifs que se donnent les différents acteurs, institutions et territoires tout en respectant la sécurisation de la donnée récoltée. C&#146;est pourquoi nous vous proposons les COForms pour faire des études, des appels à projet, des questionnaires… <br>et plus si affinité.",
		"Data visualized, Trasnformed and interconnected to truly understand it." => "La donnée doit être visualisée, traitée et interconnectée pour l&#146;utiliser à sa juste valeur",
//revoir
		"The observatory is dynamic insofar as it is connected to the information collected from different sources.<br> The information is transformed into visualisations. <br>Makes it easy to analyze within a territory or a sectorial perspective" => "L&#146;observatoire est dynamique puisqu&#146;il est directement connecté avec la récolte d&#146;information provenant potentiellement de différentes sources.<br> L&#146;information est alors plus facile à comprendre et à intépréter lorsque celle-ci est organisée. <br> C&#146;est aussi une manière de voir tout ce qui se passe sur un territoire avec une approche sectorielle possible",
		"Concret and practical for an efficient, connected, smart and learning territory" => "Concret et Pratique pour un territoire efficient, connecté, intelligent et apprenant",
		"A territory where information is fluid and transparent, where developing projects and actions are permanently shared. Common tools are being developed and improved in order to create a territory as open as Wikipedia, as connected as Facebook, whose activity converge towards the creation and consolidation of Commons. Implementation on an efficient, connected, smart and learning territory" => "Un territoire où l&#146;information est fluide et transparente, toutes les actions qui se développent sont transmises et partagées en permanence. Des outils en commun se développent et s'améliorent pour créer un territoire aussi ouvert que Wikipedia, aussi connecté que Facebook, dont l'activité oeuvre et converge vers la création et la consolidation de biens communs. Mise en application sur un territoire efficient, connecté, intelligent et apprenant",
		"The local, connected and participative sectors. Tools that enhance knowledge, understanding and collaboration" => "Les secteurs locaux connectés et participatifs. Des outils pour mieux COnnaitre, COmprendre et COllaborer",
		"Local sector" => "Les secteurs locaux connectés et participatifs. Des outils pour mieux COnnaitre, COmprendre et COllaborer",
		"New collaborative work tools and methods of cooperative economy" => "Nouveaux outils et méthodes de travail Collaboratif d&#146;Économie COopérative",
		"Improve collective work dynamics through a new digital technology that support decision making processes, teams and organization management.<br>Experiment a process that promotes intrapreneurship, role clarity and activities management through a simplified process coupled with a strong methodology.<br>Encourage employee involvement and change in posture. Offer available tools and methodology in order to disseminate the project and the approach.<br>Develop the activities towards new innovative, agile and collaborative work forms and organisation" => "Améliorer des dynamiques collectives de travail via une nouvelle technologie numérique d&#146;aide à la décision et au management des hommes et de l&#146;organisation.<br>Expérimenter un processus favorisant l&#146;intrapreneuriat, la clarté des rôles et la gestion de l&#146;activité à travers un processus simplifié appuyé sur une méthodologie solide.<br>Favoriser l&#146;implication des collaborateurs et le changement de posture. Rendre la méthodologie et les outils disponibles dans le but d&#146;essaimer le projet et la démarche.<br>Faire évoluer les activités vers de nouvelles formes de collaboration et d&#146;organisation du travail plus agiles et innovantes",
		"Free and Open Source Software – FOSS" => "Logiciel libre et Open Source (Free and Open Source Software – FOSS)",
		"Our free and open source tools" => "Nos outils connectés libre et open source",
		"Hosted and available FOSS tools connected to your account" => "Hébergés et mise à disposition d’outils FOSS connecté à votre compte",
// revoir
		"The free and open source software model provides interesting processes and tools from which women and men can create, exchange, share, and operate software and knowledges efficiently. The free and open source software can play an important role as a convinient developing instrument insofar as its free and open model makes it a natural contributor to ongoing efforts that converge towards Millenium Development Goald (MDG)" => "Le modèle de logiciel libre et Open Source fournit des outils et processus intéressants grâce auxquels les femmes et les hommes peuvent créer, échanger, partager et exploiter des logiciels et des connaissances efficacement. Le logiciel libre et Open Source peut jouer un rôle important en tant qu&#146;instrument pratique de développement dans la mesure où son modèle libre et ouvert participe des efforts convergeant vers les Objectifs du millénaire pour le développement (OMD)",
		"" => "",




		// Former trad
		"You're not <span class='text-dark'>communected</span>"=>"Vous n'êtes pas <span class='text-dark'>communecté-e</span>",
		"You are"=>"Vous êtes",
		"communected to" => "communecté-e à",
		"Be communected permits you to get smart informations<br>localy performed."=>"Être communecté-e vous permet de capter en direct les informations pertinentes<br>qui se trouvent autour de vous.",
		"Be communected"=> "Communectez-vous",
		"Change your communexion" => "Changer de communexion",
		"communect you : London, Paris, Brussels ?"=>"communectez-vous : Nantes, Strasbourg, Avignon ?",
		"To use the network efficiently, we advice you to be <i><b>communected</b></i>"=>"Pour utiliser le réseau à pleine puissance, nous vous conseillons de vous <i><b>communecter</b></i>",
		"Indicate your <b>living place</b>, to keep informed about what's happened around you automatically."=>"Indiquez de préférence votre <b>commune de résidence</b>, pour garder un œil sur ce qui se passe près de chez vous, de façon automatique.",
		"An the moment"=> "En ce moment",
		"on the network"=> "sur le réseau",
		"in" => "à",
		"More informations"=>"En savoir plus",
		"Friends of communecter" => "Les amis de communecter",
		"Constantly improving" => "EN AMÉLIORATION CONTINUE",
		"Communecter is open source platform, built in collaborative way"=>"Communecter est une plateforme open source, construite de façon collaborative",
		"Join"=>"Rejoignez",
		"Open Atlas NGO"=>"L'association Open Atlas",
		"Partners"=> "Partenaires",
		"Legal notice"=>"Mentions Légales",
		"Developpers"=>"Développeurs",
		"Communectors"=>"Communecteurs",
		"Project Communecter"=>"Projet Communecter",
		"Editors"=>"Editeurs",
		"Designers"=>"Designeurs",
		"Contributors"=>"Contributeurs",
		"CONTACT"=>"CONTACT",
		"Welcome on"=>"Bienvenue sur",
		"Welcome on Communecter"=>"Bienvenue sur Communecter",
		"SEARCH"=>"RECHERCHE",
		"ADS"=>"ANNONCES",
		"AGENDA"=>"AGENDA",
		"LIVE"=>"LIVE",
		"Show map" => "Afficher la carte",
		"Our values" => "Nos valeurs",
		"The search engine"=>"Le moteur de recherche",
		"Find & connect with local actors"=>"Pour retrouver facilement les acteurs de son territoire",
		"Local ads"=>"Petites annonces locales",
		"For local and community exchanges"=>"Pour simplifier les échanges locaux<br>de biens et de services en tout genre<br>(petites annonces)",
		"A common agenda"=>"Un agenda commun",
		"All local events in a click away"=>"Pour connaître en temps réel toute l'activité événementielle locale",
		"A common news stream"=>"Un fil d'actualité commun",
		"Local Message sharing and group communication"=>"Pour diffuser vos messages autrement :<br>publiquement et géographiquement",
		"Terms and conditions of use"=>"Conditions d'utilisations",
		"General information"=>"Informations générales",
		"Test a other communexion" => "Testez une autre communexion",
		"To find resource you need or help others"=>"Trouver les ressources dont j'ai besoin ou aider",
		"SHARING"=>"ENTRAIDE",
		"Sharing ressources"=>"Partager des ressources",
		"Print out" => "Imprimer",
		"Graph View" => "Vue en graphe",
		"Commons"=>"Pour le bien commun",
		"Society 2.2.main"=>"Société 2.2.main",
		"Open"=>"Libre",
		"Free price" => "À prix libre",
		"Collective intelligence"=>"Intelligence Collective",
		"Connected territory"=>"Territoire Connecté",
		"Linked Data"=>"Données liées",
		"Shared Informations"=>"Informations Partagées",
		"Protected data"=>"Données Protégées",
		"No advertisement"=>"Pas de pub",

		"Open Source"=>"Logiciel libre",
		"The values"=> "Les valeurs",
		"All documentation"=>"Toute la documentation",
		"+ More infos"=>"+ Plus d'infos",
		"Searching results, upcoming events,<br>local initiaves, community members"=>"vos résultats de recherche, les membres d'un groupe,<br>les évènements de votre agenda, etc, etc",
		"LA CONNAISSANCE DU TERRITOIRE ACCESSIBLE A TOU.T.E.S" => "LA CONNAISSANCE DU TERRITOIRE ACCESSIBLE A TOU.T.E.S",
		"Wherever you are on Communecter<br>you can consult informations on the map"=>"Où que vous soyez dans Communecter<br>vous pouvez afficher les informations sur une carte",
		"Geolocation"=> "Géolocalisation",
		"Map" => "Cartographie",
		"Task assignment"=> "Attribution de tâches",
		"Voting system with or whitout amendment"=>"Système de vote en concertation avec ou sans amendement",
		"Use Cooperative spaces for decision making, tasking, and experiment transparency and horizontality"=>"Utilisez L&#146;espace coopératif comme outil de décision et d'action. Expérimenter une gouvernance transparente et horizontale.",
		"Cooperative space"=> "Espace coopératif",
		"Exchanges of ressources" => "Un espace d'entraide",
		"To share needs, offers, services, competences for more efficiency"=>"Pour simplifier l'accès aux ressources du territoire,<br>et faire connaître vos besoins locaux",
		"A market place"=> "Une place de marché",
		"Communecter, a connected territorial search engine"=>"Communecter, un moteur de recherche territorial connecté",
		"For Whom ?"=>"Pour qui ?",
		"For me" => "Pour Moi",
		"For my community" => "Pour ma communauté",
		"For commons" => "Pour le commun",
		"What ?"=> "Quoi ?",
		"Why ?"=>"Pourquoi ?",
		"A connected territorial search engine"=>"Un moteur de recherche territorial connecté",
		"Be a pillar of Communecter<br>make a donation to our association"=> "Devenez un des piliers de Communecter<br>en faisant un don régulier à l'association",
		"No advertisement, no business with your privacy, only Openness and Passion"=> "Pas de publicités ! Pas de commerce avec vos données !<br>Que du libre et de la passion !",
		"But to make it possible, we need you !" => "Mais pour que ce soit possible, on a besoin de vous !",
		"From each according to his means, to each according to his needs" => "De chacun selon ses moyens, à chacun selon ses besoins",
		"Stronger together" => "Ensemble on est plus fort",
		"Donate" => "Faire un don",
		"Make it last longer"=>"Rendez le plus fort",
		"Collective intelligence at service for citizens"=>"L'intelligence collective au service du citoyen",
		"I learn about my territory"=>"Je connais mon territoire",
		"I know what's happened around me"=>"Je sais ce qu&#146;il se passe autour de moi",
		"I am connected to my community"=>"Je suis connecté à ma communauté",
		"I can promote ideas and activities and enjoy organization helping tools"=>"Je valorise ce que je fait tout en profitant des outils de gestions d'organisations",
		"A smart collective intelligence"=>"Une intelligence collective en action",
		"I take part to the building of territorial knowledge base"=>"Je participe à la construction d&#146;une base de connaissance territoriale",

		"Cooperative" => "Coopérative",
		"We all believe in something better and building it together" => "On doit pouvoir tout améliorer ensemble",
		"5 main applications" => "Applications principales",
		"Search engine" => "Un moteur de recherche",
		"We are all Open" => "Open Nous-Sommes",
		"Everything we do is <a href='https://github.com/pixelhumain'>OpenSource</a> and built in collaborative way" => "Tout ce qu'on fait est <a href='https://github.com/pixelhumain'>OpenSource</a> et co-construit",
		"We are experimenting new ways of gouvernance,<br>managing territory, implicating local actor into everything and everywhere" => "Nous expérimentons de façon continue des nouvelles approches de gouvernance,<br>les approches territoriales et l'implication massive d'acteurs locaux",

		"Online Doc" => "Documentation en ligne",
		"Team" => "Notre équipe",
		"They form a key part of the adventure"=>"Ils font partie de l'aventure",
		"Share and imagine great ideas" => "Partagez vos idées et imaginez la suite",
		"Ideas Design Graphics Video" => "Idées Design Graphisme Vidéos",
		"Core Development team" => "La team de dev",
		"Project Management" => "Gestion de projet",
		"Non Governmental Organization" => "Association Loi 1901",
		"Active contributors and soon a cooperative" => "Contributeurs actifs et bientot une SCIC",
		"All people we meet." => "Pleins de rencontre",
		"Money for bills & Love to live." => "Le nerf de la guerre",
		"Good tools have great testers" => "Pas de grands outils sans de bons testeurs",
		"Help share & destroy bugs" => "Zone de destruction massive de bugs",
		"Connecting Systems together" => "Connecter les systèmes ensembles",
		"Open Source Tools For Communities" => "Liste d'outils open source au service des communautés",
		"Top Level Sharing process" => "Le premier niveau de partage",
		"5 year Thinking Process" => "5 années de reflexion",
		"Goodies we can pass around" => "Goodies à partager",
		"Read Us" => "En savoir +",
		"helps us to host our tools" => "nous aide pour l'hébergement de nos outils",

		"an innovative societal network" => 
			"un réseau sociétal innovant",

		"created for citizens actors of change" => 
			"au service des citoyens acteurs du changement",

		"Because the need to <b>communicate</b> locally has never been so important" => 
			"Parce que le besoin de <b>communiquer</b> localement n'a jamais été aussi grand",

		"we've made <b>communexion</b> the <b><i>keystone</i></b> of all our applications" =>
			"nous avons fait de la <b>communexion</b> la <b><i>clé de voûte</i></b> de toutes nos applications",

		"The specialty of <b>Communecter</b> is to give you access to the data you are interested in" =>
			"La spécialité de <b>Communecter</b> c'est de vous donner accès aux données qui vous intéressent",

		"according to the <b>geographical area(s)</b> you selected" =>
			"en fonction des <b>zone(s) géographique(s)</b> que vous selectionnez",

		"Are you ready for geo-communication" =>
			"Êtes-vous prêt pour la géo-communication",

		"Imagine a world where your messages can be broadcast" => 
			"Imaginez un monde où vos messages peuvent être diffusés",

		"in a public and geographical way" =>
			"publiquement et géographiquement",

		"Create debates, share information, ask for help,<br> share an idea, propose a project, ask a question" =>
			"Créer des débats, partager des informations, demander de l'aide, <br> partager une idée, proposer un projet, poser une question",

		"You will find a thousand reasons to use" =>
			"Vous trouverez mille raisons d'utiliser",

		"IN LIVE" =>
			"LE LIVE",

		"Our territories are rich with thousands of citizen initiatives, associations, projects, events! <br>It is by weaving close links between these initiatives that we will transform our society sustainably." =>
			"Nos territoires sont riches de milliers d'initiatives citoyennes, d'associations, de projets, d'événements !<br>
             C'est en tissant des liens étroits entre ces initiatives que nous transformerons durablement notre société.",

		"That's why <b>Communecter</b> invites you to geo-locate your data as much as possible, <br>to give <b>local visibility</b> to your actions" =>
			"C'est pourquoi <b>Communecter</b> vous invite à géo-localiser vos données autant que possible,<br>
			afin de donner de la <b>visibilité locale</b> à vos actions",
			
		"This is also <i>geo-communication !</i>" =>
			"C'est aussi ça <i>la géo-communication !</i>",
			
		"Because the desire to organize together leads to the need to decide together...<br><b>Communecter</b> offers to all referenced groups" =>
			"Parce que l'envie de s'organiser ensemble, mène au besoin de décider ensemble...<br>
		     <b>Communecter</b> offre à tous les groupes référencés",
			
		"voting tools, and task management" =>
			"des outils de vote, et de gestion des tâches",
			
		"To accompany you in the process of decisions and actions,<br>it's also part of our goals" =>
			"Vous accompagner dans les processus de décisions et d'actions,<br>ça fait aussi parti de nos objectifs",
			
		"Collective decisions" =>
			"Décisions collectives",
			
		"Horizontal governance" =>
			"Gouvernance horizontale",
			
		"+ more details in video" =>
			"+ de détails en vidéo",
		"No activity around you. Be the first to show that your territory is moving"=>"Pas d&#146;activités autour de vous. Soyez le⋅la premier⋅ère à montrer que ça bouge chez vous",
		"Add an event, a point of interest, a classified"=>"Ajoutez un événement, un point d&#146;intérêt, une annonce",
		"If nobody shares what he knows, nothing could be commonly bigger."=>"Si personne ne partage ce qu&#146;il sait, alors rien ne peut collaborativement plus grand",
		"Around me"=>"Autour de moi",
		"Be the first to reference an element on your territory"=> "Soyez le⋅la premier⋅ère à référencer un acteur sur votre territoire",
		"You are the main protagonist to create its free and open map"=>"Vous en êtes le⋅la protagoniste principale afin de créer sa carte libre et ouverte",
		"Reference your city hall, a NGO, a local business, a place or an initiative <b>you know around you</b>."=>"Référencez votre mairie, une association, un commerce solidaire, un lieu ou une initiative <b>que vous aimez</b>",

		"Add feeds" => "Ajouter des flux",
		"My news feeds" => "Mes flux d'actus",

		"Area of the software sources in free access allowing, on the one hand, the free use of the software concerned and its derivatives, on the other hand, the exchanges leading to the optimization or the common development of the product used with its designers always mentioned." => "Domaine des sources logicielles en accès libre permettant, d’une part, la libre utilisation du logiciel concerné et ses dérivés, d’autre part, les échanges amenant à l’optimisation ou le développement commun du produit utilisé avec ses concepteurs toujours mentionnés.",
		"You are guaranteed that there will never be any advertising on the platform and none of your data will be monetized." => "Vous avez la garantie qu'il n'y aura jamais de publicité faite sur la plateforme et aucune de vos données ne seront monétisées.",
		"If we fight against" => "Si nous nous battons contre les",
		"it is because we refuse to see our data manipulated. We guarantee that your personal data will never be manipulated" => "c'est que nous refusons de voir nos données manipulées. Nous vous garantissons qu'il n'y aura jamais de manipulation de vos données personnelles",
		"The platform is free to use, but your contribution is more than welcome. For example, a recurring donation of 2€/month, paid by 100 people, would allow us to pay the servers for one year" => "La plateforme est libre et gratuite d'utilisation, mais votre contribution est plus que bienvenue. Par exemple un don récurrent de 2€/mois, versé par 100 personnes, nous permettrait de payer les serveurs pour une année",
		"" => "Terre de tradition adaptée aux échanges par les flux immatériel numériques et/ou matériels (aéroports, ports, gares, réseaux routiers) apportant à toute personne habitant ce territoire, même isolé, une capacité à établir des liens internationaux",
		"A distributed intelligence, constantly enhanced, coordinated in real time, which leads to an effective mobilization of skills" => "Une intelligence partout distribuée, sans cesse valorisée, coordonnée en temps réel, qui aboutit à une mobilisation effective des compétences",
		"We like to imagine and experiment, with and for you, new tools and methods to make our territories more efficient and to feel that the society evolves positively and in a participative way. Do not hesitate to contact us if we can help you develop an idea" => "On aime imaginer et expérimenter, avec et pour vous de nouveaux outils et méthodes pour rendre nos territoires plus efficient et sentir que la société évolue positivement et de facon participative. N'hésitez pas à nous contactez si on peut vous aider a développer une idée",
		"The commons are resources shared among a community of users who themselves determine the framework and norms regulating the management and use of their resource" => "Les communs sont des ressources partagées entre une communauté d’utilisateurs qui déterminent eux-mêmes le cadre et les normes régulant la gestion et l’usage de leur ressource",
		// Footer
		"<b>Hyper-active contributors</b> <br> These people have a strong involvement in the project. Salaried or not of Open Atlas they invest sometimes for several years to develop the project. <These historical contributors to COmmunecter are delighted to welcome new people. It is not a closed circle in which you have to prove yourself but well and truly a very friendly open community :p.<br> Open Atlas regularly hires interns and civic services.<br><br> <b>Ambassadors</b> <br> There is also a nice community of ambassadors who develop the use of COmmunecter on the territory or in the community where they are.<br><br> <b>Active Pixels</b> <br> They regularly contribute to the project in a very diverse way. The ambassadors are of course also part of this set <br><br> <b>Friendly collectives and communities</b> <br> We are in relationship with many collectives. We feel particularly close and regularly exchange with La Myne, Chez Nous and La Raffinerie.<br> Our exchanges with other big communities (OpenStreetMap, Zero Waste, ...)" => "<b>Contributeur·ice·s hyper-actifs </b> <br> Ces personnes ont une forte implication au sein du projet. Salarié ou non d'Open Atlas ils s'investissent parfois depuis plusieurs années pour développer le projet. <br> Ces contributeur·ice·s historiques à COmmunecter sont ravi·e·s d'accueillir de nouvelles personnes. Ce n'est pas un cercle fermé dans lequel vous devez faire vos preuves mais bel et bien une communauté ouverte très sympathique :p.<br> Open Atlas embauche régulièrement des stagiaires et des services civique.<br><br> <b> Ambassadeur·ice·s </b> <br> Il existe également une belle communauté d'ambassadeurs qui développe l'usage de COmmunecter sur le territoire ou dans la communauté ou elles sont.<br><br> <b>Pixels actifs</b> <br> Ils contribuent régulièrement au projet de manière très diverse. Les ambassadeurs font bien sûr également partie de cette ensemble <br><br> <b> Collectifs et communautés amis</b> <br> Nous sommes en relation avec de nombreux collectifs. On sent particulièrement proche et on échange régulièrement avec La Myne, Chez Nous et La Raffinerie.<br> Nos échanges avec d'autres grosses communautés (OpenStreetMap, Zéro Déchet, ...)",
		"<b>History of the project</b> <br> Communecter is a project currently carried by the Open Atlas association but whose roots lie in fifteen years of experimentations, trials, loss of motivation, rediscovered hopes, meetings, friendships and permanent innovation for all the actors who have gradually joined the adventure until today. <br><br>Since its creation in 2008, the Open Atlas association has been working on projects related to the commons, cartography and participatory democracy. It is a local association that works on territorial development in Reunion Island." => "<b>Histoire du projet</b> <br> Communecter est un projet actuellement porté par l’association Open Atlas mais dont les racines se trouvent dans une quinzaine d’années d'expérimentations, d’essais, de pertes de motivation, d’espoirs retrouvés, de rencontres, d’amitiés et d'innovation permanente pour l’ensemble des acteurs qui ont rejoint peu à peu l’aventure jusqu’à aujourd’hui. <br><br>Depuis sa création en 2008, l'association Open Atlas travaille sur des projets liés aux biens communs, à la cartographie et à la démocratie participative. C’est une association locale qui oeuvre au développement territorial à La Réunion.",
		"<b>Which platform would you like?</b> <br> We propose you to create your own personalized platform (called COstum). Thanks to our ingenious system you will benefit from the information present in communecter.org, and what will be published on your platform will also be visible on communecter.org. <br> - Holoptism and stigmergy of your Organization, Project, Event<br> - Call for projects system <br> - Sector observatory <br> - Citizen action platform <br> - Community management and resource enhancement <br> - Social network <br> Knowledge harvesting <br> - Volunteer time management & time bank <br><br> <b>Are you an organization? </b> <br> Communecter responds to the need of different context and adapts to your objectives, if you know the objectives you would like to achieve , we will help you to achieve them, and if necessary we can create custom tools and mutualize. <br> - a group of citizens or an Association <br> - a Cooperative or a Federation <br> - a Company <br> - a sector or a Company Branch <br> - a Territory: a region, a department, an agglomeration, a Commune <br> - Digital Public Space <br> - Media <br>" => "<b>Quelle plateforme souhaitez vous ?</b> <br> Nous vous proposons de créer votre propre plateforme personnalisée (appelée COstum). Grâce à notre ingénieux système vous bénéficierez des informations présentes dans communecter.org, et ce qui sera publié sur votre plateforme sera également visible sur communecter.org. <br> - Holoptisme et stigmergie de votre Organisation, Projet, Evenenement<br> - Système d'appel à projets <br> - Observatoire de secteur <br> - Plateforme d'action citoyenne <br> - Gestion de communauté et valorisation de ressources <br> - Réseau social <br> Récolte de connaissances <br> - Gestion du temps bénévole & banque du temps <br><br> <b>Vous êtes une organisation ?</b> <br> Communecter répond au besoin de différent contexte et s’adapte à vos objectifs, si vous connaissez les objectifs que vous aimeriez atteindre , nous vous aiderons à les atteindre, et si nécessaire nous pouvons créer des outils sur mesure et mutualiser. <br> - un group de citoyens ou une Association<br> - une Coopérative ou une Fédération <br> - Entreprise <br> - un secteur ou une Filière de société <br> - un Territoire : Un région, un département, une agglomération, une Commune <br> - Espace Public Numérique <br> - Média <br>",
		"<b>The role of ambassador</b> <br> The COmmunecter tool is built thanks to the involvement of many people: developers, communicators, testers, donors... They make this tool grow allowing us to network locally. But this work would be nothing without the work done by our field ambassadors. <br> <b>Massive communication plays an important role in the appropriation of the tool, but the ambassadors will always have more impact than the most beautiful newsletter in the world.</b><br> In direct relation with the actors of the territory, they propose, discuss and bring up the needs of the citizens of our communes. <br> The Open Atlas association networks and accompanies all the people wishing to use COmmunecter locally. Do not hesitate to introduce yourself in the group dedicated to the ambassadors." => "<b>Le rôle d'ambassadeur </b> <br> L'outil COmmunecter se construit grâce à l'implication de nombreuses personnes : développeurs, communicants, testeurs, donateur·e·s... Ils font grandir cet outil permettant de nous mettre en réseau localement. Mais ce travail ne serait rien sans le travail effectué par nos ambassadeurs de terrain. <br> <b>La communication massive joue un rôle important dans l'appropriation de l'outil, mais les ambassadeurs auront toujours plus d'impact que la plus belle newsletter du monde.</b><br> En relation direct avec les acteurs du territoires, ils proposent, discutent et font remonter les besoins des citoyen·ne·s de nos communes. <br> L'association Open Atlas met en réseau et accompagne toutes les personnes souhaitant utiliser COmmunecter localement. N'hésitez pas à vous présenter dans le groupes dédié aux ambassadeurs.",
		"COmmunecter on play store" => "COmmunecter sur play store",
		"Install" => "Installer",
		"Introduction to the code" => "Introduction au code",
		"TERRITORIAL ENGINE" => "MOTEUR TERRITORIAL",
		"OPEN SOURCE PROJECTS WE USE AND LOVE" => "LES PROJETS OPEN SOURCE QUE NOUS UTILISONS ET AIMONS",
		"and lots of pluggins" => "et de nombreux plugins",
		"Everything is MODULAR" => "Tout est MODULAIRE",
		"TECHNICAL UNIVERSE" => "UNIVERS TECHNIQUE",

		//fediverse
		"Communecter is growing: Join us in the Fediverse !" => "Communecter s'agrandit : Rejoignez-nous dans le Fediverse !",
		"Communecter is expanding its horizons by becoming a member of the Fediverse network. This means that you can now interact with a broader audience and enjoy an even more diverse sharing experience." => "Communecter élargit ses horizons en devenant membre du réseau du Fediverse. Cela signifie que vous pouvez désormais interagir avec un public plus large et profiter d'une expérience de partage encore plus diversifiée.",
		"How does it work ?" => "Comment ça marche ?",
		"Interact with a wider audience" => "Interagir avec un public plus large"
	);
?>
