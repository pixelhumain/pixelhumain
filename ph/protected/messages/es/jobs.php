<?php
/**
 * Extends Locale data for 'en_US'.
 * In this file you can put custom locale settings that will be
 * merged with the ones provided by the framework
 * ( that are stored in <framework_dir>/i18n/data/ )
 */

	return array(
		"The job posting id is mandatory to retrieve the job posting !" =>  "¡El ID de publicación de trabajo es obligatorio para recuperar la publicación de trabajo!",
		"New Job Offer" => "Nueva oferta de trabajo",
		"Fill the form"        => "Llene el formulario",
		"Job Posting" => "Oferta de trabajo",
		"Your job offer has been updated with success" => "Tu oferta de trabajo ha sido actualizada exitosamente.",
		"Can not insert the job : unknown field " => "No se puede insertar el trabajo: error desconocido ",
		"Problem inserting the new job offer" => "Hubo un problema insertando la nueva oferta de trabajo",
		"Your job offer has been added with succes" => "Tu oferta de trabajo ha sido añadida con éxito.", 
		"Can not update the job : you are not authorized to update that job offer !" => "¡ No se puede actualizar el trabajo : no tienes autorización para actualizar esta oferta de trabajo !",
		"Error updating the Organization : address is not well formated !" => "¡Error al actualizar la Organización: la dirección no está bien formateada !",
		"Can not remove the job : you are not authorized to update that job offer !" => "¡No puede eliminar el trabajo: no tienes autorización para actualizar esta oferta de trabajo !",
		"Your job offer has been deleted with success" =>"Tu oferta de trabajo ha sido eliminada con éxito.",		
	);


?>