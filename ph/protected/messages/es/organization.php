<?php
/**
 * Extends Locale data for 'en_US'.
 * In this file you can put custom locale settings that will be
 * merged with the ones provided by the framework
 * ( that are stored in <framework_dir>/i18n/data/ )
 */

	return array(
		"tester" => "Examinador",
		"Your organization has been added with success. Check your mail box : you will recieive soon a mail from us." => "Tu organización ha sido agregada con éxito. Revisa tu casilla de correo: pronto recibirás un correo de nosotros",
		"invalid Captcha Test"  => "Captcha invalida",
		"The organization has been updated" => "La organización ha sido actualizada.",
		"The organization id is mandatory to retrieve the organization !" => "¡La identificación de la organización es obligatoria para recuperar la organización !",
		"The Parent organization doesn't exist !" => "La organización matríz no existe!",
		"Missing List data in 'lists' collection, must have organisationTypes, typeIntervention, public" => " Los datos de la Lista que faltan en la colección de 'listas' deben tener la Tiposdeorganización, Tiposdeintervención, público",
		"Problem inserting the new organization" => "Error al insertar la nueva organización",
		"You have to fill a name for your organization" => "Debes llenar el nombre de tu organización.",
		"You have to fill the type of your organization" => "Debes llenar el tipo de tu organización..",
		"An organization with the same name already exist in the plateform" => "Una organización con el mismo nombre ya existe en la plataforma",
		"The organization id is unkown : contact your admin" => "La identificación de la organización es desconocida: contacta al administrador",
		"Unauthorized Access." => "Acceso no autorizado",
		"The invitation process completed with success" => "El proceso de invitación ha sido completado con éxito",
		"Can not update this organization : you are not authorized to update that organization !" => "No se puede actualizar esta organización: ¡No tienes autprización para actuañizar esta organización!",
		"Are you sure you want to remove this person's :" => "Estás seguro de querer eliminar a esta persona :",
		"ROLE REMOVED SUCCESFULLY!!" => "¡ROL ELIMINADO EXITOSAMENTE!!",
		"Add me as member of" => "Agregarme como miembro de",
		"Edit your informations" => "Edita tu información",
		"Remove from my organization" => "Eliminar de mi organización",
		"Add me as member of the organization" => "Agregarme como miembro de la organización",
		"Are you sure you want to remove the connection with " => "¿Estás seguro de querer eliminar la conexión con ",
		"The link has been removed successfully." => "La liga ha sido eliminada exitosamente.",
		"Error deleting the link : " => "Error eliminado la liga: ", 
		"Do you really want to become a member of the organization : " => "Realmente quieres convertirte en miembro de la organzación : ",
		"You are now a member of the organization : " => "Ahora eres miembro de la organización : ",
		"Become a member of this organization : " => "Convertirme en miembro de esta organización",
		"I AM A MEMBER" => "YO SOY MIEMBRO",
		"NOT A MEMBER" => "NO SOY MIEMBRO",
		"Contact information" => "Información de contacto",

		"A citizen ask to become an admin of one of your organization" => "Un ciudadano pidió convertirse en administrador de una de tus organizaciones",
		"A citizen ask to become admin of" => "Un ciudadano pidió convertirse en administrador de",
		"A citizen ask to become member of" => "Un ciudadano pidió convertirse en miembro de",
		"A citizen ask to become contributor of" => "Un ciudadano pidió convertirse en colaborador de",
		//Delete organization
		"This action is permanent and will close this Organization (Removed from search engines, and lists) !" => "¡Esta acción es permanente y cerrará esta Organización (Eliminada de motores de búsqueda y listas)!",
		"Are you sure you want to delete the organization : " => "Estás seguro de que quieres eliminar esta organización : ",
		"DISABLED" => "DESHABILITADO",
		"Organization disabled !" => "¡La organización está deshabilitada!",
		"The organization has been updated" => "La organización ha sido actualizada",


		//TYPES
		"NGO" => "Negocio",
		"LocalBusiness" => "Negocio local",
		"Local Business" => "Negocio local",
		"Group" => "Grupo",
		"Government Organization" => "Organización gubernamental",
		"the NGO" => "la asociación",
		"the localBusiness" => "el negocio local",
		"Local Business" => "Negocio Local",
		"the group" => "el grupo",
		"the Group" => "el Grupo",
		"the government organization" => "la organización gubernamental",
	);
?>
