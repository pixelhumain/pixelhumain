<?php
/**
 * Extends Locale data for 'en_US'.
 * In this file you can put custom locale settings that will be
 * merged with the ones provided by the framework
 * ( that are stored in <framework_dir>/i18n/data/ )
 */

	return array(
        "Evaluate your projet as commons"=> "Evalúa tu proyecto como comunes",
        "Contact information" => "Información de contacto",
        "Edit your informations" => "Edita tu información",
        "The project has been updated" => "El proyecto ha sido actualizado.",
        "Project licence" => "Licencia de proyecto",
        //Title
        "test" => "prueba",
        "MY PROJECTS" => "MIS PROYECTOS",
        "PROJECTS" => "PROYECTOS",
        "Projects" => "Proyectos",
        "PROJECT TIMELINE" => "LÍNEA DE TIEMPO DEL PROYECTO",
        "PROJECT INFORMATIONS" => "INFORMACIONES DEL PROYECTO",
        "PROJECT DESCRIPTION" => "DESCRIPCIÓN DEL PROYECTO",
        "Project description" => "Descripción del proyecto",
        "CHART" => "GRÁFICO",
        "Chart" => "Gráfico",
        //Informations 
        "Maturity" => "Madurez",
        "Project maturity" => "Madurez del proyecto",
        "idea" => "idea",
        "started" => "empezado", 
        "development" => "desarrollo",
        "testing" => "en prueba",
				"concept" => "concepto",
				"mature" => "maduro",
        "Enter the project's name" => "Ingrese el nombre del proyecto",
        "Write the project's description" => "Write the project's description",
        "Write the project's short description" => "Escribe la descripción breve del proyecto.",
        "Enter the project's maturity" => "Ingresa la madurez del proyecto",
        "Enter the project's start" => "Ingresa el inicio del proyecto.",
        "Enter the project's end" => "Ingresa el final del proyecto.",
        "Enter the project's licence" => "Ingrese la licencia del proyecto",
        "Enter the project's url" => "Introduce la url del proyecto",
        //Porject form
        "Add a new project" => "Añadir un nuevo proyecto",
        "If you want to create a new project in order to make it more visible : it's the best place<br/>You can as well organize your project team, plan tasks, discuss, take decisions...<br/>Depending on the project visibility, contributors can join the project and help<br>to make it happen ! " => "Si quieres crear un nuevo proyecto para hacerlo más visible: ¡es el mejor lugar!<br/>Dependiendo de la visibilidad del proyecto, los colaboradores pueden unirse al proyecto y ayudar",
        "Project duration" => "Duración del proyecto",
        //Contributors
        "CONTRIBUTORS" => "CONTRIBUYENTES",
        "Contributor successfully removed" =>  "Contribuidor eliminado con éxito",
        "Add contributor" => "Añadir colaborador",
        "Connect People or Organizations that are part of the project" => "Conecta personas u organizaciones que forman parte del proyecto.",
        //Chart
        "Edit properties" => "Editar propiedades",
        "Create Chart<br/>Opening<br/>Values<br/>Governance<br/>To explain the aim and draw project conduct" => "Crear gráfico<br/>Apertura<br/>Valores<br/>Governanza<br/>Para explicar el objetivo y dibujar el comportamiento del proyecto.",
        "Add project's properties" => "Añadir propiedades del proyecto",
        "Degree of project's openness (0% = very closed, 100% = very opened)" => "Grado de apertura del proyecto (0% = muy cerrado, 100% = muy abierto)",
        "Add<br/>A new<br/>Property" => "Añadir<br/>A new<br/>Propiedad",
        "No contributor for this property" => "Ningún contribuyente para esta propiedad",
        "Your project is updated" => "Tu proyecto está actualizado",
        "Old projects" => "Proyectos antiguos",
        "Display/Hide old projects" => "Mostrar / Ocultar proyectos antiguos",
        "Create new projects<br>To show your current activity<br>And what's happened around people" => "Crear nuevos proyectos<br>Para mostrar tu actividad actual<br>Y lo que pasa alrededor de la gente",
        "Add a contributor to this project" => "Añadir un colaborador a este proyecto",
        "A project can have People as contributors or Organizations" => "Un proyecto puede tener personas como contribuyentes u organizaciones",
        "No contributor for this project" => "no contribuidor para este proyecto", 
        "Create or Contribute <br>Build Things<br>Find Help<br>Organize<br>Local or distant<br>Projects" => "Crear o colaborar <br> Construir cosas <br> encontrar ayuda <br>organizar <br> Local o distante<br> Proyectos", 
        "Create new projects <br>To show your current activity<br>And what's happened around people" => "Crear nuevos proyectos <br> para mostrar tu actual actividad <br> y lo que ocurre alrededor de las personas",  
	);


?>