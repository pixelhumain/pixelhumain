<?php

return array(

//GLOBAL INTERFACE 

"Rooms"=>"Cuartos",

"Resolutions"=>"Resoluciones",
"Actions"=>"Acciones",
"Proposals"=>"Propuestas",
"Rooms"=>"Espacios CO",

"resolution"=>"resolución",
"action"=>"Acción",
"proposal"=>"propuesta",
"CO-space"=>"Espacio CO",

"Create action" => "Crear una acción",
"Add action" => "Añadir una acción",
"My actions" => "Mis acciones",
"To do" => "Por hacer",
"Done" => "Terminado",
"Archived" => "Archivado",
"Disabled" => "Desactivado",
"Resolved" => "Resuelto",

"Create proposal" => "Crear porpuesta",
"Add proposal" => "Añadir una propuesta",
"My proposals" => "Mis propuestas",
"Amendables" => "Enmendables",
"Amendable" => "Enmendable",
"To vote" => "A votar",
"Closed" => "Cerrados",

"mine" => "míos",
"Author" => "Autor",
"amendables" => "Enmendables",
"amendable" => "Enmendable",
"tovote" => "A votar",
"todo" => "Todo",
"archived" => "archivado",
"disabled" => "desactivado",
"resolved" => "resuelto",
"closed" => "cerrado",
"done" => "hecho",

"late" => "tarde",
"progress" => "progreso",
"nodate" => "sin fecha",
"startingsoon" => "Próximamente",

"Create room" => "Crear un nuevo espacio",

"Topic" => "Tema",

"No proposal" => "Sin propuesta",
"No action" => "Sin acción",
"No resolution" => "Sin resolución",

"end" => "fin",
"Search in actions" => "Buscar en acciones",
"Search in proposals" => "Buscar en propuestas",
"Search in resolution" => "Buscar en resolución",

"up" => "Arriba",
"down" => "Abajo",
"white" => "Blanco",
"uncomplet" => "Incompleto",

"Agree" => "Acuerdo",
"Disagree" => "Desacuerdo",
"Abstain" => "Abstención",
"Uncomplet" => "Incompleto",

"number of voters" => "Número de votantes",
"Your amendement has been save with success" => "Tu argumento ha sido salvado con éxito",
"Your vote has been save with success" => "Tu voto ha sido salvado con éxito",
"processing save" => "Procesando guardar",
"processing delete" => "Procesando eliminación",
"processing delete ok" => "Procesando eliminación ok",
"processing delete KO" => "Error en el proceso de eliminación",
"You already voted the same way" => "Ya has votado en el mismo sentido",
"Refresh data" => "Actualizar datos",
"processing" => "Procesando",
"processing ok" => "Procesamiento terminado",
"all status" => "todo el estado",

"in this space" => "en el espacio",
"The final vote will be on the content of your proposal" => "El voto final será sobre el contenido de tu propuesta.",
"For clarity, please provide additional information about your proposal in the next section." => "Para mayor claridad, proporciona información adicional sobre tu propuesta en la siguiente sección.",
"A proposal is used to discuss and seek community input on a given idea or question" => "Una propuesta se utiliza para discutir y buscar aportes de la comunidad sobre una idea o pregunta dada ",

"Votes are disabled during the amendment period" =>" Los votos están desactivados durante el período de enmienda ",

"Enable amendments" =>" Activar las enmindas",

"anonymous" => "anónimo",
"nominative" => "nominativo",

"activated" => "activado",
"disabled" => "desactivado",

"Rule of majority" => "Regla de mayoría",
"give a value between 50% and 100%" => "da un valor entre 50% y 100%",

"Anonymous voting" => "Votos anónimos",

"Do you want to keep secret voters identity ?" => "¿Quieres mantener en secreto la identidad de los votantes?",

"changing vote enabled" => "cambiando voto habilitado ",
"changing vote forbiden" => "cambiando voto prohibido",

"Authorize changing vote ?" => "¿Autorizar cambiar el voto ?",
"Do you want to allow voters to change the vote ?" => "¿Perimitir a los votantes cambiar su voto?",

"Your proposal must have more than " => "Tu porpuesta debe ser más que ",
"of votes" => "de votos",
"favorables" => "favorables",
"to be validated" => "para ser validados",

"Impossible to go over 100%" => "Imposible superar el 100%",
"Impossible to go under 50%" => "Imposible ir debajo del 50%",

"End date of voting session" => "Fecha final de la votación",
"End date of amendment session (start of voting session)" => "Fecha final de la enmienda (comenzar la sesión de votos)",
"End date of amendment session" => "Fin del periodo de enmienda",

"Edit my proposal" => "Modificar mi propuesta",
"Back to amendements" => "Volver a enmiendas",
"Open votes" => "Abrir votos",
"Disabled my proposal" => "Desactivar mi propuesta",
"Close my proposal" => "Cerrar mi propuesta",

"Edition disabled" => "Modificaciones desactivadas",
"amendement session has begun" => "la sesión de enmiendas ha comenzado",
"vote session has begun" => "la votación ha coemnzado",

"Update datas"         => "Actualizar datos",
"Enlarge reading space" => "Ampliar espacio de lectura",
"Reduce reading space" => "Réducir espacio de lectura",
"your are the author of this proposal" => "Tú eres el autor de esta propuesta",
"is the author of this proposal" => "es el autor de esta propuesta",
"Close this window" => "Cerrar esta ventana",
"You are not allowed to access this content" => "Tú no tienes permiso para acceder a este contenido",

"You must be member or contributor" => "Debes ser miembro o colborador",
"Login to enter in this space" => "Inicia sesión para entrar a este espacio",
"Proposal submited to amendements" => "Porpuesta sometida a enimendas ",

"until" => "hasta que",
"End of amendement session" => "Fin de la sesión de enmiendas",
"You can submit your amendements and vote amendement proposed by other users" => "Puedes enviar tus enmiendas y las enmiendas de voto propuestas por otros usuarios ",

"Proposal" => "Propuesta",
"temporaly" => "temporalmente",
"definitively" => "definitivamente",
"Validated" => "Validado",
"Refused" => "Negada",
"validated" => "validada",
"refused" => "negada",
"adopted" => "validada",
"adopteds" => "aceptados",
"voter" => "votante",

"Be the first to vote" => "Sé el primero en votar",
"Vote open until undefined date" => "Voto abierto hasta fecha indefinida",

"End of vote session" => "fin de la votación",
"You did vote" => "Tú votaste",
"You did not vote" => "Tú no votaste",
"You voted for this answer" => "Tú votaste por esta respuesta",
"The <b>resolution</b> is done : " => "La <b>resolución</b> se ha hecho : ",
"The proposal is" => "la propuesta es",
"Show the resolution" => "Mostrar la reslución",

"Submit an amendement" => "Enviar una enmienda",
"Amendement session is closed" => "El período de enmienda ha cerrado",

"to submit amendements" => "para enviar enmiendas",

"Show amendements" => "Mostrar enmiendas",
"Show all amendements" => "Mostrar todas las enmiendas",
"List of amendements" => "Lista de enmiendas",
"Majority" => "Mayoría",
"Add" => "Añadir",
"No amendement" => "No hay enmienda",
"No amendement validated" => "No hay enmiendas validadas",
"Amendement disabled" => "Enmienda dehabilitada",
"More informations, arguments, exemples, demonstrations, etc" => "Más informaciones, agrumentos, ejemplos, demostraciones, etc",
"External links" => "Ligas externas",
"Debat" => "Debate",
"Add an argument" => "Añadir un argumento",
"Add an action" => "Añadir una acción",

"For" => "Para",
"Neutral" => "Neutral",
"Against" => "Contra",

"I agree" => "Estoy de acuerdo",
"I disagree" => "Estoy en desacuerdo",

"You must be member or contributor to participate" => "Debes ser miembro o colaborador para participar",


"Edit this space" => "editar este espacio",
"You are not logged" => "Ya no estas conectado",
"My roles" => "Mis roles",
"You have no role in" => "No tienes ningún rol en ",
"this projects" => "este proyecto",
"this organizations" => "esta organización",
"Ask an admin to get the appropriated role to access this space" => 
		"Pregunta a un administrador para obtener el rol apropiado para acceder a este espacio ",
"This space is open only for this roles" => "Este espacio está abierto solo para estos roles",
"You must be member or contributor to contribuate" => "Debes ser miembro o colaborador para contribuír",
"Drag / drop to an other space" => "Arrastra / suelta a otro espacio ",
"Processing amendements" => "Procesando enmiendas",
"No vote" => "Sin voto",
"Reload window" => "Cargar ventana de nuevo",
"Show vote details" => "Mostrar los detalles del voto",


"You must be member or contributor to vote" => "Debes ser miembro o colaborador para votar",

"votes are anonymous" => "los votos son anónimos",
"You can not change your vote" => "No puedes cambiar tu voto",

"Changing vote" => "Chambiando voto",

"Allowed" => "Permitido",
"Not allowed" => "No permitido",

"VOTE NOW" => "VOTAR AHORA",
"RESULTS" => "RESULTADOS",

"List of actions linked with this resolution" => "Lista de acciones ligadas con la resolución",

"No action for this resolution" => "Sin acción para esta resolución",

"Access restricted only for" => "Acceso restingido solo para",

"Your amendement is too short ! Minimum : 10 caracters" => "Tu enmienda es muy corta ! Mínimo : 10 caracteres",

"Thanks for your participation !" => "¡Gracias por tu participación!",

"Loading comments" => "Cargando los comentarios",


"Amendements" => "Eniendas",
"Write your amendement" => "Escribe tus enmiendas",
"If your amendement is adopted, it will be added to the principale proposal, <br>and will incorporated the final proposal, submited to vote." => 
"Si tu enmienda es adoptada, será añadida a la propuesta princial,<br>y será incorporada a la propuesta final, enviar a voto.",

"your amendement" => "tu enmienda",
"Size max : 1000 caracters" => "El tamaño máximo es de 1000 caracteres",

"Size min : 10 caractères" => "El tamaño mínimo es de 10 caracteres",
"Save my amendement" => "Guardar mi enmienda",

"You can change your vote anytime" => "Puedes cambiar tu voto en cualquier momento",

"Vote open until" => "Voto abierto hasta",

"voters" => "votantes",

"click to vote" => "clic para votar",
"number of voters" => "número de votantes",
"Delete my amendement" => "Eliminar mi enmienda",

"Collective moderation" => "Moderación colectiva",

"open in CO space" => "abrir en el espacio CO",

"Survey in process" => "Enviando",

"The <b>vote</b> is ended : " => "El <b>voto</b> ha terminado",

"Answer" => "Respuesta",
"the answer" => "la respuesta",
"No answer" => "Sin respuesta",

"click to vote" => "Da clic para votar",

"Please login to post a message" => "Por favor, incia sesión para postear un mensaje",
"I'm logging in" => "Estoy conectado en ",
"I create my account" => "Crear cuenta",
"free registration" => "inscripción gratuita",

"Chat"=>"Chat",

"create a survey" => "crear una propuesta",
"Survey name" => "Nombre de la propuesta",
"Survey text" => "Texto de la propuesta",

"Surveys are published for all your followers" => "Las propuestas son visibles para todos tus seguidores",
"amendementAndVote" => "enmiendayvotar", 
"If Yes, votes are enabled during the amendment period" => "Si Sí, los votos son inaccesibles durante el periodo de enmienda", 
"Amendments and votes at the same time" => "Enmiendas y votos al mismo tiempo", 
"Delete my proposal" => "Eliminar mi propuesta", 
"Validated2" => "", 
"Refused2" => "", 
"validated2" => "", 
"refused2" => "", 
"adopted2" => "", 
"adopteds2" => "", 
"You did not vote yet" => "No has votado todavía", 
"See votes" => "Ver los votos", 
"Vote" => "Votar", 
"Please login to vote" => "Por favor, inicia sesión para votar", 
"Add measure" => "añadir medida", 
"Size min : 10 caracters" => "Tamaño mínimo: 10 caracteres", 
"Invite to vote" => "Invitar a votar",
"Welcome in your cooperative space" => "Bienvenido a tu espacio cooperativo", 
"What is it useful ?" => "¿Para qué sirve?", 
"This space is design to offer a tool for <b>collective organization</b> in order to have transparency in the common decisions" => "Este especio está diseñado para ofrecer una herramienta para <b>organizaciones colectivas </b> para ser transparentes en las decisiones comunes", 
"You can submit proposals to concert {what}" => "Puedes enviar propuestas para concierto {what}", 
"all the members of your organization" => "todos los miembros de tu organización", 
"all the contributors of the project" => "todos los colaboradores del proyecto", 
"It will permit to deal with differents actions (taks) to realize in your activity and attribute them to the {who}" => "Te permitirá lidiar con diferentes acciones (tareas) para realizar en tu actividad y atribuirlas a {who}", 
"How does it work ?" => "¿Cómo funciona?", 
"Because {who}, you start with thematic space depending on your needs" => "Porque {who}, comienzas con un espacio temático según tus necesidades", 
"each project is different" => "cada proyecto es diferente", 
"each organization is different" => "cada organización es diferente", 
"For instance, if you deal with a sport club, you can create a space named 'depense about the club', inside the members can add their proposals link to this theme" => "Por ejemplo, si tú lidias con un equipo deportivo, puedes crear un espacio llamado ´depender del club´, dentro los miembros pueden agregar sus propuestas a este tema", 
"Each space created will be receive proposals and actions linked to its theme" => "Cada espacio creado recibirá propuestas y acciones ligadas a su temas", 
"Let's start !" => "¡Comencemos!", 
"Create your first room clicking on the following button" => "Crear tu primer cuarto dando clic en el siguiente botón", 
"You will find this button again in the left menu of your screen" => "Encontrarás este botón nuevamente en el menú izquierdo de tu pantalla", 
"Your new room will appear on the left menu too, and you will be able to add your first proposal clicking on this button" => "Tu nuevo espacio también aparecerá en el menú izquierdo y podrás añadir tu primera propuesta dando clic en este botón", 
"Proposal, Amendment, Vote..." => "Propuesta, Enmienda, voto", 
"This is the 3 steps of collective decision processus we offer you" => "Estos son los tres pasos del proceso de decisiones colectivas que te ofrecemos ", 
"<b>1 - Proposal :</b> a proposal is writing by {who}" => "<b>1 - Propuesta :</b> una propuesta fue escrita por {who}", 
"a member of the organization" => "un miembro de la organización", 
"a contributor of the project" => "un colaborador del proyecto", 
"The author can activate or deactivate the <i>process of amendment</i>. He choose also the date limit of survey" => "El autor puede activar o desactivar el <i>proceso de enmienda</i>. También escoge la fecha límite de encuesta",
"<b>2 - Amend :</b> an amendment is an enhancement, submit to collective decision, in order to correct, complete or cancel all or a part of the proposal in process.<br/><i>(Currently, it is only possible to add proposal added informations. The update and and the delete is not already develop)</i>" => "<b>2 - Enmienda :</b> una enmienda es un mejora, someter a decisión colectiva, con el fin de corregir, completar o cancelar toda o parte de la propuesta en proceso<br/><i>(Actualmente, solo es posible añadir propuestas que añadan información. La actualización y la eliminación ni está desarrollada todavía)</i>", 
"{who} can purpose an amendment on proposals<br/>Each amendment is submit to vote.<br/>When amendment time limit is over, validating amendment are automatically adding to the original proposal and the vote on the proposal starts." => "{who} puede proponer una enmienda o una propuesta <br/> Cada enmienda se somete a votación.<br/>Cuando termine el tiempo de enmienda, la enmienda de validación se agrega automáticamente a la propuesta original y comienza la votación de la propuesta", 
"All contributors" => "Todos los colaboradores", 
"All members" => "Todos los miembros", 
"Notice : Amendment period can be deactivated by the proposal's author to launch directly vote process" => "Aviso: el autor de la propuesta puede desactivar el período de enmienda para iniciar directamente el proceso de votación",
"<b>3 - Voting :<b> When potential amendment time is finished, voting is starting and you community can give its opinion" => "<b>3 - Votación :<b> Cuando finaliza el tiempo de enmienda potencial, comienza la votación y la comunidad puede opinar", 
"And then" => "Y entonces", 
"When voting time is over, the proposal is closed, and becomes a <i>resolution</i><br/>You can find out all resolutions <b class='letter-green'>accepted</b> or <b class='letter-red'>refused</b> in each room in the following section :" => "Cuando el tiempo de votación acabe, la propuesta se cerrará, y convertirá en una <i>resolución</i><br/> Tú puedes averiguar todas las resoluciones <b clase= ´carta-verde'> aceptada</b> o <b clase= ´carta-roja'> negada </b>en cada cuarto en las siguientes secciones ", 
"And about actions ?" => "¿Y sobre acciones?", 
"Actions can be freely created in each room<br/>They can be correlated to a proposal" => "Las acciones pueden ser libremente creadas en cada cuarto<br/> Pueden ser correlatores de una propuesta", 
"Delete a room" => "Eliminar un espacio", 
"Are you sure to delete this cooperative space ?" => "¿Estás seguro de querer eliminar este espacio cooperativo?", 
"All the proposals, resolutions and actions linked to this room will be deleted" => "Todas las propuestas, resoluciones y acciones ligadas a este espacio se eliminarán", 
"Yes, delete this room" => "Sí, eliminar este espacio", 
"Delete the room : {what}" => "Eliminar el espacio: {what}",
'Please login to go further' => 'Por favor, inicie sesión para continuar', // espagnol
);
