<?php 
	return array(
		"Our circuits" => "Nuestros circuitos",
		"Result for" => "Resultado de",
		"Any destination" => "Cualquier destinación",
		"Number of travellers" => "Número de pasajeros",
		"You travel" => "Tu viaje",
		"Date of travel" => "Fecha de viaje",
		"Adapted time" => "Tiempo adaptado",
		"Show" => "Mostrar",
		"Portal administor" => "Portal administrativo",
		"My Dashboard" => "Mi tablero",

		"Contact us" => "Contáctanos",
		"E-mail *" => "E-mail *",
		"Name" => "Nombre",
		"Your situation" => "Tu situación",
		"Object" => "Objeto",
		"Your message" => "Tu mensaje",
		"Back" => "Volver",
		"what's your name ?" => "¿Cuál es tu nombre?",
		"your mail address : exemple@mail.com" => "Tu dirección de correo : exemple@mail.com",
		"tourist or professional ?" => "¿Turista o profesional ?",
		"what's about ?" => "¿Cuál es el tema?",
		"SEND" => "ENVIAR",
		"security" => "securidad",
		"thanks to copy the code below to be able to send your message" => "gracias por copiar el código debajo para poder enviar tu mensaje",
		"copy the code here" => "Copia el código aquí",
		"Security code is not correct" => "código incorrecto",
		"Your message has been sent." => "Tu mensaje ha sido enviado.",
		"Back to home page" => "Volver a la página de inicio",
		"Thanks for your message" => "Gracias por tu mensaje",


		"Error, your message has not been sent" => "Hubo un error, tu mensaje no ha sido enviado",

		"We are sorry" => "¡Lo sentimos!",		
	); 
?>