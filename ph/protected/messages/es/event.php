<?php

	return array(
		"EVENT" => "Eventos",
                "Event's informations" => "Información del evento",
                "Event removed" => "Evento eliminado",
                "Invalid end-Date" => "Fecha de cierre del evento inválida",
                "Invalid date" => "Fecha incorrecta",
                "Event already exist" => "Este evento ya existe.",
                "Event well updated" => "Tu evento ha sido actualizado correctamente.",
                "Create an Event" => "Crear un evento",
                "Leave this Event"                 => "Salir de este evento",
                "Join this Event"                 => "Participar en este evento",
                "Join"                                         => "Participar",
                "Event Categories" => "Categorías de eventos",
                "You organize an event ? Share it in the agenda !" => "¿Organizaste un evento? ¡Compártelo en tu agenda !",
                "Add an event"=> "Añadir un evento",
                "Only people can attend to an event" => "Solo las personas pueden asistir a tu evento",
                "Create and Attend<br/>Local Events<br/>To build up local activity<br/>To help local culture<br/>To create movement" 
                => "Crea y asiste a <br/>evento local...<br/>Para construir tu actividad local,<br/>para ayudar a la cultura local,<br/>Para crear un movimiento.",
                "Create sub-events to show the event's program.<br/>And Organize the event's sequence"=> "Crear sub-elementos para mostrar el programa del evento.<br/>Y organizar la secuencia del evento.",
                "Attendees" => "Participantes",
                "Guests" => "Invitados",
                "Invite attendees to the event" => "Invita a los asistentes al evento",
                "See modifications done on this event" => "Ver las modificaciones realizadas en este evento",
                "attendee" => "participante",
                "guest" => "invitado",
                "Contact information" => "Información de contacto",
                "Edit your informations" => "Editar tu información",
                "The event has been updated" => "El evento ha sido actualizado",
                "Update the organizer" => "Actualizar el organizador",
                "Add an organizer" => "Añadir un organizador",
                "From"=>"De",

                //Type
                "concours" => "Concursos",
            "festival" => "Festival",
            "market" => "Mercado",
            "others" => "Otros",
            "concert" => "Concierto",
            "exposition" => "Exposición",
            "getTogether" => "Rencuentro",
            "meeting" => "Reunión",
            "competition" => "Competencia",
            "course" => "Curso",
            "workshop" => "Taller",
            "conference" => "Conferencia",
            "debate" => "Debate",
	)
?>