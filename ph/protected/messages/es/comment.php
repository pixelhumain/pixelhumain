<?php

	return array(
		"Comments" => "Comentarios",
		"comments" => "comentarios",
		"comment" => "comentario",
		"answers" => "respuestas",
		"answer" => "respuesta",
		"Popular" => "Popular",
		"Abuse" => "Denunciar",
		"Say Something" => "Expresarse",
		"You are going to declare this comment as abuse : please fill the reason ?" => "Este comentario tiene un contenido ofensivo : ¿cuál es el motivo ?",
		"Please fill a reason" => "Hay que llenar la causa para declarar un comentario ofensivo",
		"You already declare this comment as abused." => "Ya declaró este comentario ofensivo",
		"Error calling the serveur : contact your administrator." => "Ha ocurrido un error. Gracias por enviar un mensaje a la administración",
		"Validate" => "Validar",
		"Cancel" => "Cancelar",
		"The comment has been posted" => "Comentario publicado",
		"Something went really bad" => "Ha ocurrido un error", 
		"Agree with that" => "¡Estoy de acuerdo!",
		"Disagree with that" => "¡Estoy en desacuerdo",
		"Report an abuse" => "Declarar al moderador",
		"This comment has been deleted because of his content." => "Comentario borrado - identificado como abusivo",
		"You are going to delete this comment : are your sure ?" => "Está suprimiendo un comentario, ¿estás seguro?",
		"No comment" => "Ningún comentario",
		"Update your comment"=>"Modificar su comentario",
		"Show more comments"=>"Ver más comentarios",
		"Your comment is empty"=>"Su comentario está vacío",
		"Do you want to delete this comment"=>"Quiere suprimir este comentario",
		"Loading comments"=>"Cargando los comentarios",
	)

?>
