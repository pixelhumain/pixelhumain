<?php return array(
"Commons"=>"Commons",
"Agriculture | Food"=>"Kmetijstvo | Hrana",
"Health"=>"Zdravje",
"Waste"=>"Odpadki",
"Development | Transport | Construction"=>"Razvoj | Promet | Gradbeništvo",
"Education | Childhood"=>"Izobraževanje | Otroštvo",
"Citizenship"=>"Državljanstvo",
"Economy"=>"Gospodarstvo",
"Energy | Climate"=>"Energija | Podnebje",
"Culture | Animation"=>"Kultura | Zabava",
"Biodiversity"=>"Biotska raznovrstnost",
"ICT | Numeric | Internet"=>"IKT | Številke | Internet",
"Food"=>"Hrana",
"Transport"=>"Prevoz",
"Education"=>"Izobraževanje",
"Energy"=>"Energija",
"Environnement"=>"Okolje",
"Numeric"=>"Številčno",
"Sales & rents"=>"Prodaja",
"Share"=>"Delite",
"To share"=>"Deliti",
"Give"=>"Dajte",
"To give"=>"Dati",
"Sale"=>"Hale",
"For sale"=>"Za prodajo",
"Rent"=>"Najem",
"For rent"=>"Za najem",
"Looking for"=>"Iščete",
"Jobs"=>"Delovna mesta",
"Job offers"=>"Ponudbe za delo",
"Offers"=>"Ponudbe",
"Technology"=>"Tehnologija",
"TV / Video"=>"Televizija / video",
"IT"=>"IT",
"Touchpads"=>"Plošče na dotik",
"Telephony"=>"Telefonija",
"Camera"=>"Fotoaparat",
"Audio device"=>"Zvočna naprava",
"Property"=>"Lastnina",
"House"=>"Hiša",
"Flat"=>"Ravna stran",
"Ground"=>"Zemlja",
"Parking"=>"Parkirišče",
"Office"=>"Pisarna",
"Vehicles"=>"Vozila",
"Car"=>"Avto",
"SUV"=>"SUV",
"Utility"=>"Storitve",
"Moto"=>"Moto",
"Scooter"=>"Skuter",
"Boat"=>"Čoln",
"Small car"=>"Majhen avtomobil",
"Bikes"=>"Kolesa",
"Car equipment"=>"Oprema za avtomobile",
"2-wheeled equipment"=>"dvokolesna oprema",
"Boat equipment"=>"Oprema za čolne",
"Bike equipment"=>"Oprema za kolesa",
"Home"=>"Domov",
"Household appliances"=>"Gospodinjski aparati",
"Furniture"=>"Pohištvo",
"Baby equipment"=>"Otroška oprema",
"Animals"=>"Živali",
"Various"=>"Različne",
"Leisure"=>"Prosti čas",
"Sports"=>"Šport",
"Musical instrument"=>"Glasbeni instrument",
"Sound system"=>"Zvočni sistem",
"CD / DVD"=>"CD / DVD",
"Toy"=>"Igrača",
"Games"=>"Igre",
"Books / Comics"=>"Knjige / stripi",
"Collections"=>"Zbirke",
"DIY"=>"DIY",
"Gardening"=>"Vrtnarjenje",
"Art / Deco"=>"Umetnost / Deco",
"Model making"=>"Izdelava modela",
"Childcare"=>"Varstvo otrok",
"Fashion"=>"Moda",
"Clothes"=>"Oblačila",
"Shoes"=>"Čevlji",
"Accessories"=>"Dodatki",
"Watches"=>"Ure",
"Jewels"=>"Dragulji",
"Purchase-Comptability-Management"=>"Upravljanje nabavno-računovodske sposobnosti",
"Arts-Crafts"=>"Umetnostne obrti",
"Bank-Insurance"=>"Banka-zavarovalnica",
"Building-Public_works"=>"Gradnja-javna_dela",
"Building & Public works"=>"Stavba",
"Trade-Sales"=>"Trgovina in prodaja",
"Communication-Multimedia"=>"Komunikacija - multimedija",
"Consultant-Market research"=>"Svetovalec - tržne raziskave",
"Natural_spaces"=>"Naravni_prostori",
"Agriculture"=>"Kmetijstvo",
"Fishing"=>"Ribolov",
"Fishing & hunting"=>"Ribolov",
"Animals_care"=>"Skrb za živali",
"Animals care"=>"Skrb za živali",
"Hotel_business"=>"Hotel_poslovanje",
"Hotel & catering"=>"Hotel",
"Catering"=>"Catering",
"Tourism"=>"Turizem",
"Tourism & animation"=>"Turizem",
"Animation"=>"Animacija",
"Industry"=>"Industrija",
"IT-Telecommunication"=>"IT-Telekomunikacije",
"Installation-Maintenance"=>"Namestitev in vzdrževanje",
"Marketing-Strategy"=>"Strategija trženja",
"Human_Ressources"=>"Human_Ressources",
"Human ressources"=>"Človeški viri",
"Secretariat-Assistantship"=>"Sekretariat in pomoč",
"Home_services"=>"Storitve za dom",
"Personal services"=>"Osebne storitve",
"Public_services"=>"Javne storitve",
"Spectacle"=>"Očala",
"Sport"=>"Šport",
"Transport-Logistic"=>"Prevozno-logistični",
"R&D, engineering"=>"R",
"Army & security"=>"Vojska",
"Environement & natural spaces"=>"Okolje",
"Training"=>"Usposabljanje",
"Jobs offers"=>"Ponudbe delovnih mest",
"Internship"=>"Pripravništvo",
"I am looking for"=>"Iščem",
"I recruit"=>"Zaposlujem",
"Link, Url"=>"Povezava, Url",
"Tool"=>"Orodje",
"Machine"=>"Stroj",
"Software"=>"Programska oprema",
"Human ressource"=>"Človeški viri",
"Material ressource"=>"Materialni viri",
"Financial ressource"=>"Finančni viri",
"Documentation"=>"Dokumentacija",
"Geojson / UMap"=>"Geojson / UMap",
"Compost pickup"=>"Prevzem komposta",
"Video"=>"Videoposnetek",
"Shared library"=>"Skupna knjižnica",
"Recovery center"=>"Center za okrevanje",
"Trash"=>"Odpadki",
"History"=>"Zgodovina",
"To see"=>"Za ogled",
"Fun place"=>"Zabaven kraj",
"Public place"=>"Javni prostor",
"Art pieces"=>"Umetniški izdelki",
"Street art"=>"Ulična umetnost",
"Open mic"=>"Odprti mikrofon",
"Stand"=>"Stojalo",
"Other"=>"Drugo",
"Measure"=>"Ukrep",
"Todo"=>"Vse",
"Contest"=>"Natečaj",
"Festival"=>"Festival",
"Market"=>"Trg",
"Others"=>"Drugo",
"Concert"=>"Koncert",
"Exhibition"=>"Razstava",
"Get together"=>"Sestanite se skupaj",
"Meeting"=>"Srečanje",
"Competition"=>"Tekmovanje",
"Course"=>"Tečaj",
"Workshop"=>"Delavnica",
"Conference"=>"Konferenca",
"Debate"=>"Razprava",
"Film"=>"Film",
"Crowdfunding"=>"Množično financiranje",
"Fair"=>"Fair",
"Protest"=>"Protest",
"Hiking"=>"Pohodništvo",
"hiking"=>"pohodništvo",
"Tasting"=>"Degustacija",
"Second-hand store, Jumble sale"=>"Trgovina z rabljenimi predmeti, razprodaja Jumble",
"Radio Show"=>"Radijska oddaja",
"Training, Awareness"=>"Usposabljanje, ozaveščanje",
"Forum, Meeting, Trade Fair"=>"Forum, srečanje, sejem",
"Open house"=>"Dan odprtih vrat",
"Film projection"=>"Projekcija filmov",
"tasting"=>"degustacija",
"secondHandStoreJumbleSale"=>"secondHandStoreJumbleSale",
"radioShow"=>"radioShow",
"trainingAwareness"=>"usposabljanjeZavedanje",
"forumMeetingTradeFair"=>"forumMeetingTradeFair",
"openHouse"=>"odprta hiša",
"filmProjection"=>"filmProjekcija",
"NGO"=>"NEVLADNA ORGANIZACIJA",
"LocalBusiness"=>"Lokalna podjetja",
"Local Business"=>"Lokalno poslovanje",
"Group"=>"Skupina",
"Government Organization"=>"Vladna organizacija",
"the NGO"=>"nevladna organizacija",
"the localBusiness"=>"localBusiness",
"the group"=>"skupina",
"the Group"=>"Skupina",
"the government organization"=>"vladna organizacija",
"Financier"=>"Finančni",
"Partner"=>"Partner",
"Sponsor"=>"Sponzor",
"Organizor"=>"Organizator",
"President"=>"Predsednik",
"Director"=>"Direktor",
"Speaker"=>"Govornik",
"Intervener"=>"Intervenient",
"Need"=>"Potrebujete",
"Needs"=>"Potrebuje",
"need"=>"potrebujete",
"Offer"=>"Ponudba",
"offer"=>"ponudba",
"Need something"=>"Potrebujete nekaj",
"Offer something"=>"Ponudite nekaj",
"They need something"=>"Potrebujejo nekaj",
"They offer something"=>"Ponujajo nekaj",
"Type of ressource"=>"Vrsta vira",
"Choose a category"=>"Izberite kategorijo",
"Sub category"=>"Podkategorija",
"Service"=>"Storitev",
"Charity"=>"Dobrodelnost",
"Househelp"=>"Pomočnica za gospodinjstvo",
"Washing"=>"Pranje",
"Farming"=>"Kmetijstvo",
"General maintenance"=>"Splošno vzdrževanje",
"Animalcare"=>"Skrb za živali",
"Children care"=>"Skrb za otroke",
"Ederly care"=>"Ederly oskrba",
"Disabled care"=>"Skrb za invalide",
"Hosting help"=>"Pomoč pri gostovanju",
"Competence"=>"Kompetence",
"Artistic"=>"Umetniški",
"Laws"=>"Zakoni",
"IT Development"=>"Razvoj IT",
"Handcraft"=>"Rokodelstvo",
"Restauration"=>"Obnova",
"Building"=>"Stavba",
"Material"=>"Material",
"Electronic"=>"Elektronska stran",
"Primary ressources"=>"Primarni viri",
"Building tools"=>"Gradbena orodja",
"Gardening tools"=>"Orodje za vrtnarjenje",
"Medical stuff"=>"Medicinske stvari",
"Job offer"=>"Ponudba za delo",
"Are you searching an offer or you post one"=>"Iščete ponudbo ali jo objavite",
"Chat inside CO"=>"Klepet znotraj CO",
"Chat outside CO"=>"Klepet zunaj CO",
"Channel Name"=>"Ime kanala",
"Type of Chat ?"=>"Vrsta klepeta ?",
"Public channels can be searched and random people can join"=>"Po javnih kanalih lahko iščete in se jim lahko pridružijo naključni ljudje.",
"Private channels can only be viewed by it's members"=>"Zasebne kanale si lahko ogledajo samo člani",
"public or private ?"=>"javno ali zasebno?",
"Public"=>"Javna stran",
"Private"=>"Zasebno",
"Only posts"=>"Samo objave",
"Activity of communecters"=>"Dejavnost komunajzarjev",
"Surveys"=>"Ankete",
); ?>