<?php return array(
"only"=>"samo",
"Document has been deleted"=>"Dokument je bil izbrisan",
"files are allowed!"=>"datoteke so dovoljene!",
"Error! Wrong HTTP method!"=>"Napaka! Napačna metoda HTTP!",
"Something went wrong with your upload!"=>"Pri nalaganju je šlo nekaj narobe!",
"Document deleted"=>"Črtan dokument",
"Document saved successfully"=>"Dokument je bil uspešno shranjen",
"Image deleted"=>"Izbrisana slika",
"You have no rights upload document on this item, just write a message !"=>"Nimate pravice do nalaganja dokumentov o tem predmetu, samo napišite sporočilo !",
"You are not allowed to modify the document of this item !"=>"Dokumenta tega izdelka ne smete spreminjati !",
"Collection of bookmarks<br>To classified favorite urls<br>And share it to the community"=>"Zbirka zaznamkov<br>Za razvrstitev najljubših naslovov<br>in deljenje s skupnostjo",
"Collection of files<br>To download and share documents<br>With the community"=>"Zbirka datotek<br>Za prenos in deljenje dokumentov<br>S skupnostjo",
); ?>