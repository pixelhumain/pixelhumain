<?php return array(
"The job posting id is mandatory to retrieve the job posting !"=>"Id objave delovnega mesta je obvezen za pridobitev objave delovnega mesta !",
"New Job Offer"=>"Ponudba za novo službo",
"Fill the form"=>"Izpolnite obrazec",
"Job Posting"=>"objava delovnega mesta",
"Your job offer has been updated with success"=>"Vaša ponudba za zaposlitev je bila uspešno posodobljena",
"Can not insert the job : unknown field "=>"Ne morem vstaviti delovnega mesta : neznano polje",
"Problem inserting the new job offer"=>"Težava z vstavljanjem nove ponudbe za zaposlitev",
"Your job offer has been added with succes"=>"Vaša ponudba za delo je bila uspešno dodana",
"Can not update the job : you are not authorized to update that job offer !"=>"Ne morem posodobiti delovnega mesta: niste pooblaščeni za posodobitev te ponudbe za delo!",
"Error updating the Organization : address is not well formated !"=>"Napaka pri posodabljanju organizacije: naslov ni dobro oblikovan!",
"Can not remove the job : you are not authorized to update that job offer !"=>"Ne morete odstraniti delovnega mesta: niste pooblaščeni za posodobitev te ponudbe delovnih mest !",
"Your job offer has been deleted with success"=>"Vaša ponudba za zaposlitev je bila uspešno izbrisana",
); ?>