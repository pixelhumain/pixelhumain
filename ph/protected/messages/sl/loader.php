<?php return array(
"Loading"=>"Nalaganje",
"Thanks to be patient a moment"=>"Hvala, da ste potrpežljivi trenutek",
"1 little coin to contribute"=>"1 kovanec za prispevek",
"Help us to forward our work"=>"Pomagajte nam nadaljevati naše delo",
"More informations about our financial situation"=>"Več informacij o našem finančnem položaju",
"Make a donation to the NGO"=>"Donacija za nevladno organizacijo",
"1 little coin for a great puzzle"=>"1 kovanec za odlično sestavljanko",
"Contribute"=>"Prispevajte",
"Member ( 1€/month )"=>"Član ( 1€/mesec )",
"Sponsor ( 50€/month )"=>"Sponzor ( 50€/mesec )",
"Make a donation"=>"Naredite donacijo",
); ?>