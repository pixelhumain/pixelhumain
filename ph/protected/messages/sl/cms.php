<?php return array(
"Menu construction"=>"Gradbeni meni",
"Add navigation bar"=>"Dodajanje navigacijske vrstice",
"Right menu"=>"Desni meni",
"Left menu"=>"Levi meni",
"Top left menu"=>"Zgornji levi meni",
"Top right menu"=>"Zgornji desni meni",
"Menu at the bottom"=>"Meni na dnu",
"Manage existing"=>"Upravljanje obstoječih",
"Build your menu"=>"Sestavite svoj meni",
); ?>