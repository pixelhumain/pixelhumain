<?php return array(
"{what} Your application package is well submited"=>"{what} Vaša vloga je dobro oddana",
"{what} A new application package is added"=>"{kaj} Dodan je nov paket aplikacij",
"A new project has been submitted on {what}"=>"Vložen je bil nov projekt na {what}",
"{who} submits a new project"=>"{kdo} predloži nov projekt",
"As admin of this survey, the next step is to validate this submission.<br>You will find all answers for this appliancation on the following link"=>"Naslednji korak, ki ga boste naredili kot administrator te ankete, je potrditev te prijave.<br>Vse odgovore za to aplikacijo boste našli na naslednji povezavi",
"See the application package"=>"Oglejte si paket za prijavo",
"Congratulations, your project is well submitted on {what}"=>"Čestitke, vaš projekt je dobro predložen na {what}",
"Hello {who}"=>"Pozdravljeni {kdo}",
"We have succesfully received your application package. Our team will study it during the validation period until the 1rst September 2018<br/><br/>After this period of project's validation, we will send you new instructions to continue your participation to {what}"=>"Uspešno smo prejeli vašo prijavo. Naša ekipa ga bo preučila v obdobju potrjevanja do 1. septembra 2018<br/><br/>Po tem obdobju potrjevanja projekta vam bomo poslali nova navodila za nadaljevanje sodelovanja v projektu {what}",
"Thank you for your application"=>"Zahvaljujemo se vam za prijavo",
"This step {num} hasn't been filed yet"=>"Ta korak {številka} še ni bil vložen",
"Go back to this form"=>"Vrnite se na ta obrazec",
); ?>