<?php return array(
"Loading"=>"Ładowanie",
"Thanks to be patient a moment"=>"Dzięki za chwilę cierpliwości",
"1 little coin to contribute"=>"1 mała moneta na wkład własny",
"Help us to forward our work"=>"Pomóż nam rozwijać naszą pracę",
"More informations about our financial situation"=>"Więcej informacji o naszej sytuacji finansowej",
"Make a donation to the NGO"=>"Przekaż darowiznę na rzecz organizacji pozarządowej",
"1 little coin for a great puzzle"=>"1 mała moneta za wspaniałą układankę",
"Contribute"=>"Contribute",
"Member ( 1€/month )"=>"Członek ( 1€/miesiąc )",
"Sponsor ( 50€/month )"=>"Sponsor ( 50€/miesiąc )",
"Make a donation"=>"Przekaż darowiznę",
); ?>