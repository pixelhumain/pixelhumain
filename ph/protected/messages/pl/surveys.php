<?php return array(
"{what} Your application package is well submited"=>"{Twój pakiet aplikacyjny jest dobrze złożony",
"{what} A new application package is added"=>"{what} Dodawany jest nowy pakiet aplikacji",
"A new project has been submitted on {what}"=>"Nowy projekt został złożony na {what}",
"{who} submits a new project"=>"{who} zgłasza nowy projekt",
"As admin of this survey, the next step is to validate this submission.<br>You will find all answers for this appliancation on the following link"=>"Jako administrator tego badania, następnym krokiem jest zatwierdzenie tego zgłoszenia.<br>Wszystkie odpowiedzi dotyczące tej aplikacji znajdziesz na poniższym linku",
"See the application package"=>"Zobacz pakiet aplikacyjny",
"Congratulations, your project is well submitted on {what}"=>"Gratulacje, twój projekt jest dobrze przedłożony na {what}",
"Hello {who}"=>"Witam {who}",
"We have succesfully received your application package. Our team will study it during the validation period until the 1rst September 2018<br/><br/>After this period of project's validation, we will send you new instructions to continue your participation to {what}"=>"Pomyślnie otrzymaliśmy Twój pakiet aplikacyjny. Nasz zespół przeanalizuje go w okresie walidacji do 1 września 2018 r.<br/><br/>Po tym okresie walidacji projektu prześlemy Ci nowe instrukcje, abyś mógł kontynuować swój udział w projekcie",
"Thank you for your application"=>"Dziękujemy za zgłoszenie",
"This step {num} hasn't been filed yet"=>"Ten krok {num} nie został jeszcze zgłoszony",
"Go back to this form"=>"Wróć do tego formularza",
); ?>