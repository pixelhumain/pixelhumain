<?php return array(
"Menu construction"=>"Menu budowy",
"Add navigation bar"=>"Dodaj pasek nawigacyjny",
"Right menu"=>"Prawe menu",
"Left menu"=>"Lewe menu",
"Top left menu"=>"Lewe górne menu",
"Top right menu"=>"Prawe górne menu",
"Menu at the bottom"=>"Menu na dole",
"Manage existing"=>"Zarządzaj istniejącymi",
"Build your menu"=>"Zbuduj swoje menu",
); ?>