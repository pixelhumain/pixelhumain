<?php return array(
"You need to set a upload directory and URL.<br/>Check the documentation."=>"Musisz ustawić katalog przesyłania i adres URL.<br/>Sprawdź dokumentację.",
"Set the user Id to use this type of storage"=>"Ustawienie identyfikatora użytkownika, który ma korzystać z tego typu pamięci masowej",
"Set the CKEditor container to initialize this widget"=>"Ustaw kontener CKEditor, aby zainicjować ten widget",
); ?>