<?php return array(
"only"=>"tylko",
"Document has been deleted"=>"Dokument został usunięty",
"files are allowed!"=>"pliki są dozwolone!",
"Error! Wrong HTTP method!"=>"Błąd! Nieprawidłowa metoda HTTP!",
"Something went wrong with your upload!"=>"Coś poszło nie tak przy wysyłaniu!",
"Document deleted"=>"Dokument usunięty",
"Document saved successfully"=>"Dokument został zapisany pomyślnie",
"Image deleted"=>"Obraz usunięty",
"You have no rights upload document on this item, just write a message !"=>"Nie masz prawa przesyłać dokumentów na ten temat, po prostu napisz wiadomość !",
"You are not allowed to modify the document of this item !"=>"Nie wolno modyfikować dokumentu tej pozycji !",
"Collection of bookmarks<br>To classified favorite urls<br>And share it to the community"=>"Kolekcja zakładek<br>Do klasyfikowania ulubionych stron<br>I udostępniania ich społeczności",
"Collection of files<br>To download and share documents<br>With the community"=>"Zbiór plików<br>Do pobierania i udostępniania dokumentów<br>Społeczności",
); ?>