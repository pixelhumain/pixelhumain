<?php return array(
"Our circuits"=>"Våra kretsar",
"Result for"=>"Resultat för",
"Any destination"=>"Vilken destination som helst",
"Number of travellers"=>"Antal resenärer",
"You travel"=>"Du reser",
"Date of travel"=>"Datum för resan",
"Adapted time"=>"Anpassad tid",
"Show"=>"Visa",
"Portal administor"=>"Portaladministratör",
"My Dashboard"=>"Min instrumentpanel",
"Contact us"=>"Kontakta oss",
"E-mail *"=>"E-post *",
"Name"=>"Namn",
"Your situation"=>"Din situation",
"Object"=>"Objekt",
"Your message"=>"Ditt meddelande",
"Back"=>"Tillbaka",
"what's your name ?"=>"Vad heter du?",
"your mail address : exemple@mail.com"=>"Din e-postadress: exemple@mail.com",
"tourist or professional ?"=>"Turist eller yrkesman?",
"what's about ?"=>"vad handlar det om?",
"SEND"=>"SKICKA",
"security"=>"säkerhet",
"thanks to copy the code below to be able to send your message"=>"Tack för att du kopierar koden nedan för att kunna skicka ditt meddelande.",
"copy the code here"=>"kopiera koden här",
"Security code is not correct"=>"Säkerhetskoden är inte korrekt",
"Your message has been sent."=>"Ditt meddelande har skickats.",
"Back to home page"=>"Tillbaka till hemsidan",
"Thanks for your message"=>"Tack för ditt meddelande",
"Error, your message has not been sent"=>"Fel, ditt meddelande har inte skickats",
"We are sorry"=>"Vi är ledsna",
); ?>