<?php return array(
"The job posting id is mandatory to retrieve the job posting !"=>"Id-numret är obligatoriskt för att hämta jobbannonsen !",
"New Job Offer"=>"Nytt jobberbjudande",
"Fill the form"=>"Fyll i formuläret",
"Job Posting"=>"jobbannonsering",
"Your job offer has been updated with success"=>"Ditt jobberbjudande har uppdaterats med framgång",
"Can not insert the job : unknown field "=>"Kan inte lägga in jobbet: okänt fält",
"Problem inserting the new job offer"=>"Problem med att infoga det nya jobberbjudandet",
"Your job offer has been added with succes"=>"Ditt jobberbjudande har lagts till med framgång",
"Can not update the job : you are not authorized to update that job offer !"=>"Kan inte uppdatera jobbet : du har inte rätt att uppdatera det jobberbjudandet !",
"Error updating the Organization : address is not well formated !"=>"Fel i uppdateringen av organisationen: adressen är inte välformulerad !",
"Can not remove the job : you are not authorized to update that job offer !"=>"Du kan inte ta bort jobbet : du har inte rätt att uppdatera jobberbjudandet!",
"Your job offer has been deleted with success"=>"Ditt jobberbjudande har tagits bort med framgång",
); ?>