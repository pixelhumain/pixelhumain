<?php return array(
"only"=>"endast",
"Document has been deleted"=>"Dokumentet har raderats",
"files are allowed!"=>"filer är tillåtna!",
"Error! Wrong HTTP method!"=>"Fel! Fel HTTP-metod!",
"Something went wrong with your upload!"=>"Något gick fel med din uppladdning!",
"Document deleted"=>"Dokumentet raderat",
"Document saved successfully"=>"Dokumentet har sparats framgångsrikt",
"Image deleted"=>"Bild raderad",
"You have no rights upload document on this item, just write a message !"=>"Du har inga rättigheter att ladda upp dokument om detta objekt, skriv bara ett meddelande!",
"You are not allowed to modify the document of this item !"=>"Det är inte tillåtet att ändra dokumentet för detta objekt!",
"Collection of bookmarks<br>To classified favorite urls<br>And share it to the community"=>"Samling av bokmärken<br>För att klassificera favoriturls<br>Och dela dem med gemenskapen",
"Collection of files<br>To download and share documents<br>With the community"=>"Samling av filer<br>För att ladda ner och dela dokument<br>Med gemenskapen",
); ?>