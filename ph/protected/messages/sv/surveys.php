<?php return array(
"{what} Your application package is well submited"=>"{vad} Ditt ansökningspaket är väl skickat",
"{what} A new application package is added"=>"{vad} Ett nytt ansökningspaket läggs till",
"A new project has been submitted on {what}"=>"Ett nytt projekt har lämnats in för {vad}",
"{who} submits a new project"=>"{who} lämnar in ett nytt projekt",
"As admin of this survey, the next step is to validate this submission.<br>You will find all answers for this appliancation on the following link"=>"Som administratör av denna undersökning är nästa steg att validera denna ansökan.<br>Du hittar alla svar för denna ansökan på följande länk",
"See the application package"=>"Se ansökningspaketet",
"Congratulations, your project is well submitted on {what}"=>"Grattis, ditt projekt är väl inlämnat på {what}",
"Hello {who}"=>"Hallå {vem}",
"We have succesfully received your application package. Our team will study it during the validation period until the 1rst September 2018<br/><br/>After this period of project's validation, we will send you new instructions to continue your participation to {what}"=>"Vi har mottagit ditt ansökningsformulär. Vårt team kommer att studera den under valideringsperioden fram till den 1 september 2018<br/><br/>Efter denna period av projektvalidering kommer vi att skicka dig nya instruktioner för att fortsätta ditt deltagande i {what}",
"Thank you for your application"=>"Tack för din ansökan",
"This step {num} hasn't been filed yet"=>"Detta steg {num} har inte lämnats in ännu",
"Go back to this form"=>"Gå tillbaka till detta formulär",
); ?>