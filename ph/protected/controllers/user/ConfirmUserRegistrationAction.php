<?php

namespace PixelHumain\PixelHumain\controllers\user;
use CAction;
use Rest;
use Yii;

class ConfirmUserRegistrationAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($email,$app)
    {
        //TODO : add a test adminUser
        //isAppAdminUser
        $user = Yii::app()->mongodb->citoyens->findAndModify( array("email" => $email),
                                                              array('$set' => array("applications.".$app.".registrationConfirmed"=>true) ) );
        $user = Yii::app()->mongodb->citoyens->findOne( array( "email" => $email ) );
        return Rest::json( $user );
    }
}