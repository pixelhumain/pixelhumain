<?php

namespace PixelHumain\PixelHumain\controllers\user;
use CAction;
use Rest;
use Yii;

/**
 * [actionGetWatcher get the user data based on his id]
 * @param  [string] $email   email connected to the citizen account
 * @return [type] [description]
 */
class GetUserAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($email)
    {
        $user = Yii::app()->mongodb->citoyens->findOne( array( "email" => $email ) );
        return Rest::json( $user );
    }
}