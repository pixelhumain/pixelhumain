<?php

namespace PixelHumain\PixelHumain\controllers\user;

use CAction;
use MongoId;
use Rest;
use Yii;

class GetActionIncrementationValueAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $element = Yii::app()->mongodb->selectCollection($_POST['collection'])->findOne( array("_id" => new MongoId( $_POST['id'] ) ),array($_POST['action']));
        return Rest::json( $element );
    }
}