<?php
/*
Gets all users corresponding to a certain request
- set the where clause according to POST parameters
- a fields can be added to the selection clause, in order to retreive only certain fields from DB
 */

namespace PixelHumain\PixelHumain\controllers\user;

use Citoyen;
use Rest;
use Yii;

class GetPeopleByAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $res = Citoyen::getPeopleBy($_POST);
        return Rest::json( $res );
    }
}