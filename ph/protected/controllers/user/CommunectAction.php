<?php

namespace PixelHumain\PixelHumain\controllers\user;
use CAction;
use Citoyen;
use Rest;
use Yii;

class CommunectAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $res = Citoyen::communect( $_POST["email"] , $_POST["cp"] );
        return Rest::json($res);
    }
}