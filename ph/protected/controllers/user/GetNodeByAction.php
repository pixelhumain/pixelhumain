<?php

namespace PixelHumain\PixelHumain\controllers\user;

use CAction;
use MongoId;
use Rest;
use Yii;

class GetNodeByAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($type, $count=null)
    {
        if(isset(Yii::app()->session["userId"]))
        {
	        $user = Yii::app()->mongodb->citoyens->findOne( array("_id" => new MongoId( Yii::app()->session["userId"] ) ));
	        if(isset($user[$type]))
	        {
		        if(!$count)
		            $res = array($type => $user[$type]);
		        else
		            $res = array('count' => count ( $user[$type] ));
		    } else
		    	$res = array('result' => false,"msg"=>"ThisTypeDoesntExistFortheUser");
	    } else
	    	$res = array('result' => false,"msg"=>"noUserLogguedIn");
        return Rest::json( $res );
    }
}