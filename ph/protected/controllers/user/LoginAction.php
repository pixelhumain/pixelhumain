<?php

namespace PixelHumain\PixelHumain\controllers\user;
use CAction;
use Citoyen;
use Rest;
use Yii;

class LoginAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $email = $_POST["email"];
        $loginRegister = (isset($_POST["loginRegister"]) && $_POST["loginRegister"] ) ? true : null ; 
        $res = Citoyen::login( $email , $_POST["pwd"], $loginRegister); 
        if( isset( $_POST["app"] ) )
			$res = array_merge($res, Citoyen::applicationRegistered($_POST["app"],$email));

        return Rest::json($res);
    }
}