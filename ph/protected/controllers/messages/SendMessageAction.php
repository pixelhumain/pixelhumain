<?php
class SendMessageAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        if( isset( $_POST['email'] ) && isset( $_POST['msg'] ) )
        {
        	$app = (isset($_POST['app'])) ? $_POST['app'] : null;
            $res = Message::createMessage($_POST['email']  , $_POST['msg'], $app );
        } else
            $res = array('result' => false , 'msg'=>'something somewhere went terribly wrong');
        return Rest::json($res);
    }
}