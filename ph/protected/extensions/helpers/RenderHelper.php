<?php 
class RenderHelper {

    public static function convertPathYii1ToYii2($path,$alias='@modules') {
    	if (isset($path)) {
    		$path = str_replace('.', '/', $path);
            return $alias.'/'.$path;
    	}
    }

}

