<?php 
class HtmlHelper {
    public static function random_color(){
        mt_srand((double)microtime()*1000000);
        $c = '';
        while(strlen($c)<6){
            $c .= sprintf("%02X", mt_rand(0, 255));
        }
        return $c;
    }

    public static function echoIfSetOr($var , $default = null) {
    	if (isset($var)) {
    		echo $var;
    	} elseif (isset($default)) {
    		echo $default;
    	}
    }

    /**
     * Register using yii methode the css and javascript files.
     * The helper will choose the way to register the file depending on :
     * 1. It's an ajax request or not
     * 2. It's a css or a javascript file
     * @param array $files an array with the file path of the css and js to register. 
     * The paths must be relative from the baseUrl. Ex : '/assets/plugins/bootstrap-datepicker/css/datepicker.css'
     * @return true if everything done right
     */
    public static function registerCssAndScriptsFiles($files,$path=null) {

        Yii::app()->params["version"] = Yii::app()->params["versionAssets"] ? Yii::app()->params["versionAssets"] : Yii::app()->params["version"];

        // $cs = Yii::app()->getClientScript();
        if($path == null)
            $path = Yii::app()->request->baseUrl;
        $ajaxRequest = Yii::app()->request->isAjaxRequest;
        if(!empty(Yii::app()->params["overWrite"]))
            $pathOverwrite = Yii::app()->getModule(Yii::app()->theme->name)->getAssetsUrl()."/";

        foreach ($files as $file) {
            $extention = pathinfo($file,PATHINFO_EXTENSION);
            if ($extention == "js" || $extention == "JS") {
                if(Yii::app()->params["gulp"]){
                    $fileVersion = $file;
                } else {
                    $fileVersion = $file."?v=".Yii::app()->params["version"] ;
                }
                self::scriptFile($ajaxRequest, $path.$fileVersion);
                if( !empty($pathOverwrite) && in_array(substr($file, 1), Yii::app()->params["overWrite"]["assets"])  ) {
                    $file = substr($file, 1)."?v=".Yii::app()->params["version"] ;
                    //$file = substr($file, 1);
                    self::scriptFile($ajaxRequest, $file, $pathOverwrite.$file);
                }

            } else if ($extention == "css" || $extention == "CSS") {
                if(Yii::app()->params["gulp"]){
                    $fileVersion = $file;
                } else {
                    $fileVersion = $file."?v=".Yii::app()->params["version"] ;
                }
                self::cssFile($ajaxRequest, $path.$fileVersion);
                if( !empty($pathOverwrite) && in_array(substr($file, 1), Yii::app()->params["overWrite"]["assets"])  ) {
                    $file = substr($file, 1)."?v=".Yii::app()->params["version"];
                    self::cssFile($ajaxRequest, $pathOverwrite.$file);
                }
            } else {
                
                //unknown extension
                throw new InvalidArgumentException("unkonw file extension : ".$extention);
            }
        }
        
        return true;
       
    }


    public static function scriptFile($ajaxRequest, $path) {
        $cs = Yii::app()->getClientScript();
        Yii::app()->params["version"] = Yii::app()->params["versionAssets"] ? Yii::app()->params["versionAssets"] : Yii::app()->params["version"];
         if($ajaxRequest){
            //var_dump($path);
            $parsePath = parse_url($path);
            $pathVersion = $path."?v=".Yii::app()->params["version"];
            if(isset(Yii::app()->params["gulp"]) && isset(Yii::app()->params["assetsUrl"]["jsUrl"]) and !isset($parsePath["scheme"])){
                echo CHtml::scriptFile(Yii::app()->params["assetsUrl"]["jsUrl"].$pathVersion);
            } else {
                echo CHtml::scriptFile($pathVersion);
            }
        } else {
            $cs->registerScriptFile($path, CClientScript::POS_END, array(), 2);
            $parsePath = parse_url($path);
            //var_dump(parse_url($path));
            if(Yii::app()->params["gulp"]){
            if(isset(Yii::app()->params["assetsUrl"]["jsUrl"]) and !isset($parsePath["scheme"])){
                Yii::app()->clientScript->scriptMap['/'.basename($path)] = Yii::app()->params["assetsUrl"]["jsUrl"].$path.'?v='.Yii::app()->params["version"];
                Yii::$app->assetManager->assetMap['/'.basename($path)] = Yii::app()->params["assetsUrl"]["jsUrl"].$path.'?v='.Yii::app()->params["version"];
            } else {
                Yii::app()->clientScript->scriptMap['/'.basename($path)] = $path.'?v='.Yii::app()->params["version"];
                Yii::$app->assetManager->assetMap['/'.basename($path)] = $path.'?v='.Yii::app()->params["version"];
            }
            }
            
        }
    }

    public static function cssFile($ajaxRequest, $path) {
        Yii::app()->params["version"] = Yii::app()->params["versionAssets"] ? Yii::app()->params["versionAssets"] : Yii::app()->params["version"];
        $cs = Yii::app()->getClientScript();
        if($ajaxRequest){
            $parsePath = parse_url($path);
            $pathVersion = $path."?v=".Yii::app()->params["version"];
            if(isset(Yii::app()->params["gulp"]) && isset(Yii::app()->params["assetsUrl"]["cssUrl"]) and !isset($parsePath["scheme"])){
                echo CHtml::cssFile(Yii::app()->params["assetsUrl"]["cssUrl"].$pathVersion);
            } else {
                echo CHtml::cssFile($pathVersion);
            }
        } else {
            $cs->registerCssFile($path);
            $parsePath = parse_url($path);
            //var_dump(parse_url($path));
            if(Yii::app()->params["gulp"]){
                if(isset(Yii::app()->params["assetsUrl"]["cssUrl"]) and !isset($parsePath["scheme"])){
                Yii::app()->clientScript->scriptMap['/'.basename($path)] = Yii::app()->params["assetsUrl"]["cssUrl"].$path.'?v='.Yii::app()->params["version"];
                Yii::$app->assetManager->assetMap['/'.basename($path)] = Yii::app()->params["assetsUrl"]["cssUrl"].$path.'?v='.Yii::app()->params["version"];
            } else {
                Yii::app()->clientScript->scriptMap['/'.basename($path)] = $path.'?v='.Yii::app()->params["version"];
                Yii::$app->assetManager->assetMap['/'.basename($path)] = $path.'?v='.Yii::app()->params["version"];
                }
            }
        }
    }

    public static function bundle($fileName)
    {
        Yii::app()->params["version"] = Yii::app()->params["versionAssets"] ? Yii::app()->params["versionAssets"] : Yii::app()->params["version"];
        if (!Yii::app()->params["gulp"] || !$fileName) {
            return;
        }
        $layerFile = "../../layerfile.json";
        if (!file_exists(realpath($layerFile))) {
            return;
        }
        $layerFileJSON = file_get_contents($layerFile, FILE_USE_INCLUDE_PATH);
        $layerFileJSON = json_decode($layerFileJSON, true);
        $keyLayer = array_search($fileName, array_column($layerFileJSON["layers"], 'layerName'));
        if ($keyLayer === false) {
            return;
        }
        $cssAnsScriptFilesModuleAll = array_merge($layerFileJSON["layers"][$keyLayer]["js"], $layerFileJSON["layers"][$keyLayer]["css"]);
        $layerNameOut = $layerFileJSON["layers"][$keyLayer]["layerNameOut"];
        $version = Yii::app()->params["version"];
        $jsUrl = isset(Yii::app()->params["assetsUrl"]["jsUrl"]) ? Yii::app()->params["assetsUrl"]["jsUrl"] : Yii::app()->baseUrl;
        $cssUrl = isset(Yii::app()->params["assetsUrl"]["cssUrl"]) ? Yii::app()->params["assetsUrl"]["cssUrl"] : Yii::app()->baseUrl;
        $urls = [
            'js' => $jsUrl . '/web/js/' . $layerNameOut . '.all.min.js?v=' . $version,
            'css' => $cssUrl . '/web/css/' . $layerNameOut . '.all.min.css?v=' . $version,
        ];
        $assetManager = Yii::$app->assetManager;
        $clientScript = Yii::app()->clientScript;
        foreach ($cssAnsScriptFilesModuleAll as $file) {
            $extention = strtolower(pathinfo($file, PATHINFO_EXTENSION));
            $assetManager->assetMap['/'.basename($file)] = $urls[$extention];
            $clientScript->scriptMap['/'.basename($file)] = $urls[$extention];
        }
    }

    public static function assetsUrlImg($image){
        if($image){
            if(isset(Yii::app()->params["assetsUrl"]["imgUrl"])){
                $url = Yii::app()->params["assetsUrl"]["imgUrl"].$image;
            } else {
                $url = Yii::app()->createUrl('/'.$image) ;
            }
        } else {
            $url = '';
        }
        return $url;
        }


    // public static function registerCssAndScriptsFilesOverwrite() {
    //     if(!empty(Yii::app()->params["overWrite"])){
    //         $cs = Yii::app()->getClientScript();
    //         $path = Yii::app()->getModule('terla')->getAssetsUrl()."/";
    //         $ajaxRequest = Yii::app()->request->isAjaxRequest;
    //         foreach (Yii::app()->params["overWrite"]["assets"] as $key => $file) {
                
    //             $extention = pathinfo($file,PATHINFO_EXTENSION);
    //             if ($extention == "js" || $extention == "JS") {
    //                 $file = $file."?v=".Yii::app()->params["version"] ;
    //                 if($ajaxRequest){
    //                     echo CHtml::scriptFile($path.$file);
    //                 } else {
    //                     $cs->registerScriptFile($path. $file , CClientScript::POS_END, array(), 2);
    //                 }
    //             }
    //             else if ($extention == "css" || $extention == "CSS") {
    //                 if(file_exists ( $path.$file ) ) {
    //                     if($ajaxRequest){
    //                         echo CHtml::cssFile($path.$file);
    //                     } else {
    //                         $cs->registerCssFile($path.$file);
    //                     }
    //                 }
    //             } else {
    //                 throw new InvalidArgumentException("unkonw file extension : ".$extention);
    //             }
    //         } 
    //     }
    //     //exit;
    //     return true;
    // }
}
?>