<?php
class CacheHelper {
    /* 
    get : retrieve the value with a key (if any) from cache*
    set : store the value with a key into cache
    add : store the value only if cache does not have this key
    delete : delete the value with the specified key from cache
    flush : delete all values from cache 
    */
    public static function isInitialized() {
        if(Yii::app()->cache){
            return true;
        } else {
            return false;
        }
    }

    public static function get($id) {
        if(self::isInitialized()){
            $value=Yii::app()->cache->get($id);
            return $value;
        } else {
            return false;
        }
    }

    public static function set($id,$value) {
        if(self::isInitialized()){
            //var_dump($id);
            return Yii::app()->cache->set($id,$value);
        } else {
            return $value;
        }
    }

    public static function delete($id) {
        if(self::isInitialized()){
            return Yii::app()->cache->delete($id);
        } else {
            return true;
        }
    }

    public static function flush() {
        if(self::isInitialized()){
            return Yii::app()->cache->flush();
        } else {
            return true;
        }
    }

    public static function getCostum($id=null,$type=null,$slug=null){

      //  $elParams = ["shortDescription", "profilImageUrl", "profilThumbImageUrl", "profilMediumImageUrl","profilBannerUrl", "name", "tags", "description","costum", "links", "profilRealBannerUrl","slug"];
        

        if(@$_GET["slug"] && !@$_POST["costumSlug"]){
            $slug = $_GET["slug"];
        } else if(@$_POST["costumSlug"]){
            $slug = $_POST["costumSlug"];
        } else if(!empty($slug)){
            $slug = $slug ;

        } else if((isset($type) || isset($_POST["costumType"])) && (isset($id) || isset($_POST["costumId"]))){
            $type = ($type || @$_POST["costumType"]);
            $id = ($id || $_POST["costumId"]);
        }else {
            $id = (!empty($id)) ? $id : @$_GET["id"];
        }

        if(isset($slug)){
            $id = $slug;
        }
        /*if(isset($slug)){
            $el = Slug::getElementBySlug($slug, $elParams );
            $tmp['contextType'] = $el["type"];
            $tmp['contextId'] = $el["id"];
            if(@$el["el"]["costum"]){
                $id = (@$el["el"]["costum"]['id']) ? $el["el"]["costum"]['id'] : $el["el"]["costum"]['slug'];
            }
            else {
                $id = "cocampagne"; 
            }

        } else if(isset($type) && isset($id)){
            $el = array("el"=>Element::getByTypeAndId($type, $id, $elParams ));
            $tmp['contextType'] = $type;
            $tmp['contextId'] = $id;
            if(@$el["el"]["costum"]){
                $id = (@$el["el"]["costum"]['id']) ? $el["el"]["costum"]['id'] : $el["el"]["costum"]['slug'];
            }
            
        }*/

        if(!empty($id)){
   
            
        }
        else if(@$_GET["host"]){
            //$id = $_GET["host"];
            $id = Costum::getSlugByHost($_GET["host"]);
        }else if(@$_POST["costumSlug"]){
            $id = $_POST["costumSlug"];
        }
        $cacheCostum=self::get($id);
        if(@$_POST["costumEditMode"])
            $cacheCostum["editMode"]=$_POST["costumEditMode"];
        return $cacheCostum;
    }
}
