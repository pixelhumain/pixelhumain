<?php

namespace PixelHumain\PixelHumain\components;

class Action extends \yii\base\Action
{
    public function getController()
    {
        return $this->controller;
    }
}