<?php


namespace PixelHumain\PixelHumain\components;


use Yii;
use yii\base\ViewContextInterface;

class View extends \ahmadasjad\yii2PlusYii1\View
{
    private $sideBar1;

    public function renderPartial($view, $params=[], $context=''){
        if (!$context instanceof ViewContextInterface) {
            $context = $this->context;
        }
        return $this->render($view, $params, $context);
    }


    public function findViewFile($view, $context = null): string {
        $file = parent::findViewFile($view, $context);
        if(!file_exists($file) && strpos($view,'.')){
            $altFile = Yii::getPathOfAlias($view);
            if (file_exists($altFile.'.php')){
                $file = $altFile.'.php';
            }
        }

        return $file;
    }

    public function setSidebar1($value) {
        $this->sideBar1 = $value;
    }

    public function getSidebar1() {
        return $this->sideBar1;
    }

    public function setCostum($value)
    {
        $this->context->costum = $value;
    }

    public function getCostum()
    {
        return $this->context->costum;
    }

    public function setAppConfig($value)
    {
        $this->context->appConfig = $value;
    }

    public function getAppConfig()
    {
        return $this->context->appConfig;
    }

    public function setModule($value)
    {
        $this->context->module = $value;
    }

    public function getModule()
    {
//        var_dump($this->context);
//        die();
        return $this->context->module;
    }
}