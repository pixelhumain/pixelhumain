<?php

namespace PixelHumain\PixelHumain\components\api\controllers;
use CAction;
use Yii;

class IndexAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $controller=$this->getController();
        //echo Yii::app()->params["modulePath"].Yii::app()->controller->module->id.'.views.api.';
	return	$controller->render("application.components.api.views.index", 
        	array( "path" => Yii::app()->params["modulePath"].Yii::app()->controller->module->id.'.views.api.') );
    }
}