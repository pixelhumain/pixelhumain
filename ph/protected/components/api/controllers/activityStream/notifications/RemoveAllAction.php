<?php

namespace PixelHumain\PixelHumain\components\api\controllers\activityStream\notifications;
use ActivityStream;
use CAction;
use Rest;
use Yii;

/**
 * a notification has been read by a user
 * remove it's entry in the notify node on an activity Stream for the current user
 * @return [json]
 */
class RemoveAllAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $res = array();
        if( Yii::app()->session["userId"] )
        {
            $res = ActivityStream::removeNotificationsByUser();
        } else
            $res = array('result' => false , 'msg'=>'something somewhere went terribly wrong');
            
        return Rest::json($res);
    }
}