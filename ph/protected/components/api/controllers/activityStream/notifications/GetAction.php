<?php

namespace PixelHumain\PixelHumain\components\api\controllers\activityStream\notifications;
use ActivityStream;
use CAction;
use Rest;
use Yii;

/**
 * a notification has been read by a user
 * remove it's entry in the notify node on an activity Stream for the current user
 * @return [json] 
 */
class GetAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $res = array();
        if( Yii::app()->session["userId"] )
        {
        	$params = array("notify.id"=>Yii::app()->session["userId"]);
            /*if( isset($_GET["ts"])) 
            	$params["timestamp"] = array('$gt'=>(int)$_GET["ts"]);*/

            $res = ActivityStream::getNotifications($params);
        } else
            $res = array('result' => false , 'msg'=>'something somewhere went terribly wrong');
            
        return Rest::json($res);
    }
}