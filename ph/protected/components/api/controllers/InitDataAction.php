<?php
/*
Initialising data for a module
will import all needed datasets to get a module working properly 
 */

namespace PixelHumain\PixelHumain\components\api\controllers;
use Admin;
use Rest;
use Yii;

class InitDataAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
    	$res = Admin::initModuleData( Yii::app()->controller->module->id );
    	return Rest::json( $res );
    }
}