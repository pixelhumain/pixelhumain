<?php


namespace PixelHumain\PixelHumain\components;

use Yii;

class UrlManager extends \yii\web\UrlManager
{
public function parseRequest($request) {
    $url = Yii::$app->request->url;
    $route = $request->getQueryParam($this->routeParam, '');
    /*var_dump($_SERVER);
    var_dump($_REQUEST);
    var_dump($url);
    var_dump($route);
    var_dump($request);
    var_dump(parse_url($url));
    exit;*/
    // si dans $url il y a index.php?r=
    if ($pos = strpos($url, 'index.php?r=') == true) {
    if (is_array($route)) {
        $route = '';
    }

    return [(string) $route, []];
    }

    $arrayParse = parse_url($url);
    // si r est vide ou si $url est différent de !== '/' parse request normalement
    if (empty($_REQUEST['r']) || $arrayParse['path'] !== '/') {
        return parent::parseRequest($request);
    }

    Yii::trace('Pretty URL not enabled. Using default URL parsing logic.', __METHOD__);
    if (is_array($route)) {
        $route = '';
    }

    return [(string) $route, []];
}
}