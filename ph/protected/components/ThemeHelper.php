<?php


namespace PixelHumain\PixelHumain\components;


use Yii;

class ThemeHelper
{
    const CO2 = 'CO2';
    const PH_DORI = 'ph-dori';
    const NETWORK = 'network';
    const EMPTY = 'empty';
    const JUNTOS = 'juntos';

    const THEME_CONFIG = [
        self::CO2 => [
            'basePath' => '@app/themes/CO2',
            'baseUrl' => '@web/themes/CO2',
            'pathMap' => [
                '@app/views' => '@themes/CO2/views',
                '@modules/co2/views' => '@themes/CO2/views',
            ],
        ],
        self::PH_DORI => [
            'basePath' => '@app/themes/ph-dori',
            'baseUrl' => '@web/themes/ph-dori',
            'pathMap' => [
                '@app/views' => '@themes/ph-dori/views',
                '@modules/ph-dori/views' => '@themes/ph-dori/views',
            ],
        ],
        self::NETWORK => [
            'basePath' => '@app/themes/network',
            'baseUrl' => '@web/themes/network',
            'pathMap' => [
                '@app/views' => '@themes/network/views',
                '@modules/network/views' => '@themes/network/views',
            ],
        ],
        self::EMPTY => [
            'basePath' => '@app/themes/empty',
            'baseUrl' => '@web/themes/empty',
            'pathMap' => [
                '@app/views' => '@themes/empty/views',
                '@modules/empty/views' => '@themes/empty/views',
            ],
        ],
    ];

    public static function setWebsiteTheme(string $theme_name)
    {
        Yii::app()->setTheme($theme_name);
        $theme_config = self::THEME_CONFIG[$theme_name];
        $theme_config = array_merge(['class' => 'yii\base\Theme'], $theme_config);
        Yii::$app->getView()->theme = Yii::createObject($theme_config);
    }
}