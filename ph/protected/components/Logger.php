<?php


namespace PixelHumain\PixelHumain\components;

use yii\base\UnknownMethodException;
use yii\log\Logger as BaseLogger;
use CLogger;

class Logger extends BaseLogger
{
    private $old;

    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    private function getOldLogger()
    {
        if (!$this->old) {
            $this->old = new CLogger();
        }
        return $this->old;
    }

    public function __call($name, $params)
    {
        try {
            return parent::__call($name, $params);
        } catch (UnknownMethodException $e) {
            return call_user_func_array([$this->getOldLogger(), $name], $params);
        }
    }

    public static function __callStatic( string $name , array $arguments ) {
        try {
            return parent::__callStatic($name, $arguments);
        } catch (UnknownMethodException $e) {
            return call_user_func_array([CLogger::class, $name], $arguments);
        }
    }
}
