<?php

Yii::import("parsedown.Parsedown", true);
$Parsedown = new Parsedown();

?>
<style type="text/css">
	
h1 {
	font-size: 16px;
}

.blue{
	color : #195391;
}

.lightgreen{
	color : #a9ce3f;
}

.darkgreen{
	color : #4db88c;
}

.body { 
	font-family: Arial,Helvetica Neue,Helvetica,sans-serif; 
}

/*table.first {
    color: #003300;
    font-family: helvetica;
    border: 3px solid black;
}

table.first th {
    border: 1px solid black;
    font-size: 8pt;
}

table.first td .entete {
     font-size: 10pt;
     font-weight: bold;
}*/

table{
   border: 3px solid black; 
}

table th{
   border: 1px solid black;
   text-align : center;
   padding: 10px;
   background-color : #f3f1f1;
}

table td{
   border: 1px solid black;
}

</style>

<?php
if (!empty($answer["answers"]["action"]["project"]["0"]["name"])){
    $nameact = $answer["answers"]["action"]["project"]["0"]["name"];
}else if (!empty($answer["answers"]["action"]["project"]["0"]["id"]) && !empty($answer["answers"]["action"]["project"]["0"]["type"])){
    $actionel = Element::getByTypeAndId($answer["answers"]["action"]["project"]["0"]["type"], $answer["answers"]["action"]["project"]["0"]["id"]);
    if (!empty($actionel["name"])){
        $nameact = $actionel["name"];
    }
}else{
    $nameact = "";
}


?>

<div class="body">
	<span style="text-align: center;">
		<span class="darkgreen">Action :</span>
		<span class="lightgreen"><?php echo $nameact; ?>
		</span>
	</span>

	<?php
	$color1 = "#E63458";
	$wizardUid = "wizardForm";
    $i = 0;
    if(!empty($forms)){
        foreach ( $forms as $keyL => $form){
            // if($i < 1){
            //     $params =[ 
            //         "parentForm"=>$parentForm,
            //         "formId" => $formId,
            //         "form" => $subForm,
            //         "answer" => $answer,
            //         "mode" => $mode,
            //         "canEdit" => $canEdit,
            //         "canEditForm" => $canEditForm,
            //         "el" => $el 
            //     ];

            //     echo $this->renderPartial("survey.views.tpls.forms.formbuilder",$params ,true );

            // }
            // $i++;
 // sommomForm1|sommomForm2|sommomForm3|sommomForm4|sommomForm5


            if( !empty($form["inputs"])  ){
                foreach ( $form["inputs"] as $key => $input) { 
                    $editQuestionBtn = "";
                    $tpl = $input["type"];
                    if(in_array($tpl, ["textarea","markdown","wysiwyg"]))
                        $tpl = "tpls.forms.textarea";
                    else if(empty($tpl) || in_array($tpl, ["text","button","color","date","datetime-local","email","image","month","number","radio","range","tel","time","url","week","tags"]))
                        $tpl = "tpls.forms.text";
                    else if(in_array($tpl, ["sectionTitle"]))
                        $tpl = "tpls.forms.".$tpl;

                    if( stripos( $tpl , "tpls.forms." ) !== false )
                        $tplT = explode(".", $input["type"]);

                    $kunikT = explode( ".", $input["type"]);
                    $keyTpl = ( count($kunikT)>1 ) ? $kunikT[ count($kunikT)-1 ] : $input["type"];
                    $kunik = $keyTpl.$key;
                    $answers = null;
                    $answerPath = "answers.".$key.".";
                    $answerPath = "answers.".$form["id"].".".$key.".";
                    if( isset($answer["answers"][$form["id"]][$key]) && count($answer["answers"][$form["id"]][$key])>0 )
                                $answers = $answer["answers"][$form["id"]][$key];
                    if(isset($answer["answers"][$key]))
                                $answers = $answer["answers"][$key];

                    $p = [ 
                        "input"     => $input,
                        "type"      => $input["type"], 
                        "answerPath" => $answerPath,
                        "answer"        => $answer,
                        "answers"   => $answers ,//sub answers for this input
                        "label"     => $input["label"] ,//$ct." - ".$input["label"] ,
                        "titleColor"=> "#16A9B1",
                        "info"      => isset($input["info"]) ? $input["info"] : "" ,
                        "placeholder" => isset($input["placeholder"]) ? $input["placeholder"] : "" ,
                        "form"      => $form,
                        "key"       => $key, 
                        "kunik"     => $kunik,
                        "mode"     => $mode,
                        "canEdit"   => $canEdit,
                        "canEditForm"   => $canEditForm,
                        "canAdminAnswer"  => $canAdminAnswer,
                        "editQuestionBtn" => "",
                        "saveOneByOne" => "",
                        "canAnswer"   => $canAnswer,
                        "wizard"    => true ,
                        "el" => $el ];



                    if(isset($tplT[2]) && $tplT[2] == "select" && isset($input["options"]))
                        $p["options"] = $input["options"];
                    
                    if($input["type"] == "tags") 
                        $initValues[$key] = $input;
                    
                    echo "<span>";

                       echo $this->renderPartial( "survey.views.".$tpl , $p );
                    echo "</span>";
                }
            }

        }
    }

	?>
</div>