<?php
$this->renderPartial('webroot.themes.'.Yii::app()->theme->name.'.views.layouts.mail.header',  array("logo" => @$logo, "url" => $url));

?>
<table class="row" style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;width: 100%;position: relative;display: table;">
    <tbody>
    <tr style="padding: 0;vertical-align: top;text-align: left;">
        <th class="small-12 large-12 columns first" style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0 auto;text-align: left;line-height: 19px;font-size: 15px;padding-left: 16px;padding-bottom: 16px;width: 564px;padding-right: 8px;">
            <table style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;width: 100%;">
                <tr style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0;text-align: left;line-height: 19px;font-size: 15px;">
                    <b>
                        <h5 style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin-top: 15px;text-align: center;line-height: 1.3;word-wrap: normal;margin-bottom: 10px;font-size: 20px;">
                            Repondre au formulaire <?= $formName ?> de <?= $formParent["name"] ?>
                        </h5>
                    </b>
                    <br/>
                    <p>
                        Pour pouvoir repondre au formulaire veuillez valider votre email en cliquant <a href="<?= $urlToSend ?>">ici</a>
                    </p>
                </tr>
            </table>
        </th>
    </tr>
    </tbody>
</table>
</center></td></tr></table>
<center style="font-size: 13px; margin-top:20px;">Envoyé depuis: <?= $_SERVER['HTTP_HOST'] ?></center>
</body></html>