<?php
$logoHeader=(@$logoHeader) ? $logoHeader : "";
$urlRedirect= (!empty($baseUrl) ? $baseUrl : Yii::app()->getRequest()->getBaseUrl(true) );

$btnLabel = isset($btnRedirect["label"]) ?  $btnRedirect["label"] : null;
$btnUrl= isset($btnRedirect["hash"]) ? $urlRedirect.$btnRedirect["hash"] : null;
//Yii::app()->language = $language;

if(!empty($invitorUrl))
	$invitorName='<a href="'.$invitorUrl.'" target="_blank">'.$invitorName.'</a>';
if(!empty($url) && empty($baseUrl)){
    $urlRedirect=Yii::app()->getRequest()->getBaseUrl(true).$url;
}
$headerCss=(isset($style) && isset($style["header"])) ? $style["header"] : "";

 ?>
    <!-- HIDDEN PREHEADER TEXT -->
    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="body" style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">
        <!-- LOGO -->
        <tr>
            <td bgcolor="#f4f4f4 " align="center">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                    <tr>
                        <td align="center" valign="top" style="padding: 40px 10px 40px 10px;"> </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#f4f4f4 " align="center" style="padding: 0px 10px 0px 10px;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                    <tr>
                        <td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 4px; line-height: 48px;background-color: #0a464e;">
                            <?php if (!empty($logo)) { ?>
                               <img src="<?php echo Yii::app()->getRequest()->getBaseUrl(true).$logo?>" style="width: 30%;display: block;border: 0px;" />
                           <?php } ?>
                       </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;padding-bottom: 50px;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                    <tr>
                        <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
                            <div style="margin: 0;white-space: pre-line;text-align: center;">
                            		<?php 
                            		if(!empty($html) && !empty($btnLabel) && !empty($btnUrl))
                            			echo "<p style='text-align:left'>".$html."</p>";
                            		else if(!empty($html))
                            			echo $html;
                            		else if(!empty($msg))
                            			echo nl2br(htmlentities($msg));
                            		?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#ffffff" align="left" style="padding: 0px 30px 40px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
                            <p style="margin: 0;white-space: pre-line;"><br><?= @$sign ?></p>
                            <div style="margin: 0;white-space: pre-line;text-align: center;">
                                   <?php echo "<br><a style='color: #ffffff;text-decoration: none;background-color: #994aff;
                                   border-color: #00a8b3;padding:20px' href='$btnUrl' target='_blank'>$btnLabel</a>" ?>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>