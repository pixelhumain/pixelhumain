  <?php

$parentType = $target["type"];

$logoHeader=(@$logoHeader) ? $logoHeader : "";
$urlRedirect= (!empty($baseUrl) ? $baseUrl : Yii::app()->getRequest()->getBaseUrl(true) );

$urlSite=$urlRedirect;
$urlInvite=$urlRedirect."/#page.type.".$parentType.".id.".$target["id"];
if(!isset($invitedUserId) && isset($invitor)){
  $invitedUserId=$invitor["id"];
  $invitorName=$invitor["name"];
}
//if(@$url){
	//$urlInvite=$urlRedirect.$url;
	//$urlSite=$urlInvite;
	//if(strrpos($url, "costum") !== false)
		//$urlInvite=$urlSite."#page.type.".$parentType.".id.".$target["id"];
//}

$urlActions=$urlSite."/co2/link/validateinvitationbymail/userId/".$invitedUserId."/targetType/".$target["type"]."/targetId/".$target["id"];
if(!empty($url)){
  if(strpos($baseUrl, Yii::app()->getRequest()->getBaseUrl(true)) !== false){
    if(strrpos($url, "survey") !== false || strrpos($url, "costum") !== false){
        //$urlRedirect=Yii::app()->getRequest()->getBaseUrl(true).$url;
        $urlActions=str_replace($url, "", $urlActions)."/redirect/".str_replace("/", ".", $url); //: ltrim($url, '/');
      }
    }else
      $urlActions=$urlActions."/costum/true";
}
echo $this->renderPartial('webroot.themes.'.Yii::app()->theme->name.'.views.layouts.mail.header', array("logo"=>@$logoHeader, "url"=> $urlSite));
//$url = $urlRedirect."/#page.type.".$parentType.".id.".$target["id"];
$verb = (isset($value["verb"])) ? $value["verb"] : "friend";
$typeOfDemand =  (isset($value["typeOfDemand"])) ? $value["typeOfDemand"] : "friend";;
$verbAction=$verb;
if($typeOfDemand=="admin")
	$verbAction="administrate";
else{
	if($verb=="contribute")
		$verbAction="contribute to";
	else if ($verb=="participate")
		$verbAction="participate to";
  else if($verb=="friend"){
    $verbAction="become friend with";
    $urlInvite=$urlRedirect."/#page.type.".Person::COLLECTION.".id.".$invitedUserId;
  }
  else
    $verbAction="join";
}

if(in_array($parentType, [Organization::COLLECTION,Project::COLLECTION,Event::COLLECTION])){    
	$whereThis=Yii::t("common","this ".Element::getControlerByCollection($parentType));
	$whereThe=Yii::t("common","the ".Element::getControlerByCollection($parentType));
}else if($parentType==Person::COLLECTION){
  $whereThis=$whereThe=$invitor["name"];
}
else{
	$whereThis=$whereThe=$target["name"];
}
  ?>
<?php 
if(isset($costumTpl)){
	echo $this->renderPartial($costumTpl, array(
		"invitedUserId"=>$invitedUserId,
		"url"=>$url,
		"baseUrl"=>$url,
		"logoHeader"=>$logoHeader,
		"urlRedirect"=>$urlRedirect,
		"validationKey"=>@$validationKey,
		"urlActions"=>@$urlActions,
		"urlValidation"=>@$urlValidation,
		"invitorUrl"=>@$invitorUrl,
		"target"=>@$target,
		"invitorName"=>@$invitorName,
		"message"=>@$message,
		"title"=>@$title,
		"msg"=>@$msg,
		"footerTpl"=>@$footerTpl));
}else{
?>
    <table class="row" style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;width: 100%;position: relative;display: table;"><tbody><tr style="padding: 0;vertical-align: top;text-align: left;"> <!-- Horizontal Digest Content -->
      <th class="small-12 large-12 columns first" style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0 auto;text-align: left;line-height: 19px;font-size: 15px;padding-left: 16px;padding-bottom: 16px;width: 564px;padding-right: 8px;">

              <h1 class="text-center" style="color: inherit;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 35px 0px 15px 0px;margin: 0;text-align: center;line-height: 1.3;word-wrap: normal;margin-bottom: 10px;font-size: 34px;"><?php echo Yii::t("mail",
                    "Invitation to {what} {where}", 
                    array(
                      "{what}"=>Yii::t("mail", $verbAction), 
                      "{where}"=>"<a href='".$urlInvite."' target='_blank'>".(($verb=="friend") ? $invitor["name"] : $target["name"])."</a>"
                    )
                  ); ?></h1>
            <table style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;width: 100%;">
              <tr style="padding: 0;vertical-align: top;text-align: left;">
                <th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0;text-align: left;line-height: 19px;font-size: 15px;">
                  <!--http://localhost:8888/ph/images/betatest.png-->
               <!--  <a href="<?php //echo $urlRedirect ?>" style="color: #e33551;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0;text-align: left;line-height: 1.3;text-decoration: none;">
                  <img align="right" width="200" src="<?php //echo $urlRedirect."/images/bdb.png"?>" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;width: auto;max-width: 100%;clear: both;display: block;border: none;" alt="Intelligence collective"></a> -->
                <b>
                <h5 style="color: inherit;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0;text-align: left;line-height: 1.3;word-wrap: normal;margin-bottom: 10px;font-size: 20px;"></h5></b><br>
                <?php 
                  $msg=($typeOfDemand=="friend") ? "{who} invites you to become friend" : "You have been invited on {what} by {who}";
                  echo Yii::t("mail",$msg,array("{what}"=>Yii::t("common", "the ".Element::getControlerByCollection($parentType)), "{who}"=>"<b><a href='".$urlRedirect."/#page.type.".Person::COLLECTION.".id.".$invitedUserId."' target='_blank'>".ucfirst($invitorName)."</a></b>")) ?>.<br>
                <br><br>
                </th>
              </tr>
            </table>
            <table style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;width: 100%;">
            
              <tr style="padding: 0;vertical-align: top;text-align: left;">
               <th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0;text-align: right;width:50%;line-height: 19px;font-size: 15px;">
                <a href="<?php echo $urlActions."/answer/true" ?>" style="color: white;
                    font-family: Helvetica, Arial, sans-serif;
                    font-weight: normal;
                    padding: 10px 40px;
                    margin: 0;
                    text-align: left;
                    line-height: 1.3;
                    text-decoration: none;
                    width: 40%;
                    margin-right: 2%;
                    border-radius: 3px;
                    background-color: #84d802;
                    font-size: 15px;
                    font-weight: 800;"><?php echo Yii::t("common", "Accept") ?></a>
                </th>
                <th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0;text-align: left;line-height: 19px;font-size: 15px;width:50%;">
               
                <a href="<?php echo $urlActions."/answer/false" ?>" style="color: white;
                  font-family: Helvetica, Arial, sans-serif;
                  font-weight: normal;
                  padding: 0;
                  margin: 0;
                  text-align: left;
                  line-height: 1.3;
                  text-decoration: none;
                  width: 40%;
                  margin-left: 2%;
                  border-radius: 3px;
                  background-color: #e33551;
                  font-size: 15px;
                  font-weight: 800;
                  padding: 10px 40px;"><?php echo Yii::t("common", "Refuse") ?></a>
                </th>
                
              </tr>
              </table>
              <table style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;width: 100%;">
            
              <tr style="padding: 0;vertical-align: top;text-align: left;">
            
               <th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0;text-align: left;line-height: 19px;font-size: 15px;">
                 <br><br>
              <?php echo Yii::t("mail","If you validate, you will be added as {what} else the link between you and {where} will be destroyed",array("{what}"=>Yii::t("common", $typeOfDemand), "{where}"=>$whereThe))."." ?>
              <br>
              <br>
              <?php echo Yii::t("mail","You can find out the invitation and all informations about {what} following directly {url}",array("{what}"=>$whereThis, "{url}"=> "<a href='".$urlInvite."' style='color: #e33551;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0;text-align: left;line-height: 1.3;text-decoration: none;'>".Yii::t("mail", "this link")."</a>")) ?> 
              <br>
              <br>
              <?php echo Yii::t("mail","If the link doesn&apos;t work, you can copy it in your browser&apos;s address"); ?> :
              <br><div style="word-break: break-all;"><?php echo $urlInvite?></div>
              
            </th>
          </tr>         
        </table>

          </th>
    </tr>

    <tr style="padding: 0;vertical-align: top;text-align: left;">
      <td style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0;text-align: left;line-height: 19px;font-size: 15px;">
        
        <?php  echo $this->renderPartial('webroot.themes.'.Yii::app()->theme->name.'.views.layouts.mail.footer', array('url' => $urlRedirect, "name" => (!empty($title) ? $title : null) )); ?>

      </td>

    </tr>

  </tbody>
</table>
<?php } ?>
