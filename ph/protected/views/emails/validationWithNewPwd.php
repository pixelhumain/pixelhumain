<?php

	$logoHeader=(@$logoHeader) ? $logoHeader : "";
	$urlRedirect= (!empty($baseUrl) ? $baseUrl : Yii::app()->getRequest()->getBaseUrl(true) );
	$urlValidation=$urlRedirect."/".$this->module->id.'/person/activate/user/'.$user.'/validationKey/'.Person::getValidationKeyCheck($user);
	if(!empty($url) && empty($baseUrl) ) {
		$urlRedirect=Yii::app()->getRequest()->getBaseUrl(true).$url;
	}
	if(!empty($url)) {
		if(strpos($baseUrl, Yii::app()->getRequest()->getBaseUrl(true)) !== false) {
			if(strrpos($url, "survey") !== false || strrpos($url, "costum") !== false){
				$urlValidation=str_replace($url, "", $urlValidation)."/redirect/".str_replace("/", ".", $url);
			}	
		} else {
			$urlValidation=$urlValidation."/costum/true";
		}
	}
	if(isset($urlToRedirect)) {
		$urlValidation .= "/toredirect/".str_replace("/", ".", $urlToRedirect);
	}
	echo $this->renderPartial('webroot.themes.'.Yii::app()->theme->name.'.views.layouts.mail.header', array("logo"=>@$logoHeader, "url"=> $urlRedirect, "headerTpl"=>$headerTpl ?? null));

	//$this->renderPartial('webroot.themes.'.Yii::app()->theme->name.'.views.layouts.mail.header');
?>

<table class="row masthead" style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;background: white;width: 100%;position: relative;display: table;">
	<tbody>
		<tr style="padding: 0;vertical-align: top;text-align: left;"> <!-- Masthead -->
			<th class="small-12 large-12 columns first last" style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0 auto;text-align: left;line-height: 19px;font-size: 15px;padding-left: 16px;padding-bottom: 16px;width: 564px;padding-right: 16px;">
				<table style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;width: 100%;">
					<tr style="padding: 0;vertical-align: top;text-align: left;">
						<th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0;text-align: left;line-height: 19px;font-size: 15px;">
							<h1 class="text-center" style="color: #e33551;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 35px 0px 15px 0px;margin: 0;text-align: center;line-height: 1.3;word-wrap: normal;margin-bottom: 0px;font-size: 34px;"><?php echo Yii::t("mail","Finalization of temporary account creation") ?></h1>
						</th>
					</tr>
				</table>
			</th>
		</tr>
	</tbody>
</table>
<table class="row" style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;width: 100%;position: relative;display: table;">
    <tbody>
        <tr style="padding: 0;vertical-align: top;text-align: left;"> <!-- Horizontal Digest Content -->
	        <th class="small-12 large-12 columns first" style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0 auto;text-align: left;line-height: 19px;font-size: 15px;padding-left: 16px;padding-bottom: 16px;width: 564px;padding-right: 8px;">
                <table style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;width: 100%;">
                    <tr style="padding: 0;vertical-align: top;text-align: left;">
                        <th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0;text-align: left;line-height: 19px;font-size: 15px;">
                            <h3 style="color: inherit;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0;text-align: left;line-height: 1.3;word-wrap: normal;margin-bottom: 10px;font-size: 22px;"><?php echo Yii::t("mail","Welcome on {what}",array("{what}"=>$title)) ?></h3>
                            <!-- <img align="right" width="" src="<?php echo Yii::app()->getRequest()->getBaseUrl(true)."/images/bdb.png"?>" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;width: auto;max-width: 100%;clear: both;display: block;border: none;" alt="betatest"> -->
                            
                            <?php echo Yii::t("mail","You answered the form {what} using this email, so we created an account with your email address. To continue to use this email, please activate your account by clicking on this link", array('{what}' => $formTitle)) ?>.<br>
							<a href="<?php echo $urlValidation ?>" target="_blank" style="color: #e33551;font-family: Helvetica, Arial, sans-serif;font-weight: bold;padding: 0;margin: 0;text-align: left;line-height: 1.3;text-decoration: none;font-size: large;"><?php echo Yii::t("mail","Validation of my account") ?></a><br><br>
                            <?php echo Yii::t("mail","To connect you, you can use this password now") ?> : <br><h5 style="color:#e33551;font-family: Helvetica, Arial, sans-serif;font-weight: bold;padding: 0;margin: 0;text-align: left;line-height: 1.3;word-wrap: normal;margin-bottom: 10px;font-size: 20px;"><?php echo $pwd?></h5><br>
                            <br>
                            <?php echo Yii::t("mail", "You can then change the password in your {where}", array("{where}" => '<a href="'.$urlRedirect.'/#page.type.citoyens.id.'.$user.'.view.settings" style="color: #e33551;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0;text-align: left;line-height: 1.3;text-decoration: none;"> '.Yii::t("mail", "settings").'</a>' )) ?>
                            .<br><br>
                            
                        </th>
                    </tr>
                </table>
	        </th>
        </tr>
    </tbody>
</table>

		<?php echo $this->renderPartial('webroot.themes.'.Yii::app()->theme->name.'.views.layouts.mail.footer', array('url' => $urlRedirect, "name" => (!empty($title) ? $title : null), "footerTpl" => (!empty($footerTpl) ? $footerTpl : null) )); ?>