<?php
//PIXEL HUMAIN specific modules 
return [
	'citizenToolKit',
	'co2' => ['class' => Co2Module::class],
//	'network' => ['class' => NetworkModule::class],
	'api' => ['class' => ApiModule::class],
	'datatools',
	"graph" => ['class' => GraphModule::class],
	"cotools" => ['class' => CotoolsModule::class],
	"learn",
	"copi",
	"connect",
	"places" => ['class' => PlacesModule::class],
	//"ressources", //kill
	//"classifieds",
	//"onepage",
	"cooperation",
	"costum" => ['class' => CostumModule::class],
	//'terla',
	'chat' => ['class' => ChatModule::class],
	//'cococarto', 
	"survey" => ['class' => SurveyModule::class],
	"interop" => ['class' => InteropModule::class],
	"map" => ['class' => MapModule::class],
	"eco" => ['class' => EcoModule::class],
	"dda" => ['class' => DdaModule::class],
	"custom",
	//"mynetwork",
	"news" => ['class' => NewsModule::class],
    'opauth' => [
            'opauthParams' => [
                'security_salt' => '14185260605a69dde7421038.7567589517702445395a69de17b8f6b3.52478134',
                'Strategy' => [
                    'Facebook' => [
            			//https://developers.facebook.com/x/apps/534906546570011/settings/basic/
            			'app_id' => '534906546570011',
            			'app_secret' => '2b4d7a98eeac103e3ca6ec9387e58bdc'
                    ],
            		'Twitter' => [
            			//https://dev.twitter.com/apps/5741723/show
            			'key' => 'bI9MbnygLBkgI0zlNKuuw',
            			'secret' => 'wgKuhTmq9LjtEpfYz0wwhiSLvzfrDtX81ENZDze9uo'
                    ],
            		'Google' => [
            			//https://cloud.google.com/console/project/1035825037059/apiui/credential
            			'client_id' => '1035825037059-tcrscaie3brdsu5cl8gekna0opeqkgn4.apps.googleusercontent.com',
            			'client_secret' => 'yNeJg-EFe5lF-eiPGm8UbqSD'
                    ],
            		'LinkedIn' => [
            			//https://www.linkedin.com/secure/developer
            			'api_key' => '77o6zfdg1ulh2a',
            			'secret_key' => 'xrWLQrYq9D54FDha'
                    ],
            		'OpenID' => [],
                ],
            ],
    ],
];
