<?php

use yii\web\UrlNormalizer;

$params = require __DIR__ . '/params.php';
$authClientCollection = require __DIR__ . '/oauthClientConfig.php';
//$db = require __DIR__ . '/db.php';
$activeModules = require __DIR__ .'/modules_config.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__, 2),
    'viewPath' => dirname(__DIR__, 2).DIRECTORY_SEPARATOR.'views',
    //'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
         '@modules' => dirname(__DIR__, 5).'/modules',
         '@dda' => dirname(__DIR__, 5).'/modules/dda',
         '@news' => dirname(__DIR__, 5).'/modules/news',
         '@themes' => dirname(__DIR__, 3).'/themes',
         '@runtime' => dirname(__DIR__, 2).'/runtime',
    ],
    'modules' => $activeModules,
	'controllerNamespace' => 'PixelHumain\PixelHumain\controllers',
    'defaultRoute' => 'co2',
    'bootstrap' => ['log'],
    'components' => [
        'urlManager' => [
            'class' => PixelHumain\PixelHumain\components\UrlManager::class,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'normalizer' => [
                'class' => UrlNormalizer::class,
                // use temporary redirection instead of permanent for debugging
//                'action' => \yii\web\UrlNormalizer::ACTION_REDIRECT_TEMPORARY,
                'action' => null,
//                'normalizeTrailingSlash' => false,
            ],
            'rules' => require __DIR__.'/url_rules.php',
        ],
        'view' => [
            'class' => \PixelHumain\PixelHumain\components\View::class,
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'fs' => [
            'class' => 'creocoder\flysystem\LocalFilesystem',
            'path' => 'upload',
        ],
        /*'awss3Fs' => [
            'class' => 'creocoder\flysystem\AwsS3Filesystem',
            'key' => '',
            'secret' => '',
            'bucket' => '',
            'region' => 'your-region',
            // 'version' => 'latest',
            // 'baseUrl' => 'your-base-url',
            // 'prefix' => 'your-prefix',
            // 'options' => [],
            // 'endpoint' => 'http://my-custom-url'
        ],*/
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error'],
                    // 'logVars' => [ '_GET', '_POST', '_FILES', '_COOKIE', '_SESSION', '_SERVER' ],
                    'logVars' => [],
                    'logFile'=> '@runtime/application.log',
                    'except' => ['yii\web\HttpException:404'],
                ]
            ],
        ],
    ],
    /*'components' => [
        'request' => [
            'cookieValidationKey' => '',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        //'db' => $db,
    ],*/
    'params' => $params,
];

if(isset($authClientCollection) && is_array($authClientCollection) && isset($authClientCollection['clients']) && is_array($authClientCollection['clients']) && is_countable($authClientCollection['clients']) && count($authClientCollection['clients']) > 0){
    $config['components']['authClientCollection'] = $authClientCollection;
}

/*if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}*/

return $config;
