<?php

use ahmadasjad\yii2PlusYii1\UrlRuleCamelCase;

return [
//                '<controller:\w+>/<id:\d+>' => '<controller>/view',
    [
        'class' => UrlRuleCamelCase::class,
        'pattern' => '<controller:[\w-]+>/<id:\d+>',
        'route' => '<controller>/view',
    ],

//                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
    [
        'class' => UrlRuleCamelCase::class,
        'pattern' => '<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>',
        'route' => '<controller>/<action>',
    ],

//                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
    [
        'class' => UrlRuleCamelCase::class,
        'pattern' => '<controller:[\w-]+>/<action:[\w-]+>',
        'route' => '<controller>/<action>',
    ],
    [
        'class' => UrlRuleCamelCase::class,
        'pattern' => '<controller:[\w-]+>',
        'route' => '<controller>',
    ],

//                '<module:\w+>/<controller:\w+>/<id:\d+>' => '<module>/<controller>/view',
    [
        'class' => UrlRuleCamelCase::class,
        'pattern' => '<module:[\w-]+>/<controller:[\w-]+>/<id:\d+>',
        'route' => '<module>/<controller>/view',
    ],

//                '<module:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<module>/<controller>/<action>',
    [
        'class' => UrlRuleCamelCase::class,
        'pattern' => '<module:[\w-]+>/<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>',
        'route' => '<module>/<controller>/<action>',
    ],

//                '<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
    [
        'class' => UrlRuleCamelCase::class,
        'pattern' => '<module:[\w-]+>/<controller:[\w-]+>/<action:[\w-]+>',
        'route' => '<module>/<controller>/<action>',
    ],
//                [
//                    'class' => UrlRuleCamelCase::class,
//                    'pattern' => '<module:[\w-]+>/<controller:[\w-]+\/[\w-]+>/<action:[\w-]+>',
//                    'route' => '<module>/<controller>/<action>',
//                ],
    [
        'class' => UrlRuleCamelCase::class,
        'pattern' => '<module:[\w-]+>/<controller:[\w-]+>/<action:[\w-]+><rest:[\/\w\,\;\-\=\.]*>',
        'route' => '<module>/<controller>/<action>',
    ],
    [
        'class' => UrlRuleCamelCase::class,
        'pattern' => '.well-known/webfinger',
        'route' => 'api/activitypub/webfinger',
    ],
    [
        'class' => UrlRuleCamelCase::class,
        'pattern' => '.well-known/host-meta',
        'route' => 'api/activitypub/host-meta',
    ],
];