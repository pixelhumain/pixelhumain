var commentObj = {
  openPreview : function(type, id, path, title, coformKey, formId,options){
    if(typeof options == "undefined" || (typeof options != "undefined" && typeof options.notCloseOpenModal == "undefined"))
        $("#openModal").modal("hide");
    $("#openModal").removeAttr("tabindex");
    mylog.warn("***************************************");
    mylog.warn("***************************************");
    title=decodeURI(title);
    hashPreview="?preview=comments."+type+"."+id;
    urlPreview = baseUrl+'/'+moduleId+"/comment/index/type/"+type+"/id/"+id;
    if(notNull(path)){
      urlPreview+="/path/"+path;
      hashPreview+="."+path;
    }
      //urlCtopenPreviewElement( baseUrl+'/'+moduleId+"/"+url);
    if(notNull(title))
      hashPreview+="."+encodeURI(title);
    else
      title="";
    hashT=location.hash.split("?");
    getStatus=searchInterface.getUrlSearchParams();
    urlHistoric=hashT[0].substring(0)+hashPreview;
    if(getStatus != "") urlHistoric+="&"+getStatus;
    if(typeof options == "undefined" || (typeof options != "undefined" && typeof options.notCloseOpenModal == "undefined"))
        history.replaceState({}, null, urlHistoric);
    $("#modal-preview-comment .title-comment").html(title);
    coInterface.showLoader("#modal-preview-comment .comment-tree");
    $("#modal-preview-comment").show(200);

    getAjax('#modal-preview-comment .comment-tree' , urlPreview, function(){
      commentObj.bindModalPreview();
    },"html");
    //reload coform input after added comment
    if(coformKey && formId){
      commentObj.coformKey = coformKey;
      commentObj.formId = formId;
      commentObj.afterSaveReload=function(){
        //alert("afterSaveReload"+commentObj.coformKey+","+commentObj.formId)
        //commentObj.closePreview();
        if (typeof reloadInput == "function") {
          reloadInput( commentObj.coformKey, commentObj.formId );
        }

      }
    }
    if($("#btn-comment-"+id).length != 0){
        commentObj.afterSaveReload = function(){
            ajaxPost(
                null,
                baseUrl+'/'+moduleId+"/comment/countcommentsfrom",
                {
                  "type" : type,
                  "id" : id,
                  "path" : path
                },
                function(data){
                  $("#btn-comment-"+id).html(data.count+" <i class='fa fa-2xx fa-commenting'></i>")
                }
            );
        }
    }
  },
  bindModalPreview : function(){
    $("#modal-preview-comment .btn-close-preview").off().on("click", function(){
      commentObj.closePreview();
    });

    $(".main-container").on("click",function() {
      if( $("#modal-preview-comment").css("display") == "block" ){
        commentObj.closePreview();
      }
    });

  },
  closePreview: function(){
    //mylog.log("close preview");
    $(".main-container").off();
    $("#modal-preview-comment").css("display","none");
    urlCtrl.manageHistory(false);
  }
};
var visibleCommentsIds = {
  toUpdateComments: [],
  alreadyUpdatedComments: [],
};
function updateSeenStatus(idsComment, callBack = () => {}) {
  var objToSend = {
    id: idsComment, collection: "comments", value: "now", setType: "isoDate", path: "views." + userId + ".date"
  };
  if (userId) {
    ajaxPost("", baseUrl + "/co2/aap/commonaap/action/update_seen_status", objToSend, function (data) {
        callBack(data);
      }, function (error) {
        mylog.error("ajaxPost error", error)
      }, "json"
    );
  }
}
const isValidURL = (params) => {
    const pattern = new RegExp("^(https?:\\/\\/)"
        + "((([a-zA-Z\\d]([a-zA-Z\\d-]{0,61}[a-zA-Z\\d])?)\\.)+[a-zA-Z]{2,}|"
        + "([a-zA-Z\\d]([a-zA-Z\\d-]{0,61}[a-zA-Z\\d])?))"
        + "(\\:\\d+)?(\\/[-a-zA-Z\\d%_.~+]*)*"
        + "(\\?[;&a-zA-Z\\d%_.~+=-]*)?"
        + "(\\#[-a-zA-Z\\d_]*)?"
        + "(?<!\\/)$", "i");

    return !!pattern.test(params);
};
function initCommentsTools(listObjects, type, canComment, parentId, path){
	//mylog.log("comments.js initCommentsTools", listObjects, type, canComment, parentId, path);
	//ajoute la barre de commentaire & vote up down signalement sur tous les medias
	$.each(listObjects, function(key, media){
		if(typeof media._id != "undefined" || typeof media.id != "undefined"){
			mylog.log("type",media);
			//media.target = "news";
			var commentCount = 0;
			idMedia=(typeof media._id != "undefined") ? media._id['$id'] : media.id;
			if ("undefined" != typeof media.commentCount)
				commentCount = media.commentCount;

			idSession = typeof idSession != "undefined" ? idSession : false;
      if (visibleCommentsIds.toUpdateComments.indexOf(idMedia) < 0 && visibleCommentsIds.alreadyUpdatedComments.indexOf(idMedia) < 0 && $("#item-comment-" + idMedia).length > 0) {
        visibleCommentsIds.toUpdateComments.push(idMedia);
        visibleCommentsIds.alreadyUpdatedComments.push(idMedia);
      }

			var sectionHtmlCount = '';
      if (typeof interactionsCount != "undefined" && media.vote) {
        interactionsCount[`${type}.${idMedia}`] = {
          vote: $.extend(true, {}, media.vote),
        };
      }
			if(userId != "" && typeof media.voteCount != "undefined"){
				sectionHtmlCount = "<a href='javascript:;' onclick='getReactionList(\""+idMedia+"\",\""+type+"\");' class='content-reactions-"+type+" content-reactions-"+type+"-"+idMedia+"'>";
				totalReaction=0;
				$.each(media.voteCount, function(e, v){
					addClassCount="";
					if( userId != "" 
						&& "undefined" != typeof media.vote 
						&& "undefined" != typeof media.vote[userId] 
						&& "undefined" != typeof media.vote[userId].status 
						&& media.vote[userId].status==e
          ) {
						addClassCount="currentUserVote"+userId;
          }
					sectionHtmlCount +="<div class='emojicon"+type+" "+e+" "+addClassCount+" pull-left' data-count='"+v+"'></div>";
					totalReaction=totalReaction+v;
				});
				sectionHtmlCount+="<span class='pull-left margin-left-5 totalCountReaction' data-count='"+totalReaction+"'>"+totalReaction+"</span>";
				sectionHtmlCount+="</a>"
			}
	/*if(commentCount > 0){
	sectionHtmlCount+="<span class='nbNewsComment pull-right newsAddComment' data-media-id='"+idMedia+"''>" + commentCount + " ";
	sectionHtmlCount+=(commentCount>1) ? trad.comments : trad.comment;
	sectionHtmlCount+="</span>";
	}*/
	/*if(commentCount == 0 && idSession) lblCommentCount = "<i class='fa fa-comment'></i>  "+trad.commenton;
	if(commentCount == 1) lblCommentCount = "<i class='fa fa-comment'></i> <span class='nbNewsComment'>" + commentCount + "</span> "+trad.comment;
	if(commentCount > 1) lblCommentCount = "<i class='fa fa-comment'></i> <span class='nbNewsComment'>" + commentCount + "</span> "+trad.comments;
	if(commentCount == 0 && !idSession) lblCommentCount = "0 <i class='fa fa-comment'></i> ";*/


			var actionOn="";
			if(type=="news"){
        var userConnected = userId !== "";
				labelComments= (userConnected)?trad.commenton:trad.comment;
				if(commentCount>0){
					labelComments="<span class='nbNewsComment'>"+commentCount+"</span> ";
					labelComments+=(commentCount>1) ? trad.comments : trad.comment;
				}
				actionOn = '<a href="javascript:" class="newsAddComment letter-blue lblComment" data-media-id="'+idMedia+'"><i class="fa fa-comment"></i> '+labelComments+'</a>';
			}else if (type=="comments"){
				parentId=(notNull(parentId)) ? parentId : idMedia;
				countReplies=(typeof media.replies != "undefined") ? Object.keys(media.replies).length : 0;
				s=(countReplies > 1) ? "s" : "";
				lblReply = (countReplies == 0) ? trad.answerOn : trad["answer"+s];
				if(countReplies >= 1) lblReply = "<span class='nbNewsComment'>"+countReplies+"</span> "+lblReply;
				pathComment=(typeof media.path != "undefined") ? media.path : null;

				actionOn= '<a class="lblComment" href="javascript:answerComment(\''+idMedia+'\', \''+idMedia+'\',\''+media.contextType+'\', \''+pathComment+'\')"><i class="fa fa-reply fa-rotate-180"></i> '+lblReply+'</a>';
			}
			// SHARE ACTION AND COUNT SHARE
			if(type=="news"){
				idMediaShare=media._id['$id'];
				typeMediaShare = "news";
			if(media.type=="activityStream") {
				idMediaShare = media.object.id;
				typeMediaShare = media.object.type;
			}
			if(userId != "" && typeof media.scope != "undefined" && typeof media.scope.type != "undefined" && media.scope.type != "private"){
				actionOn =  actionOn+
				     "<button class='text-dark btn btn-link no-padding margin-right-5 btn-share bold'"+
				        " style='margin-top:-1px;font-size:18px;'" +
				        " data-id='"+idMediaShare+"' data-type='"+typeMediaShare+"'>"+
				        "<i class='fa fa-retweet'></i> "+trad.share+
				     "</button>";
			}

			var countShare = media.sharedBy.length-1;
			if(countShare > 1)
			sectionHtmlCount =  sectionHtmlCount+
			     "<small class='pull-right tooltips' data-original-title='ce message a été partagé "+countShare+" fois'"+
			        " style='margin-top:3px;'>" +
			        "<i class='fa fa-share'></i> "+countShare+
			     "</small>";
			}
			if(sectionHtmlCount!=""){
				sectionHtmlCount="<div class='col-xs-12 no-padding sectionHtmlCount-"+type+"-"+idMedia+"'>"+sectionHtmlCount+"</div>"
				if(type!="comments")
					sectionHtmlCount+="<hr class='col-xs-12 margin-top-5 margin-bottom-5 no-padding'></hr>";
			}

			actionOn = actionOn+voteCheckAction(idMedia, media, type);
			if(type=="comments"){
				if(typeof media.author.id != "undefined" && media.author.id == userId){
					actionOn += '<a class="tooltips margin-left-10" '+
					 'data-toggle="tooltip" data-placement="top" title="'+trad.update+'" '+
					 'href="javascript:editComment(\''+idMedia+'\')"><i class="fa fa-pencil"></i>'+
					'</a>'+
					'<a class="tooltips margin-left-10" data-toggle="tooltip" data-placement="top" title="'+trad.delete+'" '+
					'href="javascript:confirmDeleteComment(\''+idMedia+'\',$(this))"><i class="fa fa-trash"></i>'+
					'</a>';
				}
			}
			voteTools = sectionHtmlCount + actionOn;

			//mylog.log("comments.js initCommentsTools footer", type, idMedia);
			$("#footer-"+type+"-"+idMedia).html(voteTools);

			if(userId != "")
				initReactionTools(idMedia, type);


			if(type=="comments" && typeof media.replies != "undefined" && notNull(canComment)){
				//mylog.log("comments.js initCommentsTools reback", media.replies, type, canComment, idMedia);
				initCommentsTools(media.replies, type, canComment, idMedia);
			}
		}
	});

	directory.bindShareElt();
	if(userId != "")
		initReportAbuse();
	$(".newsAddComment").off().click(function(){
		var id = $(this).data("media-id");
    var type = $(this).data("type");
		showCommentsTools(id, type);
	});
}

function initReportAbuse(){
  $('.reportAbuse').off().on("click",function(){
      id=$(this).data("id");
      if($(".commentVoteUp[data-id='"+id+"']").hasClass("text-green") || $(".commentVoteDown[data-id='"+id+"']").hasClass("text-orange"))
          toastr.info(trad.youcantmakeactionafterabuse);
      reportAbuse($(this));
  });

}

function abuseActionSuccess($this, data, action){
  if (data.userAllreadyDidAction) {
    toastr.info(trad.youalreadydeclareabuse+$this.data("type"));
  } else {
    toastr.success(data.msg);
    if (action == "reportAbuse") {
      count = parseInt($this.data("count"));
      $this.data( "count" , count+1 );
      icon = $this.children(".label").children(".fa").attr("class");
      $this.children(".label").html($this.data("count")+" <i class='"+icon+"'></i>");
    } else {
      $('.abuseCommentTable #comment'+$this.data("id")).remove();
      $('.nbCommentsAbused').html((parseInt($('.nbCommentsAbused').html()) || 0) -1);
    }
  }
}

function updateCommentView(commentId) {
  $("#item-comment-" + commentId).removeClass("highlight-this")
  $(`#item-comment-${ commentId } #footer-comments-${ commentId } > a.lblComment.has-newanswer`).removeClass("has-newanswer")
  const toUpdateCommentIndex = visibleCommentsIds.toUpdateComments.indexOf(commentId);
  if (visibleCommentsIds.alreadyUpdatedComments.indexOf(commentId) < 0 || toUpdateCommentIndex > -1) {
    toUpdateCommentIndex > -1 ? visibleCommentsIds.toUpdateComments.splice(toUpdateCommentIndex, 1) : "";
    visibleCommentsIds.alreadyUpdatedComments.indexOf(commentId) < 0 ? visibleCommentsIds.alreadyUpdatedComments.push(commentId) : "";
    updateSeenStatus(commentId, function(res) {
      if (discussionComments.common.refreshLeftPanelSeenOneComment) {
        discussionComments.common.refreshLeftPanelSeenOneComment(commentId)
      }
    })
  }
}

function abuseNotifInRockerChat(idComment, reason, comment, channel = "coabus") {
  ajaxPost(null, baseUrl + '/survey/answer/rcnotification/action/addabusnewsco', {
    id: idComment,
    reason: reason,
    comment: comment,
    channelChat: channel
  }, function(){});
}

var faceMocions = [];
function initReactionTools(idObject, type){
  faceMocions[type+idObject] = $("#footer-"+type+"-"+idObject+' .reaction-news').faceMocion({
        emociones:emojiconReactions,
         callback: function(contentDiv, emo) {
         // mylog.log($(e).parent());
          $refNews=$(".reaction-news."+contentDiv.attr('id-referencia'));
          actionOnMedia(contentDiv, "vote",  false, {status: emo});
        }
  });
    //  })
}
function actionOnMedia(contentDiv,action,method, detail) {
  //var type="news";
  //if(typeof parentTypeComment != "undefined")
    //type = parentTypeComment;
  var typeObj=contentDiv.data("type");
  var idObj=contentDiv.data("id");
  params=new Object,
  params.id=idObj,
  params.collection=typeObj,
  params.action=action;
  if(notNull(detail)){
    params.details=detail;
  }

  if(method){
    params.unset=method;
  }
  ajaxPost(
    null,
    baseUrl+'/'+moduleId+"/action/addaction/",
    params,
    function(data) {
      if(!data.result)
          toastr.error(data.msg);
      else {
        if (data.userAllreadyDidAction)
          toastr.info(data.msg);
        else {
          if(action=="reportAbuse"){
            abuseNotifInRockerChat(data.element["_id"]["$id"], detail["reason"], detail["comment"]);
            abuseActionSuccess(contentDiv, data, action);
//                toastr.success(trad["thanktosignalabuse"]);

            //to hide menu
            //$(".newsReport[data-id="+params.id+"]").hide();
          }
          else{
            if(data.inc>0){
              if(action=="vote") {
                callbackVote(typeObj, idObj, data.element.vote[userId].status);
                if (typeof discussionSocket != "undefined" && typeof discussionSocket.emitEvent != "undefined" && userId) {
                  discussionSocket.emitEvent(
                    wsCO, 
                    "change_comment", 
                    {
                      userId: userId,
                      onUpdate: "addVote", 
                      typeObj: typeObj,
                      idObj: idObj,
                      voteStatus: data.element.vote[userId].status, 
                    }
                  );
                }
              }
              toastr.success(trad["voteaddedsuccess"]);
            }
            else{
              if(action=="vote") {
                removeVote(typeObj, idObj, data.removedVoteStatus)
                if (typeof discussionSocket != "undefined" && typeof discussionSocket.emitEvent != "undefined" && userId) {
                  discussionSocket.emitEvent(
                    wsCO, 
                    "change_comment", 
                    {
                      userId: userId,
                      onUpdate: "removeVote", 
                      typeObj: typeObj,
                      idObj: idObj,
                      voteStatus: data.removedVoteStatus, 
                    }
                  );
                }
              }
              toastr.success(trad["voteremovedsuccess"]);
            }
          }
        }
      }
    },
    function(data){
          toastr.error("Error calling the serveur : contact your administrator.");

    });
  updateCommentView(params.id);
}
/**
 * 
 * @param {string} type 
 * @param {string} id 
 * @param {string} status 
 * @param {string} currentUserId used specifically for socket event 
 */
function callbackVote(type, id, status, currentUserId = userId){
  htmlContent="";
  //Check if a reaction is already done on object to know if vote count container exists
  if($(".content-reactions-"+type+"-"+id).length<=0){
    htmlContent="<a href='javascript:;' onclick='getReactionList(\""+id+"\",\""+type+"\");' class='content-reactions-"+type+" content-reactions-"+type+"-"+id+"'>"+
        "<span class='pull-left margin-left-5 totalCountReaction' data-count='0'></span>"+
      "</a>";
    targetDom=".sectionHtmlCount-"+type+"-"+id;
  }
  //Check if a reaction or a comment is already done on object to know if count container exists in footer
  if($(".sectionHtmlCount-"+type+"-"+id).length<=0){
    htmlContent="<div class='col-xs-12 no-padding sectionHtmlCount-"+type+"-"+id+"'>"+htmlContent+"</div>";
    if(type!="comments")
      htmlContent+="<hr class='col-xs-12 margin-top-5 margin-bottom-5 no-padding'></hr>";
    targetDom="#footer-"+type+"-"+id;
  }
  if(htmlContent!="")
    $(targetDom).prepend(htmlContent);
  //Check if user reaction voted on this object
  if(currentUserId != userId && typeof interactionsCount != "undefined" && typeof interactionsCount[`${type}.${id}`] != "undefined" && interactionsCount[`${type}.${id}`].vote && interactionsCount[`${type}.${id}`].vote[currentUserId]) {
    const currentStatus = interactionsCount[`${type}.${id}`].vote[currentUserId].status;
    countLastVote=parseInt($(".content-reactions-"+type+"-"+id+" ." + currentStatus).attr("data-count"));
    if(countLastVote>1){
        $(".content-reactions-"+type+"-"+id+" ." + currentStatus).attr("data-count", (countLastVote-1));
    } else {
      $(".content-reactions-"+type+"-"+id+" ." + currentStatus).remove()
    }
  } else if ($(".content-reactions-"+type+"-"+id+" .currentUserVote"+currentUserId).length > 0){
    countLastVote=parseInt($(".content-reactions-"+type+"-"+id+" .currentUserVote"+userId).attr("data-count"));
    if(countLastVote>1){
        $(".content-reactions-"+type+"-"+id+" .currentUserVote"+currentUserId).attr("data-count", (countLastVote-1)).removeClass("currentUserVote"+currentUserId);
    } else {
      $(".content-reactions-"+type+"-"+id+" .currentUserVote"+currentUserId).remove()
    }
  }else{
    //Increment total of vote account
    incTotal=parseInt($(".content-reactions-"+type+"-"+id+" .totalCountReaction").attr("data-count"))+1;
    $(".content-reactions-"+type+"-"+id+" .totalCountReaction").attr("data-count", incTotal).text(incTotal);
  }
  //Check if this status vote already existed for this object
  if($(".content-reactions-"+type+"-"+id+" .emojicon"+type+"."+status).length <=0)
    $(".content-reactions-"+type+"-"+id).prepend(`<div class='emojicon${type} ${status} ${currentUserId == userId ? "currentUserVote" + userId : ""} pull-left' data-count='1'></div>`);
  else
    $(".content-reactions-"+type+"-"+id+" .emojicon"+type+"."+status).addClass(currentUserId == userId ? "currentUserVote" + userId : "").attr("data-count", parseInt($(".content-reactions-"+type+"-"+id+" .emojicon"+type+"."+status).attr("data-count"))+1);
  if (typeof interactionsCount != "undefined" && typeof interactionsCount[`${type}.${id}`] != "undefined") {
    interactionsCount[`${type}.${id}`].vote[currentUserId] = {
      status: status,
      date: null
    };
  } else if (typeof interactionsCount != "undefined") {
    interactionsCount[`${type}.${id}`] = {
      vote: {
        [currentUserId]: {
          status: status,
          date: null
        }
      }
    };
  }
}

function removeVote(type, id, status, makeReset = true, currentUserId = userId){
  var totalVote=parseInt($(".content-reactions-"+type+"-"+id+" .totalCountReaction").attr("data-count"))-1;
  if(totalVote > 0){
    $(".content-reactions-"+type+"-"+id+" .totalCountReaction").attr("data-count", totalVote).text(totalVote);
  }else{
    $(".content-reactions-"+type+"-"+id+" .totalCountReaction").attr("data-count", 0).text("");
  }

  var $statusEmoji = $(`.content-reactions-${type}-${id} .emojicon${type}.${status}`);
  if($statusEmoji.length > 0){
    var count = parseInt($statusEmoji.attr("data-count")) - 1;
    $statusEmoji.attr("data-count", count)
    if(count < 1)
      $statusEmoji.remove()
  }

  currentUserId == userId ? $(".content-reactions-"+type+"-"+id+" .currentUserVote"+userId).removeClass("currentUserVote"+userId) : ""
  if (typeof interactionsCount != "undefined" && typeof interactionsCount[`${type}.${id}`] != "undefined" && interactionsCount[`${type}.${id}`].vote && interactionsCount[`${type}.${id}`].vote[currentUserId]) {
    delete interactionsCount[`${type}.${id}`].vote[currentUserId];
  }
  if (makeReset) {
    faceMocions[type+id].reset()
  }
}

function getReactionList(idMedia, type){
    showModalReactions();
    getAjax('#reactionsContent' ,baseUrl+'/'+moduleId+"/action/list/type/"+type+"/id/"+idMedia+"/actionType/vote",function(){},"html");
}
//lance le chargement des commentaires pour une publication
function showCommentsTools(id, type="news"){
  //mylog.log("showCommentsTools", id);
    if(!$("#commentContent"+id).hasClass("hidden")){
      $(".commentContent").html("");
      $(".commentContent").removeClass("hidden");
      $(".footer-comments").html("");
      $('#commentContent'+id).html('<div class="text-dark margin-bottom-10"><i class="fa fa-spin fa-refresh"></i> '+trad.chargingcomments+' ...</div>');
      getAjax('#commentContent'+id ,baseUrl+'/'+moduleId+"/comment/index/type/"+type+"/id/"+id,function(){

      },"html");
    }else{
      $("#commentContent"+id).removeClass("hidden");

    }
}

function voteCheckAction(idVote, newsObj, mediaTarget) {
  var voteUpCount = reportAbuseCount = voteDownCount = 0;
  textUp="text-dark";
  textDown="text-dark";
  textReportAbuse="text-dark";
  mediaTarget = (notNull(mediaTarget)) ? mediaTarget : newsObj.target.type;

  if ("undefined" != typeof newsObj.reportAbuse && "undefined" != typeof newsObj.reportAbuseCount && newsObj.reportAbuseCount > 0) {
    reportAbuseCount = newsObj.reportAbuseCount;
    if ("undefined" != typeof newsObj.reportAbuse[idSession]){
      textReportAbuse= "text-red";
      $(".newsReportAbuse[data-id="+idVote+"]").off();
    }
  }
  valueVote="";
  if(userId != "" && "undefined" != typeof newsObj.vote && "undefined" != typeof newsObj.vote[userId])
    valueVote="data-value='"+newsObj.vote[userId].status+"'";

  voteHtml= "<span class='reaction-news' data-count='"+voteUpCount+"' data-id='"+idVote+"' data-type='"+mediaTarget+"' "+valueVote+"></span>";
  if( ( mediaTarget!="news" || reportAbuseCount > 0) && userId != "" ){
    classPull=(mediaTarget=="news") ? "pull-right":"";
    voteHtml+="<a href='javascript:;' class='reportAbuse "+classPull+" margin-left-10' data-count='"+reportAbuseCount+"' data-id='"+idVote+"' data-type='"+mediaTarget+"'>"+
      "<span class='label "+textReportAbuse+" no-padding'>"+reportAbuseCount+" <i class='fa fa-flag'></i></span>"+
    "</a>";
  }
    //onclick='newsReportAbuse(this, \""+idVote+"\")'
  return voteHtml;
}



function bindEventTextArea(idTextArea, idComment, contextType, isAnswer, parentCommentId, commentUp, pathContext){
    var idUx = (parentCommentId == "") ? idComment : parentCommentId;

    // fait en sorte que ca ne pette pas la recuperation de objectId
    var objectId = $('#commentContent'+ idUx).data('objectid');
    $(idTextArea).css('height', "34px");
    $("#container-txtarea-"+idUx).css('height', "34px");
    autosize($(idTextArea));
    if (notEmpty(visibleCommentsIds.toUpdateComments) && !isAnswer) {
      updateSeenStatus(visibleCommentsIds.toUpdateComments);
      visibleCommentsIds.toUpdateComments.splice(0, visibleCommentsIds.toUpdateComments.length)
    }

    $(idTextArea).on('keyup ', function(e){
      if(e.which == 13 && !e.shiftKey && $(idTextArea).val() != "" && $(idTextArea).val() != " " && !isUpdatedComment) {
        if(!mentionsInit.isSearching){

          //submit form via ajax, this is not JS but server side scripting so not showing here
          saveComment($(idTextArea).val(), parentCommentId, idTextArea, pathContext);
          
          // is activitypub news
          if(isValidURL(objectId)){
              $.post(`${baseUrl}/api/activitypub/comment`, {
                  parentId:idUx,
                  comment:$(idTextArea).val()
              }, function(){

              })
          }
          $(idTextArea).val("");
          $(idTextArea+", #container-txtarea-"+idUx).css('height', 34);

          //var heightTxtArea = $(idTextArea).css("height");
          //$("#container-txtarea-"+idUx).css('height', heightTxtArea);
        }else
          mentionsInit.isSearching=false;
      }
    });

    $(idTextArea).bind ("input propertychange", function(e){
      var heightTxtArea = $(idTextArea).css("height");
          $("#container-txtarea-"+idUx).css('height', heightTxtArea);
    });

    var focusListener = function () {
      handleTextareaFocus(idTextArea, idComment, true);
    }
    var writingListener = function (e) {
      handleWriting(idTextArea, idComment, pathContext, $(this), e.key);
    }
    $(idTextArea)[0] ? $(idTextArea)[0].removeEventListener("focus", focusListener) : "";
    $(idTextArea)[0] ? $(idTextArea)[0].addEventListener("focus", focusListener) : "";
    $(idTextArea)[0] ? $(idTextArea)[0].removeEventListener("keyup", writingListener) : "";
    $(idTextArea)[0] ? $(idTextArea)[0].addEventListener("keyup", writingListener) : "";

    if(contextType=="news"){
      mentionsInit.get(idTextArea);
      if(notNull(commentUp)){
        text=commentUp.text;
        $(idTextArea).val(text);
        //$(idTextArea).mentionsInput("update", data.mentions);
        if(typeof commentUp.mentions != "undefined" && commentUp.mentions.length != 0){
         // text=data.text;
          $.each(commentUp.mentions, function(e,v){
            if(typeof v.slug != "undefined")
              text=text.replace("@"+v.slug, v.name);
          });
          $(idTextArea).val(text);
          $(idTextArea).mentionsInput("update", commentUp.mentions);
        }
      }
    }
  }

  function handleTextareaFocus (idTextArea, idComment, hasFocus = true) {
    updateCommentView(idComment)
  }
  function handleWriting (idTextArea, idComment, path, $this, key) {
    var isCurentLyWriting = false;
    if (key == "Backspace" || key == "Delete") {
      if ($this.val() == "") {
        isCurentLyWriting = false;
      } else {
        isCurentLyWriting = true;
      }
    }
    if ($this.val() != "" && !discussionComments.alreadyNotifyDiscussionMember) {
      if (
        typeof discussionSocket != "undefined" && typeof discussionSocket.emitEvent != "undefined" && 
        userId && typeof userConnected != "undefined" && userConnected.name
      ) {
        discussionSocket.emitEvent(
          wsCO, 
          "change_user_activity", 
          {
            userId: userId, 
            onUpdate: "userWriting", 
            commentId: idComment, 
            userActivity: "writing",
            rubric: path,
          }
        );
      }
      discussionComments.alreadyNotifyDiscussionMember = true;
    } else if ($this.val() == "" && !isCurentLyWriting) {
      if (
        typeof discussionSocket != "undefined" && typeof discussionSocket.emitEvent != "undefined" && 
        userId && typeof userConnected != "undefined" && userConnected.name
      ) {
        discussionSocket.emitEvent(
          wsCO, 
          "change_user_activity", 
          {
            userId: userId, 
            onUpdate: "userWriting", 
            commentId: idComment, 
            userActivity: "stopWriting",
            rubric: path,
          }
        );
        discussionComments.alreadyNotifyDiscussionMember = false;
      }
    }
    if (key != "Backspace" && key != "Delete") {
      isCurentLyWriting = true;
    }
    if (key == "Enter") {
      isCurentLyWriting = false;
      discussionComments.alreadyNotifyDiscussionMember = false
    }
  }

  function showMoreComments(id, hiddenCount){ 
  	mylog.log("showMoreComments", id, hiddenCount);
    $(".hidden-"+hiddenCount).removeClass("hidden");
    $(".link-show-more-" + (hiddenCount-10)).addClass("hidden");
  }

  function hideComments(id, level){
  	mylog.log("#comments-list-"+id + " .item-comment", level);
    //$("#comments-list-"+id + " .item-comment").addClass("hidden");
    if(level<=1){
      $("#commentContent"+id).addClass("hidden");
      //mylog.log("scroll TO : ", $('#newsFeed'+id).position().top, $('#newsHistory').position().top);
      $('.my-main-container').animate({
            scrollTop: $('#newsFeed'+id).position().top + $('#newsHistory').position().top
        }, 400);
    }
    else
      $("#comments-list-"+id).addClass("hidden");
  }



  //When a user already did an action on a comment the other buttons are disabled
  function disableOtherAction(commentId, action,method) {
    mylog.log("disableOtherAction", method);
    if(method){ //unset

      mylog.log("disableOtherAction 1", action);
      $(".reportAbuse[data-id='"+commentId+"']").data("voted", false);

      var count = $(action+"[data-id='"+commentId+"']").data("countcomment");
      mylog.log("count 1", count);
      $(action+"[data-id='"+commentId+"']").data("countcomment", count-1);
      $(action+"[data-id='"+commentId+"'] .countC").html(count-1);
    }
    else{ //set
      mylog.log("disableOtherAction 2", method);
     // $(".commentVoteUp[data-id='"+commentId+"']").removeClass("text-green").data("voted",true);
      //$(".commentVoteDown[data-id='"+commentId+"']").removeClass("text-orange").data("voted",true);
      $(".reportAbuse[data-id='"+commentId+"']").data("voted",true);

      //if (action == ".commentVoteUp") $(".commentVoteUp[data-id='"+commentId+"']").addClass("text-green");
      //if (action == ".commentVoteDown") $(".commentVoteDown[data-id='"+commentId+"']").addClass("text-orange");
      if (action == ".reportAbuse") $(".reportAbuse[data-id='"+commentId+"']").addClass("text-red");

      var count = $(action+"[data-id='"+commentId+"']").data("countcomment");
      $(action+"[data-id='"+commentId+"']").data("countcomment", count+1);
      $(action+"[data-id='"+commentId+"'] .countC").html(count+1);
    }
  }

  function saveComment(textComment, parentCommentId, domElement, path){

    textComment = $.trim(textComment);
    if(!notEmpty(parentCommentId)) parentCommentId = "";
    if(textComment == "") {
      toastr.error(trad.yourcommentisempty);
      return;
    }
    var argval = $("#argval").val();
    newComment={
      parentCommentId: parentCommentId,
      text : textComment,
      contextId : context["_id"]["$id"],
      contextType : contextType,
      argval : argval
    };
    if(notNull(path) && path != "null")
      newComment.path=path;
    newComment=mentionsInit.beforeSave(newComment, domElement);

    const commentPlaceHolderId = "commentPlaceholder" + Math.floor(Math.random() * 1000000)
    var newCommentPlaceholder = $.extend({}, true, newComment)
    newCommentPlaceholder["_id"] = { $id: commentPlaceHolderId }
    showOneCommentPlaceholder(
      newCommentPlaceholder, 
      parentCommentId, parentCommentId!="", commentPlaceHolderId, argval, null);
    $.ajax({
      url: baseUrl+'/'+moduleId+"/comment/save/",
      data: newComment,
      type: 'post',
      global: false,
      dataType: 'json',
      success:
        function(data) {
          if(!data.result){
            toastr.error(data.msg);
          }
          else {
            toastr.success(data.msg);
            if (typeof discussionComments != "undefined" && discussionComments.common && discussionComments.common.updateAnswerBtnNb) {
              discussionComments.common.updateAnswerBtnNb("#footer-comments-"+parentCommentId, parentCommentId)
            }
            var count = $("#footer-"+data.newComment.contextType+"-"+data.newComment.contextId+" .nbNewsComment").text();
            if(!notEmpty(count)) count = 0;
            //mylog.log(count, context["_id"]["$id"]);
            comments[data.id.$id]=data.newComment;
            if(data.newComment.contextType=="news"){
              mentionsInit.reset(domElement);
              count = parseInt(count);
              var newCount = count +1;
              var labelCom = (newCount>1) ? trad.comments : trad.comment;
              $("#footer-"+data.newComment.contextType+"-"+data.newComment.contextId+" .lblComment").html("<i class='fa fa-comment'></i><span class='nbNewsComment'>"+newCount+"</span> "+labelCom);
              $("#newsFeed"+context["_id"]["$id"]+" .newsAddComment").data('count', newCount);
            // }else{
            //  $("#newsFeed"+context["_id"]["$id"]+" .lblComment").html("<i class='fa fa-comment'></i> <span class='nbNewsComment'>1</span> commentaire");
            //  $("#newsFeed"+context["_id"]["$id"]+" .newsAddComment").data('count', 1);
            }
            if(typeof data.newComment.parentId != "undefined" && notEmpty(data.newComment.parentId)){
              var count = $("#footer-comments-"+data.newComment.parentId+" .nbNewsComment").text();
              count=(typeof count == "undefined" || !notEmpty(count)) ? 0 : count;
              var newCount = parseInt(count) +1;
              var labelCom = (newCount>1) ? trad.answers : trad.answer;
              $("#footer-comments-"+data.newComment.parentId+" .lblComment").html("<i class='fa fa-reply fa-rotate-180'></i><span class='nbNewsComment'>"+newCount+"</span> "+labelCom);
            }
            latestComments = data.time;

            var isAnswer = parentCommentId!="";
            mentionsArray=null;
            if(typeof data.newComment.mentions != "undefined"){
              mentionsArray=data.newComment.mentions;
            }
            $("#item-comment-" + commentPlaceHolderId).remove();
            showOneComment(data.newComment, parentCommentId, isAnswer, data.id.$id, argval, mentionsArray);   
            bindEventActions();
            if (
              typeof discussionSocket != "undefined" && typeof discussionSocket.emitEvent != "undefined" && 
              userId && typeof userConnected != "undefined" && userConnected.name
            ) {
              discussionSocket.emitEvent(
                wsCO, 
                "change_comment", 
                {
                  userId: userId, 
                  userConnected: userConnected.name, 
                  onUpdate: "newComment", 
                  commentId: data.id.$id, 
                  newCommentId: data.id.$id,
                  newCommentData: data.newComment
                }
              );
            }
            ajaxPost(null, typeof coWsConfig != "undefined" && coWsConfig.pingRefreshViewUrl ? coWsConfig.pingRefreshViewUrl : null, {
              action : "commentUpdated",
              answerId : newComment.contextId,
              userSocketId : typeof aapObj != "undefined" && aapObj.userSocketId ? aapObj.userSocketId : (exists(coWsData) && notEmpty(coWsData) && notEmpty(coWsData.id) ? coWsData.id : null),
              emiterSocketId: (typeof coWsData != "undefined" && coWsData && coWsData.id ? coWsData.id : null),
              responses : data,
            }, null, null, {contentType : 'application/json'});

            if(typeof commentObj.afterSaveReload != "undefined")
              commentObj.afterSaveReload()
          }
        },
      error:
        function(data) {
          toastr.error(trad.somethingwentwrong);
        }
    });

  }

  function updateComment(id, newText,dom){
    newComment=new Object;
    newComment.text=newText;
    newComment=mentionsInit.beforeSave(newComment, dom);
   // updateField("Comment",id,"text",newComment.text,false, true);
    $.ajax({
      type: "POST",
      url: baseUrl+"/"+moduleId+"/comment/update",
      data: { "id" : id ,"params" : newComment },
      success: function(data){
        if(data.result) {
          comments[id].text=newComment.text;
          if(typeof newComment.mentions != "undefined"){
          // updateField("Comment",id,"mentions",newComment.mentions,false, true);
            newComment.text = mentionsInit.addMentionInText(newComment.text,newComment.mentions);
            comments[id].mentions=newComment.mentions;
          }
          $('.text-comment-'+id).html(newComment.text);
          toastr.success(data.msg);
          }
        else
          toastr.error(data.msg);
      },
      dataType: "json"
    });
  }
  function showOneComment(newComment, idComment, isAnswer, idNewComment, argval, mentionsArray, extraParams = {}){
  	//mylog.log("comments.js showOneComment ", newComment, idComment, isAnswer, idNewComment, argval, mentionsArray);
    textComent=mentionAndLinkify(idNewComment,newComment, true);
    idContainer=(typeof newComment.parentId != "undefined" && notEmpty(newComment.parentId)) ? newComment.parentId : newComment.contextId;
    const hideIfNbDaySup = extraParams.ifNbDaySup ? extraParams.ifNbDaySup : null;
    const showDateOnly = extraParams.showDateOnly ? extraParams.showDateOnly : null;
    var returnDate = directory.showDatetimePost(newComment.collection, idNewComment, (newComment.postedDate ? newComment.postedDate : newComment.created), hideIfNbDaySup, showDateOnly);
    var classArgument = "";
    if(argval == "up") classArgument = "bg-green-comment";
    if(argval == "down") classArgument = "bg-red-comment";
    if(argval == "") classArgument = "bg-white-comment";
    var commentAuthor = $.extend(true, {}, userConnected)
    if (newComment && newComment.authorData) {
      commentAuthor = $.extend(true, {}, newComment.authorData)
    }
    imgCommentUser=(typeof commentAuthor.profilThumbImageUrl != "undefined" && commentAuthor.profilThumbImageUrl!="")? baseUrl+commentAuthor.profilThumbImageUrl:parentModuleUrl+'/images/thumbnail-default.jpg';
    var html = `
      <div class="col-xs-12 no-padding margin-top-5 item-comment ${ classArgument }" id="item-comment-${ idNewComment }">
        <img src="${ imgCommentUser }" class="img-responsive pull-left img-circle"
            style="margin-right:5px;margin-top:10px;height:32px;width:32px;"
        >
        <span class="pull-left content-comment col-xs-12 no-padding">            
          <span class="text-black col-xs-12 comment-container-white">
            <span class="text-dark"><strong>${ commentAuthor.name }</strong></span>
            <span class="timeline-comments-${ idNewComment } pull-right date-com"></span><br>
            <span class="text-comment text-comment-${ idNewComment } col-xs-12 no-padding"> ${ textComment } </span>
          </span><br>
          <small class="bold">
            <div class="col-xs-12 pull-left no-padding" id="footer-comments-${ idNewComment }" style="padding-left:15px !important;"></div>
          </small>
        </span>  
        <div id="comments-list-${ idNewComment }" class="hidden pull-left col-xs-11 no-padding answerCommentContainer"></div>
      </div>`;
    if(!isAnswer){
      $("#comments-list-"+idContainer).prepend(html);
      $("#comments-list-"+idContainer).find(".noComment").remove();
      $(".timeline-comments-"+idNewComment).html(returnDate);
      if($("#comments-list-"+idContainer).length <= 0 && typeof idParentProposal != "undefined" && idParentProposal != "" && $("#comments-list-"+idParentProposal).length){
          $("#comments-list-"+idParentProposal).prepend(html);
          $("#comments-list-"+idParentProposal).find(".noComment").remove();
          $(".timeline-comments-"+idNewComment).html(returnDate);
      }
    }else{
      if ($("#comments-list-"+idContainer + " > .item-comment").length > 0 && $("#comments-list-"+idContainer + " > .item-comment").last().length > 0) {
          $("#comments-list-"+idContainer + " > .item-comment").last().after(html);
      } else {
        $('#container-txtarea-'+idContainer).after(html);
      }
      $(".timeline-comments-"+idNewComment).html(returnDate);
    }
    initCommentsTools({newComment}, "comments", true, idComment);
  }
  function showOneCommentPlaceholder(newComment, idComment, isAnswer, idNewComment, argval, mentionsArray, extraParams = {}){
    textComent=mentionAndLinkify(idNewComment,newComment, true);
    idContainer=(typeof newComment.parentCommentId != "undefined" && notEmpty(newComment.parentCommentId)) ? newComment.parentCommentId : newComment.contextId;
    const hideIfNbDaySup = extraParams.ifNbDaySup ? extraParams.ifNbDaySup : null;
    const showDateOnly = extraParams.showDateOnly ? extraParams.showDateOnly : null;
    var returnDate = "";
    var classArgument = "";
    if(argval == "up") classArgument = "bg-green-comment";
    if(argval == "down") classArgument = "bg-red-comment";
    if(argval == "") classArgument = "bg-white-comment";
    var commentAuthor = $.extend(true, {}, userConnected)
    if (newComment && newComment.authorData) {
      commentAuthor = $.extend(true, {}, newComment.authorData)
    }
    imgCommentUser=(typeof userConnected.profilThumbImageUrl != "undefined" && userConnected.profilThumbImageUrl!="")? baseUrl+commentAuthor.profilThumbImageUrl:parentModuleUrl+'/images/thumbnail-default.jpg';
    var html = `
      <div class="col-xs-12 no-padding margin-top-5 item-comment ${ classArgument } comment-placeholder" id="item-comment-${ idNewComment }">
        <img src="${ imgCommentUser }" class="img-responsive pull-left img-circle"
            style="margin-right:5px;margin-top:10px;height:32px;width:32px;"
        >
        <span class="pull-left content-comment col-xs-12 no-padding">            
          <span class="text-black col-xs-12 comment-container-white">
            <span class="text-dark"><strong>${ commentAuthor.name }</strong></span>
            <span class="timeline-comments-${ idNewComment } pull-right date-com"></span><br>
            <span class="text-comment text-comment-${ idNewComment } col-xs-12 no-padding"> ${ textComment } </span>
          </span><br>
          <small class="bold">
            <span class="text-comment bs-ml-4 comment-loader col-xs-12 no-padding"> Publication <img width="20" src="${ modules.co2.url + "/images/three-dot-load.gif" }"/> </span>
            <div class="col-xs-12 pull-left no-padding" id="footer-comments-${ idNewComment }" style="padding-left:15px !important;"></div>
          </small>
        </span>  
        <div id="comments-list-${ idNewComment }" class="hidden pull-left col-xs-11 no-padding answerCommentContainer"></div>
      </div>`;
    if(!isAnswer){
      $("#comments-list-"+idContainer).prepend(html);
      $("#comments-list-"+idContainer).find(".noComment").remove();
      $(".timeline-comments-"+idNewComment).html(returnDate);
      if($("#comments-list-"+idContainer).length <= 0 && typeof idParentProposal != "undefined" && idParentProposal != "" && $("#comments-list-"+idParentProposal).length){
          $("#comments-list-"+idParentProposal).prepend(html);
          $("#comments-list-"+idParentProposal).find(".noComment").remove();
          $(".timeline-comments-"+idNewComment).html(returnDate);
      }
    }else{
      // if ($("#comments-list-"+idContainer + " > .item-comment").length > 0 && $("#comments-list-"+idContainer + " > .item-comment").last().length > 0) {
      //     $("#comments-list-"+idContainer + " > .item-comment").last().after(html);
      // } else {
        $('#container-txtarea-'+idContainer).after(html);
      // }
      $(".timeline-comments-"+idNewComment).html(returnDate);
    }
  }
/*  function linkify(inputText) {
      var replacedText, replacePattern1, replacePattern2, replacePattern3;

      //URLs starting with http://, https://, or ftp://
      replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
      replacedText = inputText.replace(replacePattern1, '<a href="$1" target="_blank" class="text-azure">$1</a>');

      //URLs starting with "www." (without // before it, or it'd re-link the ones done above).
      replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
      replacedText = replacedText.replace(replacePattern2, '$1<a href="http://$2" target="_blank" class="text-azure">$2</a>');

      //Change email addresses to mailto:: links.
      //replacePattern3 = /(([a-zA-Z0-9\-\_\.])+@[a-zA-Z\_]+?(\.[a-zA-Z]{2,6})+)/gim;
      //replacedText = replacedText.replace(replacePattern3, '<a href="mailto:$1">$1</a>');

      return replacedText;
  }*/



  function appendAnswerCommentContent(idComment, parentCommentId,contextType, path){ 
    //si l'input existe déjà on sort
    if($('#container-txtarea-'+parentCommentId).length > 0) return;
    var html = "";
    html = '<div id="container-txtarea-'+parentCommentId+'" class="content-new-comment">' +

            '<img src="'+ (userConnected.profilThumbImageUrl ? userConnected.profilThumbImageUrl : modules.co2.url + "/images/thumb/default_citoyens.png") +'" class="img-responsive pull-left img-circle" '+
            '  style="margin-right:10px;height:32px;width:32px">'+
          
            '<div class="ctnr-txtarea">';

    html +=   '<textarea rows="1" style="height:1em;" class="form-control textarea-new-comment" ' +
                      'id="textarea-new-comment'+parentCommentId+'" placeholder="'+trad.youranswer+'..."></textarea>' +
            
            '</div>' +
          '</div>';
    if ($("#comments-list-"+parentCommentId + " > .item-comment").length > 0) {
      $("#comments-list-"+parentCommentId + " > .item-comment").last().after(html);
    } else {
      $("#comments-list-"+parentCommentId).prepend(html);
    }

    bindEventTextArea('#textarea-new-comment'+parentCommentId, idComment, contextType, true, parentCommentId, null, path);
      
    
    
  }
  function answerComment(idComment, parentCommentId,contextType, path){ 
    if(userId == ""){
      $('#modalLogin').modal({
        show:true
      })
      return false;
    }
    //mylog.log("answerComment", parentCommentId, $("#comments-list-"+parentCommentId).hasClass("hidden"));

    	if($("#argval").length > 0){
		  $(".textarea-new-comment").removeClass("bg-green-comment bg-red-comment");
		  $(".textarea-new-comment").addClass("bg-white-comment").attr("placeholder", "Votre commentaire");
		  $("#argval").val("");
		}

    updateCommentView(idComment)

		if(!$("#comments-list-"+parentCommentId).hasClass("hidden"))
		  $("#comments-list-"+parentCommentId).addClass("hidden");
		else
		  $("#comments-list-"+parentCommentId).removeClass("hidden");

		//si l'input existe déjà on sort
		if($('#container-txtarea-'+parentCommentId).length > 0) return;
		var html = "";
		html = '<div id="container-txtarea-'+parentCommentId+'" class="content-new-comment">' +

		        '<img src="'+profilThumbImageUrlUser+'" class="img-responsive pull-left img-circle" '+
		        '  style="margin-right:10px;height:32px;width:32px">'+

		        '<div class="ctnr-txtarea">';

		html +=   '<textarea rows="1" style="height:1em;" class="form-control textarea-new-comment" ' +
		                  'id="textarea-new-comment'+parentCommentId+'" placeholder="'+trad.youranswer+'..."></textarea>' +

		        '</div>' +
		      '</div>';
		$("#comments-list-"+parentCommentId).prepend(html);

		bindEventTextArea('#textarea-new-comment'+parentCommentId, idComment, contextType, true, parentCommentId, null, path);



  }
  function reportAbuse(obj) {
    var message = "<div id='reason' class='radio'>"+
      "<h3 class='margin-top-10'>"+trad.whyareyoureportabuse+" ?</h3>" +
      "<hr>" +
      "<label><input type='radio' name='reason' value='Propos malveillants' checked>"+trad.eviltongues+"</label><br>"+
      "<label><input type='radio' name='reason' value='Incitation et glorification des conduites agressives'>"+trad.incitementagressivedriving+"</label><br>"+
      "<label><input type='radio' name='reason' value='Affichage de contenu gore et trash'>"+trad.displaygorytrash+"</label><br>"+
      "<label><input type='radio' name='reason' value='Contenu pornographique'>"+trad.pornographiccontent+"</label><br>"+
        "<label><input type='radio' name='reason' value='Liens fallacieux ou frauduleux'>"+trad.deceitfullinks+"</label><br>"+
        //"<label><input type='radio' name='reason' value='Mention de source erronée'>Mention de source erronée</label><br>"+
        "<label><input type='radio' name='reason' value='Violations des droits auteur'>"+trad.copyrightinfringement+"</label><br><br>"+
        "<textarea class='form-control' style='resize: vertical; height: 100px;' id='reasonComment' placeholder='"+trad["Leave your comment"]+"...'></textarea><br>"+
        trad.explainmoderationprocess+" <a href='"+baseUrl+"/doc/Conditions%20G%C3%A9n%C3%A9rales%20d\'Utilisation.pdf' target='_blank'>"+trad.generaltermsofuse+"</a><br>" +
      "<span class='text-red'><i class='fa fa-info-circle'></i> "+trad.allreportisdefinive+"</span><br>" +
     // "<hr>" +
     // "<span class=''><i class='fa fa-arrow-right'></i> Le contenu sera signalé par un <i class='fa fa-flag text-red'></i> s'il fait l'objet d'au moins 2 signalements</span><br>" +
      //"<span class='text-red-light'><i class='fa fa-arrow-right'></i> Le contenu sera masqué s'il fait l'objet d'au moins 5 signalements</span><br>" +
      //"<span class=''><i class='fa fa-arrow-right'></i> Le contenu sera supprimé par les administrateurs s'il enfreint les conditions d'utilisations</span>" +
      "</div>";
    var boxComment = bootbox.dialog({
      message: message,
      title: '<span class="text-red"><i class="fa fa-flag"></i> '+trad.reportanabuse,
      buttons: {
        annuler: {
          label: trad.cancel,
          className: "btn-default",
          callback: function() {}
        },
        danger: {
          label: trad.sendreport,
          className: "btn-danger",
          callback: function() {
            // var reason = $('#reason').val();
        details={
          reason : $("#reason input[type='radio']:checked").val(),
          comment : $("#reasonComment").val()
        }
        actionOnMedia(obj, "reportAbuse", false,details);
        //actionAbuse(comment, "reportAbuse", details);
        disableOtherAction(obj.data("id"), '.reportAbuse');
        //copyCommentOnAbuseTab(comment);
        return true;
          }
        },
      }
    });

    boxComment.on("shown.bs.modal", function() {
      $.unblockUI();
    });

    boxComment.on("hide.bs.modal", function() {
      $.unblockUI();
    });
}
function showModalReactions(obj) {
    var message = "<div id='reactionsContent'>"+
        "<i class='fa fa-spin fa-spinner'></i>"+
      "</div>";
    var boxComment = bootbox.dialog({
      message: message,
      title: ' ',
      buttons: {
        annuler: {
          label: trad.close,
          className: "btn-default",
          callback: function() {}
        }
      }
    });

    boxComment.on("shown.bs.modal", function() {
      $.unblockUI();
    });

    boxComment.on("hide.bs.modal", function() {
      $.unblockUI();
    });
}
