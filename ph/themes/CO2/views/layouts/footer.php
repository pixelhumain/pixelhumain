<?php $layoutPath = 'webroot.themes.'.Yii::app()->theme->name.'.views.layouts.'; ?>

<style>
    .footer-above{
        background-color: #F2F2F2!important;
    }
    .btn-outline{
        background-color: rgba(255,255,255,0.5);
    }
    .btn-outline.join-us{
        background-color: #2C3E50;
        border: 2px solid #2C3E50;
        color: #f5e414;
        font-size: 35px;
        margin-top: -7px;
    }
    .btn-outline.join-us:hover{
        color: #2C3E50;
        background-color: #f5e414;
    }
    .font-blackoutT{
        color: #2C3E50;
    }
    .col-footer-ph{
        line-height: 70px;
    }

    .col-footer-ph .font-blackoutT, .col-footer-ph .join-us {
        vertical-align: middle;
        line-height: 50px !important;
    }
    @media screen and (max-width: 1024px) {

        .col-footer a.lbh{
            padding: 4px;
            display: inline-block;
        }
    }

    @media (max-width: 768px) {

        .col-footer-ph{
            text-align: right!important
        }
    }


</style>

<?php
$themeParams = $this->appConfig;  

$costum = $this->costum;
if (isset($costum["htmlConstruct"]["footer"]["activated"]) && $costum["htmlConstruct"]["footer"]["activated"] || @$useFooter) {
    if (@$useFooter) {
        $costum = CacheHelper::getCostum();
        $costum["editMode"] = true;
    }
    $footerCMS = Cms::getCmsFooter($costum["contextId"]);
    if (!empty($footerCMS)) {
        $footerCMSId = array_keys($footerCMS)[0];
        $footerCMS = $footerCMS[$footerCMSId];

        $data = array(
            "v" => $footerCMS,
            "titleArray" => array(),
            "el" => [],
            "page" => "allpages",
            "cmsList" => array(),
            "costumData" =>$costum,
            "firstLevelBlock"=>true,
            "canEdit"    => $costum["editMode"]);
        if (isset($footerCMS["path"])) {
            $path = $footerCMS["path"];
            $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
            $pathExplode = explode('.', $footerCMS["path"]);
            $count = count($pathExplode);
            $content = isset($footerCMS['content']) ? $footerCMS['content'] : [];
            $kunik = $pathExplode[$count - 1] . $footerCMS["_id"];
            $blockKey = (string)$footerCMS["_id"];
            $blockName = (string)@$footerCMS["name"];
            $params = [
                "cmsList"   => array(),
                "blockKey"  => $blockKey,
                "blockCms"  => $footerCMS,
                "page"      => "footer",
                "canEdit"   => $costum["editMode"],
                "type"      => $path,
                "kunik"     => $kunik,
                "content"   => $content,
                'blockName' => $blockName,
                "costum"    => $costum,
                "el"=> [],
                'range'     => @$i,
                "defaultImg" => Yii::app()->controller->module->assetsUrl . "/images/thumbnail-default.jpg"
            ];
            $width = [
                "modeLg" =>  isset($footerCMS["modeLg"]) ? $footerCMS["modeLg"] : "12",
                "modeMd" =>  isset($footerCMS["modeMd"]) ? $footerCMS["modeMd"] : "12",
                "modeSm" =>  isset($footerCMS["modeSm"]) ? $footerCMS["modeSm"] : "12",
                "modeXs" =>  isset($footerCMS["modeXs"]) ? $footerCMS["modeXs"] : "12"
            ]; ?>
            <?php if($costum["editMode"]) { ?>
                <div class="text-center" style="position: absolute;bottom: -35px;z-index: 100;width: 100%;">
                    <button class="btn btn-primary editFooter" data-id="<?= $footerCMSId ?>" data-kunik="<?= $kunik ?>"><?php echo Yii::t("common", "Edit")?> footer</button>
                </div>
            <?php } ?>

            <?php 
            if (is_file($this->getViewFile("costum.views." . $path, $params))) {
                $dataAnchorTarget = (!empty($footerCMS["anchorTarget"])) ? " data-anchor-target='" . substr($footerCMS["anchorTarget"], 1) . "' " : "";
                $classLoading = "";
                ?>

                <div class="block-footer sp-footer sortable-<?= $kunik ?> col-xs-12 no-padding col-lg-<?= $width["modeLg"] ?> col-md-<?= $width["modeMd"] ?> col-sm-<?= $width["modeSm"] ?> col-xs-<?= $width["modeXs"] ?> block-container-<?= $kunik ?> <?= $classLoading ?>">
                    <?php
                    echo $this->renderPartial("costum.views." . $path, $params); 
                    ?>
                </div>
                <script type="text/javascript">
                    $(".block-footer .cmsbuilder-block").removeClass("cmsbuilder-block");
                    $(".block-footer .cmsbuilder-block").removeClass("cmsbuilder-block-droppable");
                    $(".block-footer .sp-text").removeClass("sp-text");
                </script>
                <?php
            } else { ?>
                <div class="col-xs-12 text-center" id="<?php echo (string)$footerCMS["_id"]; ?>">
                    <?php echo $this->renderPartial("costum.views.tpls.blockNotFound", ["blockKey" => $blockKey]) ?>
                </div>
                <?php
            }
        }
    }

}else if(isset($themeParams["footer"]) && !empty($themeParams["footer"])){ 
    if(isset($themeParams["footer"]) && isset($themeParams["footer"]["url"])) {
        echo $this->renderPartial( $themeParams["footer"]["url"] );
    }else{ /*?>
        <footer class="text-center col-xs-12 pull-left no-padding">
            <div class="col-xs-12 col-footer-ph text-right">
                <span class="font-blackoutT" style="font-size:20px;">Power by</span> 
                <a href="https://www.communecter.org/#@pixelhumain" target="_blank">
                    <img class="lzy_img" src="" data-src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/img/LOGO_PIXEL_HUMAIN.png" style="margin-top: -20px;" height=70>
                </a>
                <span class="font-blackoutT" style="font-size:20px;margin-left: -11px;">Join</span>
                <a href="https://gitlab.adullact.net/pixelhumain" target="_blank" class="btn-social btn-outline join-us">
                    <i class="fa fa-fw fa-gitlab"></i>
                </a>
            </div>

        </footer>
        <script type="text/javascript">
            jQuery(document).ready(function() { 
                mylog.log("render footer ","/pixelhumain/ph/themes/CO2/views/layouts/footer.php")

            });
        </script>
        <?php */} 
} ?>



