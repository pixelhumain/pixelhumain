<?php
/**
 * @var $this \PixelHumain\PixelHumain\components\View
 */
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\Config;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\MatomoTrackerWidget;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActor;
use PixelHumain\PixelHumain\modules\costum\components\CmsBuilderWidget;
use Preference;

\PixelHumain\PixelHumain\assets\MainSearchLayoutAsset::register($this);
\PixelHumain\PixelHumain\assets\Yii1Assets::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>

<!-- ****************************** THEME CO2 : mainSearch 2 ******************************-->
<?php

$layoutPath = '@themes/' . Yii::app()->theme->name . '/views/layouts/';
$themeAssetsUrl = Yii::app()->theme->baseUrl . '/assets';
$parentModuleId = (@Yii::app()->params["module"]["parent"]) ?  Yii::app()->params["module"]["parent"] : $this->module->id;
$modulePath = (@Yii::app()->params["module"]["parent"]) ?  "../../../" . $parentModuleId . "/views"  : "..";
$versionAssets = (@Yii::app()->params["versionAssets"]) ?  Yii::app()->params["versionAssets"]  : Yii::app()->params["version"];

$cs = Yii::app()->getClientScript();

$CO2DomainName = isset(Yii::app()->params["CO2DomainName"]) ? Yii::app()->params["CO2DomainName"] : "CO2";

//Network::getNetworkJson(Yii::app()->params['networkParams']);

if (!isset($this->appConfig))
    $this->appConfig = CO2::getThemeParams();

$metaTitle = (isset($this->module->pageTitle)) ? $this->module->pageTitle : $this->appConfig["metaTitle"];
$metaDesc = (isset($this->module->description)) ? $this->module->description : @$this->appConfig["metaDesc"];
$metaAuthor = (isset($this->module->author)) ? $this->module->author : @$this->appConfig["metaAuthor"];
$metaImg = (isset($this->module->image)) ? Yii::app()->getRequest()->getBaseUrl(true) . $this->module->image : Yii::app()->getRequest()->getBaseUrl(true) . "/" . @$this->appConfig["metaImg"];
$metaRelCanoncial = (isset($this->module->relCanonical)) ? $this->module->relCanonical : "https://www.communecter.org";
$keywords = "";
if (isset($this->module->keywords))
    $keywords = $this->module->keywords;
else if (isset($this->keywords))
    $keywords = $this->keywords;
if (isset($this->module->favicon))
    $favicon = $this->module->favicon;
else
    $favicon = (isset($this->module->assetsUrl)) ? $this->module->assetsUrl . "/images/favicon.ico" : "/images/favicon.ico";

$params = $this->appConfig;
?>

<html lang="en" class="no-js">

<head>

    <?php $this->head() ?>
    <title>
        <?php

            $title = (Yii::t("common", $metaTitle) != "") ? Yii::t("common", $metaTitle) : (isset($this->costum["name"]) ? $this->costum["name"] : $this->costum["title"]);

            if ( isset($this->costum) ) {
                if ( $title != "" && (isset($this->costum["editMode"]) && ($this->costum["editMode"] == "true")) ) {
                    if($title != "") {
                        $title = "CMS: " . $title;
                    } else {
                        $title = "CMS: " . $this->costum["slug"];
                    }
                }
            }
            echo $title;
        ?>
    </title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1">-->
    <meta name="viewport" content="width=device-width, height=device-height, viewport-fit=cover, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no" />
    <meta name="title" content="<?php echo $metaTitle; ?>">
    <meta name="description" content="<?php echo $metaDesc; ?>">
    <meta name="author" content="<?php echo $metaAuthor; ?>">
    <meta property="og:image" content="<?php echo $metaImg; ?>" />
    <meta property="og:description" content="<?php echo $metaDesc; ?>" />
    <meta property="og:title" content="<?php echo $metaTitle; ?>" />
    <meta property="og:url" content="<?php echo $metaRelCanoncial; ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:locale" content="fr_FR" />
    <meta property="og:site_name" content="<?php echo Yii::app()->getRequest()->getBaseUrl(true); ?>" />
    <meta name="keywords" lang="<?php echo Yii::app()->language; ?>" content="<?php echo CHtml::encode($keywords); ?>">

    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:description" content="<?php echo $metaDesc; ?>">
    <meta name="twitter:image" content="<?php echo $metaImg; ?>">
    <meta name="twitter:title" content="<?php echo $metaTitle; ?>">
    <meta name="twitter:type" content="website">
    <?php
    $localActorAp = "";
    $apHost = "";
    if (isset($_SESSION["user"]["username"])) {
        if (isset($_SESSION["user"]["preferences"]) && Preference::isActivitypubActivate($_SESSION["user"]["preferences"])) {
            $apHost = Config::HOST();
            $userConnected = ActivitypubActor::getCoPersonAsActor($_SESSION["user"]["username"]);
            if (!empty($userConnected)) {
                $localActorAp = $userConnected->get('id');
            }
        }
    }
    $matomoWidget = MatomoTrackerWidget::widget([
        'costum' => isset($this->costum) ? $this->costum : null
    ]);
    echo $matomoWidget;
    ?>
    <script type="text/javascript">
        var apHost = "<?php echo $apHost; ?>";
        var activitypubActorId = "<?php echo $localActorAp; ?>";
        var imageObserver = new IntersectionObserver((entries, imgObserver) => {
            entries.forEach((entry) => {
                if (entry.isIntersecting) {
                    const lazyImage = entry.target;
                    lazyImage.onload = () => {
                        //console.log('loagimglazy');
                        if(typeof lazyImage.callBackOnload == "function")
                            lazyImage.callBackOnload(entry.target);
                        else if (lazyImage.dom) {
                            $(lazyImage.dom).masonry();
                        }
                    }
                //    lazyImage.src = lazyImage.dataset.src;
                    //if(typeof lazyImage.dataset.src != "undefined")
                  lazyImage.src = lazyImage.dataset.src.replace("http://localhost:5080/", "https://www.communecter.org/");
                    lazyImage.classList.remove("lzy_img", "isLoading");
                    imgObserver.unobserve(lazyImage);
                    //console.log(lazyImage.dom);

                }
            })
        }, {
            rootMargin: "50px 0px 0px 0px",
            threshold: 0
        });

        var versionAssets = '<?php echo $versionAssets; ?>';
        // console.warn("render mainSearch pageContent","/pixelhumain/ph/themes/CO2/views/layouts/mainSearch.php")
        <?php if (isset($_GET['_escaped_fragment_'])) { ?>
            window.location.hash = '#<?php echo $_GET['_escaped_fragment_'] ?>';
        <?php
        }
        ?>
        // console.log("hash 0",window.location.hash);
        if (window.location.hash.indexOf('#!') === 0) {
            var hash = window.location.hash.substr(2);
            // console.log("hash",hash);
            window.location.hash = '#' + hash;
            // console.log("window.location.hash",window.location.hash);
        }

    </script>
    <link rel='shortcut icon' type='image/x-icon' href="<?php echo $favicon; ?>" />
    <link rel="canonical" href="<?php echo $metaRelCanoncial ?>" />
    <link rel="stylesheet" href="/js/preact/dist/assets/index-436038b4.css"/>
    <script  type="module" src="/js/preact/dist/assets/index-252a5a2e.js"></script>
    <script  type="module" src="/js/custom-elements/spotlight.js"></script>
    <!-- socket io -->
    <script src="https://cdn.socket.io/4.1.1/socket.io.min.js" integrity="sha384-cdrFIqe3RasCMNE0jeFG9xJHog/tgOVC1E9Lzve8LQN1g5WUHo0Kvk1mawWjxX7a" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/baffle@0.3.6/dist/baffle.min.js"></script>

    <?php if (Yii::app()->params["forceMapboxActive"] == true &&  Yii::app()->params["mapboxActive"] == true) { ?>
        <script src='https://api.mapbox.com/mapbox.js/v3.2.1/mapbox.js'></script>
        <link href='https://api.mapbox.com/mapbox.js/v3.2.1/mapbox.css' rel='stylesheet' />
        

        <!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css"> !-->

    <?php }


    /* ***********************
            add to HEAD
            ************************ */
    // CA SERT PAS CAR CA PASSE PAR registerScriptFile
    echo "<!-- start: MAIN JAVASCRIPTS -->";
    echo "<!--[if lt IE 9]>";
    //$cs->registerScriptFile(Yii::app()->request->baseUrl.'/plugins/respond.min.js' , CClientScript::POS_HEAD);
    //$cs->registerScriptFile(Yii::app()->request->baseUrl. '/plugins/excanvas.min.js' , CClientScript::POS_HEAD);
    //$cs->registerScriptFile(Yii::app()->request->baseUrl. '/plugins/jQuery/jquery-1.11.1.min.js' , CClientScript::POS_HEAD);
    echo "<![endif]-->";
    echo "<!--[if gte IE 9]><!-->";
    //           $cs->registerScriptFile(Yii::app()->request->baseUrl. '/plugins/jQuery/jquery-2.1.1.min.js' , CClientScript::POS_HEAD);
    /* HtmlHelper::registerCssAndScriptsFiles(array(
                 '/plugins/jQuery/jquery-2.1.1.min.js'
                ), null);*/
    echo "<!--<![endif]-->";
    $cssAnsScriptFilesModule = array(
        '/themes/CO2/assets/css/font-montserrat.css',
        '/themes/CO2/assets/css/font-lato.css',
        '/plugins/toastr/toastr.js',
        '/plugins/toastr/toastr.min.css'
    );

    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule);

    /* ***********************
            add to HEAD
            ************************ */

    /* ***********************
            ph core stuff
            ************************ */
    $cssAnsScriptFilesModule = array(
        '/plugins/jquery-ui-1.12.1/jquery-ui.min.js',
        '/plugins/jquery-ui-1.12.1/jquery-ui.min.css',

        '/plugins/jquery-validation/dist/jquery.validate.min.js',
        '/plugins/bootbox/5.3.2/bootbox.all.min.js',
        '/plugins/blockUI/jquery.blockUI.js',
        '/plugins/toastr/toastr.js',
        '/plugins/toastr/toastr.min.css',
        '/plugins/jquery-cookie/jquery.cookie.js',
        '/plugins/lightbox2/css/lightbox.css',
        '/plugins/lightbox2/js/lightbox.min.js',
        '/themes/CO2/assets/vendor/bootstrap/js/bootstrap.min.js',
        '/themes/CO2/assets/vendor/bootstrap/css/bootstrap.min.css',
        '/themes/CO2/assets/css/bs-space-layout.css',
        '/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css',
        '/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js',


        '/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js',
        '/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css',
        '/plugins/jquery-cookieDirective/jquery.cookiesdirective.js',
        '/plugins/ladda-bootstrap/dist/spin.min.js',
        '/plugins/ladda-bootstrap/dist/ladda.min.js',
        '/plugins/ladda-bootstrap/dist/ladda.min.css',
        '/plugins/ladda-bootstrap/dist/ladda-themeless.min.css',
        '/plugins/animate.css/animate.min.css',
        '/plugins/masonry/masonry.pkgd.min.js',
        '/plugins/jQuery-contextMenu/dist/jquery.contextMenu.min.js',
        '/plugins/jQuery-contextMenu/dist/jquery.contextMenu.min.css',
        '/plugins/jQuery-contextMenu/dist/jquery.ui.position.min.js',

        '/plugins/select2/select2.min.js',
        '/plugins/select2/select2_locale_' . Yii::app()->language . '.js',
        '/plugins/select2/select2.css',

        '/plugins/moment/min/moment.min.js',
        '/plugins/moment/min/moment-with-locales.min.js',
        '/plugins/moment-timezone/moment-timezone-with-data.js',
        '/plugins/jquery.dynForm.js',
        '/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
        '/plugins/jquery.dynSurvey/jquery.dynSurvey.js',

        '/plugins/jquery.elastic/elastic.js',
        '/plugins/underscore-master/underscore.js',
        '/plugins/jquery-mentions-input-master/jquery.mentionsInput.js',
        '/plugins/jquery-mentions-input-master/jquery.mentionsInput.css',
        '/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
        '/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
        '/plugins/bootstrap-tagsinput/src/bootstrap-tagsinput.js',
        '/plugins/bootstrap-tagsinput/src/bootstrap-tagsinput.css',
        //'/js/cookie.js' ,
        '/js/api.js',
        //'/plugins/animate.css/animate.min.css',
        '/plugins/font-awesome/css/font-awesome.min.css',
        '/plugins/font-awesome/list.js',
        '/js/axios.js',

        //'/plugins/font-awesome-custom/css/font-awesome.css',

        '/plugins/cryptoJS-v3.1.2/rollups/aes.js',
        //FineUplaoder (called in jquery.dynform.js)
        '/plugins/fine-uploader/jquery.fine-uploader/fine-uploader-gallery.css',
        '/plugins/fine-uploader/jquery.fine-uploader/jquery.fine-uploader.js',
        '/plugins/fine-uploader/jquery.fine-uploader/fine-uploader-new.min.css',
        '/plugins/autosize/autosize.js',
        '/plugins/jquery-simplePagination/jquery.simplePagination.js',
        '/plugins/jquery-simplePagination/simplePagination.css',

        '/plugins/facemotion/faceMocion.css',
        '/plugins/facemotion/faceMocion.js',
        '/plugins/showdown/showdown.min.js',
        '/plugins/fullcalendar/fullcalendar/fullcalendar.min.js',
        '/plugins/fullcalendar/fullcalendar/fullcalendar.css',
        '/plugins/fullcalendar/fullcalendar/locale/' . Yii::app()->language . '.js',
        '/plugins/to-markdown/to-markdown.js',
        '/plugins/awesome-qr.js',
        '/plugins/co-emoji-picker/co-emoji-picker.js',
        '/plugins/co-emoji-picker/co-emoji-picker.css',
        '/plugins/html5sortable/html5sortable.js',
        '/plugins/jquery-smartwizard-v6/dist/js/jquery.smartWizard.min.js',
        '/plugins/jquery-smartwizard-v6/dist/css/smart_wizard_all.min.css'
    );
    if (Yii::app()->language != "en")
        array_push($cssAnsScriptFilesModule, "/plugins/jquery-validation/localization/messages_" . Yii::app()->language . ".js");
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, null);
    /* ***********************
            END ph core stuff
            ************************ */
    $cssAnsScriptFilesModuleAll = array();
    $cssAnsScriptFilesModuleAll = array_merge($cssAnsScriptFilesModuleAll, $cssAnsScriptFilesModule);

    /* ***********************
            module stuff
            ************************ */

    $moduleAssets = (@Yii::app()->params["module"]["parent"]) ?  Yii::app()->getModule(Yii::app()->params["module"]["parent"])->getAssetsUrl()  : $this->module->assetsUrl;
    HtmlHelper::registerCssAndScriptsFiles(
        CO2::getModulesRessources(),
        $moduleAssets
    );

    $cssAnsScriptFilesModuleAll = array_merge($cssAnsScriptFilesModuleAll, CO2::getModulesRessources());

    HtmlHelper::registerCssAndScriptsFiles(
        array('/js/default/formInMap.js'),
        $moduleAssets
    );

    $cssAnsScriptFilesModuleAll = array_merge($cssAnsScriptFilesModuleAll, array('/js/default/formInMap.js'));

    HtmlHelper::registerCssAndScriptsFiles(
        array('/js/uiCoop.js'),
        Yii::app()->getModule("dda")->getAssetsUrl()
    );

    $cssAnsScriptFilesModuleAll = array_merge($cssAnsScriptFilesModuleAll, array('/js/uiCoop.js'));

    /* ***********************
            END module stuff
            ************************ */

    /* ***********************
            theme stuff
            ************************ */
    $cssAnsScriptFilesModule = array(
        //'/assets/css/sig/sig.css',
        '/assets/css/freelancer.css',
        '/assets/css/default/dynForm.css',
        '/assets/css/co-boot.css',
        '/assets/css/co-color.css',
        '/assets/css/co-template.css',
        '/assets/css/plugins.css',
        '/assets/css/floopDrawerRight.css',
        '/assets/css/cooperation.css',
        '/assets/css/profilSocial.css',
        '/assets/css/default/directory.css',
        '/assets/js/comments.js',
        '/assets/js/typeObj.js',
        '/assets/css/notifications.css',
        '/assets/css/calendar.css',
        '/assets/css/markdown.css',

    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->theme->baseUrl);

    $cssAnsScriptFilesModuleAll = array_merge($cssAnsScriptFilesModuleAll, $cssAnsScriptFilesModule);

            $cssAnsScriptFilesModule = array(
                "/js/default/socket-connection.js",
                '/css/filters.css',
                "/js/default/loginRegister.js",
                '/js/co.js',
                '/js/default/directory.js',
                '/js/default/search.js',
                '/js/default/filters.js',
                '/js/links.js',
                '/js/default/index.js',
                '/js/default/notifications.js',
                '/js/dataHelpers.js',
                '/js/sig/localisationHtml5.js',
                '/js/floopDrawerRight.js',
                '/js/sig/geoloc.js',
                '/js/default/globalsearch.js',
                '/js/sig/findAddressGeoPos.js',
                '/js/jquery.filter_input.js',
                '/js/scopes/scopes.js',
                //'/js/default/calendar.js',
                '/js/default/calendarObj.js',
                '/js/codate/codateInsideObj.js',
                '/js/json2csv.js',
                '/js/default/ocecotools.js',
                '/js/default/activitypub-starter.js',
                '/js/directory/directorySocket.js',
                '/css/activitypub-starter.css',
                "/js/default/discussionSocket.js",
                "/js/default/aacgenerique.js",
                "/css/call-loader.css",

            );


    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
    // GET NEW VERSION COMMUNECTER RESSOURCES
    if ($this->module->id != "costum") {
        $cssAnsScriptFilesModule = array(
            '/assets/css/v1.0/main.css'
        );

        HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->theme->baseUrl);
    }
    $cssAnsScriptFilesModuleAll = array_merge($cssAnsScriptFilesModuleAll, $cssAnsScriptFilesModule);
    ?>
    <!-- <script src='//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js'></script>
    <link href='//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css' rel='stylesheet' /> !-->


    <?php
    //var_dump(Yii::app()->session['userId']); exit;
    if (isset(Yii::app()->session['userId'])) {
        $myContacts = Person::getPersonLinksByPersonId(Yii::app()->session['userId']);
        $myFormContact = $myContacts;
        //var_dump($myContacts);exit();
        $getType = (isset($_GET["type"]) && $_GET["type"] != "citoyens") ? $_GET["type"] : "citoyens";
    } else {
        $myFormContact = null;
    }
    //$communexion = CO2::getCommunexionCookies();

    $me = isset(Yii::app()->session['userId']) ? Person::getById(Yii::app()->session['userId']) : null;

    if ($this->module->id != "costum") {
        $this->appConfig = CO2::getThemeParams();
        $layoutPathCO3 = "../../modules/co2/config/co3.json";
        $strSettingsCO3 = file_get_contents($layoutPathCO3);
        $settingsCO3 = json_decode($strSettingsCO3, true);
        $this->appConfig = Costum::filterThemeInCustom($settingsCO3, $this->appConfig);

        $params = $this->appConfig;
        $this->costum = null;
    }
    echo $this->renderPartial(
        $layoutPath . 'initJs',
        array("me" => $me, "parentModuleId" => $parentModuleId, "myFormContact" => @$myFormContact, /*"communexion" => $communexion,*/ "themeParams" => $params),
        $this->context
    );
    ?>

    <?php
    //$cs->registerScriptFile(Yii::app() -> createUrl($parentModuleId."/default/view/page/trad/dir/..|translation/layout/empty"));
    $this->registerJsFile(\yii\helpers\Url::to(["/" . $parentModuleId . "/default/view", 'page' => 'trad', 'dir' => '..|translation', 'layout' => 'empty']), ['position' =>  \yii\web\View::POS_HEAD]);
    ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>
<?php if (!empty($this->module->share)) header('Location: ' . $this->module->share); ?>
<?php
// Le mode d'édition d'un costum est généré dans le costum init et permet d'être activé avec l'url edit/true
/* if(isset($this->costum["editMode"]) && $this->costum["editMode"]===true){
        echo $this->renderPartial($layoutPath.'menus/cmsBuilder',array(
            "layoutPath"=>$layoutPath,
        ));
    }  */
?>

<body id="page-top" class="index">
    <?php
    CmsBuilderWidget::begin(["costum" => $this->costum]);
    $this->beginBody();
    ?>
    <!-- <script type="text/javascript">
    var d = new Date();
    var timecount = d.getTime();
    </script> -->
    <!-- **************************************
        MAP CONTAINER
        ******************************************* -->
    <div id="firstLoader">
        <?php
        if (isset($this->costum) && isset($this->costum["css"]["loader"]["loaderUrl"])) {
            $loaderUrl = $this->costum["css"]["loader"]["loaderUrl"];
            $loaderActive = $loaderUrl == "loading_modal" ? "loadingModal" : 'loader/' . $loaderUrl;
        } else
            $loaderActive = "loadingModal";

        if (isset($params["loadingModal"]))
            echo $this->renderPartial($params["loadingModal"]);
        else
            echo $this->renderPartial($layoutPath . $loaderActive, array("themeParams" => $params));
        ?>
    </div>

    <?php echo $this->renderPartial($layoutPath . 'progressBar', array("themeParams" => $params)); ?>
    <!-- <div id="mainMap">
            <?php
            // $this->renderPartial( $layoutPath.'mainMap.'.Yii::app()->params["CO2DomainName"], array("modulePath"=>$modulePath ));
            ?>
        </div> -->

    <?php
    // $this->renderPartial($layoutPath.'menusMap/'.$CO2DomainName, array( "layoutPath"=>$layoutPath, "me" => $me ) );
    ?>
    <?php
        echo $this->renderPartial("co2.views.element.createCostum", array());
    ?>
    <?php if (isset(Yii::app()->session["userId"]) && !YII_DEBUG) {
        echo $this->renderPartial($layoutPath . '/rocketchat');
    }
    ?>
    <!-- /********* MAIN-CONTAINER ***********/
            => Contain all structure of cotools (header + menu + view page + footer )
        -->
    <div class="main-container col-md-12 col-sm-12 col-xs-12 <?php //echo @$this->appConfig["appRendering"]
                                                                ?>">
        <?php echo $this->renderPartial($layoutPath . 'header', array("page" => "welcome", "layoutPath" => $layoutPath)); ?>
        <!-- /********* WELCOME PAGE ***********/
                - Home page of co or costum home directly intergrated in pageContent (view container)
                - Hash will be catch after on jquery
            -->
        <div id="mapContent" class="" style="display: none;"></div>
        <div class="pageContent">
            <?php
            echo $content;
            ?>
        </div>

        <div class="footer-cms">
            <?php echo $this->renderPartial($layoutPath . 'footer', array("page" => "welcome")); ?>
        </div>

    </div>
    <div id="modal-dashboard-account"></div>
    <div id="modal-calendar">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        <div class="col-xs-12 padding-top-50 content-modal-calendar "></div>

    </div>

    <div id="modal-preview-coop" class="shadow2 hidden"></div>
    <!--<div id="modal-preview-favorites" class="shadow2 hidden"></div>-->
    <div id="modal-settings" class="shadow2"></div>
    <div id="modal-preview-comment" class="shadow2" style="overflow-y: scroll">
        <div class="col-xs-12 no-padding">
            <h3 class="title-comment pull-left"></h3>
            <button class="btn btn-default pull-right btn-close-preview">
                <i class="fa fa-times"></i>
            </button>
        </div>
        <hr class="col-xs-12 separator">
        <div class="comment-tree col-xs-12 no-padding">
        </div>
    </div>

    <div id="floopDrawerDirectory" class="floopDrawer floopPreview"></div>

    <div class="portfolio-modal modal fade <?php echo @$this->appConfig["appRendering"] ?>" id="openModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container bg-white" id="openModalContent">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="modal-header text-dark">
                            <h3 class="modal-title text-center" id="ajax-modal-modal-title">
                                <i class="fa fa-angle-down"></i> <i class="fa " id="ajax-modal-icon"></i>
                            </h3>
                        </div>

                        <div id="ajax-modal-modal-body" class="modal-body">

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 text-center bg-white" style="padding-top:50px;padding-bottom:50px;">
                <hr>
                <a href="javascript:" style="font-size: 13px;" type="button" class="" data-dismiss="modal">
                    <i class="fa fa-times"></i> <?php echo Yii::t("common", "Back") ?>
                </a>
            </div>
        </div>
    </div>

    <?php
    echo $this->renderPartial("co2.views.element.dialogModal", $params);

    /* ***********************
            END theme stuff
            ************************ */
    $graphAssets = [
        '/plugins/d3/d3.v6.min.js'
    ];

    HtmlHelper::registerCssAndScriptsFiles(
        $graphAssets,
        Yii::app()->request->baseUrl.Yii::app()->getModule("graph")->getAssetsUrl()
    );

    $cssAnsScriptFilesModule = array(
        '/leaflet/leaflet.css',
        '/leaflet/leaflet.js',
        '/leaflet/heatmap.js',
        '/leaflet/leaflet.heatmap.js',
        '/leaflet/leaflet-curve.js',
        '/css/map.css',
        '/css/map-v2.css',
        '/markercluster/MarkerCluster.css',
        '/markercluster/MarkerCluster.Default.css',
        '/markercluster/leaflet.markercluster.js',
        '/js/map.js',
        '/js/map-v2.js',
        '/js/mapD3.js'
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule(Map::MODULE)->getAssetsUrl());

    /* ***********************
            JS and CSS for News
            ************************ */
    $cssAnsScriptFilesModule = array(
        '/css/news.css',
        '/css/timeline.css',
        '/css/form.css',
        '/js/news.js',
        '/js/init.js'
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule(News::MODULE)->getAssetsUrl());

            $cssAnsScriptFilesModule = array(
                "js/comments.js"
            );
            HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule,  Yii::app()->theme->baseUrl."/assets/" );
            $cssAnsScriptFilesModule = array(
                '/js/uiModeration.js'
            );
            HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule( "dda" )->getAssetsUrl() );

            if( !empty( $this->costum ) ){
                $costum_ressources = [
                    "/cmsBuilder/js/cmsBuilder.js",
                    "/cmsBuilder/js/carrosselObj.js",
                    "/cmsBuilder/js/configSearchObj.js",
                    "/css/blockcms/swiper/swiper-bundle.min.css",
                    "/js/blockcms/swiper/swiper-bundle.min.js",
                    "/cmsBuilder/js/swiperObj.js",
                    "/css/blockcms/cmsEngine.css",
                    "/css/blockcms/defaultcssBlock.css",
                    "/js/blockcms/cmsEngine.js",
                    "/js/blockcms/cos_cart.js",
                    "/css/blockcms/cos_cart.css",
                    "/cmsBuilder/js/costumPreviewSocket.js"
                ];

                HtmlHelper::registerCssAndScriptsFiles([
                    '/plugins/bootstrap-datetimepicker/xdsoft/jquery.datetimepicker.min.css',
                    '/plugins/bootstrap-datetimepicker/xdsoft/jquery.datetimepicker.full.min.js'
                ], Yii::app()->baseUrl);
                
                if(isset($this->costum["type"]) && $this->costum["type"] == "aap"){
                    HtmlHelper::registerCssAndScriptsFiles([
                        '/css/aap/aap.css',
                        '/js/aap/aap.js',
                        '/js/aap/aap-overload.js',
                    ], $this->module->getParentAssetsUrl() );
                }

                HtmlHelper::registerCssAndScriptsFiles([
                    '/css/coInput/co-input.css',
                    '/js/coInput/co-input.js',
					'/js/aap/aapv2.js',
                    '/js/coInput/co-input-types-css.js'
                ], $this->module->getParentAssetsUrl() );

                if(isset($this->costum["editMode"]) && ($this->costum["editMode"] == "true")){
                    $costum_ressources = array_merge($costum_ressources, [
                        "/cmsBuilder/js/jquery-co-editor.js",
                        "/cmsBuilder/js/spTextEditor.js",
                        "/cmsBuilder/js/cmsConstructor.js",
                        "/cmsBuilder/js/costumizer.js",
                        "/cmsBuilder/css/costumizer.css",
                        "/cmsBuilder/js/template.js",
                        "/css/blockcms/image-picker/image-picker.css",
                        "/js/blockcms/image-picker/paintObj.js",
                        "/js/blockcms/image-picker/image-picker.min.js",
                        "/js/blockcms/showmoreless.js",
                        "/cmsBuilder/js/costumSocket.js",
                        "/js/blockcms/csstree-validator.js"
                    ]);


                }
                $costum_ressources = array_merge($costum_ressources , [
                        "/js/blockcms/fontSelect/fontObj.js"]);
                HtmlHelper::registerCssAndScriptsFiles($costum_ressources, Yii::app()->getModule( Costum::MODULE )->getAssetsUrl());

                $slugContext=(@$this->costum["assetsSlug"]) ? $this->costum["assetsSlug"] : $this->costum["slug"];

                if( !empty( $this->costum ) &&
                    !empty( $this->costum['js'] ) &&
                    !empty( $this->costum['js']['urls'] ) ){
                    $cssAnsScriptFilesModule =  array();
                   foreach ($this->costum['js']['urls']  as $key => $value) {
                       // Teste si la chaîne contient le "/"
                       if(strpos($value, "/") !== false){
                           $cssAnsScriptFilesModule[] = '/js/'.$value;
                       } else{
                           $cssAnsScriptFilesModule[] = '/js/'.$slugContext.'/'.$value;
                       }
                    }

            HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule(Costum::MODULE)->getAssetsUrl());
        }

        if (
            !empty($this->costum) &&
            !empty($this->costum['css']) &&
            !empty($this->costum['css']['urls'])
        ) {
            $cssAnsScriptFilesModule =  array();

            foreach ($this->costum['css']['urls']  as $key => $value) {
                // Teste si la chaîne contient le "/"
                if (strpos($value, "/") !== false) {
                    $cssAnsScriptFilesModule[] = '/css/' . $value;
                } else {
                    $cssAnsScriptFilesModule[] = '/css/' . $slugContext . '/' . $value;
                }
            }

            HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule(Costum::MODULE)->getAssetsUrl());
        }
    }
    ?>

    <?php echo $this->renderPartial('@dda/views/co/pod/modalCommon', array());
    echo $this->renderPartial('@news/views/co/modalShare', array());
    if (isset(Yii::app()->session['userId'])) {
        echo $this->renderPartial($layoutPath . 'notifications');
        echo $this->renderPartial('costum.views.tpls.blockCms.menu.corner_dev');
    }
    echo $this->renderPartial("co2.views.admin.ocecotools", array());
    ?>

    <script>
        jQuery(document).ready(function() {
            var pageUrls = <?php echo json_encode($this->appConfig["pages"]); ?>;
            $.each(pageUrls, function(k, v) {
                if (typeof urlCtrl.loadableUrls[k] == "undefined")
                    urlCtrl.loadableUrls[k] = v;
                else {
                    $.each(v, function(ki, vi) {
                        urlCtrl.loadableUrls[k][ki] = vi;
                    });
                }
            });

            themeObj.init();
            $.each(modules, function(k, v) {
                if (typeof v.init != "undefined" && notNull(v.init)) {
                    mylog.log("init.js for module : ", k);
                    callB = (typeof v.callback != "undefined") ? v.callback : null;
                    lazyLoad(v.init, null, callB);
                }
            });

            if (typeof themeObj.firstLoad == "function")
                themeObj.firstLoad();
            else if (themeObj.firstLoad) {
                isUserConnected = (userId == "") ? "unlogged" : "logged";
                    if(costum){
                        var url = new URL(location.href)
                        if(!page || url.searchParams.get("hp")){

                            if(url.searchParams.get("h")){
                                var hash = "#"+url.searchParams.get("h")
                                if(url.searchParams.get("hp"))
                                    hash += "?"+atob(url.searchParams.get("hp"))

                                url.hash = hash;

                                history.replaceState({}, null, url)
                            }

                            urlCtrl.loadByHash(location.hash, null);
                        }else{
                            setTimeout(function(){ $(".progressTop").val(60)
                                $("#loadingModal").css({"opacity": 0.8});
                            }, 1);
                            setTimeout(function(){ $(".progressTop").val(80)}, 1);
                            setTimeout(function(){ $(".progressTop").val(100);}, 1);
                            setTimeout(function(){
                                $(".progressTop").fadeOut(5);
                                $("#firstLoader").fadeOut(5);
                            }, 1);
                        }
                    }else{
                    if (location.hash == "#welcome" ||
                        ((location.hash == "" || location.hash == "#") && urlCtrl.loadableUrls["#app.index"].redirect["logged"] == "welcome")) {
                        setTimeout(function() {
                        $(".progressTop").val(60)
                            $("#loadingModal").css({
                            "opacity": 0.8
                        });
                        }, 500);
                    setTimeout(function() {
                        $(".progressTop").val(80)
                    }, 500);
                        setTimeout(function() {
                            $(".progressTop").val(100);
                    }, 5000);
                        setTimeout(function() {
                            $(".progressTop").fadeOut(200);
                            $("#firstLoader").fadeOut(400);
                        }, 500);
                        urlCtrl.loadByHash(location.hash);
                    } else {
                        themeObj.firstLoad = false;
                        $(".pageContent").html("<i class=\'fa fa-spin fa-spinner\'></i>");
                        urlCtrl.loadByHash(location.hash);
                    }
                    }
            } else {
                $("#page-top").show();
            }
            /* setTimeout(function(){
                 if($('.lbh-anchor').length != 0)
                     urlCtrl.loadByAnchor()
             },3000)*/
            localStorage.removeItem("dashboardAccoundOpened")
        });

    </script>

    <?php

    // print_r($cssAnsScriptFilesModuleAll);
    /*
    Optimisation js css
   */
    //CacheHelper::flush();
    HtmlHelper::bundle(basename(__FILE__));

    ?>
    <?php
    $this->endBody();
    CmsBuilderWidget::end();
    ?>

    <spotlight-search></spotlight-search>
</body>

</html>
<?php $this->endPage() ?>
