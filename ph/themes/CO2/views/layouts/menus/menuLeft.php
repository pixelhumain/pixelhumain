<style type="text/css">
    .horizontal{
        position: fixed;
        top:  65px;
        left:0px;
        width: 65px;
    }
    .btn-show-filters.hidden-xs .topbar-badge{
        padding: 2px 5px;
        font-size: 11px;
        font-weight: bolder;
    }
    .btn-show-filters.hidden-xs:hover{
        text-decoration: underline;
    }
</style>
<?php 
    $visibleClass=(isset($params["numberOfApp"]) && $params["numberOfApp"]<=1) ? "hidden": ""; 
    $class=(isset($params["subMenu"]) && isset($params["subMenu"]["class"])) ? $params["subMenu"]["class"] : "";
?>
<div id="menuApp" class="menuLeft hidden-xs configuration-menus-costumizer <?php echo $class; ?>" data-path="htmlConstruct.menuLeft">
<?php
        foreach ($params["pages"] as $key => $value) {  
            if(Authorisation::accessMenuButton($value, "Left")){ ?>
                <a href="javascript:;" data-hash="<?php echo $key; ?>" 
                class="<?php echo $key; ?>ModBtn lbh-menu-app btn btn-link pull-left btn-menu-to-app btn-menu-vertical col-xs-12 hidden-xs hidden-top link-submenu-header">
                    <i class="fa fa-<?php echo $value["icon"]; ?>"></i>
                    <span class="tooltips-menu-btn"><?php echo Yii::t("common", @$value["subdomainName"]); ?></span>
                </a>  
        <?php   } 
        }
    ?>
</div>
<script type="text/javascript">
jQuery(document).ready(function() {
    mylog.log("render menuApp","/pixelhumain/ph/themes/CO2/views/layouts/menus/menuLeft.php")
});
</script>