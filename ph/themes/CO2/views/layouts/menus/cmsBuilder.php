<?php  
	$cssAnsScriptFilesModule = array(
        '/js/admin/admin_directory.js',
        '/js/admin/panel.js',
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl() );

    $cssAnsScriptFilesModule = array(
        '/assets/cmsBuilder/css/costumizer.css',
        '/assets/cmsBuilder/css/supercms.css',
        '/assets/cmsBuilder/js/costumizer.js'
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->theme->baseUrl);

    HtmlHelper::registerCssAndScriptsFiles(array(
        '/plugins/spectrum-colorpicker2/spectrum.min.js',
          '/plugins/spectrum-colorpicker2/spectrum.min.css'), null);
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; 
$search = '/edit/true' ;
$actual_link_no_edit = str_replace($search, '', $actual_link) ;
?>
<link rel="stylesheet" href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
<!-- Wait for your button @yorre !-->
<nav id="menu-top-costumizer">
    <div>
        <ul class="costum-title">
            <li>
                <a href="javascript:;" class="btn-menu-tooltips">
                    CO<span class="tooltips-menu-btn"><?php echo Yii::t("cms", "tutoriel"); ?></span>
                </a>
            </li>
            <li><span class="pull-left text-white elipsis" style="max-width:150px;"><?= $this->costum["title"] ?> </span></li>
        </ul>
    </div>
    <div>
        <ul class="screen-size-nav">
            <li>
                <a href="javascript:;" class="btn-costumizer-viewMode btn-menu-tooltips active" data-mode="md" 
                data-title="<?php echo Yii::t("cms", "Computer view-mode"); ?>">
                    <i class="fa fa-desktop" aria-hidden="true"></i>
                    <span class="tooltips-menu-btn tooltips-top-menu-btn"><?php echo Yii::t("cms", "Computer view-mode"); ?></span>
                </a>
            </li>
            <li>
                <a href="javascript:;" class="btn-costumizer-viewMode btn-menu-tooltips"  data-mode="sm"
                data-title="<?php echo Yii::t("cms", "Tablet view-mode"); ?>">
                    <i class="fa fa-tablet" aria-hidden="true"></i>
                    <span class="tooltips-menu-btn tooltips-top-menu-btn"><?php echo Yii::t("cms", "Tablet view-mode"); ?></span>
                </a>
            </li>
            <li>
                <a href="javascript:;" class="btn-costumizer-viewMode btn-menu-tooltips" data-mode="xs"
                data-title="<?php echo Yii::t("cms", "Mobile view-mode"); ?>">
                    <i class="fa fa-mobile" aria-hidden="true"></i>
                    <span class="tooltips-menu-btn tooltips-top-menu-btn"><?php echo Yii::t("cms", "Mobile view-mode"); ?></span>
                </a>
            </li>
        </ul>
    </div>
    <div>
        <ul class="action-nav">
            <!--<li><a href="javascript:;"><i class="fa fa-undo" aria-hidden="true"></i></a></li>-->
            <li>
                <a href="javascript:;" class="open-preview-costum" data-url="<?php echo Yii::app()->getRequest()->getBaseUrl(true).'/costum/co/index/slug/'.$this->costum["contextSlug"]; ?>">
                    <i class="fa fa-eye" aria-hidden="true"></i>
                    <span class="tooltips-menu-btn tooltips-top-menu-btn"><?php echo Yii::t("common", "Preview"); ?></span>
                </a>
            </li>
        </ul>
        <ul class="menu-right-costumizer-nav">
            <li>
                <a class="lbh-costumizer nav-costumizer btn-menu-vertical" 
                    data-link="generalInfos"
                    data-title="<?php echo Yii::t("cms", "General settings of costum"); ?>">
                        <i class="fa fa-cog" aria-hidden="true"></i>
                        <span class="tooltips-menu-btn tooltips-top-menu-btn"><?php echo Yii::t("cms", "General settings"); ?></span>
                </a>
            </li>
            <li>
                <a class="lbh-costumizer nav-costumizer btn-menu-vertical" 
                    data-link="communityAdmin"
                    data-title="<?php echo Yii::t("cms", "Administrators of the interface"); ?>">
                    <i class="fa fa-group" aria-hidden="true"></i>
                    <span class="tooltips-menu-btn tooltips-top-menu-btn"><?php echo Yii::t("cms", "Site's administrators"); ?></span>
                </a>
            </li>
          <!--  <li><a href="javascript:;"><i class="fa fa-th-list" aria-hidden="true"></i></a></li>-->
            <li>
                <a href="javascript:;" class="lbh-costumizer nav-costumizer btn-menu-tooltips" data-title="<?php echo Yii::t("cms", "Media manager"); ?>" data-link="medias">
                    <i class="fa fa-folder-open" aria-hidden="true"></i>
                     <span class="tooltips-menu-btn tooltips-top-menu-btn"><?php echo Yii::t("cms", "Media manager"); ?></span>
                </a>
            </li>
            <!--<li><a href="javascript:;"><i class="fa fa-briefcase" aria-hidden="true"></i></a></li>-->
        </ul>
    </div>
</nav> 

<!-- Wait for your button @yorre !-->
<div id="menu-left-costumizer">
    <ul>
        <li>
            <a href="javascript:;" class="lbh-costumizer nav-costumizer btn-menu-vertical" 
                data-dom="#modal-content-costumizer-administration" 
                data-title="<?php echo Yii::t("cms", "Sitemap")." > ".Yii::t("cms", "Settings of pages"); ?>" 
                data-link="pages"
                data-save-button="false">
                    <i class="fa fa-file-o" aria-hidden="true"></i>
                    <span class="tooltips-menu-btn"><?php echo Yii::t("cms", "Pages"); ?></span>               
            </a>
        </li>
        <li>
            <a href="javascript:;" class="lbh-costumizer nav-costumizer btn-menu-vertical" 
                data-dom="#modal-content-costumizer-administration" 
                data-title="<?php echo Yii::t("cms", "Edition of menu"); ?>" 
                data-link="htmlConstruct">
                <i class="fa fa-sitemap" aria-hidden="true"></i>
                <span class="tooltips-menu-btn"><?php echo Yii::t("cms", "Header, menus & footer"); ?></span>               
            </a>
        </li>
        <li>
            <a href="javascript:;" class="lbh-costumizer nav-costumizer btn-menu-vertical" 
                data-dom="#modal-content-costumizer-administration" 
                data-title="<?php echo Yii::t("cms", "Activated elements of the costums"); ?>" 
                data-link="typeObj">
                <i class="fa fa-connectdevelop" aria-hidden="true"></i>
                <span class="tooltips-menu-btn"><?php echo Yii::t("cms", "Add-on element"); ?></span>               
            </a>
        </li>
<!-- <li>
            <a href="javascript:;" class="lbh-costumizer nav-costumizer btn-menu-vertical" data-link="generalInfos"
                data-title="<?php echo Yii::t("cms", "General settings of costum"); ?>">
                <i class="fa fa-wrench" aria-hidden="true"></i>
                <span class="tooltips-menu-btn"><?php echo Yii::t("cms", "General Informations "); ?></span>        
            </a>
        </li>
        <li>
            <div href="javascript:;" class="nav-costumizer btn-sub-menu-vertical">
                <i class="fa fa-sitemap" aria-hidden="true"></i>
                <div class="tooltips-sub-menu-btn">
                    <span class="title"><?php echo Yii::t("cms", "Sitemap"); ?></span> 
                    <ul>
                        <li>
                            <a href="javascript:;" class="lbh-costumizer nav-costumizer btn-menu-vertical" 
                                data-dom="#modal-content-costumizer-administration" 
                                data-title="<?php echo Yii::t("cms", "Sitemap")." > ".Yii::t("cms", "Settings of pages"); ?>" 
                                data-link="pages"
                                data-save-button="false">
                                <?php echo Yii::t("cms", "Pages"); ?>        
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" class="lbh-costumizer nav-costumizer btn-menu-vertical" 
                                data-dom="#modal-content-costumizer-administration" 
                                data-title="<?php echo Yii::t("cms", "Edition of menu"); ?>" 
                                data-link="htmlConstruct">
                                <?php echo Yii::t("cms", "Mockup & menus"); ?>        
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" class="lbh-costumizer nav-costumizer btn-menu-vertical" 
                                data-dom="#modal-content-costumizer-administration" 
                                data-title="<?php echo Yii::t("cms", "Settings of citizen toolkit"); ?>" 
                                data-link="socialSettings">
                                <?php echo Yii::t("cms", "Citizen toolkit"); ?>        
                            </a>
                        </li>
                    </ul> 
                <div>       
            </div>
        </li>   -->
        <li>
            <div href="javascript:;" class="nav-costumizer btn-sub-menu-vertical">
                <i class="fa fa-magic" aria-hidden="true"></i>
                <div class="tooltips-sub-menu-btn">
                    <span class="title"><?php echo Yii::t("cms", "Design & fun"); ?></span> 
                    <ul>
                        <li>
                            <a href="javascript:;" class="lbh-costumizer nav-costumizer btn-menu-vertical" data-link="template" data-save-button="false">
                                <?php echo Yii::t("cms", "Template"); ?>        
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" class="lbh-costumizer nav-costumizer btn-menu-vertical" 
                                data-dom="#menu-right-costumizer" 
                                data-link="design"
                                data-title="<?php echo Yii::t("cms", "Design"); ?>">
                                <?php echo Yii::t("cms", "Web Design"); ?>        
                            </a>
                        </li>
                        <li>    
                            <a href="javascript:;" class="lbh-costumizer nav-costumizer btn-menu-vertical" data-link="css">
                                <?php echo Yii::t("cms", "CSS"); ?>        
                            </a>
                        </li>
                    </ul> 
                <div>       
            </div>
        </li>         
    </ul>
</div>

<!-- drag and drop the cms block or menu selector or other Ideeas !-->
<div id="menu-right-costumizer">
    <div class="modal-tools-costumizer">
        <div class="costumizer-title elipsis padding-left-10">
            <span class="bold italic"></span>
        </div>
        <div class="container-close">
            <button class="btn close-dash-costumizer" data-dom="#menu-right-costumizer">
                <i class="fa fa-close"></i>
            </button>
        </div>
    </div>
    <div class="contains-view-admin col-xs-12">
        
    </div>
    <div id="menu-sub-right-costumizer">
        <div class="modal-tools-costumizer">
             <div class="costumizer-title elipsis">
                <span class="bold italic"></span>
            </div>
            <div class="container-close">
                <button class="btn close-dash-costumizer" data-dom="#menu-sub-right-costumizer">
                    <i class="fa fa-close"></i>
                </button>
            </div>
        </div>
        <div class="contains-view-admin col-xs-12"></div>
    </div>
    <div id="menu-right-costumizer-hover">
         <div class="modal-tools-costumizer bg-white">
            <div class="pull-left">
                <button class="btn btn-default close-dash-costumizer" style="height: 44px;border-radius: 0px;border: 0px;" data-dom="#menu-right-costumizer-hover">
                    <i class="fa fa-arrow-left"></i>
                </button>
            </div>
             <div class="costumizer-title elipsis padding-left-10">
                <span class="bold italic"></span>
            </div>
        </div>
        <div class="contains-view-admin col-xs-12"></div>
    </div>
    <div class="modal-tools-costumizer-footer col-xs-12">
        <button class="btn btn-danger save-costumizer disabled">
            <i class="fa fa-save"></i> <?php echo Yii::t("common","Save") ?>
        </button>
    </div>
    
</div> 

<!-- For the template selector and perhaps other in future -->
<div id="modal-content-costumizer-administration">
    <div class="modal-tools-costumizer">
        <div class="costumizer-title">
            <span class="bold uppercase"></span>
        </div>
        <div class="pull-right">
            <button class="btn btn-danger save-costumizer disabled"><i class="fa fa-save"></i> <?php echo Yii::t("common","Save") ?></button>
        
            <button class="btn btn-default close-dash-costumizer" data-dom="#modal-content-costumizer-administration"><i class="fa fa-close"></i> <?php echo Yii::t("common","Close") ?></button>
        </div>
    </div>
      <div data-key="" class=" contains-view-admin col-xs-12 no-padding">
        
        </div>
</div> 

<div id="view-mode-sm"></div> 

<div id="view-mode-xs"></div> 


<script type="text/javascript">
    jQuery(document).ready(function() {
        costumizer.init();
    });
</script>