<?php 
	$idMenu=(isset($params["id"])) ? $params["id"] : @$key;
	$classMenu=(isset($params["class"])) ? $params["class"] : "";
	$classMenu.=(isset($params["addClass"])) ? " ".$params["addClass"] : "";
	if($idMenu!="mainNav")
		$classMenu.=" configuration-menus-costumizer";
	$classMenu.=" cosDyn-".$key;
	$objPath=(isset($path)) ? $path : "";
	$keyMenu=(isset($params["key"])) ? $params["key"] : @$key; 
	$commonButtonClass= (isset($params["buttonClass"])) ? $params["buttonClass"] : "";
	$connectedMode=(isset($connectedMode)) ? $connectedMode: null; 
	if(!isset($params["activated"]) || !empty($params["activated"])){
?>
		<div id="<?php echo $idMenu ?>" class="<?php echo $classMenu ?>" data-path="<?php echo $objPath ?>">
		<?php 

			if(!isset($params["buttonList"])){
				foreach($params as $key => $v){
					if(isset($v["menu"]) && $v["menu"]) {
						echo $this->renderPartial($layoutPath.'menus/constructMenu',array(
				            "params"=>$v,
				            "layoutPath"=>$layoutPath,
				            "sub"=>true ,
				            "path"=> $objPath.".".$key,
				            "key"=>$key,
				            "connectedMode"=>$connectedMode          
			    		));
			    	}
				}
			}else{
				//var_dump($params["buttonList"]);
				echo Menu::constructMenuBtn($params["buttonList"], $commonButtonClass, $connectedMode);
			}
		?>
		</div>
<?php } ?>