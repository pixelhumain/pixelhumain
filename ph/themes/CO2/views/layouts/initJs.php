<?php

use PixelHumain\PixelHumain\modules\chat\models\Chat;

$multiscopes = (empty($me) && isset( Yii::app()->request->cookies['multiscopes'] )) ?
                            Yii::app()->request->cookies['multiscopes']->value : "{}";
    $preferences = Preference::getPreferencesByTypeId(@Yii::app()->session["userId"], Person::COLLECTION);
$costum_fonts = Costum::getAllFonts($this->costum);
$costum_fonts = !empty($costum_fonts) ? $costum_fonts : null;

?>
<script>
//console.log("render Initjs","/pixelhumain/ph/themes/CO2/views/layouts/initJs.php")
    var themeUrl = "<?php echo Yii::app()->theme->baseUrl;?>";
    var themeParams = <?php echo json_encode(@$themeParams);?>;
    var domainName = "<?php echo Yii::app()->params["CO2DomainName"];?>";
    var userId = "<?php echo Yii::app()->session['userId']?>";
    var uploadUrl = "<?php echo Yii::app()->params['uploadUrl'] ?>";
    var mainLanguage = "<?php echo Yii::app()->language ?>";
    var debug = <?php echo (YII_DEBUG) ? "true" : "false" ?>;
    var currentUrl = "<?php echo "#".Yii::app()->controller->id.".".Yii::app()->controller->action->id ?>";
    var debugMap = [
        <?php if(YII_DEBUG) { ?>
           { "userId":"<?php echo Yii::app()->session['userId']?>"},
           { "userEmail":"<?php echo Yii::app()->session['userEmail']?>"}
        <?php } ?>
        ];

    var baseUrl = "<?php echo Yii::app()->getRequest()->getBaseUrl(true);?>";
    var ctrlId = "<?php echo Yii::app()->controller->id;?>";
    var actionId = "<?php echo Yii::app()->controller->action->id ;?>";
    var moduleId = "<?php echo $parentModuleId?>";
    var parentModuleUrl = "<?php echo ( @Yii::app()->params["module"]["parent"] )  ? Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl() : Yii::app()->controller->module->assetsUrl ?>";
    var moduleUrl = "<?php echo Yii::app()->controller->module->assetsUrl;?>";
    var activeModuleId = "<?php echo $this->module->id?>";
    var assetPath   = "<?php echo $this->module->assetsUrl; ?>";
    var co2AssetPath = "<?php echo Yii::app()->getModule( "co2" )->assetsUrl ?>"
    var costum = <?php echo json_encode($this->costum) ?>;
    var uploadedFont = <?php echo json_encode(Document::getLastImageByKey((isset($this->costum["contextId"]) ? $this->costum["contextId"] : null), (isset($this->costum["contextType"]) ? $this->costum["contextType"] : null), "presentation","costumFont")); ?>;
    var defaultImage = "<?php echo Yii::app()->getModule( "co2" )->assetsUrl."/images/thumbnail-default.jpg"; ?>";
    var modules = {
        //Configure here eco
        "classifieds":<?php echo json_encode( Classified::getConfig("classifieds") ) ?>,
        "jobs":<?php echo json_encode( Classified::getConfig("jobs") ) ?>,
        "ressources":<?php echo json_encode( Classified::getConfig("ressources") ) ?>,
        "poi": <?php echo json_encode( Poi::getConfig() ) ?>,
        "chat": <?php echo json_encode( Chat::getConfig() ) ?>,
        "interop": <?php echo json_encode( Interop::getConfig() ) ?>,
        //"mynetwork": <?php //echo json_encode( Mynetwork::getConfig() ) ?>,
        "map": <?php echo json_encode( Map::getConfig() ) ?>,
        "eco" : <?php echo json_encode( array(
            "module" => "eco",
            "url"    => Yii::app()->getModule( "eco" )->assetsUrl
        )); ?>,
        "survey" : <?php echo json_encode( array(
            "url"    => Yii::app()->getModule( "survey" )->assetsUrl
        )); ?>,
        "co2" : <?php echo json_encode( array(
            "url"    => Yii::app()->getModule( "co2" )->assetsUrl
        )); ?>,
        "costum": {
            "url"   : "<?php echo Yii::app()->getModule( "costum" )->assetsUrl ?>",
            "module" : "costum",
            "init"   : "<?php echo Yii::app()->getModule( "costum" )->assetsUrl ?>/costum.js",
            callback : function(){
                costum.init();
            }
        },
        "cotools" : <?php echo json_encode( array(

            "module" => "cotools",
            "init"   => Yii::app()->getModule( "cotools" )->assetsUrl."/js/init.js" ,
            "form"   => Yii::app()->getModule( "cotools" )->assetsUrl."/js/dynForm.js" ,

        )); ?>
    };
    
    

    
    
    var currentScrollTop = 0;
    var isMapEnd = false;
	//used in communecter.js dynforms
    var tagsList = [];
    <?php 
    //echo json_encode(Tags::getActiveTags()) 
    ?>
    var htmlConstructParams=<?php echo json_encode(CO2::getThemeParams()) ?>;
    //var countryList = <?php //echo json_encode(Zone::getListCountry()) ?>;
    var eventTypes = <?php asort(Event::$types); echo json_encode(Event::$types) ?>;
    var organizationTypes = <?php echo json_encode( Organization::$types ) ?>;
    var avancementProject = <?php echo json_encode( Project::$avancement ) ?>;
    var currentUser = <?php echo isset($me) ? json_encode(Yii::app()->session["user"]) : "null"?>;
    var organizerList = {}; 
    var poiTypes = <?php echo json_encode( Poi::$types ) ?>;
    var poi = <?php echo json_encode( CO2::getContextList("poi") ) ?>;
    
    var myContacts = <?php echo (@$myFormContact != null) ? json_encode($myFormContact) : "null"; ?>;
    var ocecoToolsItems = <?php echo ( class_exists("Form") && method_exists("Form", "ocecoToolsItems") && @Form::ocecoToolsItems() != null) ? json_encode(Form::ocecoToolsItems()) : "null"; ?>;
    var functionCurrentlyLoading = {};
    // var myContactsById = myContacts;
    var userConnected = <?php echo isset($me) ? json_encode($me) : "null"; ?>;
    var mentionsContact=[];
    var prestation = <?php echo json_encode( CO2::getContextList("prestation") ) ?>;
    var prestationList = prestation.categories;
    
    var roomList = <?php echo json_encode( CO2::getContextList("room") ) ?>;
    
    var directoryViewMode="<?php echo "block" ?>";
    //console.log("directoryViewMode 0 ", directoryViewMode);
    //var classifiedSubTypes = <?php //echo json_encode( Classified::$classifiedSubTypes ) ?>;
    var urlTypes = <?php asort(Element::$urlTypes); echo json_encode(Element::$urlTypes) ?>;
    
    var globalTheme = "<?php echo Yii::app()->theme->name;?>";

    var deviseTheme = themeParams && themeParams["devises"] ? themeParams["devises"] : {};
    var deviseDefault = themeParams && themeParams["deviseDefault"] ? themeParams["deviseDefault"] : {};

    var rolesList=[ tradCategory.financier, tradCategory.partner, tradCategory.sponsor, tradCategory.organizor, tradCategory.president, tradCategory.director, tradCategory.speaker, tradCategory.intervener];
    var mapIconTop = {
        "default" : "fa-arrow-circle-right",
        "citoyen":"<?php echo Person::ICON ?>", 
        "citoyens":"<?php echo Person::ICON ?>", 
        "person":"<?php echo Person::ICON ?>", 
        "people":"<?php echo Person::ICON ?>", 
        "NGO":"<?php echo Organization::ICON ?>",
        "LocalBusiness" :"<?php echo Organization::ICON_BIZ ?>",
        "Group" : "<?php echo Organization::ICON_GROUP ?>",
        "group" : "<?php echo Organization::ICON ?>",
        "Cooperative" : "<?php echo Organization::ICON ?>",
        "association" : "<?php echo Organization::ICON ?>",
        "organization" : "<?php echo Organization::ICON ?>",
        "organizations" : "<?php echo Organization::ICON ?>",
        "GovernmentOrganization" : "<?php echo Organization::ICON_GOV ?>",
        "event":"<?php echo Event::ICON ?>",
        "events":"<?php echo Event::ICON ?>",
        "project":"<?php echo Project::ICON ?>",
        "projects":"<?php echo Project::ICON ?>",
        "city": "<?php echo City::ICON ?>",
        "entry": "fa-gavel",
        "action": "fa-cogs",
        "actions": "fa-cogs",
        "poi": "fa-info-circle",
        "video": "fa-video-camera",
        "classified" : "fa-bullhorn"
    };
  
    var mapColorIconTop = {
        "default" : "dark",
        "citoyen":"yellow", 
        "citoyens":"yellow", 
        "person":"yellow", 
        "people":"yellow", 
        "NGO":"green",
        "LocalBusiness" : "azure",
        "Group" : "white",
        "group" : "green",
        "Cooperative" : "nightblue",
        "association" : "green",
        "organization" : "green",
        "organizations" : "green",
        "GovernmentOrganization" : "green",
        "event":"orange",
        "events":"orange",
        "project":"purple",
        "projects":"purple",
        "city": "red",
        "entry": "azure",
        "action": "lightblue2",
        "actions": "lightblue2",
        "poi": "dark",
        "video":"dark",
        "classified" : "azure"
    };
    var onchangeClick=true;
    var lastWindowUrl = location.hash;
    var urlBackHistory = location.hash;
    var allReadyLoadWindow=false;
    var navInSlug=false;
    var historyReplace=true;
   
    var myScopes = {};
    var initLoginRegister={
        email : '<?php echo @$_GET["email"]; ?>',
        userValidated : '<?php echo @$_GET["userValidated"]; ?>',
        pendingUserId : '<?php echo @$_GET["pendingUserId"]; ?>',
        name : '<?php echo @$_GET["name"]; ?>',
        error :'<?php echo @$_GET["error"]; ?>',
        invitor : "<?php echo @$_GET["invitor"]?>",
    };
    
    var paramsMapCO = {
        container : "#mapContent",
        hideContainer : ".pageContent",
        activePopUp : true,
        activePreview:true,
        mapOpt:{
            menuRight : false,
            btnHide : false,
            doubleClick : true
        },
        mapCustom:{
            tile : "maptiler"
        }
    }
    
    var paramsMapD3CO = {
        container : "#mapContent",
        hideContainer : ".pageContent",
        activePopUp : true,
        activePreview:true,
        mapOpt:{
            menuRight : false,
            btnHide : false,
            doubleClick : true
        },
        mapCustom:{
            tile : "maptiler"
        }
    }

    if(costum && costum.map){
        if(costum.map.menuRight)
            paramsMapCO.mapOpt.menuRight = costum.map.menuRight;
        if(costum.map.activePreview)
            paramsMapCO.activePreview = costum.map.activePreview;
        if(costum.map.forced){
            paramsMapCO.forced = costum.map.forced;
            if(costum.map.forced.latLon)
                paramsMapCO.forced.latLon = costum.map.forced.latLon;
            if(costum.map.forced.zoom)
                paramsMapCO.mapOpt.zoom = costum.map.forced.zoom;
        }
    }

    var discussionComments = {
        params: {},
        members: {
            active: {},
            onlineCounter: 0,
        },
        common: {},
        intervalId: null,
        updateTimeParams : {
            elementsSelector: "",
            timeStamp: null,
            createdTimeAttr: "time"
        },
        alreadyNotifyDiscussionMember: false,
    };
    var interactionsCount = {};
    var intervalTimerBreak = {
        default: {
            requestTimeout: 80,
            timer: 0,
        }
    };
    var themeObjCommunexion = <?php echo json_encode(CO2::getCommunexionUser()) ?>;
    var themeObjCommunexion = JSON.parse(JSON.stringify(<?php echo json_encode(CO2::getCommunexionUser()) ?>));
    var themeObjMultiscopes = <?php echo isset($me) && isset($me["multiscopes"]) ? json_encode($me["multiscopes"]) : $multiscopes; ?>;
    var fontObj = JSON.parse(JSON.stringify(<?= json_encode($costum_fonts) ?>));
    var mapCO = {};
    //configuration de web socket
    var coWsConfig = <?= json_encode(isset(Yii::app()->params['cowsClient']) ? Yii::app()->params['cowsClient'] : Yii::app()->params['cows']); ?>;
    var coWsData = {};
    // sub-section-page-name
    var subsectionPageName = "";


function expireAllCookies(name, paths) {
    var expires = new Date(0).toUTCString();
    document.cookie = name + '=; expires=' + expires;
    for (var i = 0, l = paths.length; i < l; i++) {
        document.cookie = name + '=; path=' + paths[i] + '; expires=' + expires;
    }
};

function removeCookies() {
    expireAllCookies('cityInseeCommunexion', ['/', '/ph', '/ph/co2', 'co2']);
    expireAllCookies('regionNameCommunexion', ['/', '/ph', '/ph/co2', 'co2']);
    expireAllCookies('nbCpbyInseeCommunexion', ['/', '/ph', '/ph/co2', 'co2']);
    expireAllCookies('countryCommunexion', ['/', '/ph', '/ph/co2']);
    expireAllCookies('cityName', ['/', '/ph', '/ph/co2', 'co2']);
    expireAllCookies('insee', ['/', '/ph', '/ph/co2', 'co2']);
    expireAllCookies('communexionActivated', ['/','/ph', '/ph/co2', 'co2']);
    expireAllCookies('inseeCommunexion', ['/','/ph', '/ph/co2', 'co2']);
    expireAllCookies('cpCommunexion', ['/','/ph', '/ph/co2', 'co2']);
    expireAllCookies('cityNameCommunexion', ['/','/ph', '/ph/co2', 'co2']);
    expireAllCookies('communexionType', ['/','/ph', '/ph/co2', 'co2']);
    expireAllCookies('communexionValue', ['/','/ph', '/ph/co2', 'co2']);
    expireAllCookies('communexionName', ['/','/ph', '/ph/co2', 'co2']);
    expireAllCookies('communexionLevel', ['/','/ph', '/ph/co2', 'co2']);
    expireAllCookies('multiscopes', ['/','/ph', '/ph/co2', 'co2']);
    expireAllCookies('communexion', ['/','/ph', '/ph/co2', 'co2']);
}

removeCookies();
</script>