<?php   
    $styleRing1="border-color: transparent #354c57 transparent #354c57;border-width:2px;";
    $styleRing2="border-color: transparent #9fbd38 transparent #9fbd38;border-width:2px;";
    $background="background:white";
    function separateValueAndUnit($str) {
        $pattern = '/^([\d\.]+)\s*([a-zA-Z]+)$/';
        if (preg_match($pattern, trim($str), $matches)) {
            $number = (float) $matches[1];
            $unit = $matches[2];
        } else {
            $number = (float) $str;
            $unit = null;
        }
        return ['number' => $number, 'unit' => $unit];
    }
    $refBiggerRing=320;
    $refSmallerRing=300;
    if (!function_exists("setLoaderPopup")) {
        function setLoaderPopup($p,$costum){
        $style="";    
        foreach($p as $k => $v){
            if($k=="color"){
                if(strpos($v,"this.")!==false){
                    $path=explode(".",$v);    
                    $sizeP=sizeof($path);

                    $concat=[$path[1]];
                    //var_dump($concat);exit;
                    $table = $costum[$path[1]];
                    $v=recur($table,$path,1);   
                   // var_dump($v);exit; 
                }
                $style.="border-color: transparent ".$v." transparent ".$v.";";
            }
            else if($k=="borderWidth")
                $style.="border-width:".$v.";";
        }
        return $style;
        }
    }
    if(!function_exists("recur")){
        function recur($table,$path,$i) {
                $val="";
                if (!is_array($table)) {
                    return $table;
                }
                else{
                    $i++;
                    foreach($table as $key=>$value) {
                        if($key==$path[$i]) {        
                            recur($value,$path,$i);
                            return $value;
                        }
                        $val=$value;
                    }
                    return $val;  
                }  
            }
    }

    $logoLoader=Yii::app()->theme->baseUrl.'/assets/img/logos/logo.png';
    if(isset($this->costum)){

        if (isset($this->costum["htmlConstruct"]["loadingModal"]["logo"]))
            $logoLoader = $this->costum["htmlConstruct"]["loadingModal"]["logo"];
        elseif (isset($this->costum["loaderImg"]))
            $logoLoader = $this->costum["loaderImg"];
        elseif (isset($this->costum["logo"]))
            $logoLoader = $this->costum["logo"];

        if(isset($this->costum["css"]) && isset($this->costum["css"]["loader"])){
            if(isset($this->costum["css"]["loader"]["background"]))
                $background="background:".$this->costum["css"]["loader"]["background"];
            if(isset($this->costum["css"]["loader"]["ring1"])){
                $ring1=$this->costum["css"]["loader"]["ring1"];
                if(isset($ring1["height"])){ 
                    if(separateValueAndUnit($ring1["height"])["number"] > $refBiggerRing)
                        $refBiggerRing=separateValueAndUnit($ring1["height"])["number"];
                    else
                        $refSmallerRing=separateValueAndUnit($ring1["height"])["number"];
                }
                $styleRing1=setLoaderPopup($ring1,$this->costum);
            }
            if(isset($this->costum["css"]["loader"]["ring2"])){
                $ring2=$this->costum["css"]["loader"]["ring2"];
                if(isset($ring2["height"])){ 
                    if(separateValueAndUnit($ring2["height"])["number"] > $refBiggerRing)
                        $refBiggerRing=separateValueAndUnit($ring2["height"])["number"];
                    else
                        $refSmallerRing=separateValueAndUnit($ring2["height"])["number"];
                }
                $styleRing2=setLoaderPopup($ring2,$this->costum);
            }
        }
    } 
       

        
   // }
    
    $diffPos=($refBiggerRing-$refSmallerRing)/2;
    $marginImg=($refSmallerRing-100)/2;
?>
<style type="text/css">
    <?php 
        for ($i = 300; $i <= 2000;$i=$i+100) {
            $top=($i-$refBiggerRing)/2;
            ?>
           @media (min-height:<?php echo $i ?>px) and (max-height:<?php echo $i+100 ?>px){
                .contentFirstLoading .lds-css{
                    margin-top: <?php echo $top ?>px;
                }
            }
            <?php
        }
    ?>
</style>
<div id="loadingModal" style="<?php echo $background ?>">
</div>
<div class="contentFirstLoading">
    <div class="lds-css ng-scope" style="width:<?php echo $refBiggerRing; ?>px;height:<?php echo $refBiggerRing; ?>px;">
        <div style="width:<?php echo $refSmallerRing; ?>px;height:<?php echo $refSmallerRing; ?>px;top:<?php echo $diffPos;?>px;left:<?php echo $diffPos;?>px; <?php echo $background ?>; border-radius: 100%;position:absolute;">
            <img src="<?php echo $logoLoader ?>" class="loadingPageImg" style="width:80%;object-fit:contain;object-position:center;margin-top: <?php echo $marginImg ?>px;">
        </div>
        <div class="lds-dual-ring">
            <div class="ring2" style="<?php echo $styleRing2 ?>width:<?php echo $refBiggerRing; ?>px;height:<?php echo $refBiggerRing; ?>px;top: 0px;"></div>
            <div class="ring1" 
                style="<?php echo $styleRing1 ?>width:<?php echo $refSmallerRing; ?>px;height:<?php echo $refSmallerRing; ?>px;top:<?php echo $diffPos;?>px;left:<?php echo $diffPos;?>px;"></div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var heightBigger=<?php echo json_encode($refBiggerRing) ?>;
    jQuery(document).ready(function() { 
        //mylog.log("render loadingModal ","/pixelhumain/ph/themes/CO2/views/layouts/loadingModal.php")
        //topImg=($(window).height()-heightBigger)/2;
        //$(".contentFirstLoading .lds-css").css({"margin-top":topImg+"px"});
        /*heightR1=$("#contentFirstLoading .ring1").outerHeight();
        heightR2=$("#contentFirstLoading .ring2").outerHeight();
        refBiggerRing=heightR2;
        refSmallerRing=heightR1;
        posInc="2";
        if(heightR1 > heightR2){
            refBiggerRing= heightR1;
            refSmallerRing=heightR2;
            posInc="1";
        }*/
        //$("#contentFirstLoading .lds-css").css({"width":refBiggerRing+"px", "height":refBiggerRing+"px"});
        //diffPos=(refBiggerRing-refSmallerRing)/2;
        //$("#contentFirstLoading .ring"+posInc).css({"top":diffPos+"px","left":diffPos+"px", });
        
    });

</script>