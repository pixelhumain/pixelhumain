<?php
//$communexion = CO2::getCommunexionCookies(); 
$cssAnsScriptFilesModule = array(
			'/plugins/swiper/swiper.min.css',
			'/plugins/swiper/swiper.min.js' );
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getRequest()->getBaseUrl(true));
?>
<style type="text/css">
	.explain-modal .img-tools {
		height: 40px;
	    width: auto;
	    margin: 5px;
	}

    @font-face {
	  font-family: Amatic;
	   src: url('<?php echo Yii::app()->getModule( "co2" )->assetsUrl."/font/AmaticSC-Regular.ttf"; ?>');
	}
    .bg-two-columns .text-prez h3 {
    	font-family: Amatic;
    	font-weight: 800;
    }

</style>

<div class="col-md-12 col-lg-12 col-sm-12 imageSection no-padding">

	<div class="col-md-12 content-carousel no-padding">
		<div class="home-search-content">
			<div class="center-search text-center">
				<div class="content-register-btn">
					<?php if(!isset(Yii::app()->session['userId'])) { ?>
			    	<button class="btn btn-link register-btn" data-toggle="modal" data-target="#modalRegister">
	    					<i class="fa fa-plus-circle"></i> <?php echo Yii::t("login","Create an account") ?>
	    			</button>
				<?php }  ?>
				</div>
				<h2 class="co-what text-white">
					<?php echo Yii::t("home","Your <strong> local and free </strong> social network")?>
				</h2>
				<div class="container no-padding">
				    <div class="search-bar" id="costum-scope-search">
				        <div class="row">
				          <div class="form-group col-lg-4 col-sm-4 col-md-4 col-xs-4 no-padding-right">
				            <input type="text" class="form-control" placeholder="<?php echo Yii::t("common","Where")?> ?" id="location">
				          </div>
				          <div class="dropdown-result-global-search col-xs-12 col-sm-5 col-md-4 col-lg-4 no-padding" style="display: none;"></div>

				          <form id="search-form" action="#"> 
				          <div class="form-group col-lg-8  col-sm-8 col-md-8 col-xs-8 no-padding-left">
				            <div class="input-group">        
				                <input type="text" class="form-control" id="otherInput" name="otherInput" placeholder="<?php echo Yii::t("home","A theme, an association, a citizen, a keyword") ?> ...">
				                <span class="input-group-btn">
				                    <button class="btn btn-search-home" id="btn-search-home"><span class="glyphicon glyphicon-search"></span></button>
				                </span>
				            </div>
				          </div>
				          </form>
				        </div>
				    </div>
				  </div>
			</div>
		</div>

		<?php $dataCarousel = [
			["class" => "environnement", "icon" => "tree", "image" => "environnement.jpg", "title" => Yii::t("home","Environment and biodiversity") ],
			["class" => "alimentation", "icon" => "cutlery", "image" => "alimentation.jpg", "title" => Yii::t("home","Solidarity grocery and short food channels") ],
			["class" => "environnement", "icon" => "tree", "image" => "bio.jpg", "title" => Yii::t("home","Organic and shared gardens") ],
			["class" => "energie", "icon" => "sun-o", "image" => "energie.jpg", "title" => Yii::t("home","Local and sustainable energies") ],	
			["class" => "alimentation", "icon" => "cutlery", "image" => "agriculture.jpg", "title" => Yii::t("home","Agriculture nutrition food")],
			["class" => "economie", "icon" => "money", "image" => "economie.jpg", "title" => Yii::t("home","Free currency, local currency and exchange system")],
			["class" => "education", "icon" => "book", "image" => "education.jpg", "title" => Yii::t("home","Education, extracurricular activities and teaching")],
			["class" => "sante", "icon" => "heart-o", "image" => "sante.jpg", "title" => Yii::t("home","Health, medical care and medicine")],
			["class" => "citoyen", "icon" => "user-circle-o", "image" => "citoyen.jpg", "title" => Yii::t("home","Society, citizenship, democracy")],
			["class" => "culture", "icon" => "universal-access", "image" => "culture.jpg", "title" => Yii::t("home","Culture, animation and concert")],
			["class" => "sport", "icon" => "futbol-o", "image" => "sport.jpg", "title" => Yii::t("home","Sport, adventure, discovery and leisure")],
			["class" => "transport", "icon" => "bus", "image" => "transport.jpg", "title" => Yii::t("home","Urban planning, transportation and construction")]

		]; 
		?>
			
		<?php 
			$collapse = [
				[
					"title" => Yii::t("home_youare","Citizen"), "icon" => "user",
			     	"image" => "img-Citoyennete.jpg", "illu" => "illustration-Citoyennete.png",
                    "id" => "citizen",
					"description" => [	
										["title" => Yii::t("home_youare","Give visibility to local actor"),
										"items" => [Yii::t("home_youare","Reference all your local positive actions"),
                                                    Yii::t("home_youare","Referencing local actions"),
                                                    Yii::t("home_youare","Enrich the directory by theme, by sector")
													]
										],
										["title" => Yii::t("home_youare","A different way of making society"),
										"items" => [
                                                    Yii::t("home_youare","to be aware of and contribute to the local wealth (cultural, natural, solidarity, ...)"),
                                                    Yii::t("home_youare","Share a vision and contribute to the development of your community"),
                                                    Yii::t("home_youare","Participate and contribute to local dynamics that make sense to you")
													]
										],
										["title" => Yii::t("home_youare","Getting out of the GAFAMs")],
										["title" => Yii::t("home_youare","Develop and discover your territory")],
										["title" => Yii::t("home_youare","Creating dynamics in a sector"),
										"items" => [Yii::t("home_youare","Connecting actors and bringing them together towards common goals")]
										],
										["title" => Yii::t("home_youare","Citizen's toolbox"),
										"items" => [Yii::t("home_youare","Developing and improving a common, available to all")]
										],
										["title" => Yii::t("home_youare","Participate in building open source community tools"),
										"items" => [Yii::t("home_youare","Make it free to create new tools")]
										]
									]
				],
				[
					"title" => Yii::t("home_youare","Organization"),"icon" => "users",
		  			"image" => "img-Organisation.jpg", "illu" => "illustration-Organisation.png",
                    "id" => "organization",
                    "description" => [
										["title" => Yii::t("home_youare","Create a dedicated white label platform"),
										"items" => [
                                                    Yii::t("home_youare","White label : your design and logo, your content, your image"),
                                                    Yii::t("home_youare","Connect your network"),
                                                    Yii::t("home_youare","Your projects, your activities cross and connect")
													]
										],
										["title" => Yii::t("home_youare","Social network for companies or associations (NGOs)")],
										["title" => Yii::t("home_youare","Horizontal governance tools")],
										["title" => Yii::t("home_youare","Increasing participation and listening")],
										["title" => Yii::t("home_youare","Surveys and forms"),
										"items" => [Yii::t("home_youare","With coforms, create surveys and questionnaires to create dynamics in the organization")]
										],
										["title" => Yii::t("home_youare","Specific features and new features"),
										"items" => [Yii::t("home_youare","The modularity of the platform allows the creation of new modules and functionalities")]
										]
									]
				],
				[
					"title" => Yii::t("home_youare","Sector"), "icon" => "lightbulb-o",
		  			"image" => "img-Filière.jpg", "illu" => "illustration-Filière.png" ,
                    "id" => "sector",
					"description" => [	
										["title" => Yii::t("home_youare","Sector Knowledge"),
										"items" => [
                                                    Yii::t("home_youare","Create questionnaires freely"),
                                                    Yii::t("home_youare","Generate observatories with the collected data")
													]
										],
										["title" => Yii::t("home_youare","Horizontal, open and participatory sectorial dynamics")],
										["title" => Yii::t("home_youare","Community and activity"),
										"items" => [
                                                    Yii::t("home_youare","Promote inter-knowledge within a sector"),
                                                    Yii::t("home_youare","Facilitate exchange, convergence and cooperation between actors")
													]
										],
										["title" => Yii::t("home_youare","Observatory"),
										"items" => [Yii::t("home_youare","Understand, Analyze and Act")]
										]
									]
				],
				[
					"title" => Yii::t("home_youare","Public Institutions"), "icon" => "hand-o-up",
		  			"image" => "img-Collectivite.jpg", "illu" => "illustration-Collectivite.png" ,
                    "id" => "publicInstitutions",
					"description" => [	
										["title" => Yii::t("home_youare","Calls for projects"),
										"items" => [Yii::t("home_youare","Reference, evaluate, finance, contract, monitor and measure projects supported by the community")]
										],
										["title" => Yii::t("home_youare","Toolbox for the city and any services")],
										["title" => Yii::t("home_youare","Consult your fellow citizen"),
										"items" => [Yii::t("home_youare","Create questionnaires and a citizen dynamic on the territory")]
										],
										["title" => Yii::t("home_youare","Thematic Local Observatory"),
										"items" => [
                                                    Yii::t("home_youare","Dashboard showing projects and their impacts"),
                                                    Yii::t("home_youare","Dynamically visualize the territory in numbers, in a filterable directory")
													]
										],
										["title" => Yii::t("home_youare","Observatory"),
										"items" => [Yii::t("home_youare","Understand, Analyze and Act")]
										],
										["title" => Yii::t("home_youare","Questionnaire, CERFA")]
									]
				],
				[
					"title" => Yii::t("home_youare","Project builder"), "icon" => "lightbulb-o",
		  			"image" => "img-Projet.jpg", "illu" => "illustration-Projet.png",
                    "id" => "projectBuilder",
					"description" => [	
										["title" => Yii::t("home_youare","Create a social and territorial platform")],
										["title" => Yii::t("home_youare","Interoperate (link projects)")],
										["title" => Yii::t("home_youare","Co-constructing a digital commons")]
									]
				],
				[
					"title" => Yii::t("home_youare","COmmercial for the common"), "icon" => "lightbulb-o",
		  			"image" => "img-Commun.jpg", "illu" => "illustration-Commun.png",
                    "id" => "commercialforthecommon",
					"description" => [	
										["title" => Yii::t("home_youare","Commitment to the development of the commons")],
										["title" => Yii::t("home_youare","Have a kit to understand the platform")],
										["title" => Yii::t("home_youare","Keep your autonomy in the development of the common")]
									]
				],
                [
                    "title" => Yii::t("home_youare","Third-Place"), "icon" => "lightbulb-o",
                    "image" => "img-TL.jpg", "illu" => "illustration-TL.png" ,
                    "id" => "thirdPlace",
                    "description" => [
                        ["title" => Yii::t("home_youare","Account all contributions (voluntary, volunteer, services..)"),
                            "items" => [ Yii::t("home_youare","Internal exchange currency, Time currency") ]
                        ],
                        ["title" => Yii::t("home_youare","Event and Community Management"),
                            "items" => [Yii::t("home_youare","Actor Directory, Blog, Calendar, Classifieds, Surveys")]
                        ],
                        ["title" => Yii::t("home_youare","Promote projects"),
                            "items" => [Yii::t("home_youare","Join forces and tools for third-place networks")]
                        ]
                    ]
                ]
				
			];

		?>

		<div class="carousel slide" data-ride="carousel" data-type="multi" id="myCarousel">
	      <div class="carousel-inner">
	      	<?php foreach ($dataCarousel as $key => $value) { ?>
	      		
	   
	          <div class="item <?php echo $key == 0 ? "active" : "" ?> ">
	              <div class="col-md-3 col-sm-3 col-xs-3 no-padding">
	              	<a class="home-link-search <?= $value["class"] ?>" id="environnement" href="javascript:;">
	                    <div class="our-slide">
	                        <img class="img-responsive" src="<?php echo $this->module->assetsUrl; ?>/images/home/slide/<?= $value["image"] ?>">
	                    </div>
	                    <div class="home-carousel-item-text">
	                    	<i class="fa fa-<?= $value["icon"] ?>"></i>
	                    	<h2 class="thematique-search text-white text-bold">
	                    		<?= $value["title"] ?>
	                    	</h2>
	                    </div>
	                    <div class="home-carousel-item-content"></div>
	                </a>
	              </div>
	          </div>

	          <?php } ?>
	            
	        </div>
	        
	             <a class="left carousel-control" href="#myCarousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
	            <a class="right carousel-control" href="#myCarousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
	        
	    </div>
		
	</div>



<div class="col-sm-12 col-md-12 col-xs-12 no-padding" style="background-color:#fff; max-width:100%; float:left;">
	

	<div class="col-xs-12 no-padding">

		<div class="info-home z-index-10">
			<strong>
				<?php echo Yii::t("home","Local toolbox") ?><br>
			</strong>
			<?php echo Yii::t("home","Territorial and societal development") ?>
			 <br>
			<?php echo Yii::t("home","Find local eco-responsible initiatives") ?>
		</div>

		<h2 class="co-what z-index-10">
			<?php echo Yii::t("home","<strong>What</strong> is") ?>
			<img class="logo-co" src="<?php echo $this->module->assetsUrl; ?>/images/logos/logo-full.png"> <span class="interogation">?</span>	
		</h2>
		<hr class="dashed home-mb-40">


		<div class="col-xs-12 col-sm-6 pad-contain-vid">    

			<div class="video-container z-index-10">
	    		<iframe  src="https://player.vimeo.com/video/133636468?api=1&title=0&amp;byline=0&amp;portrait=0&amp;color=57c0d4" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
	    	</div>
		</div>

		<div class="col-xs-12 col-sm-6 z-index-10 padding-bottom-30 about-co">
			<div class="col-lg-10 col-lg-offset-1">
				<h4 class="co-short-desc">
					<?php echo Yii::t("home","Communecter has been created to apply open systemic experiments <strong> in all domains</strong>.") ?>
				</h4>
				<p>
					<?php echo Yii::t("home","COmmunecter is an open source, territorial network with endless possibilities") ?>						
				</p>
				<p>
					<?php echo Yii::t("home","It's <strong>territorial approach </strong> and provides an <strong>open toolbox</strong> for all local stakeholders (citizens, NGOs, local businesses, government and public organizations), that gives the opportunity to publish its Informations, to create a network, dynamics, or develop links within a given territory.") ?>
				</p>
				<p>
					<?php echo Yii::t("home","Communecter is open source and open data. It is developed and maintained by a community of committed developers.") ?>
				</p>
				<a href="http://www.open-atlas.org/" target="_blank" class="more-info"><?php echo Yii::t("common","Learn more")?></a>
			</div>
		</div>

		<div id="particles-js"></div>

	</div>

	
	<div class="col-xs-12 all-padding-section about">
		<p class="about-text">
			<img src="<?php echo $this->module->assetsUrl; ?>/images/OpenAtlasLogo.png"> <br/><span> <?php echo Yii::t("home","<strong>is finally a cooperative</strong> to continue building together<br/>We'll soon open for participation.")?> </span>
		</p>
		<!-- <p class="text-center">
			<a href="http://www.open-atlas.org/" target="_blank" class="btn btn-about">En savoir +</a>
			<a href="/costum/co/index/slug/openAtlas" target="_blank" class="btn btn-about"></a> 
		</p> -->
	</div>

	<div class="col-xs-12 no-padding" style="background-color: #ededed;">

		<h2 class="co-what z-index-10">
			<?php echo Yii::t("home","What<strong> we propose</strong>") ?>
		</h2>
		<hr class="dashed home-mb-40">
		<div class="container all-project text-center">
			<div class="box proposition">
				<div class="top-img-name">
					<img src="<?php echo $this->module->assetsUrl; ?>/images/COSTUM.png">
				</div>
				<div class="text-on-top">
					<?php echo Yii::t("home","Customize your tools to match your personal use") ?>

				</div>
				<div class="dot-top"></div>
				<div class="vertical-top"></div>
				<a href="javascript:;" class="popover-btn" id="costum-popover">
			        <div class="circle-effect">
			            <img class="img-responsive" src="<?php echo $this->module->assetsUrl; ?>/images/logo-costum.png">
			        </div>
			    </a>
		    </div>


		    <div class="box proposition">
		    	<a href="javascript:;" class="popover-btn" id="coform-popover">
			        <div class="circle-effect">
			            <img class="img-responsive" src="<?php echo $this->module->assetsUrl; ?>/images/logo-coform.png">
			        </div>
			    </a>
			    <div class="vertical"></div>
			    <div class="dot-bottom"></div>
			    <div class="text-in-bottom">
					<?php echo Yii::t("home","Collect and gather the knowledge in order to build your ecosystem")?>
				</div>
				<div class="bottom-img-name">
					<img src="<?php echo $this->module->assetsUrl; ?>/images/COFORM.png">
				</div>
		    </div>


		    <div class="box proposition">
		    	<div class="top-img-name">
					<img src="<?php echo $this->module->assetsUrl; ?>/images/COBSERVATOIRE.png">
				</div>
		    	<div class="text-on-top">
		    		<?php echo Yii::t("home","Visualize, connect and act")?>
					
				</div>
				<div class="dot-top"></div>
		    	<div class="vertical-top"></div>
		    	<a href="javascript:;" class=" popover-btn"  id="coObservatoire-popover">
			        <div class="circle-effect">
			            <img class="img-responsive" src="<?php echo $this->module->assetsUrl; ?>/images/logo-coObservatoire.png">
			        </div>
			    </a>
		    </div>


		    <div class="box proposition">
		    	<a href="javascript:;" class="popover-btn" id="smarterre-popover">
			        <div class="circle-effect">
			            <img class="img-responsive" src="<?php echo $this->module->assetsUrl; ?>/images/logo-smarterritoire.png">
			        </div>
			    </a>
			    <div class="vertical"></div>
			    <div class="dot-bottom"></div>
			    <div class="text-in-bottom">
			    	<?php echo Yii::t("home","A sectoral and connected approach for our territories")?>		
				</div>
				<div class="bottom-img-name">
					<img  src="<?php echo $this->module->assetsUrl; ?>/images/SMARTERRITOIRE.png">
				</div>
		    </div>


		    <div class="box proposition sm-xs-pos">
		    	<div class="top-img-name">
					<img src="<?php echo $this->module->assetsUrl; ?>/images/COCITY.png">
				</div>
		    	<div class="text-on-top">
		    		<?php echo Yii::t("home","Experiment partipative municipalities")?>	
				</div>
				<div class="dot-top"></div>
		    	<div class="vertical-top"></div>
		    	<a href="javascript:;" class="popover-btn" id="cocity-popover">
			        <div class="circle-effect">
			            <img class="img-responsive" src="<?php echo $this->module->assetsUrl; ?>/images/logo-cocity.png">
			        </div>
			    </a>
		    </div>
		    

		    <div class="box proposition sm-xs-pos">
		    	<a href="javascript:;" class="popover-btn" id="oceco-popover">
			        <div class="circle-effect">
			            <img class="img-responsive" src="<?php echo $this->module->assetsUrl; ?>/images/oceco.png">
			        </div>
			    </a>
			    <div class="vertical"></div>
			    <div class="dot-bottom"></div>
			    <div class="text-in-bottom">
			    	<?php echo Yii::t("home","Experiment a horizontal governance")?>
				</div>
				<div class="bottom-img-name">
					<img src="<?php echo $this->module->assetsUrl; ?>/images/OCECO.png">
				</div>
		    </div>


		    <div class="box proposition sm-xs-pos">
		    	<div class="top-img-name">
					<img src="<?php echo $this->module->assetsUrl; ?>/images/COTOOLS.png">
				</div>
		    	<div class="text-on-top">
		    		<?php echo Yii::t("home","Our free and open source tools")?>	
				</div>
				<div class="dot-top"></div>
		    	<div class="vertical-top"></div>
		    	<a href="javascript:;" class="popover-btn" id="foss-popover">
			        <div class="circle-effect">
			            <img class="img-responsive" src="<?php echo $this->module->assetsUrl; ?>/images/cotools.png">
			        </div>
			    </a>
		    </div>

            <div class="box proposition sm-xs-pos">
                <a href="javascript:;" class="popover-btn" id="fediverse-popover">
                    <div class="circle-effect">
                        <img class="img-responsive" src="<?php echo $this->module->assetsUrl; ?>/images/fediverse.png">
                    </div>
                </a>
                <div class="vertical"></div>
                <div class="dot-bottom"></div>
                <div class="text-in-bottom">
                    <?= Yii::t("home", "Interact with a wider audience") ?>
                </div>
                <div class="bottom-img-name">
                    <img src="<?php echo $this->module->assetsUrl; ?>/images/FEDIVERSE.png">
                </div>
            </div>

		</div>
	</div>


	<div class="col-md-12 col-sm-12 col-xs-12 no-padding text-white" style="background-color: #3f4e58;">
		<div class="col-xs-12 col-sm-12 pull-left padding-20 shadow2" style="font-size: 14px;">
			<div class="col-xs-12 font-montserrat ourvalues" style="text-align:center;">
                <!-- <div class="col-md-1 col-sm-1 hidden-xs"></div> -->
                <a href="javascript:;" class="popover-btn" id="open-source-popover">
	                <div class="col-lg-3 col-md-2 col-sm-4 col-xs-6 contain-value" style="text-align:center;">
	                	<img class="img-responsive" style="margin:0 auto;" 
	                		 src="<?php echo $this->module->assetsUrl; ?>/images/home/valeurs/valeur01.png"/>
	                		 <?php echo Yii::t("home","Open Source") ?>
	                </div>
	            </a>

				<a href="javascript:;" class="popover-btn" id="no-advertisement-popover">
	                <div class="col-lg-3 col-md-2 col-sm-4 col-xs-6 contain-value">
	                	<img class="img-responsive" style="margin:0 auto;" 
	                		 src="<?php echo $this->module->assetsUrl; ?>/images/home/valeurs/valeur02.png"/>
	                		 <?php echo Yii::t("home","No advertisement") ?>
	                </div>
	            </a>

	            <a href="javascript:;" class="popover-btn" id="protected-data-popover">    
	                <div class="col-lg-3 col-md-2 col-sm-4 col-xs-6 contain-value">
	                	<img class="img-responsive" style="margin:0 auto;" 
	                		 src="<?php echo $this->module->assetsUrl; ?>/images/home/valeurs/valeur03.png"/>
	                		 <?php echo Yii::t("home","Protected data") ?>
	                </div>
				</a>


				<a href="javascript:;" class="popover-btn" id="free-price-popover"> 
	                <div class="col-lg-3 col-md-2 col-sm-4 col-xs-6 contain-value">
	                	<img class="img-responsive" style="margin:0 auto;" 
	                	 	 src="<?php echo $this->module->assetsUrl; ?>/images/home/valeurs/valeur08.png"/>
	                	 	 <?php echo Yii::t("home","Free price") ?>
	                </div>
	            </a>

				<a href="javascript:;" class="popover-btn" id="connected-territory-popover"> 
	            	<div class="col-lg-3 col-md-2 col-sm-4 col-xs-6 contain-value">
	                	<img class="img-responsive" style="margin:0 auto;" 
	                	 	 src="<?php echo $this->module->assetsUrl; ?>/images/home/valeurs/valeur06.png"/>
	                	 	 <?php echo Yii::t("home","Connected territory") ?>
	                </div>
	            </a>

	            <a href="javascript:;" class="popover-btn" id="collective-intelligence-popover">
	                <div class="col-lg-3 col-md-2 col-sm-4 col-xs-6 contain-value">
	                	<img class="img-responsive" style="margin:0 auto;" 
	                	 	 src="<?php echo $this->module->assetsUrl; ?>/images/home/valeurs/valeur07.png"/>
	                	 	 <?php echo Yii::t("home","Collective intelligence") ?>
	                </div>
	            </a>

	            <a href="javascript:;" class="popover-btn" id="society-popover">
	                <div class="visible-lg col-lg-3 contain-value">
	                	<img class="img-responsive" style="margin:0 auto;" 
	                	 	 src="<?php echo $this->module->assetsUrl; ?>/images/home/valeurs/valeur09.png"/>
	                	 	 <?php echo Yii::t("home","Society 2.2.main") ?>
	                </div>
	            </a>

				<a href="javascript:;" class="popover-btn" id="commons-popover">
	                <div class="visible-lg col-lg-3 contain-value">
	                	<img class="img-responsive" style="margin:0 auto;" 
	                	 	 src="<?php echo $this->module->assetsUrl; ?>/images/home/valeurs/valeur10.png"/>
	                	 	 <?php echo Yii::t("home","Commons") ?>
	                </div>
	            </a>


            </div>
        </div>
		
		
	</div>
                     

	<!-- Compte -->

	 <!--<div class="col-xs-12 no-padding">
		 <img class="img-responsive home-body-img" src="<?php echo $this->module->assetsUrl; ?>/images/home/IMG.jpg">
		<div class="col-xs-12 text-center absolute-content">
			<h2 class="co-what text-white">
				<?php echo Yii::t("home","<strong>A personal use </strong> for everyone")?>	
		</h2>
		<hr class="dashed">

		<div class="container-use" style="margin-top: 7%;">
			<a href="https://doc.co.tools/books/2---utiliser-loutil/page/les-%C3%A9l%C3%A9ments-du-mod%C3%A8le-territorial-de-communecter" target="_blank">
				<img class="img-responsive home-body-img" src="<?php echo $this->module->assetsUrl; ?>/images/home/utilisation.png">
			</a>
		</div>
			

		</div> 
	  </div>-->

	<div class="col-xs-12 no-padding">
		<h2 class="co-what z-index-10">
            <strong> <?php echo Yii::t("home","You are") ?></strong>	?
		</h2>
		
		<div class="tabbable-panel">
			<div class="tabbable-line">
				<ul class="nav nav-tabs ">

					<?php foreach ($collapse as $key => $value) { ?>
						<li class="<?php echo $key == 0 ? "active" : "" ?>">
							<a href="#tab_default_<?php echo $key+1 ?>" data-toggle="tab">
							<i class="hidden fa fa-<?= @$value["icon"] ?>"></i><span><?= @$value["title"] ?></span> </a>
						</li>
					<?php } ?>
					
				</ul>
				<div class="tab-content">
					<?php foreach ($collapse as $key => $value) { ?>
						<div class="tab-pane <?php echo $key == 0 ? "active" : "" ?>" id="tab_default_<?php echo $key+1 ?>">
							<div class="col-xs-12 no-padding">
								<div id="bg-two-columns" class="bg-two-columns">
									<div class="col-xs-12 div-r visible-xs">
										<div class="text-prez">
							        		<h3 class=""><?= @$value["title"] ?></h3>
							        	</div>
									</div>
							        <div class="col-lg-8 col-md-7 col-sm-6 col-xs-12 div-l">
							            <div class="polygon-img" style="background-image: url(<?php echo $this->module->assetsUrl; ?>/images/home/<?= @$value["image"] ?>);">
							            	<img class="img-illu" src="<?php echo $this->module->assetsUrl; ?>/images/home/<?php echo Yii::app()->language ?>/<?= $value["illu"] ?>" alt="">
							            </div>
							            
							        </div>
							        <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12 div-r ">
							        	<div class="text-prez">
							        		<h3 class="hidden-xs"><?= $value["title"] ?></h3>
							            <!-- <img class="img-responsive visible-xs" src="<?php echo $this->module->assetsUrl; ?>/images/home/<?= $value["image"] ?>" alt=""> -->
							            <div class="col-xs-12 contain-dataTab co-scroll no-padding">
							            	<div class="panel-group" id="accordion<?php echo $key+1 ?>">
							            	<?php foreach ($value["description"] as $kd => $vd) { ?>
							            
												<?php if (!empty($vd["items"])) { ?>
												        <div class="panel panel-default">
												          <div class="panel-heading">
												          	<h5 class="panel-title">
												          		<a class='accordion-toggle <?php echo $kd == 0 ? "" : "collapsed" ?>' data-toggle="collapse" data-parent="#accordion<?php echo $key+1 ?>" data-target="#<?= $value['id'].$kd; ?>" href="javascript:;">
												          		<?php echo @$vd["title"] ?>
												          		</a>
												          	</h5>
												          </div>
												          <div id="<?= $value['id'].$kd; ?>" class='panel-collapse collapse  <?php echo $kd == 0 ? "in" : "" ?>'>
												            <div class="panel-body">
												              	<ul>
												              	<?php foreach ($vd["items"] as $ki => $vi) { ?>
															      <li><?php echo $vi ?></li>
																<?php } ?>
															    </ul>
												            </div>
												          </div>
												        </div>

												     
												<?php }else { ?>
												        <div class='panel panel-default'>
												          <div class='panel-heading'>
												          	<h5 class='panel-title'>
												          		<span class='accordion-toggle collapsed' >
												          			<?php echo @$vd["title"] ?>
												          		</span>
												          	</h5>
												          </div>
												        </div>
												<?php } ?>
												    
											<?php } ?>
							            		</div>
							            		
							            </div>
							        	</div>
							        </div>
					    		</div>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	 <div class="col-xs-12 no-padding">
		<h2 class="co-what z-index-10">
			Nouv<strong>eautés</strong>	
		</h2>
		<hr class="dashed home-mb-40">

		<?php 
		//var_dump(Authorisation::isUserSuperAdmin(Yii::app()->session['userId'])); 
		if(isset(Yii::app()->session['userId']) && Authorisation::isUserSuperAdmin(Yii::app()->session['userId'])){ ?>
			<div class="text-center">
				
				<button id="add-actu-co" class="btn btn-primary btn-add ">
					<i class="fa fa-plus-circle"></i> Ajouter Actualité
				</button>	
			</div>
		<?php } ?>
		<div class="container Slider-swiper-container">
		<div class="swiper-container mySwiper">
		  <div class="swiper-wrapper">
		    

		  </div>
		  <div class="swiper-pagination"></div>
		  <div class="swiper-scrollbar"></div>
		</div>
		</div>

	</div>



	
	<div class="col-xs-12 all-padding-section" style="background-color: #e1f2fc; min-height: 300px;">
		<div class="col-xs-12 col-md-12 col-lg-10 col-lg-offset-1  margin-bottom-15">
			<h2 class="co-what margin-top-30 bold"><?php echo Yii::t("home","Support us")?></h2>
			<hr class="dashed">
			<div class="col-xs-12 col-sm-6 dotted-left">
				<h1 class="no-padding icon-sign">
					<i class="fa fa-laptop"></i>
				</h1>
				<h2 class="title-biff"><?php echo Yii::t("home","Join the team")?></h2>
				<?php echo Yii::t("home","You work in the IT field, you want to develop, translate or document new features ?")?>?<br>
				<?php echo Yii::t("home","Join the community of developers.")?>
				<br>
				<a href="javascript:;" class="more-info fs-16 margin-top-20 participate-dev">
					<?php echo Yii::t("common","Learn more")?></a>
			</div>
			<div class="col-xs-12 col-sm-6 dotted-left">
				<h1 class="no-padding icon-sign">
					<i class="fa fa-heart"></i>
				</h1>
				<h2 class="title-biff"><?php echo Yii::t("home","Act together")?></h2>
				<?php echo Yii::t("home","We believe that NGOs and clubs are invaluable social fabric creators that enhance community life")?><br>
				<a href="https://www.helloasso.com/associations/open-atlas/collectes/communecter" target="_blank" class="more-info fs-16 margin-top-20"><?php echo Yii::t("home","Know more about the HelloAsso website")?></a>
			</div>
		</div>
		
	</div>


	<div class="col-xs-12 all-padding-section" style="min-height: 200px;">
		<div class="col-xs-12 col-md-12 no-padding margin-top-30">
			<div class="col-xs-12 col-sm-3 col-md-4 col-lg-4 first-detail">
				<h2 class="title-biff  text-green"><i class="fa fa-envelope"></i>&nbsp; Contact</h2>
				<div class="text-foot">
					<a href="mailto:contact@communecter.org"><strong>Open Atlas</strong></a>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 te">
				<h2 class="title-biff text-green">Communecter</h2>
				<div class="text-foot">

					<a href="javascript:;" id="who-are">
						- <?php echo Yii::t("home","Who are we ?")?>
					</a><br>
					<a href="javascript:;" id="our-project">
						- <?php echo Yii::t("home","Our open source project : now and tomorrow")?>
					</a><br>
					<a href="javascript:;" id="more-doc">
						- <?php echo Yii::t("home","More documentation")?>
					</a><br>
					<a href="javascript:;" id="become-ambassador">
						- <?php echo Yii::t("home","Become a Communecter ambassador")?>
					</a><br>
					<a href="javascript:;" class="participate-dev" id="participate-dev">
						- <?php echo Yii::t("home","Want to participate to development ?")?>
					</a><br>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 te">
				<h2 class="title-biff text-green"><?php echo Yii::t("common","About") ?></h2>
				<div class="text-foot">
					<a href="#confidentialityRules" target="_blank" >
						<?php echo Yii::t("common","Privacy policy")?>
					</a><br>
					<a href="#mentions" target="_blank" >
						<?php echo Yii::t("common","Legal mentions")?>
					</a><br>
					<a href="#charte" target="_blank" >
						<?php echo Yii::t("common","Using chart")?>
					</a><br>
				</div>
			</div>


		</div>
	</div>

	




<script type="text/javascript">
var dyfPoiActu={
    		"beforeBuild":{
		        "properties" : {
	                "shortDescription" : {
	                	"label":"Description courte",
	                	"inputType" : "text"
	                },
	                "category" : {
	                	"inputType" : "hidden",
	                	"value" : "co-actualite"
	                }
		           
		        }
		    },
			 "onload" : {
		        "actions" : {
	                "setTitle" : "Ajouter une Actualité",
	                "html" : {
	                    "infocustom" : "<br/>Remplir le formulaire"
	                },
	                "presetValue" : {
	                    "type" : "article",
	                },
	                "hide" : {
	                    "breadcrumbcustom" : 1,
	                    "parentfinder" : 1,
	                }
	            }
		    }
		};
		dyfPoiActu.afterSave = function(data){
			dyFObj.commonAfterSave(data,function(){
				mylog.log("data", data);
				 //location.hash = "#documentation."+data.id;
				 urlCtrl.loadByHash(location.hash);

			});
		};
		
  

	
<?php $layoutPath = '@themes/'.Yii::app()->theme->name.'/views/layouts/';
	  echo $this->renderPartial($layoutPath.'home/peopleTalk'); ?>
var peopleTalkCt = 0;

jQuery(document).ready(function() {
	//carousel
      $('.carousel[data-type="multi"] .item').each(function(){
          var next = $(this).next();
          if (!next.length) {
              next = $(this).siblings(':first');
          }
          next.children(':first-child').clone().appendTo($(this));
          
          for (var i=0;i<2;i++) {
            next=next.next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }
            
            next.children(':first-child').clone().appendTo($(this));
          }
      });

      $('.carousel').carousel({
      	interval: 5000
      });


      	//Add Actu co
      	$("#add-actu-co").click(function(){
					dyFObj.openForm('poi',null, null,null,dyfPoiActu);
				})
	

      //onclick open modal
      $("#costum-popover").click(function(){
	    urlCtrl.modalExplain({
      		logo : "<?php echo $this->module->assetsUrl; ?>/images/logo-costum.png",
      		name : "COSTUM",
      		title : "<?php echo Yii::t("home","Your customized Communecter, tailored social network")?>",
      		text : "<?php echo Yii::t("home","Designed for allowing you to create your own specific Communecter that matches your needs.<br>You can enjoy all the new features that have been developed for Communecter.org, while having the opportunity to manage numerous settings in order to customize your plateform.")?>",
      		link : "https://doc.co.tools/books/2---utiliser-loutil/page/costum"
      	})
	  });

	  $("#coform-popover").click(function(){
	    urlCtrl.modalExplain({
      		logo : "<?php echo $this->module->assetsUrl; ?>/images/logo-coform.png",
      		name : "COFORM",
      		title : "<?php echo Yii::t("home","Freely create forms that are connected to the whole Communecter's toolbox")?>",
      		text : "<?php echo Yii::t("home","Everyone uses Google Forms. An new type of open source solution totally connected to the goals and stakes that actors, institutions and territories are setting and facing, and that would respect the securing of the collected data. That is why we offer COForms in order to launch a survey, a call for proposals, forms, ... <br> and many more options.")?>",
      		link : "https://chat.communecter.org/channel/coform"
      	})
	  });

	  $("#coObservatoire-popover").click(function(){
	    urlCtrl.modalExplain({
      		logo : "<?php echo $this->module->assetsUrl; ?>/images/logo-coObservatoire.png",
      		name : "COBSERVATOIRE",
      		title : "<?php echo Yii::t("home","Data visualized, Trasnformed and interconnected to truly understand it.")?>.",
      		text : "<?php echo Yii::t("home","The observatory is dynamic insofar as it is connected to the information collected from different sources.<br> The information is transformed into visualisations. <br>Makes it easy to analyze within a territory or a sectorial perspective")?>.",
      		link : "https://chat.communecter.org/channel/coform"
      	})
	  });

	  $("#smarterre-popover").click(function(){
	    urlCtrl.modalExplain({
      		logo : "<?php echo $this->module->assetsUrl; ?>/images/logo-smarterritoire.png",
      		name : "SMARTERRITOIRE",
      		title : "<?php echo Yii::t("home","Concret and practical for an efficient, connected, smart and learning territory")?>.",
      		text : "<?php echo Yii::t("home","A territory where information is fluid and transparent, where developing projects and actions are permanently shared. Common tools are being developed and improved in order to create a territory as open as Wikipedia, as connected as Facebook, whose activity converge towards the creation and consolidation of Commons. Implementation on an efficient, connected, smart and learning territory")?>.",
      		link : "https://chat.communecter.org/channel/cocity"
      	})
	  });

	  $("#cocity-popover").click(function(){
	    urlCtrl.modalExplain({
      		logo : "<?php echo $this->module->assetsUrl; ?>/images/logo-cocity.png",
      		name : "COCITY",
      		title : "<?php echo Yii::t("home","Cities connected with participative and open sectors. Tools that enhance knowledge, understanding and collaboration")?>.",
      		text : "<?php echo Yii::t("home","Experimenting cities differently, with participative sectors, knowledge transfers and fluid information flows will open up our cities and create new means of communications and knowledge")?>.",
      		link : "https://chat.communecter.org/channel/cocity"
      	})
	  });

	  $("#oceco-popover").click(function(){
	    urlCtrl.modalExplain({
      		logo : "<?php echo $this->module->assetsUrl; ?>/images/oceco.png",
      		name : "OCECO",
      		title : "<?php echo Yii::t("home","New tools for collaboration and methods of cooperative economy")?>",
      		text : "<?php echo Yii::t("home","Improve collective work dynamics through a new digital technology that supports decision making processes, horizontal teams and organization management.<br>Experiment a process that promotes intrapreneurship, role clarity and activities management through a simplified process coupled with a strong methodology.<br>Encourage employee involvement and switch posture.<br>Develop activity towards new innovative, agile and collaborative work forms and organisation")?>.",
      		link : "https://oce.co.tools/"
      	})
	  });
    $("#fediverse-popover").click(function(){
        urlCtrl.modalExplain({
            logo : "<?php echo $this->module->assetsUrl; ?>/images/fediverse.png",
            name : "FEDIVERSE",
            title: "<?= Yii::t("home", "Communecter is growing: Join us in the Fediverse !") ?>",
            text : "<?= Yii::t("home", "Communecter is expanding its horizons by becoming a member of the Fediverse network. This means that you can now interact with a broader audience and enjoy an even more diverse sharing experience.") ?>",
			link : {
				getingstarted: {
					label: "<?= Yii::t("home", "How does it work ?") ?>",
					icon: "question-circle",
					link: "javascript:;",
					onclick: 'activitypubGuide.actions.open(activitypubGuide.constants.steps.SETTING)'
				}
			}
		})
    });

	  $("#foss-popover").click(function(){
	    urlCtrl.modalExplain({
      		logo : "<?php echo $this->module->assetsUrl; ?>/images/cotools.png",
      		name : "COTOOLS",
      		title : "<?php echo Yii::t("home","Hosted and available FOSS tools connected to your account")?>",
      		text : "Tchat <a href='https://chat.communecter.org' target='_blank'><img class='img-tools' src='<?php echo $this->module->assetsUrl; ?>/images/chat.jpg' alt='Rocket Chat'> Rocket Chat</a><br>"+
      			   "Kanban <a href='https://wekan.communecter.org' target='_blank'><img class='img-tools' src='<?php echo $this->module->assetsUrl; ?>/images/wekan.jpg' alt='Wekan'> Wekan</a><br>"+
      			   "Drive <a href='https://conextcloud.communecter.org' target='_blank'><img class='img-tools' src='<?php echo $this->module->assetsUrl; ?>/images/cloud.jpg' alt='Next Cloud'> Next Cloud</a><br>"+
      			   "Video <a href='https://peertube.communecter.org/' target='_blank'><img class='img-tools' src='<?php echo $this->module->assetsUrl; ?>/images/peertube.jpg' alt='Peertube'> Peertube</a><br>"+
      			   "Pad <a href='https://codimd.communecter.org/' target='_blank'><img class='img-tools' src='<?php echo $this->module->assetsUrl; ?>/images/codimd.jpg' alt='Codimd'> Codimd</a><br>"+
      			   "Visio <a href='https://meet.jit.si/co' target='_blank'><img class='img-tools' src='<?php echo $this->module->assetsUrl; ?>/images/jitsi.jpg' alt='Jitsi'> Jitsi</a>"
      	})
	  });

	  $("#open-source-popover").click(function(){
	    urlCtrl.modalExplain({
      		logo : "<?php echo $this->module->assetsUrl; ?>/images/home/valeurs/valeur01.png",
      		title : "<?php echo Yii::t("home","Open Source") ?> ",
      		text : "<?php echo Yii::t("home","Area of the software sources in free access allowing, on the one hand, the free use of the software concerned and its derivatives, on the other hand, the exchanges leading to the optimization or the common development of the product used with its designers always mentioned.") ?>"
      	})
	  });

	  $("#no-advertisement-popover").click(function(){
	    urlCtrl.modalExplain({
      		logo : "<?php echo $this->module->assetsUrl; ?>/images/home/valeurs/valeur02.png",
      		title : "<?php echo Yii::t("home","No advertisement") ?> ",
      		text : "<?php echo Yii::t("home","You are guaranteed that there will never be any advertising on the platform and none of your data will be monetized.") ?>"
      	})
	  });

	  $("#protected-data-popover").click(function(){
	    urlCtrl.modalExplain({
      		logo : "<?php echo $this->module->assetsUrl; ?>/images/home/valeurs/valeur03.png",
      		title : "<?php echo Yii::t("home","Protected data") ?> ",
      		text :"<?php echo Yii::t("home","If we fight against") ?> <a href='https://gafam.laquadrature.net/' target='_blank'> GAFAMs </a>, <?php echo Yii::t("home","it is because we refuse to see our data manipulated. We guarantee that your personal data will never be manipulated") ?>.",
      	})
	  });

	  $("#free-price-popover").click(function(){
	    urlCtrl.modalExplain({
      		logo : "<?php echo $this->module->assetsUrl; ?>/images/home/valeurs/valeur08.png",
      		title : "<?php echo Yii::t("home","Free price") ?>",
      		text :"<?php echo Yii::t("home","The platform is free to use, but your contribution is more than welcome. For example, a recurring donation of 2€/month, paid by 100 people, would allow us to pay the servers for one year") ?>.",
      		link : "https://www.helloasso.com/associations/open-atlas/collectes/communecter/don"
      	})
	  });

	  $("#connected-territory-popover").click(function(){
	    urlCtrl.modalExplain({
      		logo : "<?php echo $this->module->assetsUrl; ?>/images/home/valeurs/valeur06.png",
      		title : "<?php echo Yii::t("home","Connected territory") ?>",
      		text : "<?php echo Yii::t("home","A land of tradition adapted to exchanges through immaterial digital and/or material flows (airports, ports, stations, road networks) bringing to any person living in this territory, even if isolated, a capacity to establish international links") ?> ..."
      	})
	  });

	  $("#collective-intelligence-popover").click(function(){
	    urlCtrl.modalExplain({
      		logo : "<?php echo $this->module->assetsUrl; ?>/images/home/valeurs/valeur07.png",
      		title : "<?php echo Yii::t("home","Collective intelligence") ?>",
      		text : "<?php echo Yii::t("home","A distributed intelligence, constantly enhanced, coordinated in real time, which leads to an effective mobilization of skills") ?>."
      	})
	  });

	  $("#society-popover").click(function(){
	    urlCtrl.modalExplain({
      		logo : "<?php echo $this->module->assetsUrl; ?>/images/home/valeurs/valeur09.png",
      		title : "<?php echo Yii::t("home","Society 2.2.main") ?>",
      		text : "<?php echo Yii::t("home","We like to imagine and experiment, with and for you, new tools and methods to make our territories more efficient and to feel that the society evolves positively and in a participative way. Do not hesitate to contact us if we can help you develop an idea") ?>."
      	})
	  });

	  $("#commons-popover").click(function(){
	    urlCtrl.modalExplain({
      		logo : "<?php echo $this->module->assetsUrl; ?>/images/home/valeurs/valeur10.png",
      		title : "<?php echo Yii::t("home","Commons") ?>",
      		text : "<?php echo Yii::t("home","The commons are resources shared among a community of users who themselves determine the framework and norms regulating the management and use of their resource") ?>."
      	})
	  });


	  /* footer link onclick */
	  $("#who-are").click(function(){
	    urlCtrl.modalExplain({
      		type : "lg",
      		title : "<?php echo Yii::t("home","Who are we ?") ?>",
      		align : "left",
      		text : "<?php echo Yii::t("home","<b>Hyper-active contributors</b> <br> These people have a strong involvement in the project. Salaried or not of Open Atlas they invest sometimes for several years to develop the project. <These historical contributors to COmmunecter are delighted to welcome new people. It is not a closed circle in which you have to prove yourself but well and truly a very friendly open community :p.<br> Open Atlas regularly hires interns and civic services.<br><br> <b>Ambassadors</b> <br> There is also a nice community of ambassadors who develop the use of COmmunecter on the territory or in the community where they are.<br><br> <b>Active Pixels</b> <br> They regularly contribute to the project in a very diverse way. The ambassadors are of course also part of this set <br><br> <b>Friendly collectives and communities</b> <br> We are in relationship with many collectives. We feel particularly close and regularly exchange with La Myne, Chez Nous and La Raffinerie.<br> Our exchanges with other big communities (OpenStreetMap, Zero Waste, ...)") ?> "
      	})
	  });

	  $("#our-project").click(function(){
	    urlCtrl.modalExplain({
      		type : "lg",
      		title : "<?php echo Yii::t("home","Our open source project : now and tomorrow")?>",
      		align : "left",
      		text : "<?php echo Yii::t("home","<b>History of the project</b> <br> Communecter is a project currently carried by the Open Atlas association but whose roots lie in fifteen years of experimentations, trials, loss of motivation, rediscovered hopes, meetings, friendships and permanent innovation for all the actors who have gradually joined the adventure until today. <br><br>Since its creation in 2008, the Open Atlas association has been working on projects related to the commons, cartography and participatory democracy. It is a local association that works on territorial development in Reunion Island.")?>"
      	})
	  });

	  $("#more-doc").click(function(){
	    urlCtrl.modalExplain({
      		type : "lg",
      		title : "<?php echo Yii::t("home","More documentation")?>",
      		align : "left",
      		text : "<?php echo Yii::t("home","<b>Which platform would you like?</b> <br> We propose you to create your own personalized platform (called COstum). Thanks to our ingenious system you will benefit from the information present in communecter.org, and what will be published on your platform will also be visible on communecter.org. <br> - Holoptism and stigmergy of your Organization, Project, Event<br> - Call for projects system <br> - Sector observatory <br> - Citizen action platform <br> - Community management and resource enhancement <br> - Social network <br> Knowledge harvesting <br> - Volunteer time management & time bank <br><br> <b>Are you an organization? </b> <br> Communecter responds to the need of different context and adapts to your objectives, if you know the objectives you would like to achieve , we will help you to achieve them, and if necessary we can create custom tools and mutualize. <br> - a group of citizens or an Association <br> - a Cooperative or a Federation <br> - a Company <br> - a sector or a Company Branch <br> - a Territory: a region, a department, an agglomeration, a Commune <br> - Digital Public Space <br> - Media <br>")?>"
      	})
	  });

	  $("#become-ambassador").click(function(){
	    urlCtrl.modalExplain({
      		type : "lg",
      		title : "<?php echo Yii::t("home","Become a Communecter ambassador")?>",
      		align : "left",
      		text : "<?php echo Yii::t("home","<b>The role of ambassador</b> <br> The COmmunecter tool is built thanks to the involvement of many people: developers, communicators, testers, donors... They make this tool grow allowing us to network locally. But this work would be nothing without the work done by our field ambassadors. <br> <b>Massive communication plays an important role in the appropriation of the tool, but the ambassadors will always have more impact than the most beautiful newsletter in the world.</b><br> In direct relation with the actors of the territory, they propose, discuss and bring up the needs of the citizens of our communes. <br> The Open Atlas association networks and accompanies all the people wishing to use COmmunecter locally. Do not hesitate to introduce yourself in the group dedicated to the ambassadors.")?>"
      	})
	  }); 

	  $(".participate-dev").click(function(){
	    urlCtrl.modalExplain({
      		type : "lg",
      		title : "<?php echo Yii::t("home","Want to participate to development ?")?>",
      		align : "center",
      		text : "<b> <?php echo Yii::t("home","TECHNICAL UNIVERSE")?></b><br> "+
      				 "<a href='https://play.google.com/store/apps/details?id=org.communecter.mobile&hl=en_US' target='_blank'><?php echo Yii::t("home","COmmunecter on play store")?></a> <br>  "+
      				 "<a href='https://gitlab.adullact.net/pixelhumain/codoc/-/tree/master/04%20-%20Documentation%20technique/Installer%20Communecter' target='_blank'><?php echo Yii::t("home","Install")?> Communecter</a> <br>"+
      				"<a href='https://play.google.com/store/apps/details?id=org.communecter.oceco&hl=fr' target='_blank'>OCECO </a> <br>"+
      				"<a href='https://gitlab.adullact.net/pixelhumain/codoc' target='_blank'><?php echo Yii::t("home","Introduction to the code") ?></a> <br><br> "+

      				"<b><?php echo Yii::t("home","Everything is MODULAR")?></b> <br>"+
      				"<a href='https://gitlab.adullact.net/pixelhumain/co2' target='_blank'><?php echo Yii::t("home","TERRITORIAL ENGINE")?></a> <br>"+
      				"<a href='https://gitlab.adullact.net/pixelhumain/survey' target='_blank'>COFORM </a> <br> "+
      				"<a href='https://gitlab.adullact.net/pixelhumain/costum' target='_blank'>COSTUM </a> <br> "+
      				"<a href='https://gitla.badullact.net/pixelhumain/api2' target='_blank'>API </a> <br><br> "+
      				
      				"<b><?php echo Yii::t("home","OPEN SOURCE PROJECTS WE USE AND LOVE")?></b> <br> - PHP <br> - MONGO DB <br> - JQUERY <br> - BOOTSTRAP <br> - METEOR <br> - NODEJS <br> - NOUN PROJECT <br> - <?php echo Yii::t("home","and lots of pluggins")?> !"
      	})
	  });
	  


      //onclick slide search

		$('.energie').on('click', function () {
			location.hash = "#search?tags=<?php echo Yii::t("tags","energy")?>,<?php echo Yii::t("tags","renewable energy")?>,<?php echo Yii::t("tags","energy autonomy")?>,<?php echo Yii::t("tags","consumption")?>,<?php echo Yii::t("tags","autonomy")?>";
			urlCtrl.loadByHash(location.hash);
		});
		$('.environnement').on('click', function () {
			location.hash = "#search?tags=<?php echo Yii::t("tags","climate")?>,<?php echo Yii::t("tags","environment")?>,<?php echo Yii::t("tags","biodiversity")?>,<?php echo Yii::t("tags","ecology")?>,<?php echo Yii::t("tags","nature")?>,<?php echo Yii::t("tags","planet")?>,<?php echo Yii::t("tags","tree")?>,<?php echo Yii::t("tags","animals")?>,<?php echo Yii::t("tags","preservation")?>";
			urlCtrl.loadByHash(location.hash);
		});
		$('.alimentation').on('click', function () {
			location.hash = "#search?tags=<?php echo Yii::t("tags","agriculture")?>,<?php echo Yii::t("tags","food")?>,<?php echo Yii::t("tags","nutrition")?>,<?php echo Yii::t("tags","AMAP")?>,<?php echo Yii::t("tags","solidarity grocery store")?>,<?php echo Yii::t("tags","food autonomy")?>";
			urlCtrl.loadByHash(location.hash);
		});
		$('.economie').on('click', function () {
			location.hash = "#search?tags=<?php echo Yii::t("tags","common")?>,<?php echo Yii::t("tags","contribution")?>,<?php echo Yii::t("tags","economic model")?>,<?php echo Yii::t("tags","free currency")?>,<?php echo Yii::t("tags","complementary local currency")?>,<?php echo Yii::t("tags","barter")?>,<?php echo Yii::t("tags","sharing")?>,<?php echo Yii::t("tags","exchange system")?>";
			urlCtrl.loadByHash(location.hash);
		});
		$('.education').on('click', function () {
			location.hash = "#search?tags=<?php echo Yii::t("tags","education")?>,<?php echo Yii::t("tags","childhood")?>,<?php echo Yii::t("tags","extracurricular")?>,<?php echo Yii::t("tags","pedogogy")?>,<?php echo Yii::t("tags","teaching")?>";
			urlCtrl.loadByHash(location.hash);
		});
		$('.sante').on('click', function () {
			location.hash = "#search?tags=<?php echo Yii::t("tags","health")?>,<?php echo Yii::t("tags","medical care")?>,<?php echo Yii::t("tags","hospitals")?>,<?php echo Yii::t("tags","caregivers")?>,<?php echo Yii::t("tags","medicine")?>,<?php echo Yii::t("tags","alternative")?>";
			urlCtrl.loadByHash(location.hash);
		});
		$('.citoyen').on('click', function () {
			location.hash = "#search?tags=<?php echo Yii::t("tags","society")?>,<?php echo Yii::t("tags","citizen")?>,<?php echo Yii::t("tags","citizenship")?>,<?php echo Yii::t("tags","democracy")?>,<?php echo Yii::t("tags","solidarity")?>,<?php echo Yii::t("tags","referendum")?>,<?php echo Yii::t("tags","vote")?>,<?php echo Yii::t("tags","agorah")?>";
			urlCtrl.loadByHash(location.hash);
		});
		$('.culture').on('click', function () {
			location.hash = "#search?tags=<?php echo Yii::t("tags","culture")?>,<?php echo Yii::t("tags","animation")?>,<?php echo Yii::t("tags","concert")?>,<?php echo Yii::t("tags","theater")?>";
			urlCtrl.loadByHash(location.hash);
		});
		$('.sport').on('click', function () {
			location.hash = "#search?tags=<?php echo Yii::t("tags","sport")?>,<?php echo Yii::t("tags","adventure")?>,<?php echo Yii::t("tags","discovery")?>,<?php echo Yii::t("tags","workshop")?>,<?php echo Yii::t("tags","doityourself")?>,<?php echo Yii::t("tags","leisure")?>";
			urlCtrl.loadByHash(location.hash);
		});
		$('.transport').on('click', function () {
			location.hash = "#search?tags=<?php echo Yii::t("tags","urbanism")?>,<?php echo Yii::t("tags","transport")?>,<?php echo Yii::t("tags","construction")?>,<?php echo Yii::t("tags","development")?>,<?php echo Yii::t("tags","logistics")?>,<?php echo Yii::t("tags","mobility")?>,<?php echo Yii::t("tags","delivery")?>,<?php echo Yii::t("tags","housing")?>,<?php echo Yii::t("tags","autonomous housing")?>,<?php echo Yii::t("tags","light housing")?>";
			urlCtrl.loadByHash(location.hash);
		});

		// onclick search in searchbar

		$("#btn-search-home").click(function(){
			 $( "#search-form" ).submit();
        	var str = $("#otherInput").val();
        	location.hash = "#search?text="+str;
			urlCtrl.loadByHash(location.hash);
        
    	});



    	$( "#search-form" ).submit(function( event ) {
		  var str = $("#otherInput").val();
        	location.hash = "#search?text="+str;
			urlCtrl.loadByHash(location.hash);
		  event.preventDefault();
		});


    var delay = (function(){
	  var timer = 0;
	  return function(callback, ms){
	    clearTimeout (timer);
	    timer = setTimeout(callback, ms);
	  };
	})();

	var minlength = 3,
    searchRequest = null,
    dataSearch = searchInterface.constructObjectAndUrl();

	dataSearch.searchType = ["cities"];

	$("#location").keyup(function () {
	    var value = $(this).val();

	   if (value.length >= minlength ) {
	    delay(function(){
	    	$(".dropdown-result-global-search").show();
	        if (searchRequest != null) 
	            searchRequest.abort();
	        $(".dropdown-result-global-search").html("<div class='col-xs-12 text-center'><div class='search-loader text-dark col-xs-12' style='font-size:20px;'><i class='fa fa-spin fa-circle-o-notch'></i> Recherche en cours ...</div></div>");
	        dataSearch.name = value;
	        searchRequest = $.ajax({
	            type: "POST",
	            url: baseUrl+"/" + moduleId + "/search/globalautocomplete",
	            data: dataSearch,
	            success: function(result){
	                mylog.log("inputResult",result);
	                //alert(result.results.length+" resultat");
	                if (result.results.length == 0) {
	                	var city = $("#location").val();
	                	$(".dropdown-result-global-search").html("<div class='text-center col-md-12 col-sm-12 col-xs-12 padding-10'><label class='text-dark italic'><i class='fa fa-ban'></i> Aucune ville trouvée pour '"+city+"'</label><br><span class='info letter-blue'><i class='fa fa-info-circle'></i> Il est possible qu\'aucune donnée ne soit enregistrée dans cette zone</span></div>");
	                }else {

	                	htmlView="";
		                htmlView+='<div class="text-left col-xs-12" id="footerDropdownGS" style=""><label class="text-dark margin-top-5"><i class="fa fa-angle-down"></i> '+result.results.length+' résultats</label></div><hr style="margin: 0px; float:left; width:100%;">';
						$.each(result.results, function(e,v){
							var cp = (typeof v.postalCode != 'undefined') ? v.postalCode : '';
							var country = (typeof v.country != 'undefined') ? v.country : v.countryCode;
							mylog.log("valiny",v);
							htmlView+='<a href="javascript:" class="col-md-12 col-sm-12 col-xs-12 no-padding communecterSearch item-globalscope-checker searchEntity" data-scope-id="'+v._id.$id+'" data-scope-value="'+v._id.$id+'city'+v.postalCode+'" data-scope-level1name="'+v.level1Name+'" data-scope-level="'+v.level1+'" data-scope-country="'+v.country+'" data-scope-name="'+v.name+'" data-scope-level="city" data-scope-cp="'+cp+'" data-scope-postalcodes="'+v.postalCodes+'" data-scope-type="'+v.type+'" data-scope-notsearch="true" data-append-container="#costum-scope-search .scopes-container">'+
									'<div class="col-xs-12 margin-bottom-10 city undefined ">'+
										'<div class="padding-10 informations">'+
											'<div class="entityRight no-padding">'+
												'<span class="entityName letter-red ">'+
													'<i class="fa fa-university"></i><span> '+v.name+'</span><br>'+
														'<span style="color : grey; font-size : 13px">'+country+'</span>'+
												'</span>'+
											'</div>'+
										'</div>'+
									'</div>'+
								'</a>';
						});
						$( '.dropdown-result-global-search' ).html(htmlView);

						$('.communecterSearch').on('click', function () {
							var btnScope = $(this);
							if (btnScope.data("scope-type") == "city") var typeCitie = "cities"
							else var typeCitie = "zones"
	
							if (btnScope.data("scope-cp") != "") var cpData = "cp"+btnScope.data("scope-cp")
							else var cpData = ""

							location.hash = "#search?scopeType=open&"+typeCitie+"="+btnScope.data('scope-id')+cpData;
							urlCtrl.loadByHash(location.hash);	
						});

				}


	            }
	        });
	    }, 1000 );
	   }else $(".dropdown-result-global-search").hide();
	});

});

//Show list Actualité
 params = {
	 	"type" : "article",
	 	"category" : "co-actualite",        
		searchType : ["poi"],
		fields : ["name","shortDescription","profilMediumImageUrl"]
  };
ajaxPost(
       null,
       baseUrl+"/" + moduleId + "/search/globalautocomplete",
       params,
       function(data){
      		mylog.log("CO-actualité",data.results);
      		var html = "";
          $.each(data.results, function( index, value ) {
          	
            html += 
            	'<div class="swiper-slide">'+
            		'<a href="#page.type.poi.id.'+value._id.$id+'" class="lbh-preview-element">'+
				      		'<div class="picture">';
              
                       if (typeof value.profilMediumImageUrl && value.profilMediumImageUrl != null) 
           html +=         '<img class="" src="'+value.profilMediumImageUrl+'" alt="card image">';
                       else
           html +=         '<img class="" src="<?php echo $this->module->assetsUrl; ?>/images/logo-costum.png"> ';
                      

           html +=    '</div>'+
           						'<div class="detail">';
				        
           			if (typeof value.name && value.name != null)
           html +=        '<h3 >'+value.name+'</h3>';
                      /*if (typeof value.shortDescription && value.shortDescription != null)
          html +=          '<span>'+value.shortDescription+'</span>';*/
          html +=                
              				'</div>'+
              			'</a>'+
              	'</div>';
             


          });
      		$(".swiper-wrapper").html(html);
      		coInterface.bindLBHLinks();
      		var swiper = new Swiper(".mySwiper", {
		        effect: "coverflow",
		        grabCursor: true,
		        centeredSlides: true,
		        slidesPerView: "auto",
		        autoplay: {
						    delay: 5000,
						  },
		        coverflowEffect: {
		          rotate: 20,
		          stretch: 0,
		          depth: 350,
		          modifier: 1,
		          slideShadows: true,
		        },
		        pagination: {
		          el: ".swiper-pagination",
		        },
		      });
      	}
	);


function openVideo(){
	$("#videoDocsImg").fadeOut("slow",function() {
		heightCont=$("#form-home-subscribe").outerHeight();
		$(".videoWrapper").height(heightCont);
		$(".videoWrapper").fadeIn('slow');
		 var symbol = $("#autoPlayVideo")[0].src.indexOf("?") > -1 ? "&" : "?";
  		//modify source to autoplay and start video
  		$("#autoPlayVideo")[0].src += symbol + "autoplay=1";
  		if($("#form-home-subscribe").length)
  			$(".videoWrapper .h_iframe").css({"margin-top": ((heightCont-$(".videoWrapper .h_iframe").height())/2)+"px"});
	});
}

//particle

$.getScript("<?php echo $this->module->assetsUrl; ?>/js/particles.min.js", function(){
    particlesJS('particles-js',
      {
        "particles": {
          "number": {
            "value": 80,
            "density": {
              "enable": true,
              "value_area": 800
            }
          },
          "color": {
            "value": "#9fbd38"
          },
          "shape": {
            "type": "circle",
            "stroke": {
              "width": 0,
              "color": "#36c3eb"
            },
            "polygon": {
              "nb_sides": 5
            },
            "image": {
              "width": 100,
              "height": 100
            }
          },
          "opacity": {
            "value": 0.5,
            "random": false,
            "anim": {
              "enable": false,
              "speed": 1,
              "opacity_min": 0.1,
              "sync": false
            }
          },
          "size": {
            "value": 5,
            "random": true,
            "anim": {
              "enable": false,
              "speed": 40,
              "size_min": 0.1,
              "sync": false
            }
          },
          "line_linked": {
            "enable": true,
            "distance": 150,
            "color": "#3f4e58",
            "opacity": 0.4,
            "width": 1
          },
          "move": {
            "enable": true,
            "speed": 4,
            "direction": "none",
            "random": false,
            "straight": false,
            "out_mode": "out",
            "attract": {
              "enable": false,
              "rotateX": 600,
              "rotateY": 1200
            }
          }
        },
        "interactivity": {
          "detect_on": "canvas",
          "events": {
            "onhover": {
              "enable": true,
              "mode": "repulse"
            },
            "onclick": {
              "enable": true,
              "mode": "push"
            },
            "resize": true
          },
          "modes": {
            "grab": {
              "distance": 400,
              "line_linked": {
                "opacity": 1
              }
            },
            "bubble": {
              "distance": 400,
              "size": 40,
              "duration": 2,
              "opacity": 8,
              "speed": 3
            },
            "repulse": {
              "distance": 200
            },
            "push": {
              "particles_nb": 4
            },
            "remove": {
              "particles_nb": 2
            }
          }
        },
        "retina_detect": true,
        "config_demo": {
          "hide_card": false,
          "background_color": "#b61924",
          "background_image": "",
          "background_position": "50% 50%",
          "background_repeat": "no-repeat",
          "background_size": "cover"
        }
      }
    );

});


</script>
