<?php
$background="background:white";
$color1 = "#12CB56";
$color2 =  "#f37272";
$border1 = "5px";
$border2 = "5px";
$logoLoader=Yii::app()->theme->baseUrl.'/assets/img/logos/logo.png';
if(isset($this->costum)){
    if (isset($this->costum["htmlConstruct"]["loadingModal"]["logo"]))
        $logoLoader = $this->costum["htmlConstruct"]["loadingModal"]["logo"];
    elseif (isset($this->costum["loaderImg"]))
        $logoLoader = $this->costum["loaderImg"];
    elseif (isset($this->costum["logo"]))
        $logoLoader = $this->costum["logo"];

    if(isset($this->costum["css"]) && isset($this->costum["css"]["loader"])){
        if(isset($this->costum["css"]["loader"]["background"]))
            $background="background:".$this->costum["css"]["loader"]["background"];
        if(isset($this->costum["css"]["loader"]["ring1"])){
            $ring1=$this->costum["css"]["loader"]["ring1"];
            if(isset($ring1["color"])){
                $color1 = $ring1["color"];
            }
            if(isset($ring1["borderWidth"])){
                $border1 = $ring1["borderWidth"];
            }
        }
        if(isset($this->costum["css"]["loader"]["ring2"])){
            $ring2=$this->costum["css"]["loader"]["ring2"];
            if(isset($ring2["color"])){
                $color2 = $ring2["color"];
            }
            if(isset($ring2["borderWidth"])){
                $border2 = $ring1["borderWidth"];
            }
        }
    }
}

?>
<style>
    #ct-loadding {
        height: 100%;
        position: fixed;
        width: 100%;
        z-index: 999999;
        top: 0;
        left: 0;
        -webkit-transition: all 300ms linear 0ms;
        -khtml-transition: all 300ms linear 0ms;
        -moz-transition: all 300ms linear 0ms;
        -ms-transition: all 300ms linear 0ms;
        -o-transition: all 300ms linear 0ms;
        transition: all 300ms linear 0ms;
         <?php echo $background ?>;
        -webkit-transform: scale(1);
        -khtml-transform: scale(1);
        -moz-transform: scale(1);
        -ms-transform: scale(1);
        -o-transform: scale(1);
        transform: scale(1);
    }
    #ct-loadding .loading-infinity {
        width: 120px;
        height: 60px;
        position: absolute;
        top: 50%;
        left: 50%;
        -webkit-transform: translate(-50%, -50%);
        -khtml-transform: translate(-50%, -50%);
        -moz-transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        -o-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
    }
    #ct-loadding .loading-infinity div {
        top: 0;
        left: 50%;
        width: 70px;
        height: 70px;
        animation: ctLoadingrotate 6.9s linear infinite;
    }
    #ct-loadding .loading-infinity div, #ct-loadding .loading-infinity span {
        position: absolute;
    }
    #ct-loadding .loading-infinity div:nth-child(2) {
        animation-delay: -2.3s;
    }
    #ct-loadding .loading-infinity div:nth-child(3) {
        animation-delay: -4.6s;
    }
    #ct-loadding .loading-infinity div span {
        left: -10px;
        top: 50%;
        margin: -10px 0 0 0;
        width: 20px;
        height: 20px;
        display: block;
        background: <?php echo $color1 ?>;
        box-shadow: 2px 2px 8px rgba(0, 124, 251, .09);
        border-radius: 50%;
        transform: rotate(90deg);
        animation: ctLoadingmove 6.9s linear infinite;
    }
    #ct-loadding .loading-infinity div span:before {
        animation: ctLoadingdrop1 0.8s linear infinite;
    }
    #ct-loadding .loading-infinity div span:before, #ct-loadding .loading-infinity div span:after {
        content: '';
        position: absolute;
        display: block;
        border-radius: 50%;
        width: 20px;
        height: 20px;
        background: inherit;
        top: 50%;
        left: 50%;
        margin: -10px 0 0 -10px;
        box-shadow: inherit;
    }
    #ct-loadding .loading-infinity div span:after {
        animation: ctLoadingdrop2 0.8s linear infinite 0.4s;
    }
    @keyframes spanMoveAnimation { 
        100% {
            transform: translate(32px, 10px) scale(0);
        }
    }
    @keyframes ctLoadingdrop2 { 
        0% {
            transform: translate(0, 0) scale(0.9);
        }
        100% {
            transform: translate(32px, -10px) scale(0);
        }
    }
    @keyframes ctLoadingmove {
        0%, 50% {
            left: -10px;
        }
        25% {
            background: <?php echo $color2 ?>;
        }
        75% {
            background: #85cc02;
        }
        50.0001%, 100% {
            left: auto;
            right: -10px;
        }   
    }
    @keyframes ctLoadingrotate {
        50% {
            transform: rotate(360deg);
            margin-left: 0;
        }
        50.0001%, 100% {
            margin-left: -70px;
        }
    }
    @keyframes ctLoadingdrop1 {
        100% {
            transform: translate(32px, 10px) scale(0);
        }
    } 
</style>
<div id="ct-loadding" class="ct-loader style13" style="/* display: none; */">
    <div class="loading-infinity">
        <div>
            <span></span>
        </div>
        <div>
            <span></span>
        </div>
        <div>
            <span></span>
        </div>
    </div>
</div>