<?php
$background="background:white";
$color1 = "#12CB56";
$color2 =  "#f37272";

if(isset($this->costum)){

    if(isset($this->costum["css"]) && isset($this->costum["css"]["loader"])){
        if(isset($this->costum["css"]["loader"]["background"]))
            $background="background:".$this->costum["css"]["loader"]["background"];
        if(isset($this->costum["css"]["loader"]["ring1"])){
            $ring1=$this->costum["css"]["loader"]["ring1"];
            if(isset($ring1["color"])){
                $color1 = $ring1["color"];
            }

        }
        if(isset($this->costum["css"]["loader"]["ring2"])){
            $ring2=$this->costum["css"]["loader"]["ring2"];
            if(isset($ring2["color"])){
                $color2 = $ring2["color"];
            }
        }
    }
}



// }


?>

<style type="text/css">
    .loader_4_block {
        position: absolute;
        top: 50%;
        left: 50%;
        width: 80px;
        height: 80px;
        transform: translate(-50%, -50%) rotate(0deg) translate3d(0, 0, 0);
        animation: loader 1.4s infinite ease-in-out;
    }
     .loader_4_block span {
         position: absolute;
         display: block;
         width: 40px;
         height: 40px;
         background-color: <?php echo $color1 ?>;
         animation: loaderBlock 1.4s infinite ease-in-out both;
    }
     .loader_4_block span:nth-child(1) {
         top: 0;
         left: 0;
    }
     .loader_4_block span:nth-child(2) {
         top: 0;
         right: 0;
         animation: loaderBlockInverse 1.4s infinite ease-in-out both;
    }
     .loader_4_block span:nth-child(3) {
         bottom: 0;
         left: 0;
         animation: loaderBlockInverse 1.4s infinite ease-in-out both;
    }
     .loader_4_block span:nth-child(4) {
         bottom: 0;
         right: 0;
    }
     @keyframes loader {
        0%, 10%, 100% {
            width: 80px;
            height: 80px;
        }
         65% {
             width: 130px;
             height: 130px;
        }
    }
     @keyframes loaderBlock {
        0%, 30% {
            transform: rotate(0);
        }
         55% {
            background-color: <?php echo $color2 ?>;
        }
         100% {
            transform: rotate(360deg);
        }
    }
     @keyframes loaderBlockInverse {
        0%, 20% {
            transform: rotate(0);
        }
         55% {
            background-color: <?php echo $color2 ?>;
        }
         100% {
            transform: rotate(-360deg);
        }
    }
</style>
<div id="loadingModal" style="<?php echo $background ?>">
</div>
<div class="contentFirstLoading">
	<div class="loader_4_block">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
