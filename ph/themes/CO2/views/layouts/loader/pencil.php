<?php 
    $background="rgba(0,0,0, 0.9)";
    $color1 = "#09d56e";
    $color2 =  "#232123";
    $heightPencil = "35px";
    $lineSize = "10px";
    $colorText = "white";
    $logoLoader=Yii::app()->theme->baseUrl.'/assets/img/logos/logo.png';
    
    if(isset($this->costum)){
        if(isset($this->costum["htmlConstruct"]["loadingModal"]["logo"]) || isset($this->costum["logo"]))
            $logoLoader= (isset($this->costum["htmlConstruct"]["loadingModal"]["logo"])) ? $this->costum["htmlConstruct"]["loadingModal"]["logo"] : $this->costum["logo"] ;
        if(isset($this->costum["css"]) && isset($this->costum["css"]["loader"])){
            if(isset($this->costum["css"]["loader"]["background"]))
                $background="background:".$this->costum["css"]["loader"]["background"];
            if(isset($this->costum["css"]["loader"]["ring1"])){
                $ring1=$this->costum["css"]["loader"]["ring1"];
                if(isset($ring1["color"])){
                    $color1 = $ring1["color"];
                }
                if(isset($ring1["borderWidth"])){
                    $heightPencil = $ring1["borderWidth"];
                }
            }
            if(isset($this->costum["css"]["loader"]["ring2"])){
                $ring2=$this->costum["css"]["loader"]["ring2"];
                if(isset($ring2["color"])){
                    $color2 = $ring2["color"];
                }
                if(isset($ring2["borderWidth"])){
                    $lineSize = $ring1["borderWidth"];
                }
            }
        }
    }

?>

<style type="text/css">
    .pencil-loader {
        width: 100%;
        height: 100vh;
        overflow: hidden;
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
        background: <?php echo $background ?>;
    }

    .pencil {
        position: relative;
        width: 200px;
        height: 35px;
        transform-origin: center;
        transform: rotate(135deg);
        animation: pencil-animation 7s infinite;
        z-index: 2;
    }

    @keyframes pencil-animation {
        0% {
            transform: rotate(135deg);
        }
            20% {
                transform: rotate(315deg);
        }
            45% {
                transform: translateX(300px) rotate(315deg);
        }
            55% {
                transform: translateX(300px) rotate(495deg);
        }
            100% {
                transform: rotate(495deg);
        }
    }

    .pencil_ball-point {
        position: absolute;
        left: 0;
        top: 50%;
        transform: translateY(-50%);
        background: <?php echo $color1 ?>;
        height: 10px;
        width: 10px;
        border-radius: 50px;
    }

    .pencil_cap {
        position: absolute;
        left: 0px;
        top: 50%;
        transform: translateY(-50%);
        clip-path: polygon(20% 40%, 100% 0%, 100% 100%, 20% 60%);
        background: <?php echo $color2 ?>;
        width: 12%;
        height: 100%;
    }

    .pencil_cap-base {
        position: absolute;
        left: 12%;
        top: 0;
        height: 100%;
        width: 20px;
        background: <?php echo $color2 ?>;
    }

    .pencil_middle {
        position: absolute;
        left: calc(12% + 20px);
        top: 0;
        height: 100%;
        width: 70%;
        background: <?php echo $color1 ?>;
        background-image: url("<?php echo $logoLoader ?>");
        background-position: center;
        background-repeat: no-repeat;
        background-size: contain;
    }

    .pencil_eraser {
        position: absolute;
        left: calc(12% + 70% + 20px);
        top: 0;
        height: 100%;
        width: 11%;
        border-top-right-radius: 5px;
        border-bottom-right-radius: 5px;
        background: <?php echo $color2 ?>;
    }

    .line {
        position: relative;
        top: 50px;
        right: 69px;
        height: 10px;
        width: 1000px;
        z-index: 1;
        border-radius: 50px;
        background: <?php echo $color1 ?>;
        transform: scaleX(0);
        transform-origin: center;
        animation: line-animation 7s infinite;
        border-radius: 30px;
    }

    @keyframes line-animation {
        20% {
            transform: scaleX(0);
    }
        45% {
            transform: scaleX(0.6);
    }
        55% {
            transform: scaleX(0.6);
    }
        100% {
            transform: scaleX(0);
    }
    }

    .loading-title {
        margin-top: 80px;
        font-weight: lighter;
        font-size: 20px;
        animation: fade-in-fade-out 1s linear infinite;
        letter-spacing: 2px;
        color: <?php echo $colorText ?>
    }

    @keyframes fade-in-fade-out {
        0% {
            opacity: 0;
        }
        20% {
            opacity: 0.2;
        }
        40% {
            opacity: 0.4;
        }
        60% {
            opacity: 0.6;
        }
        80% {
            opacity: 0.8;
        }
        100% {
            opacity: 1;
        }
    }
</style>

<div class="pencil-loader">
    <div class="pencil">
        <div class="pencil_ball-point"></div>
        <div class="pencil_cap"></div>
        <div class="pencil_cap-base"></div>
        <div class="pencil_middle"></div>
        <div class="pencil_eraser"></div>
    </div>
    <div class="line"></div>
    <h2 class="loading-title">Page loading...Please wait</h2>
</div>
