<?php
$background="background:white";
$color1 = "#12CB56";
$color2 =  "#f37272";
$border1 = "5px";
$border2 = "5px";
$logoLoader=Yii::app()->theme->baseUrl.'/assets/img/logos/logo.png';
if(isset($this->costum)){
    if (isset($this->costum["htmlConstruct"]["loadingModal"]["logo"]))
        $logoLoader = $this->costum["htmlConstruct"]["loadingModal"]["logo"];
    elseif (isset($this->costum["loaderImg"]))
        $logoLoader = $this->costum["loaderImg"];
    elseif (isset($this->costum["logo"]))
        $logoLoader = $this->costum["logo"];

    if(isset($this->costum["css"]) && isset($this->costum["css"]["loader"])){
        if(isset($this->costum["css"]["loader"]["background"]))
            $background="background:".$this->costum["css"]["loader"]["background"];
        if(isset($this->costum["css"]["loader"]["ring1"])){
            $ring1=$this->costum["css"]["loader"]["ring1"];
            if(isset($ring1["color"])){
                $color1 = $ring1["color"];
            }
            if(isset($ring1["borderWidth"])){
                $border1 = $ring1["borderWidth"];
            }
        }
        if(isset($this->costum["css"]["loader"]["ring2"])){
            $ring2=$this->costum["css"]["loader"]["ring2"];
            if(isset($ring2["color"])){
                $color2 = $ring2["color"];
            }
            if(isset($ring2["borderWidth"])){
                $border2 = $ring1["borderWidth"];
            }
        }
    }
}

?>
<style type="text/css">
    .center-body-loader {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }
    .loader-ball {
        /*color: #EEE;*/
        animation: anm-BL-rotate 3.33334s infinite linear;
        font-size: 200%;
        height: 220px;
        line-height: 0;
        top: 50%;
        left: 50%;
        position: absolute;
        width: 220px;
    }
    img.loader-ball-img {
        width:150px;
        height:auto;
    }
    .loader-ball > div {
        background-color: <?php echo $color1 ?>;
        border-radius: 50%;
        display: inline-block;
        height: 15px;
        position: absolute;
        width: 15px;
        vertical-align: top;
    }
    .loader-ball > div::after {
        content: "";
        display: inline-block;
        background-color: <?php echo $color2 ?>;
        position: absolute;
    }
    .loader-ball .dot1 {
        left: 0;
        top: 0;
    }
    .loader-ball .dot1::after {
        animation: anm-BL-moveH 1.66667s -1.2500025s infinite both linear;
        width: 180px;
        height: 4px;
        top: 5px;
        left: 12px;
    }
    .loader-ball .dot2 {
        bottom: 0;
        left: 0;
    }
    .loader-ball .dot2::after {
        animation: anm-BL-moveV 1.66667s -1.2500025s infinite reverse both linear;
        width: 4px;
        height: 180px;
        bottom: 12px;
        left: 5px;
    }
    .loader-ball .dot3 {
        bottom: 0;
        right: 0;
    }
    .dot3::after {
        animation: anm-BL-moveH 1.66667s -1.66667s infinite reverse both linear;
        width: 180px;
        height: 4px;
        bottom: 5px;
        right: 12px;
    }
    .loader-ball .dot4 {
        top: 0;
        right: 0;
    }
    .loader-ball .dot4::after {
        animation: anm-BL-moveV 1.66667s -0.833335s infinite both linear;
        height: 180px;
        width: 4px;
        top: 12px;
        right: 5px;
    }
    @keyframes anm-BL-rotate {
        from {
            transform: translate(-50%, -50%) rotate(360deg);
        }
        to {
            transform: translate(-50%, -50%) rotate(0deg);
        }
    }
    @keyframes anm-BL-moveV {
        from {
            transform: scale(1, 0);
            transform-origin: 0 0;
        }
        12% {
            transform: scale(1, 1);
            transform-origin: 0 0;
        }
        13% {
            transform: scale(1, 1);
            transform-origin: 0 100%;
        }
        25% {
            transform: scale(1, 0);
            transform-origin: 0 100%;
        }
        100% {
            transform: scale(1, 0);
            transform-origin: 0 100%;
        }
    }
    @keyframes anm-BL-moveH {
        from {
            transform: scale(0, 1);
            transform-origin: 0 0;
        }
        12% {
            transform: scale(1, 1);
            transform-origin: 0 0;
        }
        13% {
            transform: scale(1, 1);
            transform-origin: 100% 0;
        }
        25% {
            transform: scale(0, 1);
            transform-origin: 100% 0;
        }
        100% {
            transform: scale(0, 1);
            transform-origin: 100% 0;
        }
    }
    @media (max-width: 768px) {
        .loader-ball {
            height: 120px;
            width: 120px;
        }
        img {
            width:80px;
        }
        .loader-ball .dot1::after {
            width: 100px;
        }
        .loader-ball .dot2::after {
            height: 100px;
        }
        .loader-ball .dot3::after {
            width: 100px;
        }
        .loader-ball .dot4::after {
            height: 100px;
        }
    }
</style>
<div id="loadingModal" style="<?php echo $background ?>">
</div>
<div class="contentFirstLoading">
    <div class="center-body-loader">
        <img class="loader-ball-img" src="<?php echo $logoLoader ?>">
        <div class="loader-ball">
            <div class="dot1"></div>
            <div class="dot2"></div>
            <div class="dot3"></div>
            <div class="dot4"></div>
        </div>
    </div>
</div>

