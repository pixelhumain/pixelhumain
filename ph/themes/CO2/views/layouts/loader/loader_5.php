<?php
$background="background:white";
$color1 = "#12CB56";
$color2 =  "#f37272";
$border1 = "5px";
$border2 = "5px";
$logoLoader=Yii::app()->theme->baseUrl.'/assets/img/logos/logo.png';
if(isset($this->costum)){
    if (isset($this->costum["htmlConstruct"]["loadingModal"]["logo"]))
        $logoLoader = $this->costum["htmlConstruct"]["loadingModal"]["logo"];
    elseif (isset($this->costum["loaderImg"]))
        $logoLoader = $this->costum["loaderImg"];
    elseif (isset($this->costum["logo"]))
        $logoLoader = $this->costum["logo"];

    if(isset($this->costum["css"]) && isset($this->costum["css"]["loader"])){
        if(isset($this->costum["css"]["loader"]["background"]))
            $background="background:".$this->costum["css"]["loader"]["background"];
        if(isset($this->costum["css"]["loader"]["ring1"])){
            $ring1=$this->costum["css"]["loader"]["ring1"];
            if(isset($ring1["color"])){
                $color1 = $ring1["color"];
            }
            if(isset($ring1["borderWidth"])){
                $border1 = $ring1["borderWidth"];
            }
        }
        if(isset($this->costum["css"]["loader"]["ring2"])){
            $ring2=$this->costum["css"]["loader"]["ring2"];
            if(isset($ring2["color"])){
                $color2 = $ring2["color"];
            }
            if(isset($ring2["borderWidth"])){
                $border2 = $ring1["borderWidth"];
            }
        }
    }
}

?>
<style type="text/css">
    .center-body-loader {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }
    .loader-square {
        width: 180px;
        height: 180px;
        display: inline-block;
        position: relative;
    }
    .loader-square img {
        width: 140px;
        height: auto;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%,-50%);
    }
    .loader-square:before,
    .loader-square::after {
        content: "";
        width: 100%;
        height: 100%;
        display: inline-block;
        border-radius: 10px;
    }
    .loader-square:before {
        border: solid <?php echo $border1 ?> <?php echo $color1 ?>;
        animation: loader-sq 1s infinite;
    }
    .loader-square::after {
        position: absolute;
        display: inline-block;
        animation: loader--backwards 1s infinite;
        top: 0;
        left: 0;
        border: solid <?php echo $border2 ?> <?php echo $color2 ?>;
    }
    @keyframes loader-sq {
        50% {
            transform: rotate(45deg);
            opacity: .8;
        }
    }
    @keyframes loader--backwards {
        50% {
            transform: rotate(-45deg);
            opacity: .4;
        }
    }
    @media (max-width: 768px) {
        .loader-square {
            width: 120px;
            height: 120px;
        }
        .loader-square img {
            width: 80px;
        }
    }
</style>
<div id="loadingModal" style="<?php echo $background ?>">
</div>
<div class="contentFirstLoading">
    <div class="center-body-loader">
        <div class="loader-square">
            <img src="<?php echo $logoLoader ?>">
        </div>
    </div>
</div>