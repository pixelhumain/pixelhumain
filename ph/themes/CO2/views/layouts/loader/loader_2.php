<?php
$background="background:white";
$color1 = "#12CB56";
$color2 =  "#f37272";
$border1 = "2px";
$border2 = "2px";
$logoLoader=Yii::app()->theme->baseUrl.'/assets/img/logos/logo.png';
if(isset($this->costum)){
    if (isset($this->costum["htmlConstruct"]["loadingModal"]["logo"]))
        $logoLoader = $this->costum["htmlConstruct"]["loadingModal"]["logo"];
    elseif (isset($this->costum["loaderImg"]))
        $logoLoader = $this->costum["loaderImg"];
    elseif (isset($this->costum["logo"]))
        $logoLoader = $this->costum["logo"];

    if(isset($this->costum["css"]) && isset($this->costum["css"]["loader"])){
        if(isset($this->costum["css"]["loader"]["background"]))
            $background="background:".$this->costum["css"]["loader"]["background"];
        if(isset($this->costum["css"]["loader"]["ring1"])){
            $ring1=$this->costum["css"]["loader"]["ring1"];
            if(isset($ring1["color"])){
                $color1 = $ring1["color"];
            }
            if(isset($ring1["borderWidth"])){
                $border1 = $ring1["borderWidth"];
            }
        }
        if(isset($this->costum["css"]["loader"]["ring2"])){
            $ring2=$this->costum["css"]["loader"]["ring2"];
            if(isset($ring2["color"])){
                $color2 = $ring2["color"];
            }
            if(isset($ring2["borderWidth"])){
                $border2 = $ring1["borderWidth"];
            }
        }
    }
}



// }


?>
<style type="text/css">
    @-webkit-keyframes dyinglight {
        15% {
            transform: scale(1.6);
        }
        50% {
            transform: rotate(-89deg);
        }
        100% {
            transform: rotate(-90deg);
        }
    }

    @keyframes dyinglight {
        15% {
            transform: scale(1.6);
        }
        50% {
            transform: rotate(-89deg);
        }
        100% {
            transform: rotate(-90deg);
        }
    }
    .dl {
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
        display: inline-block;
    }

    .dl__square {
        display: block;
        width: 200px;
        height: 200px;
       /* background: <?php echo $background ?>;*/
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .dl__container {
        position: absolute;
        left: 0;
        top: 0;
        bottom: 0;
        right: 0;
        transform-origin: 50% 50% 0;
        -webkit-animation: dyinglight 1s ease infinite;
        animation: dyinglight 1s ease infinite;
        -webkit-backface-visibility: hidden;
        backface-visibility: hidden;
    }

    .dl__corner--top:before,
    .dl__corner--top:after,
    .dl__corner--bottom:before,
    .dl__corner--bottom:after {
        position: absolute;
        width: 70px;
        height: 70px;
        /*color: <?php //echo $color1 ?>;*/
        content: "";
    }

    @media (max-width: 768px) {
        .dl__square {
            width: 120px;
            height: 120px;
        }
        .dl__corner--bottom:after {
            width: 40px;
            height: 40px;
        }
    }

    .dl__corner--top:before {
        border-left: <?php echo $border1 ?> solid;
        border-top: <?php echo $border1 ?> solid;
        top: -6px;
        left: -6px;
        color: <?php echo $color1 ?>;
    }
    .dl__corner--top:after {
        border-right: <?php echo $border2 ?> solid;
        border-top: <?php echo $border2 ?> solid;
        top: -6px;
        right: -6px;
        color: <?php echo $color2 ?>;
    }

    .dl__corner--bottom:before {
        border-left: <?php echo $border2 ?> solid;
        border-bottom: <?php echo $border2 ?> solid;
        color: <?php echo $color2 ?>;
        bottom: -6px;
        left: -6px;
    }
    .dl__corner--bottom:after {
        border-right: <?php echo $border1 ?> solid;
        border-bottom: <?php echo $border1 ?> solid;
        color: <?php echo $color1 ?>;
        bottom: -6px;
        right: -6px;
    }
    .dl__square img {
        width: 80%;
        height: auto;
        /*transform: translate(-50%, -50%);
        top: 50%;
        left: 50%;
        position: absolute;*/
        animation: zoomImg 1s linear infinite;
    }
    @keyframes zoomImg {
        15% {
            transform: scale(1.6);
        }
        50% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(0deg);
        }
    }
</style>

<div id="loadingModal" style="<?php echo $background ?>">
</div>
<div class="contentFirstLoading">
    <div class="dl">
        <div class="dl__container">
            <div class="dl__corner--top"></div>
            <div class="dl__corner--bottom"></div>
        </div>
        <div class="dl__square">
            <img src="<?php echo $logoLoader ?>">
        </div>
    </div>
</div>
