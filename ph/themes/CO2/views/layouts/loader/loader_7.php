<?php
$background="background:white";
$color1 = "#12CB56";
$color2 =  "#f37272";
$border1 = "5px";
$border2 = "5px";
$logoLoader=Yii::app()->theme->baseUrl.'/assets/img/logos/logo.png';
if(isset($this->costum)){
    if (isset($this->costum["htmlConstruct"]["loadingModal"]["logo"]))
        $logoLoader = $this->costum["htmlConstruct"]["loadingModal"]["logo"];
    elseif (isset($this->costum["loaderImg"]))
        $logoLoader = $this->costum["loaderImg"];
    elseif (isset($this->costum["logo"]))
        $logoLoader = $this->costum["logo"];

    if(isset($this->costum["css"]) && isset($this->costum["css"]["loader"])){
        if(isset($this->costum["css"]["loader"]["background"]))
            $background="background:".$this->costum["css"]["loader"]["background"];
        if(isset($this->costum["css"]["loader"]["ring1"])){
            $ring1=$this->costum["css"]["loader"]["ring1"];
            if(isset($ring1["color"])){
                $color1 = $ring1["color"];
            }
            if(isset($ring1["borderWidth"])){
                $border1 = $ring1["borderWidth"];
            }
        }
        if(isset($this->costum["css"]["loader"]["ring2"])){
            $ring2=$this->costum["css"]["loader"]["ring2"];
            if(isset($ring2["color"])){
                $color2 = $ring2["color"];
            }
            if(isset($ring2["borderWidth"])){
                $border2 = $ring1["borderWidth"];
            }
        }
    }
}

?>
<style type="text/css">
    .center-body-loader {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }
    .loader-spanne {
        position: relative;
        width: 200px;
        height: 200px;
        animation: anm-LL-21-move1 1s linear infinite;
    }
    .loader-spanne .loader-spanne-img {
        position: relative;
        top: 50%;
        left: 50%;
        transform: translate(-50%,-50%);
        text-align: center;
    }
    .loader-spanne .loader-spanne-img img {
        width: 80%;
        height: auto;
        animation:no-spin 1s linear infinite;
    }
    .loader-spanne > span {
        position: absolute;
        background: <?php echo $color1 ?>;
        animation: anm-LL-move2 1s linear infinite;
    }
    .loader-spanne > span:nth-of-type(1) {
        width: 100%;
        height: <?php echo $border1 ?>;
        left: 0;
        top: 0;
    }
    .loader-spanne > span:nth-of-type(2) {
        width: <?php echo $border1 ?>;
        height: 100%;
        left: 0;
        bottom: 0;
    }
    .loader-spanne > span:nth-of-type(3) {
        width: 100%;
        height: <?php echo $border1 ?>;
        right: 0;
        bottom: 0;
    }
    .loader-spanne > span:nth-of-type(4) {
        width: <?php echo $border1 ?>;
        height: 100%;
        right: 0;
        top: 0;
    }
    @media (max-width: 768px) {
        .loader-spanne > span {
            animation: anm-LL-move2xs 1s linear infinite;
        }
        .loader-spanne {
            width: 120px;
            height: 120px;
        }
        @keyframes anm-LL-move2xs {
            0% {
                transform: scale(1);
                margin: 0;
            }
            20% {
                transform: scale(1);
                margin: 0;
            }
            60% {
                transform: scale(0.5);
                margin: 24px;
                background: <?php echo $color2 ?>;
            }
            100% {
                transform: scale(1);
                margin: 0;
            }
        }
    }
    @keyframes anm-LL-21-move1 {
        0% {
            transform: rotate(0deg);
        }
        20% {
            transform: rotate(0deg);
        }
        60% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(90deg);
        }
    }
    @keyframes anm-LL-move2 {
        0% {
            transform: scale(1);
            margin: 0;
        }
        20% {
            transform: scale(1);
            margin: 0;
        }
        60% {
            transform: scale(0.5);
            margin: 40px;
            background: <?php echo $color2 ?>;
        }
        100% {
            transform: scale(1);
            margin: 0;
        }
    }

    @keyframes no-spin {
        0% {
            transform: rotate(0deg);
        }
        20% {
            transform: rotate(0deg);
        }
        60% {
            transform: rotate(0deg);
            transform: scale(.6, .6);
        }
        100% {
            transform: rotate(-90deg);
        }
    }
</style>
<div id="loadingModal" style="<?php echo $background ?>">
</div>
<div class="contentFirstLoading">
    <div class="center-body-loader">
        <div class="loader-spanne">
            <div class="loader-spanne-img">
                <img src="<?php echo $logoLoader ?>">
            </div>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
</div>

