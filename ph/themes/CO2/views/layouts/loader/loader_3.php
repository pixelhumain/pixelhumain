<?php
$background="background:white";
$color1 = "#12CB56";
$color2 =  "#f37272";
$border1 = "5px";
$border2 = "5px";
$logoLoader=Yii::app()->theme->baseUrl.'/assets/img/logos/logo.png';
if(isset($this->costum)){
    if (isset($this->costum["htmlConstruct"]["loadingModal"]["logo"]))
        $logoLoader = $this->costum["htmlConstruct"]["loadingModal"]["logo"];
    elseif (isset($this->costum["loaderImg"]))
        $logoLoader = $this->costum["loaderImg"];
    elseif (isset($this->costum["logo"]))
        $logoLoader = $this->costum["logo"];

    if(isset($this->costum["css"]) && isset($this->costum["css"]["loader"])){
        if(isset($this->costum["css"]["loader"]["background"]))
            $background="background:".$this->costum["css"]["loader"]["background"];
        if(isset($this->costum["css"]["loader"]["ring1"])){
            $ring1=$this->costum["css"]["loader"]["ring1"];
            if(isset($ring1["color"])){
                $color1 = $ring1["color"];
            }
            if(isset($ring1["borderWidth"])){
                $border1 = $ring1["borderWidth"];
            }
        }
        if(isset($this->costum["css"]["loader"]["ring2"])){
            $ring2=$this->costum["css"]["loader"]["ring2"];
            if(isset($ring2["color"])){
                $color2 = $ring2["color"];
            }
            if(isset($ring2["borderWidth"])){
                $border2 = $ring1["borderWidth"];
            }
        }
    }
}

?>
<style type="text/css">
    .container_loader {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }
    .container_loader .container_logo {
        display:flex;
        flex-direction:row;
    }
    .container_loader .container_logo img {
        width:270px;
        height : auto;
    }
    .container_loader .loader{
        position: absolute;
        left:50%;
        margin-top:32px;
        transform: translate(-50%, -50%);
        height: <?php echo $border2 ?>;
        width:240px;
        background-color:<?php echo $color2 ?>;
    }
    .container_loader .loading{
        background-color: <?php echo $color1 ?>;
        width:80px;
        height:<?php echo $border1 ?>;
        animation: animation 1.8s infinite;
    }
    @keyframes animation {
        0% {
            transform: translateX(0px);
        }
        50% {
            transform: translateX(160px);
        }
        100% {
            transform: translateX(0px);
        }
    }
    @media (max-width: 768px) {
        .container_loader .container_logo img {
            width:110px;
        }
        .container_loader .loader{
            margin-top:12px;
            width:120px;
        }
        .container_loader .loading{
            width:40px;
        }
        @keyframes animation {
            0% {
                transform: translateX(0px);
            }
            50% {
                transform: translateX(80px);
            }
            100% {
                transform: translateX(0px);
            }
        }
    }
</style>
<div id="loadingModal" style="<?php echo $background ?>">
</div>
<div class="contentFirstLoading">
    <div class="container_loader">
        <div class="container_logo">
            <img src="<?php echo $logoLoader ?>">
        </div>

        <div class="loader">
            <div class="loading">
            </div>
        </div>
    </div>
</div>