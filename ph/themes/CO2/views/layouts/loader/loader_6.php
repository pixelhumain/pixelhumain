<?php
$background="background:white";
$color1 = "#12CB56";
$color2 =  "#f37272";
$border1 = "5px";
$border2 = "5px";
$logoLoader=Yii::app()->theme->baseUrl.'/assets/img/logos/logo.png';
if(isset($this->costum)){
    if (isset($this->costum["htmlConstruct"]["loadingModal"]["logo"]))
        $logoLoader = $this->costum["htmlConstruct"]["loadingModal"]["logo"];
    elseif (isset($this->costum["loaderImg"]))
        $logoLoader = $this->costum["loaderImg"];
    elseif (isset($this->costum["logo"]))
        $logoLoader = $this->costum["logo"];

    if(isset($this->costum["css"]) && isset($this->costum["css"]["loader"])){
        if(isset($this->costum["css"]["loader"]["background"]))
            $background="background:".$this->costum["css"]["loader"]["background"];
        if(isset($this->costum["css"]["loader"]["ring1"])){
            $ring1=$this->costum["css"]["loader"]["ring1"];
            if(isset($ring1["color"])){
                $color1 = $ring1["color"];
            }
            if(isset($ring1["borderWidth"])){
                $border1 = $ring1["borderWidth"];
            }
        }
        if(isset($this->costum["css"]["loader"]["ring2"])){
            $ring2=$this->costum["css"]["loader"]["ring2"];
            if(isset($ring2["color"])){
                $color2 = $ring2["color"];
            }
            if(isset($ring2["borderWidth"])){
                $border2 = $ring1["borderWidth"];
            }
        }
    }
}

?>
<style type="text/css">
    .center-body-loader {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }
    .loader-circle-2 {
        width: 200px;
        height: 200px;
        border: <?php echo $border1 ?> solid <?php echo $color1 ?>;
        border-radius: 100px;
        margin: 0 auto;
        position: relative;
        overflow: hidden;
    }
    .loader-circle-2 img{
        width: 80%;
        position: absolute;
        height: auto;
        top: 50%;
        left: 50%;
        transform: translate(-50%,-50%);
    }
    .loader-circle-2::before {
        content: '';
        position: absolute;
        width: 100%;
        height: 100%;
        top: 200px;
        left: 0px;
        background-color: <?php echo $color2 ?>;
        animation: anm-CL-fill 2s ease 0s alternate infinite;
        display: inline-block;
    }
    .loader-circle-2::after {
        content: '';
        position: absolute;
        width: 100%;
        height: 100%;
        box-sizing: border-box;
    }
    @keyframes anm-CL-fill {
        100% {
            top: 0;
        }
    }
    @media (max-width: 768px) {
        .loader-circle-2 {
            width: 120px;
            height: 120px;
        }
        .loader-circle-2::before {
            top: 120px;
		}
    }
</style>
<div id="loadingModal" style="<?php echo $background ?>">
</div>
<div class="contentFirstLoading">
    <div class="center-body-loader">
        <div class="loader-circle-2">
            <img src="<?php echo $logoLoader ?>">
            <div class="loader-ball-38"></div>
        </div>
    </div>
</div>

