<?php
$background="background:white";
$color1 = "#12CB56";
$color2 =  "#f37272";
$logoLoader=Yii::app()->theme->baseUrl.'/assets/img/logos/logo.png';
if(isset($this->costum)){
    if (isset($this->costum["htmlConstruct"]["loadingModal"]["logo"]))
        $logoLoader = $this->costum["htmlConstruct"]["loadingModal"]["logo"];
    elseif (isset($this->costum["loaderImg"]))
        $logoLoader = $this->costum["loaderImg"];
    elseif (isset($this->costum["logo"]))
        $logoLoader = $this->costum["logo"];

    if(isset($this->costum["css"]) && isset($this->costum["css"]["loader"])){
        if(isset($this->costum["css"]["loader"]["background"]))
            $background="background:".$this->costum["css"]["loader"]["background"];
        if(isset($this->costum["css"]["loader"]["ring1"])){
            $ring1=$this->costum["css"]["loader"]["ring1"];
            if(isset($ring1["color"])){
                $color1 = $ring1["color"];
            }
        }
        if(isset($this->costum["css"]["loader"]["ring2"])){
            $ring2=$this->costum["css"]["loader"]["ring2"];
            if(isset($ring2["color"])){
                $color2 = $ring2["color"];
            }
        }
    }
}

?>
<style type="text/css">
    .nc {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }
    .nc .newtons-cradle-img {
        width: 270px;
        height: auto;
    }
    .nc .newtons-cradle {
        display: flex;
        flex-flow: row nowrap;
        align-items: center;
        justify-content: space-between;
        margin-left: auto;
        margin-right: auto;
        width: 270px;
        margin-top: 20px;
    }
    .nc .newtons-cradle div {
        width: 18px;
        height: 18px;
        border-radius: 50%;
        /*background-color: #fc2f70;*/
    }
    @media (max-width: 768px) {
        .nc .newtons-cradle-img {
            width: 110px;
            height: auto;
        }
        .nc .newtons-cradle {
            width: 120px;
            margin-top: 20px;
        }
        .nc .newtons-cradle div {
            width: 10px;
            height: 10px;
        }
    }
    .newtons-cradle div:nth-of-type(1) {
        transform: translateX(-100%);
        animation: left-swing 0.5s ease-in alternate infinite;
    }
    .newtons-cradle div:nth-of-type(4) {
        transform: translateX(-90%);
        animation: right-swing 0.5s ease-out alternate infinite;
    }
    @keyframes left-swing {
        50%, 100% { transform: translateX(95%); }
    }
    @keyframes right-swing {
        50% {transform: translateX(-95%); }
        100% {transform: translateX(100%); }
    }

</style>
<div id="loadingModal" style="<?php echo $background ?>">
</div>
<div class="contentFirstLoading">
    <div class="nc">
        <img class="newtons-cradle-img" src="<?php echo $logoLoader ?>">
        <div class="newtons-cradle">
            <div style="background-color: <?php echo $color1 ?>;"></div>
            <div style="background-color: <?php echo $color1 ?>;"></div>
            <div style="background-color: <?php echo $color2 ?>;"></div>
            <div style="background-color: <?php echo $color2 ?>;"></div>
        </div>
    </div>
</div>

