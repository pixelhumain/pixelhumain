<?php 
    $background="rgba(255,255,255, 0.9)";
    $color1 = "#9fbd38";
    $color2 =  "#354c57";
    $logoLoader=Yii::app()->theme->baseUrl.'/assets/img/logos/logo.png';
    
    if(isset($this->costum)){
        if(isset($this->costum["htmlConstruct"]["loadingModal"]["logo"]) || isset($this->costum["logo"]))
            $logoLoader= (isset($this->costum["htmlConstruct"]["loadingModal"]["logo"])) ? $this->costum["htmlConstruct"]["loadingModal"]["logo"] : $this->costum["logo"] ;
        if(isset($this->costum["css"]) && isset($this->costum["css"]["loader"])){
            if(isset($this->costum["css"]["loader"]["background"]))
                $background="background:".$this->costum["css"]["loader"]["background"];
            if(isset($this->costum["css"]["loader"]["ring1"])){
                $ring1=$this->costum["css"]["loader"]["ring1"];
                if(isset($ring1["color"])){
                    $color1 = $ring1["color"];
                }
            }
            if(isset($this->costum["css"]["loader"]["ring2"])){
                $ring2=$this->costum["css"]["loader"]["ring2"];
                if(isset($ring2["color"])){
                    $color2 = $ring2["color"];
                }
            }
        }
    }

?>

<style type="text/css">
    .loader-content {
        display: flex;
        align-items: center;
        height: 100vh;
        width: 100%;
        background-color: <?php echo $background ?>;
    }

    .cont{
        position:absolute;
        left:50%;
        top:50%;
        transform:Translate(-50%, -50%);
    }

    .button-loading{
        background-color: <?php echo $color1 ?>;
        color: white;
        border-bottom:6px solid <?php echo $color2 ?>;   
        width: 180px;
        font-size: 28px;
        padding:15px 0px 15px 35px;
        border-radius:0px 0px 15px 15px;
        position:relative;
    }

    .button-loading:after{
        content:"";
        width:100%;
        height:1px;
        background-color:#c8d6e5;
        top:-3px;
        left:0;
        position:absolute;
    }

    .loader{
        position:absolute;
        top:30px;
        left:34px;
    }

    .button-loading .loader div{
        width:5px;
        height:5px;
        border-radius:50%;
        background-color:<?php echo $color2 ?>;
        position:absolute;
        transform-origin: -8px;
        animation: light 3s linear infinite;
    }

    .button-loading .loader div:nth-child(1){
        transform:rotate(0deg);
        animation-delay:0s;
    }

    .button-loading .loader div:nth-child(2){
        transform:rotate(60deg);
        animation-delay:.5s;
    }

    .button-loading .loader div:nth-child(3){
        transform:rotate(120deg);
        animation-delay:1s;
    }

    .button-loading .loader div:nth-child(4){
        transform:rotate(180deg);
        animation-delay:1.5s;
    }



    .button-loading .loader div:nth-child(5){
        transform:rotate(240deg);
        animation-delay:2s;
    }

    .button-loading .loader div:nth-child(6){
        transform:rotate(300deg);
        animation-delay:2.5s;
    }

    @keyframes light{
        0%{background-color:<?php echo $color2 ?>;}
        40%{background-color:<?php echo $color2 ?>;}
        50%{background-color:white;}
        60%{background-color:<?php echo $color2 ?>;}
        100%{background-color:<?php echo $color2 ?>;}
    }

    .paper{
        width:150px;
        height:160px;
        box-shadow:0px 0px 10px 1px rgba(55,55,55,.3);
        left:0;
        right:0;
        margin: auto;
        animation: paper 4s ease infinite;
        position:absolute;
        transform-origin:top;
        transform:translateY(0px) rotate(180deg);
        background-image: url("<?php echo $logoLoader ?>");
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }

    .g-cont{
        margin:0 auto;
        width:150px;
        text-align:center;
        position:absolute;
        top:73px;
        left:10px;
        right:0;
        margin: auto;
        z-index:10;
    }

    .garbage{
        height:160px;
        width:6px;
        box-shadow:0px 0px 10px 1px rgba(55,55,55,.3);
        display:inline-block;
        vertical-align:top;
        margin-right:9px;
        animation: garbage 4s ease infinite;
        background-image: url("<?php echo $logoLoader ?>");
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }

    @keyframes garbage{
        0%{height:0px;transform:translateY(0px);opacity:0;}
        20%{height:0px;transform:translateY(0px);opacity:1;}
        70%{height:160px;transform:translateY(0px);opacity:1;}
        90.1%{height:160px;transform:translateY(100px);opacity:0;}
        100%{height:0px;opacity:0;}
    }

    @keyframes paper{
        0%{height:160px;opacity:0;transform:translateY(-100px) rotate(180deg);}
        20%{height:160px;opacity:1;transform:translateY(0px) rotate(180deg);}
        70%{height:0px;opacity:1;transform:translateY(0px) rotate(180deg);}
        70.1%{height:0px;opacity:0;transform:translateY(0px) rotate(180deg);}
        100%{height:160px;opacity:0;transform:translateY(-100px) rotate(180deg);}
    }
</style>

<div class="loader-content">
    <div class="cont">
        <div class="paper"></div>
        <button class="button-loading">
            <div class='loader'>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
            Loading
        </button>
        <div class="g-cont">
            <div class="garbage"></div>
            <div class="garbage"></div>
            <div class="garbage"></div>
            <div class="garbage"></div>
            <div class="garbage"></div>
            <div class="garbage"></div>
            <div class="garbage"></div>
            <div class="garbage"></div>
        </div>
    </div>
</div>
