<?php $logo = $logo = (isset($this->costum["htmlConstruct"]["loadingModal"]["logo"])) ? $this->costum["htmlConstruct"]["loadingModal"]["logo"] : ((isset($this->costum["logo"])) ? $this->costum["logo"] : $this->module->getParentAssetsUrl()."/images/logoLTxt.jpg" ); ?>

<div class="portfolio-modal modal fade" id="modalRegister" tabindex="-1" role="dialog" aria-hidden="true">
    <form class="modal-content form-register box-register padding-top-15"  >
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        <div class="container col-lg-offset-3 col-sm-offset-2 col-lg-6 col-sm-8 col-xs-12">
            <div class="row">
                <div class="col-lg-12">
                    <span class="name" >
                        
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="loginLogo">
                                
                                    <img class="logoLoginRegister" src="<?php echo $logo;?>"/>
                                
                                </div>
                            </div>
                    </span>
                </div>
            </div>
            <div class="text-left form-register-inputs col-xs-12 margin-bottom-50">
                <div class="pull-left form-actions no-margin" style="width:100%; padding:10px;">
                    <div class="errorHandler alert alert-danger no-display registerResult pull-left " style="width:100%;">
                        <i class="fa fa-remove-sign"></i> <?php echo Yii::t("login","Please verify your entries.") ?>
                    </div>
                    <div class="alert alert-success no-display pendingProcess" style="width:100%;">
                        <i class="fa fa-check"></i> <?php echo Yii::t("login","You've been invited : please resume the registration process in order to log in.") ?>
                    </div>
                </div>
                <div class="nameRegister">
                    <label class="letter-black"><i class="fa fa-address-book-o"></i> <?php echo Yii::t("login","Name and surname") ?> *</label>
                    <input class="form-control" id="registerName" name="name" type="text" placeholder="<?php echo Yii::t("login","name and surname") ?>"><br/>
                </div>
                <div class="usernameRegister">
                <label class="letter-black"><i class="fa fa-user-circle-o"></i> <?php echo Yii::t("login","User name") ?> *</label><br>
                <input class="form-control" id="username" name="username" type="text" placeholder="<?php echo Yii::t("login","user name") ?>"><br/>
                </div>
                <div class="emailRegister">
                <label class="letter-black"><i class="fa fa-envelope"></i> <?php echo Yii::t("login","Email") ?> *</label><br>
                <input class="form-control" id="email3" name="email3" type="text" placeholder="<?php echo Yii::t("login","email") ?>"><br/>
                </div>
                <div class="passwordRegister">
                <label class="letter-black"><i class="fa fa-key"></i> <?php echo Yii::t("login","Password") ?> *</label><br/>
                <input class="form-control" id="password3" name="password3" type="password" placeholder="<?php echo Yii::t("login","password") ?>"><br/>
                </div>
                <div class="passwordAgainRegister">
                <label class="letter-black"><i class="fa fa-key"></i> <?php echo Yii::t("login","Password again") ?> *</label><br/>
                <input class="form-control" id="passwordAgain" name="passwordAgain" type="password" placeholder="<?php echo Yii::t("login","password (confirmation)") ?>">
                </div>
                <input class="form-control" id="isInvitation" name="isInvitation" type="hidden" value="false">
                <hr>
                <div class="form-group pull-left no-margin agreeContent" style="width:100%;">
                    <div class="checkbox-content pull-left no-padding">
                        <label for="agree" class="">
                            <input type="checkbox" class="agree" id="agree" name="agree">
                            <span class="cr pull-left"><i class="cr-icon fa fa-check"></i></span>
                             <span class="agreeMsg checkbox-msg letter-red pull-left">
                                <?php echo Yii::t("login","I agree to the Terms of") ?> 
                                <a href="#mentions" target="_blank" class="bootbox-spp text-dark">
                                    <?php echo Yii::t("login","Legal mentions") ?>
                                </a>
                                <?php echo Yii::t("common","and") ?>                             
                                <a href="#confidentialityRules"  class="bootbox-spp text-dark" target="_blank">
                                    <?php echo Yii::t("login","Privacy Policy") ?>
                                </a>

                            </span>
                        </label><br>
                         <label for="charte" class="margin-top-10">
                            <input type="checkbox" class="charte" id="charte" name="charte">
                            <span class="cr pull-left"><i class="cr-icon fa fa-check"></i></span>
                             <span class="agreeMsg checkbox-msg letter-red pull-left">
                                <?php echo Yii::t("login","I take in consideration ") ?> 
                                <a href="#charte" target="_blank" class="bootbox-spp text-dark">
                                    <?php echo Yii::t("login","the charte of using") ?>
                                </a>
                            </span>
                        </label>

                    </div>
                   

                    <!--<div>
                        <label for="agree" class="checkbox-inline letter-red">
                            <input type="checkbox" class="grey agree" id="agree" name="agree">
                            <?php echo Yii::t("login","I agree to the Terms of") ?> 
                            <a href="https://www.communecter.org/doc/Conditions Générales d'Utilisation.pdf" target="_blank" class="bootbox-spp text-dark">
                                <?php echo Yii::t("login","Service and Privacy Policy") ?>
                            </a>
                        </label>
                    </div>-->
                <br>
                <div class="col-xs-12"><hr></div>
                </div>

                <div class="pull-left form-actions no-margin" style="width:100%; padding:10px;">
                    <div class="errorHandler alert alert-danger no-display registerResult pull-left " style="width:100%;">
                        <i class="fa fa-remove-sign"></i> <?php echo Yii::t("login","Please verify your entries.") ?>
                    </div>
                </div>

                <a href="javascript:" class="btn btn-danger pull-left" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo Yii::t("common","Back") ?></a>
                <button class="btn btn-success text-white pull-right createBtn"><i class="fa fa-sign-in"></i> <?php echo Yii::t("login","Create account") ?></button>
                
                
                <!--<div class="col-md-12 margin-top-50 margin-bottom-50"></div>-->
            </div>      
        </div>
    </form>
</div>