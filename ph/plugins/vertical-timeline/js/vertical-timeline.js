function vertical_timeline(__selector, __arguments) {
    if (typeof jQuery === 'undefined' || typeof moment === 'undefined' || typeof __selector === 'undefined' || !__selector) return {};
    const $ = jQuery;
    let events = [];
    const temp = {
        order_asc: false,
        events: [],
        remove_events: function (__events) {
            const self = this;
            events = [];
            if (typeof __events === 'undefined') self.events = [];
            else if (typeof __events === 'object' && Array.isArray(__events)) {
                $.each(__events, function (__, __event) {
                    let indexof = -1;
                    if (typeof __event === 'string') indexof = self.events.findIndex(function (___event) { return ___event.id && ___event.id === __event });
                    else if (typeof __event === 'object' && !Array.isArray(__event)) {
                        indexof = self.events.findIndex(function(___event) {
                            let match = false;
                            for(const key in __event) {
                                if(___event[key] && ___event[key] === __event[key]) {
                                    match = true;
                                    break;
                                }
                            }
                            return match;
                        });
                    }
                    if (indexof >= 0) self.events.splice(indexof, 1);
                });
            }
        },
        noEventMessage: "La liste des éléments est vide"
    };
    const output = $.extend(true, temp, __arguments);

    function build() {
        // initialisation
        for (const _event of output.events) {
            const event_default = {
                title: 'Title',
                short_description: '',
                full_description: '',
                backgroundColor: '#c9c9c9',
                textColor: 'black'
            };
            const event = $.extend(true, event_default, _event);
            if (!event.start && !event.end) continue;
            else if (!event.start && event.end && moment(event.end).isValid()) {
                event.end = moment(event.end);
                event.start = event.end.clone().subtract(1, 'days');
            } else if (event.start && !event.end && moment(event.start).isValid()) {
                event.start = moment(event.start);
                event.end = event.start.clone().add(1, 'days');
            } else if(event.start && event.end && moment(event.start).isValid() && moment(event.end).isValid()) {
                event.start = moment(event.start);
                event.end = moment(event.end);
            } else continue;
            events.push(event);
        }

        // Reordonner
        const event_length = events.length;
        let do_order = false;
        do {
            do_order = false;
            for (let i = 0; i < event_length - 1; i++) {
                const current = events[i];
                const next = events[i + 1];

                const should_permute = output.order_asc && current.start.isAfter(next.start) || !output.order_asc && current.start.isBefore(next.start);
                if (should_permute) {
                    events[i] = next;
                    events[i + 1] = current;
                    do_order = true;
                }
            }
        } while (do_order)

        // Créer le template HTML
        var html = '<div class="timeliner"><ul>';
        if(events.length) {
            for (const event of events) {
                let date_html;
                if (event.start.format('LL') === event.end.format('LL')) {
                    date_html = `<b>${event.start.format('LL')}</b><br>${event.start.format('HH:mm')} - ${event.end.format('HH:mm')}`;
                } else if (event.start.hour() === 0 && event.start.minute() === 0 && event.end.hour() === 0 && event.end.minute() === 0) {
                    if (event.end.diff(event.start, 'days') === 1) date_html = `<b>${event.start.format('LL')}</b>`;
                    else {
                        event.end.subtract(1, 'seconds');
                        date_html = `<b>${event.start.format('LL')}</b><br><b>${event.end.format('LL')}</b>`;
                    }
                } else date_html = `<b>${event.start.format('LL')}</b> ${event.start.format('HH:mm')}<br><b>${event.end.format('LL')}</b> ${event.end.format('HH:mm')}`;

                html += `
            <li style="background-color: ${event.backgroundColor}; color: ${event.textColor}">
                <div class="vertical-timeline-content">
                    <h3 class="date">${date_html}</h3>
                    <h1 style="font-weight: initial; font-size: medium; line-height: 1.9rem; text-align: justify; text-transform: initial;"><b>${event.title}</b></h1>
                    ${event.short_description ? `<p><i>${event.short_description}</i></p>` : ''}
                    <div>${event.full_description}</div>
                </div>
            </li>`;
            }
            var selector = $(__selector);
            selector.addClass('timeline-container');
            selector.empty().html(html + '</ul></div><span class="ending-event"></span>');
            selector.find('.timeliner .vertical-timeline-content>h1').each(function (__, __element) {
                $(__element).on('click', function (__e) {
                    __e.preventDefault();
                    if (typeof output.on_click === 'function') output.on_click(events[__]);
                });
            });
        } else {
            var emptyMessageDom = $('<div>').addClass('tv-empty-message').text(output.noEventMessage);
            $(__selector).empty().append(emptyMessageDom);
        }
    }
    build();
    return output;
}