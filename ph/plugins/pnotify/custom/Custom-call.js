var stack_bottomleft = new PNotify.Stack({
    dir1: 'up',
    dir2: 'right', // Position from the top left corner.
    firstpos1 : 50,
    firstpos2 : 30 
});
var myPNotify = {
    pingNotification(
        title, 
        text, 
        options = {
            icon : 'fa fa-bell fa-3x',
            delay : 6000,
            animations : {
                in : 'slideInUp',
                out : 'fadeOutLeft'
            },
            customClass : '',
            position : stack_bottomleft
        }
    ) {
        return PNotify.notice({
            title : title,
            text : text,
            textTrusted : true,
            maxTextHeight : null,
            styling : 'ping-notif',
            addClass : options.customClass,
            maxTextHeight : null,
            icon : options.icon,
            icons : {
              prefix : PNotify.defaults.icons,
              closer : 'fa fa-times btn-aap-link fa-lg'
            },
            modules : new Map([
              ...PNotify.defaultModules,
              [PNotifyAnimate, {
                inClass : options.animations.in,
                outClass : options.animations.out
              }]
            ]),
            sticker : false,
            delay : options.delay,
            stack : options.position
        });
    }
}