<?php

if( stripos($_SERVER['SERVER_NAME'], "default") === false && 
	stripos($_SERVER['SERVER_NAME'], "127.0.0.1") === false && 
	stripos($_SERVER['SERVER_NAME'], "communecter56-dev") === false && 
	stripos($_SERVER['SERVER_NAME'], "communecter74-dev") === false && 
	stripos($_SERVER['SERVER_NAME'], "communecter-dev") === false && 
	stripos($_SERVER['SERVER_NAME'], "localhost") === false && 
	stripos($_SERVER['SERVER_NAME'], "::1") === false && 
	stripos($_SERVER['SERVER_NAME'], "localhost:8080") === false &&
	stripos($_SERVER['SERVER_NAME'], "localhost:5080") === false &&
	stripos($_SERVER['SERVER_NAME'], "qa.communecter.org") === false &&
	stripos($_SERVER['SERVER_NAME'], "local.")!==0){
	defined('YII_DEBUG') or define('YII_DEBUG',false); // PROD
	error_reporting(0);
} else {
	error_reporting(E_ALL & ~E_WARNING);	
	ini_set('display_errors', 'On');
	defined('YII_DEBUG') or define('YII_DEBUG',true);//LOCAL DEV OR QA  	
}

// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

//===================================================================================================

// change the following paths if necessary
require_once(dirname(__FILE__) . '/vendor/autoload.php');

//require dirname(__FILE__). '/protected/components/Yii.php';
require dirname(__FILE__). '/Yii2Yii1.php';

// Registering dependencies
$container = Yii::$container;
$container->set('session', ['class' => \yii\web\Session::class]);

// configuration for Yii 1 application
$yii1Config = require dirname(__FILE__) . '/protected/config/main.php';
// configuration for Yii 2 application
$yii2Config = require dirname(__FILE__) . '/protected/config/yii2/web.php';

//Application setup
$Yii1App = Yii::createWebApplication($yii1Config);
$Yii2App = new yii\web\Application($yii2Config);

//Call run() only on the version being used as main version
//$Yii1App->run();
$Yii2App->run();